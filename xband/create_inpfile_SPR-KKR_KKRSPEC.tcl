
#---------------------------------------------------------------- KKRSPEC
#      input mask for    KKRSPEC
#---------------------------------------------------------------- KKRSPEC
#
#   see procedure reate_inpfile_SPR for definition of parameters
#
proc create_inpfile_SPR-KKR_KKRSPEC {TASK wc1 wl ws wee fe } {

global BGFIL ; set BGFIL  LightPink1
global BGTSK ; set BGTSK  LightSalmon1
global BGTSK1 ; set BGTSK1  Salmon1
global BGCPA ; set BGCPA  wheat
global BGTAU ; set BGTAU  orange1
global BGWR  ; set BGWR   lavender
global BGKKR ; set BGKKR  LightSkyBlue1 
global BGK   ; set BGK    SteelBlue1
global BGCLU ; set BGCLU  LightSkyBlueFe.pot
global BGE   ; set BGE    moccasin
global BGP   ; set BGP    ivory1
global BGX   ; set BGX    cornsilk1
global BGSCF ; set BGSCF  LemonChiffon1
global BGCRL ; set BGCRL  orange1
global BGMOD ; set BGMOD  cornsilk1
global BGMAG ; set BGMAG  lavender
global BGLEX ; set BGLEX  LightPink1

global BG4; set BG4 LightSalmon1
	
global NQ DIMENSION

global VAR NT

frame $wc1.fr1
set wc1_1 $wc1.fr1
frame $wc1.fr2
set wc1_2 $wc1.fr2
frame $wc1.fr3
set wc1_3 $wc1.fr3
#
#---------------------------------------------------------------- ARPES
#

if {$TASK == "ARPES" || $TASK == "AIPES"} {
# Increment NL (NLUSER) if not done before
incr VAR(APTRES_NL_INC)
if { $VAR(APTRES_NL_INC)  == 1 } {
  incr VAR(NLUSER)
  # set first defaults
  set VAR(GRID)  1 ;# REAL
  set VAR(EMAX)  1.0
}
#------------------------------------ column 1
#
label $wc1_1.task    -text "TASK " -anchor w  -padx 2 -bg $BGTSK1
pack configure $wc1_1.task   -in $wc1_1  -anchor nw -side top -fill x
#CreateRADIOBUTTON $wc1_1 $BGTSK $wl "INPVER     "  VAR SPR-KKR-TASK-INPVER  a  $fe n 1  new old

label $wc1_1.strver1    -text "Version of the strutural input" -anchor w  -padx 2 -bg $BGE
pack configure $wc1_1.strver1   -in $wc1_1  -anchor nw -side top -fill x

CreateRADIOBUTTON $wc1_1 $BGE $wl "STRVER     "  VAR SPR-KKR-TASK_STRVER  a  $fe s 1  SPKKR_FMT  RSLAB_FMT

if {$DIMENSION == "3D"} {
#
CreateSCALE       $wc1_1 $BGE     $wl "IQ (SURF)   "      $ws 1        $NQ   0 [PREC_INT $NQ] 1        VAR SPR-KKR-SPEC3D_IQ_AT_SURF  $fe
label $wc1_1.strvers    -text "MILLER HKL 3D" -anchor w  -padx 2 -bg $BGE
pack configure $wc1_1.strvers   -in $wc1_1  -anchor nw -side top -fill x
CreateSCALE       $wc1_1 $BGE     $wl "h   "      $ws 0        10   0 2 0        VAR SPR-KKR-SPEC3D_H  $fe
CreateSCALE       $wc1_1 $BGE     $wl "k   "      $ws 0        10   0 2 0        VAR SPR-KKR-SPEC3D_K  $fe
CreateSCALE       $wc1_1 $BGE     $wl "l   "      $ws 0        10   0 2 0        VAR SPR-KKR-SPEC3D_L  $fe
}


label $wc1_1.energy    -text "ENERGY GRID" -anchor w  -padx 2 -bg $BGTSK1
pack configure $wc1_1.energy   -in $wc1_1  -anchor nw -side top -fill x
CreateENTRY       $wc1_1 $BGE     $wl "IMV_FIN_EV"     $wee VAR SPR-KKR-ENERG_IMV_FIN_EV       $fe 3 "eV"
CreateENTRY       $wc1_1 $BGE     $wl "IMV_INI_EV"     $wee VAR SPR-KKR-ENERG_IMV_INI_EV       $fe 3 "eV"
CreateENTRY       $wc1_1 $BGE     $wl "EWORK_EV  "     $wee VAR SPR-KKR-ENERG_EWORK_EV         $fe 3 "eV"
CreateSCALE       $wc1_1 $BGE     $wl "   NE     "      $ws 0        600   0 3 1        VAR NE  $fe
CreateENTRY       $wc1_1 $BGE     $wl "EMIN_EV   "     $wee VAR SPR-KKR-ENERG_EMIN_EV   $fe 3 "eV"
CreateENTRY       $wc1_1 $BGE     $wl "EMAX_EV   "     $wee VAR SPR-KKR-ENERG_EMAX_EV   $fe 3 "eV"

pack configure $wc1_1   -in $wc1  -anchor nw -side left -fill x

#puts ${SPR-KKR-ENERG_IMV_FIN_EV};
#
#------------------------------------ column 2
#
label $wc1_2.electrons    -text "Angular scan parameters for Photoelectrons" -anchor w  -padx 2 -bg $BGTSK1
pack configure $wc1_2.electrons   -in $wc1_2  -anchor nw -side top -fill x
label $wc1_2.txttheta    -text "range of polar angles" -anchor w  -padx 2 -bg $BGE
pack configure $wc1_2.txttheta   -in $wc1_2  -anchor nw -side top -fill x
CreateENTRY       $wc1_2 $BGE   $wl "THETA MIN "     $wee VAR SPR-KKR-SPEC-EL_THETA_MIN      $fe 3 "deg"
CreateENTRY       $wc1_2 $BGE   $wl "THETA MAX "     $wee VAR SPR-KKR-SPEC-EL_THETA_MAX      $fe 3 "deg"
CreateENTRY       $wc1_2 $BGE   $wl "NT"     $wee VAR SPR-KKR-SPEC-EL_NT             $fe 3 ""

label $wc1_2.txtphi    -text "range of azimuth angles" -anchor w  -padx 2 -bg $BGE
pack configure $wc1_2.txtphi   -in $wc1_2  -anchor nw -side top -fill x
CreateENTRY       $wc1_2 $BGE   $wl "PHI MIN   "     $wee VAR SPR-KKR-SPEC-EL_PHI_MIN        $fe 3 "deg"
CreateENTRY       $wc1_2 $BGE   $wl "PHI MAX   "     $wee VAR SPR-KKR-SPEC-EL_PHI_MAX        $fe 3 "deg"
CreateENTRY       $wc1_2 $BGE   $wl "NP"     $wee VAR SPR-KKR-SPEC-EL_NP             $fe 3 ""

label $wc1_2.txtpol    -text "quantattisation axis for spin polarisation" -anchor w  -padx 2 -bg $BGE
pack configure $wc1_2.txtpol   -in $wc1_2  -anchor nw -side top -fill x
CreateRADIOBUTTON $wc1_2 $BGE   $wl "pol. axis "          VAR SPR-KKR-SPEC-EL_POL_E       a  $fe s 1  PX PY PZ 

label $wc1_2.head   -text "Parameters for incomming light" -anchor w  -padx 2 -bg $BGTSK1
pack configure $wc1_2.head     -in $wc1_2  -anchor nw -side top -fill x
CreateENTRY       $wc1_2 $BGE   $wl "THETA     "     $wee VAR SPR-KKR-SPEC-PH_THETA          $fe 3 "deg"
CreateENTRY       $wc1_2 $BGE   $wl "PHI       "     $wee VAR SPR-KKR-SPEC-PH_PHI            $fe 3 "deg"
CreateENTRY       $wc1_2 $BGE   $wl "Photon Ene."    $wee VAR SPR-KKR-SPEC-PH_EPHOT          $fe 3 "eV"
CreateRADIOBUTTON $wc1_2 $BGE   $wl "Polarisation"        VAR SPR-KKR-SPEC-PH_POL_P       a  $fe s 1  P S C+ C-

pack configure $wc1_2   -in $wc1  -anchor nw -side left -fill x

#
#------------------------------------ column 3
#
label $wc1_3.mode-k-k    -text "Structure" -anchor w  -padx 2 -bg $BGTSK1
pack configure $wc1_3.mode-k-k   -in $wc1_3  -anchor nw -side top -fill x
CreateENTRY       $wc1_3 $BGE   $wl "LAYDBL FIN "     $wee VAR SPR-KKR-SPEC-STR_N_LAYDBL_FIN  $fe 3 ""
CreateENTRY       $wc1_3 $BGE   $wl "LAYDBL INI "     $wee VAR SPR-KKR-SPEC-STR_N_LAYDBL_INI  $fe 3 ""
CreateENTRY       $wc1_3 $BGE   $wl "NLAT_G_VEC "     $wee VAR SPR-KKR-SPEC-STR_NLAT_G_VEC    $fe 3 ""
CreateENTRY       $wc1_3 $BGE   $wl "N_LAYER    "     $wee VAR SPR-KKR-SPEC-STR_N_LAYER       $fe 3 ""
CreateENTRY       $wc1_3 $BGE   $wl "SURF_BAR MIN"    $wee VAR SPR-KKR-SPEC-STR_SURF_BAR_MIN  $fe 3 ""
CreateENTRY       $wc1_3 $BGE   $wl "SURF_BAR_MAX"    $wee VAR SPR-KKR-SPEC-STR_SURF_BAR_MAX  $fe 3 ""


#----------------	

pack configure $wc1_3   -in $wc1  -anchor nw -side left -fill x

#
#---------------------------------------------------------------- NIX
#


#frame $wc1.toprow  -bg $BGTSK
#pack configure $wc1.toprow -in $wc1 -anchor w -side top -fill x -expand y
#frame $wc1.toprow.a  -bg $BGTSK
#frame $wc1.toprow.b  -bg $BGTSK
#pack  $wc1.toprow.a  $wc1.toprow.b -side left -expand y -fill x
#CreateSCALE       $wc1.toprow.a $BGTSK   $wl "   NTMP   "     $ws 0        250   0 3 1        VAR NTMP  $fe
#CreateENTRY       $wc1.toprow.a $BGTSK   $wl "   TMPMIN "     $wee VAR TMPMIN       $fe 3 "K"
#CreateENTRY       $wc1.toprow.a $BGTSK   $wl "   TMPMAX "     $wee VAR TMPMAX       $fe 3 "K"
#CreateENTRY       $wc1.toprow.a $BGTSK   $wl "   TDEBYE"      $wee VAR TDEBYE       $fe 3 "K"
#
#CreateENTRY       $wc1.toprow.b $BGTSK   $wl "   TDEBYE"      $wee VAR TDEBYE       $fe 3 "K"
#
#$wc1 configure -bg $BGTSK
#
#CreateTEXT        $wc1 $BGX info \
#"�
# perform calculations for finite temperature for NTMP != 0
#
# in the energy window  TMIN - TMAX  with  NTMP  steps
#
# the root mean square displacement is estimated 
# from the Debye temperature  TDEBYE
#
#"



#
#---------------------------------------------------------------- LEED
#
} elseif {$TASK == "LEED" } {

    set VAR(SEARCHEF)  0    ;    set VAR(ImE)       0.01
    set VAR(NE)      180    ;    set VAR(GRID)  3
    if {$TASK == "XES" } {
       set VAR(EMIN) -0.2
       set VAR(EMAX)  ""
    } else {
       set VAR(EMIN)  ""
       set VAR(EMAX)  4.0
    }
    if {$TASK == "XAS" } {set VAR(GRID)  6}


if {$NT<10} {set ITDIG 1} else {set ITDIG 2}
CreateSCALE       $wc1 $BGTSK $wl "   ITXRAY " $ws 1 $NT   0 $ITDIG 1  VAR ITXRAY $fe
CreateRADIOBUTTON $wc1 $BGTSK $wl "   NCXRAY "    VAR NCXRAY     aexec $fe n 1  1 2 3 4 5 6
CreateRADIOBUTTON $wc1 $BGTSK $wl "   LCXRAY "    VAR LCXRAY     aexec $fe s 1  s p d f
## CreateRADIOBUTTON $wc1 $BGTSK $wl "   ME "      VAR XRAYME     a $fe s 1 ADA NAB GRV
if {$TASK == "XMO"|| $TASK == "XRS"} {
CreateENTRY       $wc1 $BGTSK $wl "   TAU j(-)"   $wee VAR TAUJMIN $fe 3 "eV"
CreateENTRY       $wc1 $BGTSK $wl "   TAU j(+)"   $wee VAR TAUJPLS $fe 3 "eV"
CreateRADIOBUTTON $wc1 $BGTSK $wl "   TSELECT"  VAR TSELECT    a $fe n 0 0 1 2
CreateSCALE       $wc1 $BGTSK $wl "   NE3 "       $ws 1 400  0 3 1    VAR XMONE3    $fe
CreateSCALE       $wc1 $BGTSK $wl "   EMAX3"      $ws 0  50  0 3 0.1  VAR XMOEMAX3  $fe
}
#
#---------------------------------------------------------------- AIPES
#
} else {

                    puts "no proper SPR-KKR task supplied  TASK = $TASK"

#
#---------------------------------------------------------------- NIX
#
		
} ; # TASK


} ; # proc   create_inpfile_SPR-KKR_KKRSPEC 



#---------------------------------------------------------------- KKRSPEC
#      write input specific for    KKRSPEC
#---------------------------------------------------------------- KKRSPEC
#
#   see procedure reate_inpfile_SPR for definition of parameters
#
proc write_inpfile_SPR-KKR_KKRSPEC {TASK inp} {

global VAR NT  TXTT

#
#---------------------------------------------------------------- ARPES
#
if {$TASK == "ARPES" } {

puts $inp "TASK     $TASK"
if {$VAR(SPR-KKR-TASK_STRVER) == "SPKKR_FMT"} {
puts $inp "         STRVER=1"} else {
puts $inp "         STRVER=0"
}
puts "XXXXXXXXXXXXXX: SPR-KKR-TASK_STRVER $VAR(SPR-KKR-TASK_STRVER)"
#puts $inp "         STRVER=$VAR(SPR-KKR-TASK_STRVER)  INPVER=$VAR(SPR-KKR-TASK_INPVER) "
puts $inp "         IQ_AT_SURF=[ expr { int ($VAR(SPR-KKR-SPEC3D_IQ_AT_SURF))}]"
puts $inp "         MILLER_HKL={[ expr { int ($VAR(SPR-KKR-SPEC3D_H))}],[ expr { int ( $VAR(SPR-KKR-SPEC3D_K))}],[ expr { int ($VAR(SPR-KKR-SPEC3D_L))}]}"
puts $inp ""
puts $inp "SPEC_PH THETA=$VAR(SPR-KKR-SPEC-PH_THETA)"
puts $inp "         PHI=$VAR(SPR-KKR-SPEC-PH_PHI)"
puts $inp "         POL_P=$VAR(SPR-KKR-SPEC-PH_POL_P)"
puts $inp "         EPHOT=$VAR(SPR-KKR-SPEC-PH_EPHOT)"
puts $inp ""
puts $inp "SPEC_STR N_LAYDBL={$VAR(SPR-KKR-SPEC-STR_N_LAYDBL_FIN),$VAR(SPR-KKR-SPEC-STR_N_LAYDBL_INI)}"
puts $inp "         NLAGT_G_VEC=$VAR(SPR-KKR-SPEC-STR_NLAT_G_VEC) N_LAYER=$VAR(SPR-KKR-SPEC-STR_N_LAYER)"
puts $inp "         SURF_BAR={$VAR(SPR-KKR-SPEC-STR_SURF_BAR_MIN),$VAR(SPR-KKR-SPEC-STR_SURF_BAR_MAX)}"
puts $inp ""
puts $inp "SPEC_EL THETA={$VAR(SPR-KKR-SPEC-EL_THETA_MIN),$VAR(SPR-KKR-SPEC-EL_THETA_MAX)}"
puts $inp "         PHI={$VAR(SPR-KKR-SPEC-EL_PHI_MIN),$VAR(SPR-KKR-SPEC-EL_PHI_MAX)}"
puts $inp "         NT=$VAR(SPR-KKR-SPEC-EL_NT)"
puts $inp "         NP=$VAR(SPR-KKR-SPEC-EL_NP)"
puts $inp "         POL_E=$VAR(SPR-KKR-SPEC-EL_POL_E)"
	
#puts $inp ""


#
#---------------------------------------------------------------- NIX
#
} else {

                    puts "no proper SPR-KKR writer task supplied  TASK = $TASK"

#
#---------------------------------------------------------------- NIX
#
} ; # TASK


} ; # proc   write_inpfile_SPR-KKR_KKRSPEC
