      SUBROUTINE PLOTXPS( IFIL, N,NW,
     &                    NQMAX,NTMAX,NEMAX,NCMAX,NSPMAX,NLMAX,
     &                    SPEC,
     &                    NLQ, NLT, NMIN,NMAX, WMIN,WMAX,
     &                    IQOFT,NQT,CONC,TXTT,LTXTT, INFO,LINFO, 
     &                    HEADER,LHEADER, SYSTEM,LSYSTEM, E, Y,
     &                    NQ,NT,NE,IREL,EF,
     &                    EXRS,IXRS,BXRS,WXRS,NVMAX,NEXRSMAX,WGTGAUTAB,
     &                    EEXP, IEXP, NCEXPMAX,NPOLEXPMAX )
c **********************************************************************
c *                                                                    *
c *  plot XPS - spectra                                                *
c *                                                                    *
c **********************************************************************
      REAL         EEXP(NEMAX,NCEXPMAX), IEXP(NEMAX,NCEXPMAX,NPOLEXPMAX)
      REAL         N(NEMAX,0:NTMAX,0:NCMAX,0:NSPMAX), E(NEMAX), Y(NEMAX)
      REAL         NW(NEMAX,0:NTMAX,0:NCMAX,0:NSPMAX)
      REAL         WXRS(NEXRSMAX,0:NTMAX,0:NCMAX,0:NSPMAX), 
     &             EXRS(NEXRSMAX)
      REAL         IXRS(NVMAX,NEMAX), BXRS(NVMAX,NEMAX)
      REAL         WGTGAUTAB(0:NEMAX)
      CHARACTER*80 HEADER,SYSTEM,FMT1, NTEXT, INFO
      INTEGER      LHEADER, LSYSTEM
      INTEGER      NLQ(NQMAX), NLT(NTMAX)
      INTEGER      IQOFT(NQMAX,NTMAX), NQT(NTMAX), LTXTT(NTMAX)
      REAL         CONC(NTMAX) 
      REAL         NMIN(0:NTMAX), NMAX(0:NTMAX)
      REAL         WMIN(0:NTMAX), WMAX(0:NTMAX)

      CHARACTER    SPEC*3
      CHARACTER*10 TXTT(NTMAX)
      CHARACTER*10 KW

c ----------------------------------------------------------------------
      COMMON /PLOPAR/ EMIN,EMAX, MERGE
      CHARACTER EUNIT*2
      COMMON /ENERGY/ RYOVEV, EREFER,EUNIT
      CHARACTER FMTEXP*80
      LOGICAL EXPDATAVL
      COMMON /EXPPAR/ ESEXP(2),BLSEXP(2),SCLEXP(2),NEEXP(2),
     &                NCEXP,NPOLEXP,FMTEXP,EXPDATAVL
      COMMON /BRDPAR/ WGAUSS,WLOR(2)
c ----------------------------------------------------------------------

      NTEXT = 'I_'//SPEC//'(E) (arb.u.)'
      LNTEXT = 17
      NS = 2
c ----------------------------------------------------------------------
      DO 2330 IT=1,NT
         READ(IFIL,9050) KW 
         READ(IFIL,9020) ITP
         READ(IFIL,9020) NEXRS
         READ(IFIL,9050) KW

         DO 2340 IEXRS=1,NEXRS
            READ(IFIL,9060) EXRS(IEXRS),((WXRS(IEXRS,IT,IC,IS)
     &                     ,IC=1,NLT(IT)),IS=1,NS)
 2340    CONTINUE
 2330 CONTINUE

c ----------------------------------------------------------------------
      DO 30 IT=1,NT
         READ(IFIL,9050,ERR=9999) KW
         READ(IFIL,9020,ERR=9999) ITP
         READ(IFIL,9010) FMT1
         DO 40 IE=1,NE
            READ(IFIL,9060) E(IE), 
     &                    ((N(IE,IT,IC,IS),IC=1,NLT(IT)),IS=1,NS)
   40    CONTINUE
   30 CONTINUE 
c ----------------------------------------------------------------------
c                      READING IN FINISHED
c ----------------------------------------------------------------------
      IF( EUNIT .EQ. 'eV' ) THEN
         DO 46 IEXRS=1,NEXRS
            EXRS(IEXRS) = EXRS(IEXRS) * RYOVEV
   46    CONTINUE
         DO 47 IE=1,NE
            E(IE) = E(IE) * RYOVEV
   47    CONTINUE
      END IF
      DO 48 IEXRS=1,NEXRS
         EXRS(IEXRS) = EXRS(IEXRS) - EREFER
   48 CONTINUE
      DO 49 IE=1,NE
         E(IE) = E(IE) - EREFER
   49 CONTINUE
      IF((ABS(-1.0-EMIN).LT.1E-5).OR.(ABS(-RYOVEV-EMIN).LT.1E-5)) THEN
         EMIN = E(1)
      END IF
      IF((ABS(+1.0-EMAX).LT.1E-5).OR.(ABS(+RYOVEV-EMAX).LT.1E-5)) THEN
         EMAX = E(NE)
      END IF
c ----------------------------------------------------------------------
c                     PLOT MATRIX ELEMENTS
c ----------------------------------------------------------------------

      CALL FINDEXTR( NLT,WXRS,NEXRSMAX,NTMAX,NCMAX,NSPMAX,
     &               NEXRS,NT,1,NS, WMIN,WMAX )

      DO 180 IT=1,NT
C#AXISTXT( EMIN,     EMAX,     ETEXT, LETEXT,
C#WMIN(IT), WMAX(IT), NTEXT, LNTEXT, S)

         CALL PLOTCURVES( EMIN,EMAX,WMIN(IT),WMAX(IT),
     &                    NEXRSMAX,NTMAX,NCMAX,NSPMAX,
     &                    WXRS,EXRS,Y,NEXRS, IT,IT, 1,NLT(IT), 1,NS )

  180 CONTINUE
  
c ----------------------------------------------------------------------
c                     PLOT XPS/BIS - INTENSITIES
c ----------------------------------------------------------------------
      IF((ABS(9999-WLOR(1)).LT.1E-5)) WLOR(1) = 0.5

      CALL       BROADEN( N,IXRS,BXRS,E,
     &                    WGTGAUTAB,WLORTAB, 1, 0, 
     &                    NEMAX,NTMAX,NCMAX,NSPMAX, NVMAX, 
     &                    NE, 1,NT,  IC1,NLT, 1,NS )
      
      CALL FILLUP( NLT,N,NW,NEMAX,NTMAX,NCMAX,NSPMAX,
     &             NE,NT,NS,CONC,NQT )

      CALL FINDEXTR( NLT,N,NEMAX,NTMAX,NCMAX,NSPMAX,
     &               NE,NT,0,NS, NMIN,NMAX )

      CALL SCALEN( NLT,N,NW,NMIN,NMAX, NEMAX,NTMAX,NCMAX,NSPMAX,
     &            NE,NT,NS, 100.0/NMAX(0) )

      IF( EXPDATAVL ) THEN
C#AXISTXT( EMIN,    EMAX,    ETEXT, LETEXT, 
C#NMIN(0), NMAX(0), NTEXT, LNTEXT, S)
          CALL PLOTCURVES( EMIN,EMAX,NMIN(0),NMAX(0),
     &                     NEMAX,NTMAX,NCMAX,NSPMAX,
     &                     NW,E,Y, NE, 0,NT, 0,0, 0,0)
          CALL PLOTEXP( EEXP(1,1),IEXP(1,1,1), NEEXP(1), 
     &                  ESEXP(1),BLSEXP(1),SCLEXP(1),E,NW(1,0,0,0),NE ) 
      END IF

C#AXISTXT( EMIN,    EMAX,    ETEXT, LETEXT, 
C#NMIN(0), NMAX(0), NTEXT, LNTEXT, S)

      CALL PLOTCURVES( EMIN,EMAX,NMIN(0),NMAX(0),
     &                 NEMAX,NTMAX,NCMAX,NSPMAX,
     &                 NW,E,Y, NE, 0,NT, 0,0, 1,NS )
 
C#CAPTNIT( PY,A,B,TXTT,NT )

c ----------------------------------------------------------------------
      DO 80 IT=1,NT
C#AXISTXT( EMIN,     EMAX,     ETEXT, LETEXT, 
C#     &                 NMIN(IT), NMAX(IT), NTEXT, LNTEXT, S)

         CALL PLOTCURVES( EMIN,EMAX,NMIN(IT),NMAX(IT),
     &                    NEMAX,NTMAX,NCMAX,NSPMAX,
     &                    N,E,Y, NE, IT,IT, 0,NLT(IT), 1,NS)

   80 CONTINUE

c ----------------------------------------------------------------------
      RETURN
 9999 STOP 'ERROR READING DATAFILE'

 9010 FORMAT(10X,A80)
 9020 FORMAT(10X,I10)  
 9030 FORMAT(10X,F10.5)
 9040 FORMAT(I5,1X,A10,F10.5,I5,10I3,:,(41X,10I3))
 9050 FORMAT(10X,A10)
 9060 FORMAT(8F10.4)
      END
