      SUBROUTINE INILORBRD(NVALIN,E,WLORTAB,NE)
C **********************************************************************
C *                                                                    *
C *  evaluate lorentzian broadening width   G(E)  with  E=E(I)         *
C *                                                                    *
C **********************************************************************
 
      IMPLICIT REAL   (A-H,O-Z)
            
      PARAMETER (NPW = 9)
      DIMENSION E(NE), WLORTAB(NE)

      REAL*8 W1,W11,FIT, LOGE, XPW, CF1(NPW), CF11(NPW), CFZ(NPW)
      REAL      X(3), Y(3)
      
c---------  variables needed for reading FACLOR scaling factor ------
      PARAMETER ( LCMDMAX = 500 )
      PARAMETER ( LSTRMAX = 100 )
      PARAMETER ( IFTABLE = 98, IFCONVT = 99 )
      PARAMETER ( IFC=IFCONVT, LSM=LSTRMAX )
      CHARACTER*(LSTRMAX) STR
      CHARACTER*(LCMDMAX) CMDUC, CMD00
      LOGICAL CMDFOUND, UDT, TABLE
      REAL*8 R8
c--------------------------------------------------------------------

      SAVE

      DATA CF1  
     &    /  0.001766920594D0, -0.184509337930D0,  0.409983271398D0,
     &       0.067459351773D0, -0.610733613427D0,  0.451717419499D0,
     &      -0.124878965835D0,  0.014987644299D0, -0.000661473792D0 /

      DATA CF11 
     &    /  0.048696144050D0, -0.537083543337D0,  0.492816604919D0,
     &       0.398174247185D0, -0.444398434205D0,  0.238062954735D0,
     &      -0.061552896506D0,  0.007317769806D0, -0.000324445515D0 /

      IF( NVALIN .EQ. 11 ) THEN
         NVAL = 1
      ELSE
         NVAL = NVALIN
      END IF

      IF( NVAL .LT. 1 .OR. NVAL .GT. 10 ) THEN
         WRITE(*,'(/,''  NVAL='',I3,''   G(E) set to 0'',/)') NVAL
C         DO 5 I=1,NE
C            WLORTAB(I) = 0.0
C    5    CONTINUE    
         RETURN
      END IF
          
      W1  = DBLE( (11.0 - NVAL) / (11.0-1.0) )
      W11 = DBLE( (NVAL - 1.0 ) / (11.0-1.0) )
       
      DO 10 IPW = 1, NPW
         CFZ(IPW) = W1*CF1(IPW) + W11*CF11(IPW)  
   10 CONTINUE    

      DO 20 I=1,NE
         IF (E(I).GT.0.0) THEN
            LOGE = LOG(DBLE( E(I) ))
            XPW = 1.0D0  
            FIT = CFZ(1)
            DO 30 IP=2,NPW
               XPW = XPW * LOGE
               FIT = FIT + CFZ(IP) * XPW
   30       CONTINUE                         

            WLORTAB(I) = REAL(FIT)
         ELSE
            WLORTAB(I) = 0.0
         END IF
   20 CONTINUE   
                 

C------------------------------ interpolate quadratically for E < 2.5 eV
      X(1)=0.0
      X(2)=2.5
      X(3)=3.0
      Y(1)=0.0
      Y(2) = YLAG(X(2),E,WLORTAB,0,2,NE,IEX)
      Y(3) = YLAG(X(3),E,WLORTAB,0,2,NE,IEX)

      DO 40 I=1,NE
         IF( E(I) .GT. 2.5 )  GOTO 41
         WLORTAB(I) = YLAG(E(I),X,Y,0,2,3,IEX)
   40 CONTINUE           

   41 CONTINUE           

      WRITE(6,100) NVAL
      DO 50 I=0,15
         XI = I * 45.0 / 15.0 
         GL = YLAG(XI,E,WLORTAB,0,2,NE,IEX)
         WRITE(6,'(7X,F10.5,5X,F10.5)') XI, GL
   50 CONTINUE

C--------------------------------------------------- set W = 0 for E < 0 
      DO I=1,NE
         WLORTAB(I) = MAX( 0.0001,WLORTAB(I) )
      END DO

c If requested, Lorentzian width is scaled by a factor of FACLOR

      CALL FINDCMD(5,'BROADEN', 7, CMDUC, CMD00, LCMD,
     &                CMDFOUND, TABLE, LCMDMAX, IFTABLE )
      IF( CMDFOUND ) THEN
        CALL UPDATREAL(R8,'FACLOR',6,CMDUC,LCMD,STR,LSM,IFC,UDT)
        IF( UDT ) THEN
          FACLOR = REAL( R8 )
          WRITE(6,'(/,a,f6.3)')
     $         'Valence electron broadening scaled by factor', faclor
        ELSE
          FACLOR = 1.0    
        END IF
      else
        FACLOR = 1.0
      END IF

      DO I=1,NE
         WLORTAB(I) = faclor * WLORTAB(I)
      END DO
      
 
  100 FORMAT(/,'  energy dependent contribution to Lorentz-width',
     &         ' for NVAL=',I3,/,
     &         '        E-E_F (eV)      G(E) (eV) ')
      END
