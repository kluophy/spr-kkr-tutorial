      SUBROUTINE VECGAUBRD(E, F0,FB, NV,NVMAX, NE, NEMAX,
     &                     WG, DXG, IGMAX )
C **********************************************************************
C *                                                                    *
C *    gaussian-broadening of the functions F0       width: WGAUSS     *
C *                                                                    *
C **********************************************************************
 
      IMPLICIT REAL   (A-H,O-Z)

      DIMENSION WG(0:NEMAX)

      DIMENSION E(NEMAX), EG(0:NEMAX)
      DIMENSION F0(NVMAX,NEMAX), FB(NVMAX,NEMAX)
      DIMENSION Y0(NEMAX,NV)

      IEX = 0

      DO IG=0,NEMAX
         EG(IG) = IG*DXG
      END DO

      DE= 1000.0
      DO IE=1,NE-1
         DE = MIN(DE,E(IE+1)-E(IE))
      END DO
      DE = MAX(DE,0.05)
      DECUT = IGMAX * DXG
      NDE = INT( DECUT / DE )
      DE = DECUT / FLOAT( NDE )
      DE = DE * 0.99

      W21= DE * 0.5

      DO 10 IE=1,NE
      DO 10 IV=1,NV
         FB(IV,IE) = 0.0
         Y0(IE,IV) = F0(IV,IE)
   10 CONTINUE

      DO 20 I=1,NE

         DO 30 I1=-NDE,NDE-1

            DE1= I1 *DE
            DE2= DE1+DE

            IG1 = MIN( INT(ABS(DE1)/DXG) + 1, IGMAX-1 )
            IG1 = MAX( IG1, 2 )
            WG1 = YLAG(ABS(DE1),EG(0),WG(0),IG1+1,2,IGMAX,IEX) 
            WG1 = WG1*W21

            IG2 = MIN( INT(ABS(DE2)/DXG) + 1, IGMAX-1 ) 
            IG2 = MAX( IG2, 2 )
            WG2 = YLAG(ABS(DE2),EG(0),WG(0),IG2+1,2,IGMAX,IEX)     
            WG2 = WG2*W21

            E1 = E(I) + DE1
            E2 = E(I) + DE2
            
            DO IE=1,NE
               IE1=IE
               IF( E(IE) .GT. E1 ) GOTO 101
            END DO
 101        CONTINUE
            IF( IE1 .EQ. 1 ) IE1 = 2
            W1A= (E(IE1)-E1)/(E(IE1)-E(IE1-1)) 
            W1B= 1.0-W1A

            DO IE=1,NE
               IE2=IE
               IF( E(IE) .GT. E2 ) GOTO 102
            END DO
 102        CONTINUE
            IF( IE2 .EQ. 1 ) IE2 = 2
            W2A= (E(IE2)-E2)/(E(IE2)-E(IE2-1)) 
            W2B= 1.0-W2A

            DO 40 IV=1,NV
c               Y1 = YLAG(E1,E,Y0(1,IV),IE1,2,NE,IEX)     
c               Y2 = YLAG(E2,E,Y0(1,IV),IE2,2,NE,IEX)  
c               Y1 = YLAG(E1,E,Y0(1,IV),0,2,NE,IEX)     
c               Y2 = YLAG(E2,E,Y0(1,IV),0,2,NE,IEX)  
               Y1 = W1A*Y0(IE1-1,IV)+W1B*Y0(IE1,IV)
               Y2 = W2A*Y0(IE2-1,IV)+W2B*Y0(IE2,IV)

               FB(IV,I) = FB(IV,I) + (WG1*Y1 + WG2*Y2)

   40       CONTINUE
   30    CONTINUE
   20 CONTINUE

      DO 250 IE=1,NE
      DO 250 IV=1,NV
         F0(IV,IE) = FB(IV,IE) 
  250 CONTINUE
      END
