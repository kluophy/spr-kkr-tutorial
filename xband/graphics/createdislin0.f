C*==createdislin.f    processed by SPAG 6.05Rc at 14:25 on 18 Mar 2004
      SUBROUTINE CREATEDISLIN(CDSP,ZMAT,NX,NY,EF,NIPOL,KP,NKMAX,MODE,
     &                        NKDIR,LBLKDIR,INDKDIR,FILNAM,LFN,XMIN,
     &                        KXMIN,XMAX,KXMAX,YMIN,KYMIN,YMAX,KYMAX,
     &                        ZMIN,KZMIN,ZMAX,KZMAX,XTXT,LXTXT,YTXT,
     &                        LYTXT,SRDLAB,LSRDLAB,TITLETXT,LTITLETXT,
     &                        SUBTITLE,LSUBTITLE,KNOHEADER,ICOLOR)
C **********************************************************************
C *  DUMMY version of CREATEDISLIN if DISLIN is not used               *
C *                                                                    *
C **********************************************************************
C
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C PARAMETER definitions
C
      INTEGER LSTRMAX
      PARAMETER (LSTRMAX=80)
C
C Dummy arguments
C
      CHARACTER*4 CDSP
      REAL*8 EF,XMAX,XMIN,YMAX,YMIN,ZMAX,ZMIN
      CHARACTER*(LSTRMAX) FILNAM,SUBTITLE,TITLETXT,XTXT,YTXT
      LOGICAL KNOHEADER
      INTEGER KXMAX,KXMIN,KYMAX,KYMIN,KZMAX,KZMIN,LFN,LSUBTITLE,
     &        LTITLETXT,LXTXT,LYTXT,MODE,NKDIR,NKMAX,NX,NY,NIPOL
      INTEGER INDKDIR(NKDIR),LSRDLAB(8),ICOLOR
      REAL KP(NKMAX)
      CHARACTER*8 LBLKDIR(NKDIR)
      CHARACTER*25 SRDLAB(8)
      REAL*8 ZMAT(NX,NY)
C
C*** End of declarations rewritten by SPAG
C
      STOP 'dummy version of <DISLIN> called '
C
      END
