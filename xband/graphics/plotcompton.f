C*==plotcompton.f    processed by SPAG 6.05Rc at 12:57 on 30 Jan 2002
      SUBROUTINE PLOTCOMPTON(IFIL,NQMAX,NTMAX,NLQ,NLT,NMIN,NMAX,IQOFT,
     &                       NQT,CONC,TXTT,LTXTT,INFO,LINFO,HEADER,
     &                       LHEADER,SYSTEM,LSYSTEM,NQ,NT,NE,IREL,EF,
     &                       DATASET,LDATASET)
C
C*** Start of declarations rewritten by SPAG
C
C PARAMETER definitions
C
      INTEGER NLEGMAX,NPNMAX,NS,NVMAX,NADD,LSTRMAX
      PARAMETER (NLEGMAX=2,NPNMAX=1000,NS=2,NVMAX=1000,NADD=50,
     &           LSTRMAX=100)
C
C COMMON variables
C
      REAL WGAUSS,WLOR(2)
      COMMON /BRDPAR/ WGAUSS,WLOR
C
C Dummy arguments
C
      CHARACTER*(LSTRMAX) DATASET
      REAL EF
      CHARACTER*80 HEADER,INFO,SYSTEM
      INTEGER IFIL,IREL,LDATASET,LHEADER,LINFO,LSYSTEM,NE,NQ,NQMAX,NT,
     &        NTMAX
      REAL CONC(NTMAX),NMAX(0:NTMAX),NMIN(0:NTMAX)
      INTEGER IQOFT(NQMAX,NTMAX),LTXTT(NTMAX),NLQ(NQMAX),NLT(NTMAX),
     &        NQT(NTMAX)
      CHARACTER*10 TXTT(NTMAX)
C
C Local variables
C
      REAL DPN,DXG,N(NVMAX),N1(2,NVMAX),N1DUM(2,NVMAX),
     &     NDIFF(:,:,:),NSUM(:,:,:),PNMAX,
     &     PNVEC(3),PP(NVMAX),WG(0:NVMAX),XMAX,XMIN,YMAX,YMAX2,YMIN,
     &     YMIN2
      CHARACTER*80 FNOUT,SPECFILNAM,YTXT1,YTXT2
      INTEGER I,IGMAX,IQA,IQB,IS,KNOHEADER,LFNOUT,LYTXT1,LYTXT2,NCURVES,
     &        NPN,NVAL,PVEC(3)
      CHARACTER*20 LEG(NLEGMAX)
      INTEGER NINT
      REAL REAL
      CHARACTER*5 STR
      CHARACTER*3 STR1

      ALLOCATABLE NSUM,NDIFF
      ALLOCATE (NSUM(0:NPNMAX,NQMAX,NQMAX))
      ALLOCATE (NDIFF(0:NPNMAX,NQMAX,NQMAX))
C
C*** End of declarations rewritten by SPAG
C
C **********************************************************************
C *                                                                    *
C *  plot Compton    - spectra                                         *
C *                                                                    *
C **********************************************************************
C
C
C
C ----------------------------------------------------------------------
C ----------------------------------------------------------------------
      READ (IFIL,99005) PNVEC
      READ (IFIL,99007) PNMAX
      READ (IFIL,99006) NPN
C
C
      DO I = 0,NPN
         DO IQA = 1,NQMAX
            DO IQB = 1,NQMAX
               NSUM(I,IQA,IQB) = 0.0
               NDIFF(I,IQA,IQB) = 0.0
            END DO
         END DO
      END DO
C
C
      DO I = 1,NPN + 1
         READ (IFIL,'(30e13.5)') PP(I),
     &                           ((NSUM(I,IQA,IQB),NDIFF(I,IQA,IQB),
     &                           IQA=1,NQ),IQB=1,NQ)
      END DO
      OPEN (30,FILE='scratch',STATUS='unknown')
C
C
      DPN = PP(2) - PP(1)
      DO I = 1,NPN + 1
         N1(1,I) = 0.0
         N1(2,I) = 0.0
         DO IQA = 1,NQ
            DO IQB = 1,NQ
               N1(1,I) = NSUM(I,IQA,IQB) + N1(1,I)
               N1(2,I) = NDIFF(I,IQA,IQB) + N1(2,I)
            END DO
         END DO
      END DO
C
      DO I = 1,NPN + 1
         WRITE (30,'(30e13.5)') PP(I),N1(1,I),N1(2,I)
      END DO
C
      IF ( 2*NADD+2*NPN.GT.NPNMAX )
     &      STOP 'cannot extend range. Increase npnmax'
C
Cc----------- shift the row up ,over np + nadd sites
C
      DO I = 1,NPN + 1
         PP(NPN+NADD+I) = PP(I)
         DO IS = 1,NS
C
            N1(IS,NPN+NADD+I) = N1(IS,I)
         END DO
      END DO
C---------------extend the grid of pp points down with np+nadd points
C------------------and up with nadd points
      DO I = 1,NPN + NADD
         PP(NPN+NADD-I+1) = PP(NPN+NADD+1) - DPN*REAL(I)
      END DO
      DO I = 2,NADD + 1
         PP(NADD+2*NPN+I) = PP(NADD+2*NPN+1) + DPN*REAL(I-1)
      END DO
C
C
Cc----------mirror reflexion of the points + repeating the border values
Cc_________  over the nadd points at the begining and at the  end
      DO I = 1,NPN
         DO IS = 1,NS
            N1(IS,NPN+NADD-I+1) = N1(IS,NPN+NADD+I+1)
         END DO
      END DO
      DO I = 1,NADD + 1
         DO IS = 1,NS
            N1(IS,I) = N1(IS,NADD+1)
            N1(IS,NADD+2*NPN+I) = N1(IS,NADD+2*NPN+1)
         END DO
      END DO
C
      NVAL = 2*NPN + 2*NADD + 1
C
C
C
C ----------------------------------------------------------------------
C                       BROADEN AND SCALE CURVES
C ----------------------------------------------------------------------
      IF ( (ABS(9999-WLOR(1)).LT.1E-5) ) WLOR(1) = 0.0
      WLOR(2) = WLOR(1)
      WRITE (*,*) ' WLOR   !!!!!!!!!!! ',WLOR
      WRITE (*,*) ' WGAU   !!!!!!!!!!! ',WGAUSS
      IF ( WGAUSS.GT.0.D0 ) THEN
         CALL INIGAUBRD(WGAUSS,WG,NVMAX,DXG,IGMAX)
         CALL VECGAUBRD(PP,N1,N1DUM,2,NS,NVAL,NVMAX,WG,DXG,IGMAX)
      ELSE
         PRINT *,' Gaussian broadening skipped!'
      END IF
C
C
C
C
      SPECFILNAM = SYSTEM(1:LSYSTEM)//'_compton_sum'
      OPEN (50,FILE=SPECFILNAM)
      WRITE (50,99002) 'total Compton profile for '//SYSTEM(1:LSYSTEM)
      DO I = 1,NVAL
         WRITE (50,'(30e13.5)') PP(I),N1(1,I)
      END DO
C
      SPECFILNAM = SYSTEM(1:LSYSTEM)//'_mcp'
      OPEN (51,FILE=SPECFILNAM)
      WRITE (51,99002) ' MCP for '//SYSTEM(1:LSYSTEM)
      DO I = 1,NVAL
         WRITE (51,'(30e13.5)') PP(I),N1(2,I)
      END DO
C ======================================================================
C                              XMGRACE - output
C ======================================================================
C
      XMIN = 0.D0
      XMAX = PNMAX
C
      IFIL = 90
C
      WRITE (6,99004) XMIN,XMAX
C ----------------------------------------------------------------------
C                              XMGRACE MCP
C----------------------------------------------------------------------
      YTXT1 = 'MCP (arb. units)'
      LYTXT1 = 16
      YTXT2 = ' '
      LYTXT2 = 0
C
      YMAX = -1.E-10
      YMIN = 1.E10
      DO I = 1,NVAL
         IF ( YMAX.LT.N1(2,I) ) YMAX = N1(2,I)
         IF ( YMIN.GT.N1(2,I) ) YMIN = N1(2,I)
      END DO
C
      IF ( N1(2,NPN).LT.0D0 ) THEN
         YMAX = ABS(YMIN)
         YMIN = 0.D0
      END IF
C
      WRITE (6,99001) YMIN,YMAX
      DO I = 1,3
         PVEC(I) = NINT(PNVEC(I))
      END DO
C
      WRITE (6,99003) PVEC
C
      DO I = 1,3
         IF ( PVEC(I).NE.1 .AND. PVEC(I).NE.0 ) THEN
            WRITE (6,*) 'check the scattering direction!'
            WRITE (6,*) PNVEC
            STR = 'x x x'
            STR1 = 'xxx'
            GOTO 100
         END IF
      END DO
C
C
      IF ( PVEC(1).EQ.1 ) THEN
         IF ( PVEC(2).EQ.1 ) THEN
            IF ( PVEC(3).EQ.1 ) THEN
               STR = '1 1 1'
               STR1 = '111'
            ELSE
               STR = '1 1 0'
               STR1 = '110'
            END IF
         ELSE IF ( PVEC(3).EQ.1 ) THEN
            STR = '1 1 0'
            STR1 = '110'
         ELSE
            STR = '0 0 1'
            STR1 = '001'
         END IF
      ELSE IF ( PVEC(2).EQ.1 ) THEN
         IF ( PVEC(3).EQ.1 ) THEN
            STR = '1 1 0'
            STR1 = '110'
         ELSE
            STR = '0 0 1'
            STR1 = '001'
         END IF
      ELSE IF ( PVEC(3).EQ.1 ) THEN
         STR = '0 0 1'
         STR1 = '001'
      ELSE
         STOP 'direction nonexistent!'
      END IF
C
C
 100  CONTINUE
      CALL XMGR4HEAD(SYSTEM,LSYSTEM,'mcp',3,STR1(1:3),3,FNOUT,80,
     &               LFNOUT,IFIL,1,XMIN,1,XMAX,1,YMIN,1,YMAX,1,YMIN2,0,
     &               YMAX2,0,'momentum pz (atomic units)',26,YTXT1,
     &               LYTXT1,YTXT2,LYTXT2,HEADER,LHEADER,
     &               'MCP along scattering direction '//STR(1:5),(36),
     &               KNOHEADER)
C
C
      NCURVES = 1
C
      CALL XMGRCURVES(IFIL,1,NCURVES,0,2,0,0)
CC
C
      LEG(1) = 'mcp for '//SYSTEM(1:LSYSTEM)
      LEG(2) = ' '
C
CC
C
      CALL XMGRLEGEND(IFIL,1,NCURVES,0,LEG(1),LEG(2))
C
      IF ( N1(2,NPN).LT.0D0 ) THEN
         DO I = 1,NVAL
            N(I) = -N1(2,I)
         END DO
      ELSE
C
         DO I = 1,NVAL
            N(I) = N1(2,I)
         END DO
      END IF
C
      CALL XMGR4TABLE(0,0,PP,N,1.0,NVAL,IFIL)
CcC
CC
      WRITE (6,*) '  '
      WRITE (6,*) 'MCP writen in ',FNOUT(1:LFNOUT)
      CLOSE (IFIL)
C___________________________________________________________
C
C     XMGRACE - Sum of Compton profile
C ----------------------------------------------------------------------
      IFIL = 91
C
      YTXT1 = 'Sum of Compton profile (arb. units)'
      LYTXT1 = 35
      YTXT2 = ' '
      LYTXT2 = 0
C
      YMAX = -1D-10
      YMIN = 1D10
C
      YMAX = -1.E-10
      YMIN = 1.E10
      DO I = 1,NVAL
         IF ( YMAX.LT.N1(1,I) ) YMAX = N1(1,I)
         IF ( YMIN.GT.N1(1,I) ) YMIN = N1(1,I)
      END DO
C
C
C
C
C
      CALL XMGR4HEAD(SYSTEM,LSYSTEM,'sum',3,STR1(1:3),3,FNOUT,81,
     &     LFNOUT,IFIL, 1,XMIN,1,XMAX,1,YMIN,1,YMAX,1,YMIN2,0,YMAX2,0,
     &     'momentum pz (a.u.)',18,YTXT1,LYTXT1,YTXT2,LYTXT2,
     &     HEADER,LHEADER,'Sum of Compton profile for '//
     &     SYSTEM(1:LSYSTEM),(27+LSYSTEM),KNOHEADER)
C
      CALL XMGRCURVES(IFIL,1,NCURVES,0,2,0,0)
C
C
      LEG(1) = 'cp for '//SYSTEM(1:LSYSTEM)
      LEG(2) = ' '
C
C
      CALL XMGRLEGEND(IFIL,1,NCURVES,0,LEG(1),LEG(2))
C
C
      IF ( N1(1,NPN).LT.0D0 ) THEN
         DO I = 1,NVAL
            N(I) = -N1(1,I)
         END DO
      ELSE
C
         DO I = 1,NVAL
            N(I) = N1(1,I)
         END DO
      END IF
C
      CALL XMGR4TABLE(0,0,PP,N,1.0,NVAL,IFIL)
CcC
CC
      WRITE (6,*) '  '
      WRITE (6,*) 'CP_sum writen in ',FNOUT(1:LFNOUT)
      CLOSE (IFIL)
C
C
C
CC ----------------------------------------------------------------------
C
C
C
C ----------------------------------------------------------------------
C ----------------------------------------------------------------------
      RETURN
C
99001 FORMAT (5x,f12.5,3x,'YMIN',f12.5,3x,'YMAX')
99002 FORMAT ('#  ',A,A,A)
99003 FORMAT (5x,I2,1x,I2,1x,I2,1x,'SCATTERING DIRECTION')
99004 FORMAT (5X,f12.5,3x,'XMIN',f12.5,3x,'XMAX')
99005 FORMAT (15x,3F6.4)
99006 FORMAT (19x,I3)
99007 FORMAT (22x,f7.3)
C
C
C
      END
