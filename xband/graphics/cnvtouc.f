      SUBROUTINE CNVTOUC(STR,I1,I2)
C   ********************************************************************
C   *                                                                  *
C   *   convert string STR(L1:L2)  to upper case                       *
C   *                                                                  *
C   ********************************************************************
      CHARACTER*(*) STR

      IA  = ICHAR( 'a' )
      IZ  = ICHAR( 'z' )
      INC = ICHAR( 'A' ) - IA

      DO 10 I=I1,I2  
         J = ICHAR( STR(I:I) )
         IF( J .GE. IA .AND. J .LE. IZ ) THEN
            STR(I:I) = CHAR( J + INC )
         END IF   
   10 CONTINUE
      END
