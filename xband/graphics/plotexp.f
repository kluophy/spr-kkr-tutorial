      SUBROUTINE PLOTEXP( EEXP,IEXP,NEEXP, 
     &                    ESEXP,BLSEXP,SCLEXP,E,I,NE ) 
C **********************************************************************
C *                                                                    *
C *      plot experimental spectra                                     *
C *                                                                    *
C ********************************************************************** 
      REAL EEXP(NEEXP), IEXP(NEEXP), E(NE), I(NE)
      
      IF( ABS(ESEXP)+ABS(BLSEXP)+ABS(SCLEXP) .LT. 0.0001 ) THEN
         THEOMAX = 0.0
         DO 10 IE=1,NE
            IF( I(IE) .GT. THEOMAX ) THEN
               THEOMAX = I(IE)
               ETHEOMAX= E(IE)
            END IF
   10    CONTINUE
         EXPMIN = +9.9E+9
         EXPMAX = -9.9E+9
         DO 20 IE=1,NEEXP
            EXPMIN = MIN( EXPMIN,IEXP(IE))  
            IF( IEXP(IE) .GT. EXPMAX ) THEN
               EXPMAX = IEXP(IE)
               EEXPMAX= EEXP(IE)
            END IF
   20    CONTINUE
         
         ESEXP = EEXPMAX - ETHEOMAX 
         BLSEXP = EXPMIN
         SCLEXP= THEOMAX / ( EXPMAX - EXPMIN ) 
         WRITE(6,*) ' no scaling of experimental data provided'
         WRITE(6,*) ' scaling determined from peak position'
         WRITE(6,9000) ' ES=',ESEXP,'   BLS=',BLSEXP,' SCL=',SCLEXP    
         
         DO 30 IE=1,NEEXP
            EEXP(IE) =  EEXP(IE) -  ESEXP
            IEXP(IE) = (IEXP(IE) - BLSEXP) * SCLEXP 
   30    CONTINUE 
      END IF
 9000 FORMAT(3(A,F8.3))

C# CURVE(1,NEEXP, EEXP,IEXP, 'LINEAR' )

      END
