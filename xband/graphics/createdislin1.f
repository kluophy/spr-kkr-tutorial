C*==createdislin.f    processed by SPAG 6.05Rc at 14:25 on 18 Mar 2004
      SUBROUTINE CREATEDISLIN(CDSP,ZMAT,NX,NY,EF,NIPOL,KP,NKMAX,MODE,
     &                        NKDIR,LBLKDIR,INDKDIR,FILNAM,LFN,XMIN,
     &                        KXMIN,XMAX,KXMAX,YMIN,KYMIN,YMAX,KYMAX,
     &                        ZMIN,KZMIN,ZMAX,KZMAX,XTXT,LXTXT,YTXT,
     &                        LYTXT,SRDLAB,LSRDLAB,TITLETXT,LTITLETXT,
     &                        SUBTITLE,LSUBTITLE,KNOHEADER,ICOLOR)
C **********************************************************************
C *  add libs=$DISLIN/libdislin_d.so ; f77 *.f $libs
C *                                                                    *
C *  CDSP                  display format for dislin
C *  ZMAT                  zmatrix of block spectral function          *
C *  NX,NY                 size of zmatrix 1..NX,1..NY                 *
C *  EF                    fermi energie in ymin,ymax coordinates      *
C *  NIPOL                 number of interpolations on plotdatagrid    *
C *
C *  KP
C *  NKMAX
C *
C *  MODE                  mode=1: k-E plot   mode=2: k-k plot         *
C *                        mode=3: surroundlabels (k-k plot)
C *  NKDIR                 number of k segments                        *
C *  LBLKDIR               label of segment (indexed 1..NKDIR)         *
C *  INDKDIR               index of segment start (indexed 1..NKDIR)   *
C *                                                                    *
C *  FILNAM    LFN         file name to be created and length          *
C *                                                                    *
C *  XMIN      KXMIN       x-start and key to fix (0) or float (1)     *
C *  XMAX      KXMAX       x-end   and key to fix (0) or float (1)     *
C *  YMIN      KYMIN       y-start and key to fix (0) or float (1)     *
C *  YMAX      KYMAX       y-end   and key ...                         *
C *  ZMIN      KZMIN       z-start and key ...                         *
C *  ZMAX      KZMAX       z-end   and key ...                         *
C *  XTXT      LXTXT       text for x-axis label                       *
C *  YTXT      LYTXT       text for y-axis label                       *
C *  SRDLAB    LSRDLAB     surround label for k-k plot in mode 3       *
C *                                                                    *
C *     7-----6-----5   index and position of SRDLAB()                 *
C *     |           |                                            
C *     8           4
C *     |           |
C *     1-----2-----3
C *                                                                    *
C *                                                                    *
C *  TITLETXT  LTITLETXT   title and length of string                  *
C *  SUBTITLE  LSUBTITLE   subtitle and length of string               *
C *  KNOHEADER             key to get (false) or suppress (true) title *
C *  ICOLOR                Define color map to be used: Default 8      *
C *     1 ='SMALL' defines a small colour table with the 8 colours:    *
C *     2 = 'VGA'  defines the 16 standard colours of a VGA card.      *
C *     3 = 'RAIN' defines 256 colours arranged in a rainbow           *
C *                where 0 means black and 255 means white.            *
C *     4 = 'SPEC' defines 256 colours arranged in a rainbow           *
C *                where 0 means black and 255 means white.            *
C *                This colour table uses more violet colours          *
C *                than 'RAIN'.                                        *
C *     5  = 'GREY' defines 256 grey scale colours where               *
C *                0 means black and 255 is white.                     *
C *     6 = 'RRAIN' is the reverse colour table of 'RAIN'.             *
C *     7 = 'RSPEC' is the reverse colour table of 'SPEC'.             *
C *     8 = 'RGREY' is the reverse colour table of 'GREY'.             * 
C *     9 = 'TEMP'  defines a temperature colour table                 *
C *                 
C **********************************************************************
C
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C PARAMETER definitions
C
      INTEGER LSTRMAX
      PARAMETER (LSTRMAX=80)
C
C Dummy arguments
C
      CHARACTER*4 CDSP
      REAL*8 EF,XMAX,XMIN,YMAX,YMIN,ZMAX,ZMIN
      CHARACTER*(LSTRMAX) FILNAM,SUBTITLE,TITLETXT,XTXT,YTXT
      LOGICAL KNOHEADER
      INTEGER KXMAX,KXMIN,KYMAX,KYMIN,KZMAX,KZMIN,LFN,LSUBTITLE,
     &        LTITLETXT,LXTXT,LYTXT,MODE,NKDIR,NKMAX,NX,NY,NIPOL
      INTEGER INDKDIR(NKDIR),LSRDLAB(8)
      REAL KP(NKMAX)
      CHARACTER*8 LBLKDIR(NKDIR)
      CHARACTER*25 SRDLAB(8)
      REAL*8 ZMAT(NX,NY)
C
C Local variables
C
      DOUBLE PRECISION DBLE
      REAL*8 DX,DY,XA,XB,XLBL(3),YA,YB
      CHARACTER*80 KLBL(:,:)
      INTEGER LNGKLBL(:,:),NMAJ,NMIN,PX,PX0,PXDIS,PXLEN,PY,PY0,
     &        PYDIS,PYLEN,PZDIS,PZLEN,ID,LB,NLMESS,ICOLOR
      CHARACTER*5 COLOR(9)
      DATA COLOR /'SMALL','VGA  ','RAIN ','SPEC ','GREY ','RRAIN','
     &     RSPEC','RGREY','TEMP ' /

      ALLOCATABLE KLBL,LNGKLBL
      ALLOCATE (KLBL(NKDIR,3),LNGKLBL(NKDIR,3))
C
C*** End of declarations rewritten by SPAG
      IF(ICOLOR .LT. 1 .OR. ICOLOR .GT. 9 ) ICOLOR=8

C
C ----------------------------------------------------------------------
C                       set range  parameters
C ----------------------------------------------------------------------
C
      CALL XMGRTICKS(XMIN,XA,KXMIN,XMAX,XB,KXMAX,0.0D0,DX,1)
      CALL XMGRTICKS(YMIN,YA,KYMIN,YMAX,YB,KYMAX,0.0D0,DY,1)

C
C ----------------------------------------------------------------------
C         create filename   FILNAM(1:LFN)//'.ps'   and open
C ----------------------------------------------------------------------
C
      IF ( CDSP.NE.'XWIN' ) THEN
         CALL SETFIL(FILNAM(1:LFN))
C  COUNT: new file version, DELETE: overwrite existing file
         CALL FILMOD('DELETE')
C
      ELSE
C to enable refresh of window content
         CALL X11MOD('STORE')
         CALL SCRMOD ('REVERSE')
C
      END IF
C ------------------------------------------------ start dislin graphics
C
C XWIN / POST
      CALL METAFL(CDSP)
      CALL DISINI
C TeX mode (has to be enabled at call to graf3)
      CALL TEXMOD('ON')
C
C border around page
      CALL PAGERA
C choose font complex
      CALL COMPLX
C      CALL PSFONT('Times-Roman')
C
C
C ---------------------------------------------------------------- title
      IF ( .NOT.KNOHEADER ) THEN
C
C      CALL DISLINESCS(,TITLE,LTITLE)
C
         CALL TITLIN(TITLETXT(1:LTITLETXT),1)
         CALL TITLIN(SUBTITLE(1:LSUBTITLE),3)
      END IF
C
C ------------------ axis text
C
      CALL NAME(XTXT(1:LXTXT),'X')
      CALL NAME(YTXT(1:LYTXT),'Y')
C
C order:bottom, left, top, right
C none:   --
C line:   line
C ticks:  lines + ticks
C labels: lines + ticks + labels
C name:   lines + ticks + labels + axisname
C
      IF ( MODE.EQ.1 ) THEN
          IF(LBLKDIR(1)(1:4).EQ. 'NONE') THEN
C if no label setted than use numbers
              CALL SETGRF('LABELS','NAME','NONE','NONE')
          ELSE 
C if label setted
              CALL SETGRF('LINE','NAME','LINE','TICKS')
          END IF
      ELSE
         CALL SETGRF('NAME','NAME','TICKS','TICKS')
      END IF
C
      IF ( MODE.EQ.3 ) THEN
C     number of ticks between axis labels
         CALL TICKS(0,'X')
         CALL TICKS(0,'Y')
C determines which label types will be plotted on axis
         CALL LABELS('NONE','XY')
         CALL NAMDIS(150,'XY')
C only one tick between start and end (graf3)
         DX = (XB-XA)
         DY = (YB-YA)
      END IF
C
C size of coloured rectangles automatically by graf3
      CALL AUTRES(NX,NY)
C no colorbar
      CALL NOBAR
C position of axis system
      CALL AXSPOS(300,1850)
C define axis length of coloured axis system
      CALL AX3LEN(2200,1400,1400)
C
C x_anfang x_ende first_xlabel x_step, same for y and z
C ------------------------- plot line at fermi energy level
      CALL GRAF3(XA,XB,XA,DX,YA,YB,YA,DY,ZMIN,ZMAX,ZMIN,(ZMAX
     &     -ZMIN)*0.01)
C
C matrix xpoints ypointe xinterpolation yinterpolation
C Set colour map for surface plot
      CALL SETVLT (COLOR(ICOLOR))
C Set colour for lines and labels (0=black)
      CALL SETCLR (0)
      CALL CRVMAT(ZMAT,NX,NY,NIPOL,NIPOL)
C character height in plot coordinates
      CALL HEIGHT(50)
C
C
      IF ( MODE.EQ.1 ) THEN

C ------------------------- plot line at fermi energy level
         CALL RLINE(XA,EF,XB,EF)
         CALL ADDLAB('$E_F$',EF,0,'y')
C
C------------------------- manually position xaxislabel
C
C set messag height to default axisname height
         CALL HEIGHT(36)
C origin of axis system (0,0) in user coords
         CALL GETPOS(PX0,PY0)
C GETSP1 returns the distance between axis ticks and labels
         CALL GETSP1(PXDIS,PYDIS,PZDIS)
         PY = PY0 + PXDIS
C GETSP2 returns the distance between axis labels and names
         CALL GETSP2(PXDIS,PYDIS,PZDIS)
         PY = PY + PXDIS
C text height
C         CALL GETHGT(PYDIS)
         PYDIS = 50
         PY = PY + PYDIS
C tick length
         CALL GETTCL(NMAJ,NMIN)
         PY = PY + MAX(NMAJ,NMIN)
C length of axis
         CALL GETLEN(PXLEN,PYLEN,PZLEN)
C length of stringexpression (correct height has to be set)
         PYDIS = NLMESS(XTXT(1:LXTXT))
         PX = PX0 + 0.5*(PXLEN-PYDIS)
C
         CALL MESSAG(XTXT(1:LXTXT),PX,PY)
C
C re set character height in plot coordinates
         CALL HEIGHT(50)
C
C --------------------------------- plot vertical lines
C split LBLKDIR() as in  plotband.f
C
         IF(LBLKDIR(1)(1:4) .NE. 'NONE') THEN
            CALL PREPKDIRLAB(NKDIR,INDKDIR,LBLKDIR,KLBL,LNGKLBL,'\\')
C     
            DO ID = 1,NKDIR
C     
               IF ( ID.EQ.1 ) THEN
                  XLBL(1) = 0D0
               ELSE
                  XLBL(1) = DBLE(KP(INDKDIR(ID-1)+1))
               END IF
               XLBL(3) = DBLE(KP(INDKDIR(ID)))
               XLBL(2) = (XLBL(3)+XLBL(1))/2.0
C     
C user coordinates
               IF ( ID.NE.NKDIR ) CALL RLINE(XLBL(3),YA,XLBL(3),YB)
C
C axis label
               DO LB = 1,3
                  CALL ADDLAB(KLBL(ID,LB)(1:LNGKLBL(ID,LB)),XLBL(LB),
     &            	0,'X')
               END DO
C     
            END DO
         END IF

C end mode .eq. 1
      ELSE IF ( MODE.EQ.3 ) THEN
C
C axis midposition
         DX = 0.5*(XB-XA)
         DY = 0.5*(YB-YA)
C
         CALL ADDLAB(SRDLAB(1)(1:LSRDLAB(1)),XA,0,'X')
         CALL ADDLAB(SRDLAB(2)(1:LSRDLAB(2)),DX,0,'X')
         CALL ADDLAB(SRDLAB(3)(1:LSRDLAB(3)),XB,0,'X')
C
         CALL ADDLAB(SRDLAB(5)(1:LSRDLAB(5)),XB,0,'XTOP')
         CALL ADDLAB(SRDLAB(6)(1:LSRDLAB(6)),DX,0,'XTOP')
         CALL ADDLAB(SRDLAB(7)(1:LSRDLAB(7)),XA,0,'XTOP')
C
         CALL ADDLAB(SRDLAB(4)(1:LSRDLAB(4)),DY,0,'YLEFT')
         CALL ADDLAB(SRDLAB(8)(1:LSRDLAB(8)),DY,0,'Y')
C
      END IF
C
      CALL TITLE
C      CALL TEXMOD('OFF')
      CALL DISFIN
C
      END
C*==prepkdirlab.f    processed by SPAG 6.05Rc at 14:25 on 18 Mar 2004
      SUBROUTINE PREPKDIRLAB(NKDIR,INDKDIR,LBLKDIR,KLBL,LNGKLBL,BSLASH)
C
C                 prepare kdir labels
C
C *  when calling PREPKDIRLAB BSLASH should be set to '\\'.      
C *  for any compiler / machine this should be converted to      
C *  have the escape character  '\'  in the labels at the end    
C * (e.g. required for ifort vs. g77)
C
C  call to ENSUREBSLASH(BSLASH,'\\') could be used instead of BSLASH arg 
C
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      INTEGER NKDIR
      INTEGER INDKDIR(NKDIR),LNGKLBL(NKDIR,3)
      CHARACTER*1 BSLASH
      CHARACTER*80 KLBL(NKDIR,3)
      CHARACTER*8 LBLKDIR(NKDIR)
C
C Local variables
C
      CHARACTER C1
      INTEGER I,ID,LB
C
C*** End of declarations rewritten by SPAG
C
      DO ID = 1,NKDIR
         DO LB = 1,3
            I = LB*3 - 2
            C1 = LBLKDIR(ID)(I:I)
            IF ( (C1.EQ.'G' .OR. C1.EQ.'g') ) THEN
C               KLBL(ID,LB) = '!x'//LBLKDIR(ID)((I+1):(I+1))//'!f{}'
C               LNGKLBL(ID,LB) = 2 + 1 + 4
C
               C1 = LBLKDIR(ID)((I+1):(I+1))
               IF ( C1.EQ.'G' .OR. C1.EQ.'g' ) THEN
                  KLBL(ID,LB) = BSLASH//'Gamma'
                  LNGKLBL(ID,LB) = 6
               ELSE IF ( C1.EQ.'L' .OR. C1.EQ.'l' ) THEN
                  KLBL(ID,LB) = BSLASH//'Lambda'
                  LNGKLBL(ID,LB) = 7
               ELSE IF ( C1.EQ.'S' .OR. C1.EQ.'s' ) THEN
                  KLBL(ID,LB) = BSLASH//'Sigma'
                  LNGKLBL(ID,LB) = 6
               ELSE IF ( C1.EQ.'D' .OR. C1.EQ.'d' ) THEN
                  KLBL(ID,LB) = BSLASH//'Delta'
                  LNGKLBL(ID,LB) = 6
               ELSE
C     what should i do here
                  STOP 'unknown greek letter in LBLKDIR'
                  KLBL(ID,LB) = '!x'//C1//'!f{}'
                  LNGKLBL(ID,LB) = 2 + 1 + 4
               END IF
C
            ELSE
               KLBL(ID,LB) = C1
               LNGKLBL(ID,LB) = 1
            END IF
         END DO
         IF ( ID.GT.1 ) THEN
            IF ( KLBL(ID,1).EQ.KLBL(ID-1,3) ) THEN
               KLBL(ID,1) = ' '
               LNGKLBL(ID,1) = 1
            ELSE
C
C               KLBL(ID-1,3) = KLBL(ID-1,3)(1:LNGKLBL(ID-1,3))//'   '
C               LNGKLBL(ID-1,3) = LNGKLBL(ID-1,3) + 3
C               KLBL(ID,1) = '   '//KLBL(ID,1)(1:LNGKLBL(ID,1))
C               LNGKLBL(ID,1) = LNGKLBL(ID,1) + 3
C
C------------- combine 2 k-labels on seg ID-1 and delete that for seg ID
C
               KLBL(ID-1,3) = KLBL(ID-1,3)(1:LNGKLBL(ID-1,3))
     &                        //' '//KLBL(ID,1)(1:LNGKLBL(ID,1))
               LNGKLBL(ID-1,3) = LNGKLBL(ID-1,3) + 1 + LNGKLBL(ID,1)
               KLBL(ID,1) = ' '
               LNGKLBL(ID,1) = 1
            END IF
         END IF
      END DO
C
      END

