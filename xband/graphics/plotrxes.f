
      SUBROUTINE PLOTRXES( IFIL,
     &                    NQMAX,NTMAX,NEMAX,NCMAX,NCSTMAX,NSPMAX,NLMAX,
     &                    SPEC,
     &                    NLQ, NLT, NMIN,NMAX, 
     &                    IQOFT,NQT,CONC,TXTT,LTXTT, INFO,LINFO, 
     &                    HEADER,LHEADER, SYSTEM,LSYSTEM, E, EF,
     &                    NT,NS,NE,
     &                    IXRS,BXRS,NVMAX,WGTGAUTAB,WLORTAB,
     &                    EEXP, IEXP, NCEXPMAX,NPOLEXPMAX, 
     &                    NCXRAY, LCXRAY, ITXRSGRP, ECORE, 
     &                    KTYPCOR,KAPCOR,MM05COR,NKPCOR,IKMCOR, IN,  
     &                    AD,AT,RD,RT, TD,TT,TE, KTABLE, SCLFAC, 
     &                    NOCORESPLIT, NOTABEXT )
C **********************************************************************
C *                                                                    *
C *    the programm reads in XAS - spectra from the  LMTO - or  KKR -  *
C *    package and broadens the spectra according the directives       *
C *                                                                    *
C *    relativistic version                                            *
C *                                                                    *
C *              NPOL = 2:        NPOL = 3:                            *
C *    IPOL =  1  <==>  (-)     1  <==>  (-)                           *
C *                             2  <==>  (+)                           *
C *            2  <==>  (+)     3  <==>  (z)                           *
C *                                                           14/05/94 *
C ********************************************************************** 
c ----------------------------------------------------------------------
      PARAMETER ( LCMDMAX = 500 )
      PARAMETER ( LSTRMAX = 100 )
      PARAMETER ( IFTABLE = 98, IFCONVT = 99 )
      PARAMETER ( IFC=IFCONVT, LSM=LSTRMAX )
c ----------------------------------------------------------------------


      REAL         EEXP(NEMAX,NCEXPMAX), IEXP(NEMAX,NCEXPMAX,NPOLEXPMAX)
      REAL         E(NEMAX)
      REAL         WAD(:,:,:),
     &             WAT(:,:,:)
      REAL         AD(NEMAX,0:NTMAX,0:NCSTMAX,0:NSPMAX),
     &             AT(NEMAX,0:NTMAX,0:NCSTMAX,0:NSPMAX),
     &             RD(NEMAX,0:NTMAX,0:NCSTMAX,0:NSPMAX), 
     &             RT(NEMAX,0:NTMAX,0:NCSTMAX,0:NSPMAX)

      CHARACTER*80 HEADER,SYSTEM, NTEXT, INFO
      CHARACTER    SHELL(5)*1, SPEC*3, EXT*3
      CHARACTER*10 TXTT(NTMAX), STR10
      CHARACTER*10 KW
      
      INTEGER LHEADER, LSYSTEM, TABNVAL
      INTEGER NLQ(NQMAX), NLT(NTMAX)
      INTEGER IQOFT(NQMAX,NTMAX), NQT(NTMAX), LTXTT(NTMAX)
      INTEGER NCXRAY(NTMAX), LCXRAY(NTMAX)
      INTEGER ITXRSGRP(NTMAX)
      INTEGER ICST1(2), ICST2(2), KTYP(2), KTYPCOR(NCSTMAX)
      INTEGER KAPCOR(NCSTMAX), MM05COR(NCSTMAX),
     &        NKPCOR(NCSTMAX),  IKMCOR(NCSTMAX,2)
      INTEGER IN(NEMAX)
      INTEGER ATOMNUMB
      
      REAL    IXRS(NVMAX,NEMAX), BXRS(NVMAX,NEMAX)
      REAL    WGTGAUTAB(0:NEMAX)
      REAL    CONC(NTMAX) 
      REAL    NMIN(0:NTMAX), NMAX(0:NTMAX)
      REAL    ECORE(NTMAX,NCSTMAX)
      REAL    TD(NEMAX),TT(NEMAX),TE(NEMAX), WLORTAB(NEMAX)
      REAL    SUMK(2), ECOREAV(2), RAV(2),RDF(2),VUC
      LOGICAL MLD, CHIXAS      

c ----------------------------------------------------------------------
      COMMON /PLOPAR/ EMIN,EMAX, MERGE
      COMMON /BRDPAR/ WGAUSS,WLOR(2)
      CHARACTER EUNIT*2 
      COMMON /ENERGY/ RYOVEV, EREFER,EUNIT
      CHARACTER FMTEXP*80
      LOGICAL EXPDATAVL
      COMMON /EXPPAR/ ESEXP(2),BLSEXP(2),SCLEXP(2),NEEXP(2),
     &                NCEXP,NPOLEXP,FMTEXP,EXPDATAVL
c ----------------------------------------------------------------------
      CHARACTER*(LCMDMAX) CMDUC, CMD00
      CHARACTER*(LSTRMAX) DATASET, STR
      CHARACTER*(LSTRMAX) XASFILE
      LOGICAL FOUND, TABLE, CMDFOUND, UDT, KTABLE
      LOGICAL NOCORESPLIT, NOTABEXT
c ----------------------------------------------------------------------

      DATA    SHELL / 'K', 'L', 'M', 'N', 'O' /                  

      ALLOCATABLE WAD,WAT
      ALLOCATE (WAD(0:NTMAX,0:NCSTMAX,0:NSPMAX))
      ALLOCATE (WAT(0:NTMAX,0:NCSTMAX,0:NSPMAX))

      IF( NOCORESPLIT ) THEN
         NOTABEXT    = .TRUE.
         KTABLE      = .TRUE.
      END IF
      KTABLE      = .TRUE.
c      
      NTEXT = 'I_'//SPEC//'(E) (arb.u.)'

      ITCHECK = 0
      IA = ICHAR( 'a' )

c ----------------------------------------------------------------------
      WRITE(6,'(//,'' '',70(''*''),/,
     & ''              reading XAS information'',/,'' '',70(''*''))')
c ----------------------------------------------------------------------
      CALL FINDCMD(5,'XASFILE',7,CMDUC,CMD00,LCMD,FOUND,TABLE,
     &                LCMDMAX,IFTABLE)
      IF( .NOT. FOUND  )  STOP ' XASFILE not specified'

      IPOS = 1
      CALL GTVALSTR(XASFILE,LXASFILE,LSM,CMD00,IPOS,LCMDMAX)
      IFIL2 = 2
      OPEN( IFIL2, FILE=XASFILE )
      WRITE(6,*) ' XASFILE:    ',XASFILE(1:LXASFILE)

      READ(IFIL2,'(10X,A)') SPEC
      CALL RDHEAD( IFIL2, AD,AT, NQMAX,NTMAX,NEMAX,NCMAX,NSPMAX,
     &             NLQ, NLT, NMIN, NMAX,
     &             IQOFT,NQT,CONC,TXTT,LTXTT, INFO,LINFO,
     &             HEADER,LHEADER, SYSTEM,LSYSTEM,
     &             NQ,NT,NE,IREL,EF )

      CALL       RDRXAS( SPEC,IFIL2,NEXAS,NPOL,E,AD,AT,MLD,RYOVEV,EUNIT,
     &                   ECORE,NCST,KAPCOR,MM05COR,NKPCOR,IKMCOR,
     &                   NCXRAY, LCXRAY, ITXRSGRP, NTXRSGRP, CHIXAS,VUC,
     &                   KUNIT,NEMAX,NCSTMAX,NSPMAX,NTMAX)

      WRITE(6,'(/,'' XAS read for '',I3,'' energies'',
     &            '' in the range of '',F10.4,'' -- '',F10.4)')
     &        NEXAS, E(1),E(NEXAS)    

      DO ITXG=1,NTXRSGRP

         DO ICST = 1,NCST
            DO IPOL = 1,NPOL
               WAD(ITXG,ICST,IPOL) = 0.0
               WAT(ITXG,ICST,IPOL) = 0.0
            END DO
         END DO

         DO IE = 1,NEXAS
            IF( NEXAS .EQ. 1 ) THEN
               WGTE = 1.0
            ELSE
               IF( (IE .EQ. 1) .OR. (IE .EQ. NEXAS) ) THEN
                  WGTE = 0.5 / FLOAT(NEXAS-1)
               ELSE 
                  WGTE = 1.0 / FLOAT(NEXAS-1)
               END IF
            END IF

            DO ICST = 1,NCST
               DO IPOL = 1,NPOL
                  WAD(ITXG,ICST,IPOL) = 
     &            WAD(ITXG,ICST,IPOL) + WGTE*AD(IE,ITXG,ICST,IPOL)

                  WAT(ITXG,ICST,IPOL) = 
     &            WAT(ITXG,ICST,IPOL) + WGTE*AT(IE,ITXG,ICST,IPOL)
               END DO
            END DO
         END DO
      END DO


c ----------------------------------------------------------------------
      WRITE(6,'(//,'' '',70(''*''),/,
     & ''              reading XES information'',/,'' '',70(''*''))')
c ----------------------------------------------------------------------

      REWIND IFIL

      READ(IFIL,'(10X,A)') SPEC
      CALL RDHEAD( IFIL, RD,RT, NQMAX,NTMAX,NEMAX,NCMAX,NSPMAX,
     &             NLQ, NLT, NMIN, NMAX,
     &             IQOFT,NQT,CONC,TXTT,LTXTT, INFO,LINFO,
     &             HEADER,LHEADER, SYSTEM,LSYSTEM,
     &             NQ,NT,NE,IREL,EF )

      CALL       RDRXAS( SPEC,IFIL,NE,NPOL,E,RD,RT, MLD,RYOVEV,EUNIT,
     &                   ECORE,NCST,KAPCOR,MM05COR,NKPCOR,IKMCOR,
     &                   NCXRAY, LCXRAY, ITXRSGRP, NTXRSGRP,CHIXAS,VUC,
     &                   KUNIT,NEMAX,NCSTMAX,NSPMAX,NTMAX)

      DO ITXG=1,NTXRSGRP
      DO IE=1,NE

         DO ICST = 1,NCST
            RD(IE,ITXG,ICST,0) = 0.0
            RT(IE,ITXG,ICST,0) = 0.0

c ----------------------------------- sum over polarisation for emission
            DO IPOL = 1,NPOL
               RD(IE,ITXG,ICST,0) =
     &         RD(IE,ITXG,ICST,0) + RD(IE,ITXG,ICST,IPOL)
               RT(IE,ITXG,ICST,0) =
     &         RT(IE,ITXG,ICST,0) + RT(IE,ITXG,ICST,IPOL)
            END DO


c ------------------------ weight with polarisation dependent population
c -------------------------------------- i.e. XAS absorption coefficient
            DO IPOL = 1,NPOL
               RD(IE,ITXG,ICST,IPOL) =    WAD(   ITXG,ICST,IPOL) 
     &                                   * RD(IE,ITXG,ICST,0)
               RT(IE,ITXG,ICST,IPOL) =    WAT(   ITXG,ICST,IPOL) 
     &                                   * RT(IE,ITXG,ICST,0)
            END DO

         END DO

      END DO
      END DO

c ======================================================================

c ------------------------------------------- adjust tables if necessary
      DO ITXG = 2,NTXRSGRP
         IF( ITXRSGRP(ITXG) .LE. ITXRSGRP(ITXG-1) ) THEN
           WRITE(*,*) 'ITXRSGRP(I) <= ITXRSGRP(I-1) for I=',ITXG
            STOP
         END IF
      END DO

      SUM = 0.0
      DO ITXG = 1,NTXRSGRP
         IT = ITXRSGRP(ITXG)
         IF( IT .LT. ITXG ) STOP 'IT < ITXG'
         WRITE(*,*) 'adjusting table for ITXG IT:',ITXG,IT
         LTXTT(ITXG) = LTXTT(IT)
         TXTT(ITXG)  = TXTT(IT)
         NLT(ITXG)   = NLT(IT)
         NQT(ITXG)   = NQT(IT)
         CONC(ITXG)  = CONC(IT)
         SUM = CONC(IT)
         DO II=1,NQT(ITXG)        
            IQOFT(II,ITXG) = IQOFT(II,IT)
         END DO
      END DO
      IF( SUM .EQ. 0 ) THEN 
         DO ITXG = 1,NTXRSGRP
            CONC(ITXG)  = 1.0
         END DO
      END IF

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      IF( ITCHECK .NE. 0 ) THEN
         TOP=0.0
         DO ICST=1,NCST
            DO IPOL=1,NPOL
               DO IE=1,NE
                  TOP = MAX( TOP, RD(IE,ITCHECK,ICST,IPOL) )
               END DO
            END DO
         END DO
         ETOP = E(NE)
         ETOP = E(1) + 10

         DO IPOL=1,NPOL
C#AXISTXT( E(1),ETOP,  ETEXT, LETEXT,
C#0.0, TOP,   NTEXT, LNTEXT, S)
            CALL PLOTCURVES( E(1),ETOP, 0.0,TOP,
     &                       NEMAX,NTMAX,NCMAX,NSPMAX,  RD,E,TT,
     &                       NE, ITCHECK,ITCHECK, 1,NCST, IPOL,IPOL )
         END DO
      END IF
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

C -------------------------------------------------- sort  input E-table
      IN(1)=1
      DO 100 I=2,NE
         EI = E(I)
         IF( EI .LT. E(IN(I-1)) ) THEN
            N1 = 1
              DO 110 N=N1,(I-1)
              IF( EI .GE. E(IN(N)) ) GOTO 110
                 JJ= I
                 DO 120 J=N,(I-1)
                    IN(JJ)=IN(JJ-1)
                    JJ    = JJ - 1
  120            CONTINUE
                 IN(N) = I
                 GOTO 100
  110       CONTINUE
         END IF
 
         IN(I)= I
  100 CONTINUE
C
      DO I=2,NE
         JL= IN(I-1)
         J = IN(I)
         IF( (E(J)-E(JL)) .LT. 0.0 )
     &   WRITE(6,*) ' sorting NOT successful:',I,J,E(J), (E(J)-E(JL))
      END DO  
C -------------------------------------------------------- arrays sorted
C
      WRITE(6,9140) '   current E-range: ',E(IN(1)),E(IN(NE)),EUNIT
 

      NEF = -9999

      DO I=1,NE
         J       = IN(I)
         TE(I) = E(J) - EREFER  
         IF( TE(I) .LE. EF ) NEF= I 
      END DO

      INTPOLEF = 1  
      IF( SPEC .EQ. 'XAS' .AND. ABS(EREFER-E(1 )) .LT. 1D-4 ) THEN
         NEF      = 1
         INTPOLEF = 0
      END IF
      IF( SPEC .EQ. 'XES' .AND. ABS(EREFER-E(NE)) .LT. 1D-4 ) THEN
         NEF      = NE
         INTPOLEF = 0
      END IF

ccccccerefer = e(1)   erefer = E_FERMI(eV) read in READHEAD
      EF     = 0.0
      WRITE(*,*) '  EREFER              ', EREFER,'  eV  ===  E_Fermi'
      WRITE(*,*) '  NEF                 ', NEF        
      WRITE(*,*) '  EF                  ', EF        
      WRITE(*,*) '  Interpolate for EF  ', INTPOLEF        

      CALL CPR4VEC( E, TE, NE )
 
      DO IPOL = 1,NPOL
         DO ICST = 1,NCST
            DO ITXG = 1,NTXRSGRP
               DO IE=1,NE
                  TD(IE) = RD(IN(IE),ITXG,ICST,IPOL)
                  TT(IE) = RT(IN(IE),ITXG,ICST,IPOL)
               END DO    
               CALL CPR4VEC( RD(1,ITXG,ICST,IPOL), TD, NE )
               CALL CPR4VEC( RT(1,ITXG,ICST,IPOL), TT, NE )
            END DO  
         END DO  
      END DO  

      IF( NEF .EQ. -9999 ) THEN
         IF( ABS(E(1)-EF) .GT. 0.0005 ) THEN
            PRINT *,' '
            PRINT *,'****************************** '
            PRINT *,'    E_F not in energy range    '
            PRINT *,'         NEF set to 1          '
            PRINT *,'****************************** '
         END IF          
         NEF = 1 
      ELSE            
C --------------------------------------------------- interpolate to E_F
         IF( INTPOLEF .EQ. 1 ) THEN
         I1 = NEF
         I2 = NEF + 1
         W1 = (E(I2) - EF) / (E(I2) - E(I1))
         W2 = (EF - E(I1)) / (E(I2) - E(I1))
         DO IPOL = 1,NPOL
            DO ICST = 1,NCST
               DO ITXG = 1,NTXRSGRP
                  RD(I1,ITXG,ICST,IPOL) =  W1*RD(I1,ITXG,ICST,IPOL)
     &                                    +W2*RD(I2,ITXG,ICST,IPOL) 
                  RT(I1,ITXG,ICST,IPOL) =  W1*RT(I1,ITXG,ICST,IPOL)
     &                                    +W2*RT(I2,ITXG,ICST,IPOL) 
               END DO
            END DO
         END DO
         E(I1) = EF
         END IF
      END IF          

C ---------------------------------------------- XAS: set = 0 below  E_F
C ---------------------------------------------- XES: set = 0 above  E_F
      IF( SPEC .EQ. 'XAS' ) THEN
         IE1=1
         IE2=NEF-1
      ELSE
         IE1=NEF
         IE2=NEMAX
      END IF
      DO IPOL = 1,NPOL
         DO ICST = 1,NCST
            DO ITXG = 1,NTXRSGRP
               DO IE  = IE1,IE2
                  RD(IE,ITXG,ICST,IPOL) = 0.0
                  RT(IE,ITXG,ICST,IPOL) = 0.0
               END DO
            END DO
         END DO
      END DO
      
C ---------------------------------------XAS: extend E-range  below  E_F
      IF( SPEC .EQ. 'XAS' ) THEN
         IF( NOTABEXT ) THEN
            NADD =  0
         ELSE
            NADD = 50
         END IF
         NN   = NADD - NEF + 1
         IF( EUNIT .EQ. 'eV' ) THEN
            DELE = 25.0
         ELSE
            DELE = 25.0/RYOVEV
         END IF
             
         DO J=NE,1,-1
            I    = J + NN
            E(I) = E(J)
            DO IPOL = 1,NPOL
               DO ICST = 1,NCST
                  DO ITXG = 1,NTXRSGRP
                     RD(I,ITXG,ICST,IPOL) = RD(J,ITXG,ICST,IPOL)
                     RT(I,ITXG,ICST,IPOL) = RT(J,ITXG,ICST,IPOL)
                  END DO
               END DO
            END DO
         END DO
         NE  = NE  + NN
         NEF = NEF + NN 
   
         DO IE=1,NEF-1
            E(IE) = E(NEF) - DELE*(FLOAT(NEF-IE)/FLOAT(NADD))**3
            DO ICST = 1,NCST
               DO IPOL = 1,NPOL
                  DO ITXG = 1,NTXRSGRP
                     RD(IE,ITXG,ICST,IPOL) = 0.0
                     RT(IE,ITXG,ICST,IPOL) = 0.0
                  END DO
               END DO
            END DO
         END DO
      ELSE
C ---------------------------------------XES: extend E-range  above  E_F
         NADD = 50
         IF( EUNIT .EQ. 'eV' ) THEN
            DELE = 25.0
         ELSE
            DELE = 25.0/RYOVEV
         END IF
             
         NE  = NE  + NADD
   
         DO IE=NEF+1,NE
            E(IE) = E(NEF) + DELE*(FLOAT(IE-NEF)/FLOAT(NADD))**3
            DO ICST = 1,NCST
               DO IPOL = 1,NPOL
                  DO ITXG = 1,NTXRSGRP
                     RD(IE,ITXG,ICST,IPOL) = 0.0
                     RT(IE,ITXG,ICST,IPOL) = 0.0
                  END DO
               END DO
            END DO
         END DO
      END IF

      DO I=2,NE
         IF( E(I) .EQ. E(I-1) ) E(I) = E(I)*1.00001
      END DO
 
      PRINT *,' '
      WRITE(6,9140) ' adjusted  E-range: ',E(1),E(NE),EUNIT
      WRITE(6,9142) '              NEF : ',NEF,
     &              '              NE:   ',NE
 9140 FORMAT(A,F10.4,:,' ... ',F10.4,' ',A)
 9142 FORMAT(A,I4,:,/,A,I4,/)

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      IF( ITCHECK .NE. 0 ) THEN
         ETOP = E(NEF) + 10

         DO IPOL=1,NPOL
C#AXISTXT( E(1),ETOP,  ETEXT, LETEXT,
C#0.0, TOP,   NTEXT, LNTEXT, S)
            CALL PLOTCURVES( E(1),ETOP, 0.0,TOP,
     &                       NEMAX,NTMAX,NCMAX,NSPMAX,  RD,E,TT,
     &                       NE, ITCHECK,ITCHECK, 1,NCST, IPOL,IPOL )
         END DO
      END IF
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
                    
C ---------------------------------------- find KTYP for each core state
      NKTYP   = 0
      DO 210 ICST=1,NCST  
         DO K=1,NKTYP
            IF( KAPCOR(ICST) .EQ. KTYP(K) ) THEN
               KTYPCOR(ICST) = K
               GOTO 210
            END IF
         END DO
         NKTYP         = NKTYP + 1
         IF( NKTYP .GT. 2 ) THEN
            STOP ' NKTYP > 2  >>>>>> check core states'
         END IF
         KTYP(NKTYP)   = KAPCOR(ICST)
         KTYPCOR(ICST) = NKTYP
  210 CONTINUE

      IZ   = ATOMNUMB( TXTT(ITXRSGRP(1))(1:2) )
      IF( E(NE) .GT. 50.0 ) THEN
         NVAL = 0
      ELSE
         NVAL = TABNVAL( IZ )
      END IF
      
      WRITE(6,9142) ' '//TXTT(ITXRSGRP(1))(1:2)
     &               //'            atomic number ',IZ,
     &              ' number of valence electrons ',NVAL

      IF( WLOR(1) .GT. 9000.0 )  THEN
         WRITE(6,'('' Lorentzian width taken from table '',/)')
         DO K=1,NKTYP
            WLOR(K) = WCOREHOLE( IZ,NCXRAY(1),LCXRAY(1),K )
         END DO  
      END IF
               
      DO K=1,NKTYP
         SUMK(K)   = 0
         ECOREAV(K)= 0.0
      END DO  

      IF( NOCORESPLIT ) THEN
         WRITE(*,*) ' ************ CORE SPLITTING WILL BE SUPPRESSED'
         DO ITXG=1,NTXRSGRP
            DO ICST=1,NCST
               ECORE(ITXG,ICST) = ECORE(1,1)
            END DO
         END DO
      END IF
               
      WRITE(6,'('' ICST KAP TYPE      ECORE    '',      
     &          '' W_LOR     W_GAU   ('',A,'')'')') EUNIT
      DO ITXG=1,NTXRSGRP
         WGT= NQT(ITXRSGRP(ITXG))*CONC(ITXRSGRP(ITXG))
         wgt= 1.0
         write(*,*) ITXG,ITXRSGRP(ITXG),NQT(ITXRSGRP(ITXG)),
     *              CONC(ITXRSGRP(ITXG))
         DO ICST=1,NCST
            K          = KTYPCOR(ICST) 
            ECOREAV(K) = ECOREAV(K) + WGT * ECORE(ITXG,ICST) 
            SUMK(K)    = SUMK(K)    + WGT
            WRITE(6,599) ICST,KAPCOR(ICST),K,ECORE(ITXG,ICST),
     &                   WLOR(K), WGAUSS
         END DO  
      END DO  

  599 FORMAT(3I4,4X,F10.3,1X,F8.2,2X,F8.2)

      DO K=1,NKTYP
         ECOREAV(K)= ECOREAV(K) / SUMK(K) 
         WRITE(6,'(''  average:'',I2,4X,F10.3)') K, ECOREAV(K) 
      END DO  

      NCSUM = 0
      DO K=1,NKTYP
         NC = 0 
         ICST1(K) = 1000
         ICST2(K) =    0
         DO ICST=1,NCST
            IF( K .EQ. KTYPCOR(ICST) ) THEN
               NC = NC + 1
               ICST1(K) = MIN( ICST1(K), ICST )
               ICST2(K) = MAX( ICST2(K), ICST )
            END IF
         END DO  
         IF( NC .NE. (ICST2(K)-ICST1(K)+1) ) THEN
            WRITE(*,*) ' K       = ', K
            WRITE(*,*) ' ICST1/2 = ', ICST1(K),ICST2(K)
            WRITE(*,*) ' NC      = ',NC
            STOP '  ????????????????????????'
         END IF
         
         NCSUM = NCSUM + NC
      END DO  

      IF( NCSUM .NE. NCST ) THEN
          WRITE(*,*) ' NCST    = ',NCST 
          WRITE(*,*) ' NCSUM   = ',NCSUM
          STOP '  ????????????????????????'
      END IF

C ----------------------------------------------------------- broadening

      DO K=1,NKTYP   
         DO ITXG = 1,NTXRSGRP 
            NLT(ITXG) = ICST2(K)
         END DO  
         
         CALL    BROADEN( RD,IXRS,BXRS,E,
     &                    WGTGAUTAB,WLORTAB,K,NVAL, 
     &                    NEMAX,NTMAX,NCMAX,NSPMAX, NVMAX, 
     &                    NE, 1,NTXRSGRP, ICST1(K),NLT, 1,NPOL )

         CALL    BROADEN( RT,IXRS,BXRS,E,
     &                    WGTGAUTAB,WLORTAB,K,NVAL, 
     &                    NEMAX,NTMAX,NCMAX,NSPMAX, NVMAX, 
     &                    NE, 1,NTXRSGRP, ICST1(K),NLT, 1,NPOL )

      END DO  

      DCORMIN = +100000.0
      DCORMAX = -100000.0
      DO ITXG=1,NTXRSGRP
         DO ICST=1,NCST
            DO JCST=1,NCST
               IF( KTYPCOR(ICST) .NE. KTYPCOR(JCST) ) THEN
                  D = ABS( ECORE(ITXG,ICST) - ECORE(ITXG,JCST) )
                  DCORMIN = MIN( D, DCORMIN )
                  DCORMAX = MAX( D, DCORMAX )
               END IF
            END DO  
         END DO  
      END DO  
      
      IF( NKTYP .GT. 1 )  PRINT *,' '
      KOVLAP = 0
      DO K1=        1,NKTYP
         DO K2=(K1+1),NKTYP 
            DCOR = ABS(ECOREAV(K1)-ECOREAV(K2))
            IF( DCOR .LT. (E(NE)-E(1)) ) THEN
               WRITE(*,'(''  spectra of type '',I2,'' and '',I2,
     &                   '' overlap:'', 
     &                   ''  DE_av  = '',F6.3,'' '',A,/,
     &              36X, ''  DE_min = '',F6.3,'' '',A,/,
     &              36X, ''  DE_max = '',F6.3,'' '',A,/)') 
     &         K1,K2, DCOR,EUNIT, DCORMIN,EUNIT, DCORMAX,EUNIT
               KOVLAP  = 1
            END IF
         END DO  
      END DO  
      IF( KOVLAP .EQ. 1 .AND. MERGE .EQ. 9999 ) MERGE = 1
      IF( NKTYP  .LE. 1 .OR.  MERGE .EQ. 9999 ) MERGE = 0   
      IF( NOCORESPLIT )                         MERGE = 0

c mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm          
      IF( MERGE .EQ. 0 ) THEN 
         PRINT *,' NO merging of spectra  '
         DO ITXG = 1,NTXRSGRP
            DO ICST = 1,NCST
               K           = KTYPCOR(ICST) 
               DE          = ECOREAV(K) - ECORE(ITXG,ICST)
               DO IPOL = 1,NPOL
                  DO I    = 1,NE    
                     EI = E(I) - DE
                     AD(I,ITXG,ICST,IPOL) = 
     &                       YLAG(EI,E,RD(1,ITXG,ICST,IPOL),0,2,NE,IEX)
                     AT(I,ITXG,ICST,IPOL) = 
     &                       YLAG(EI,E,RT(1,ITXG,ICST,IPOL),0,2,NE,IEX)
                  END DO  
               END DO  
            END DO  
         END DO  
      ELSE
c mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm          
         KREF = 1
         KBOT = 1
         DO K=2,NKTYP
            IF( ECOREAV(K) .GT. ECOREAV(KREF) )   KREF = K
            IF( ECOREAV(K) .LT. ECOREAV(KBOT) )   KBOT = K
         END DO  
         WRITE(6,'(''  E_ref:  '',I2,4X,F10.3)') KREF, ECOREAV(KREF) 
         WRITE(6,'(''  E_bot:  '',I2,4X,F10.3)') KBOT, ECOREAV(KBOT) 

         CALL CPR4VEC( TE, E, NE )

         JE = 0
         DO IE=1,NE
            IF( E(IE) .GT. DCORMIN ) THEN 
               JE = IE - 1
               GOTO 565
            END IF
         END DO  
  565    CONTINUE 
         IF( JE .EQ. 0 ) STOP 'JE=0: no E(IE) > DCORMIN found'
         
         IF( JE + (NE-NEF+1) .GT. NEMAX ) THEN
            WRITE(*,*) ' NE gets too large by merging '
            WRITE(*,*) ' NE_new = ',JE + (NE-NEF+1) 
            WRITE(*,*) ' NEMAX  = ',NEMAX
            STOP
         END IF

         DO IE=NEF,NE
            JE = JE + 1
            TE(JE) = (E(IE)-E(NEF)) + DCORMIN
         END DO  
         NEREF = JE

         DO ICST = 1,NCST
            DO ITXG = 1,NTXRSGRP
               K  = KTYPCOR(ICST) 
               DE = ECOREAV(KREF) - ECORE(ITXG,ICST)
               DO I    = 1,NEREF
                  EI = TE(I) - DE
                  IF( EI .LT. E(1) ) THEN 
                     DO IPOL = 1,NPOL
                        AD(I,ITXG,ICST,IPOL) = 0.0
                        AT(I,ITXG,ICST,IPOL) = 0.0
                     END DO  
                  ELSE
                     IF( EI .GT. E(NE) ) THEN 
                        DO IPOL = 1,NPOL  
                           AD(I,ITXG,ICST,IPOL) = RD(NE,ITXG,ICST,IPOL)
                           AT(I,ITXG,ICST,IPOL) = RT(NE,ITXG,ICST,IPOL)
                        END DO  
                     ELSE
                        DO IPOL = 1,NPOL
                           AD(I,ITXG,ICST,IPOL) =
     &                        YLAG(EI,E,RD(1,ITXG,ICST,IPOL),0,2,NE,IEX)
                           AT(I,ITXG,ICST,IPOL) =
     &                        YLAG(EI,E,RT(1,ITXG,ICST,IPOL),0,2,NE,IEX)
                        END DO  
                     END IF
                  END IF
               END DO  
            END DO  
         END DO  
  
         NE = NEREF
         DO I=1,NE
            E(I) = TE(I)     
         END DO  
                   
         PRINT *,' spectra were merged together on common E-scale '
         PRINT *,' new number of E-points NE=', NE  
         PRINT *,' '

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
         IF( ITCHECK .NE. 0 ) THEN
            EBOT = -1
            ETOP = E(NEF) + DCORMAX + 10

            DO IPOL=1,NPOL
C#AXISTXT( EBOT,ETOP,  ETEXT, LETEXT,
C#0.0, TOP,   NTEXT, LNTEXT, S)
               CALL PLOTCURVES( EBOT,ETOP, 0.0,TOP,
     &                          NEMAX,NTMAX,NCMAX,NSPMAX,  AD,E,TT,
     &                          NE, ITCHECK,ITCHECK, 1,NCST, IPOL,IPOL )
            END DO
         END IF
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      END IF       
c mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm          
      IF( EUNIT .EQ. 'eV' ) THEN
         DEFAULT = RYOVEV 
      ELSE
         DEFAULT = 1.0               
      END IF

      IF( ABS(ABS(EMIN)-DEFAULT) .LT. 0.01 ) THEN
         EMIN = E(1)
      END IF
      IF( ABS(ABS(EMAX)-DEFAULT) .LT. 0.01 ) THEN
         EMAX = E(NE)
      END IF

c --------------------- combine spectra with common KAPPA for core state
      DO K    = 1,NKTYP
         DO ITXG = 1,NTXRSGRP
            DO IPOL = 1,NPOL
               DO IE   = 1,NE    
                  RD(IE,ITXG,K,IPOL) = 0.0
                  RT(IE,ITXG,K,IPOL) = 0.0 
               END DO  
            END DO  
         END DO  
      END DO  

      DO ITXG = 1,NTXRSGRP
         DO ICST = 1,NCST
            K           = KTYPCOR(ICST) 
            DO IPOL = 1,NPOL
               DO I    = 1,NE    
                  RD(I,ITXG,K,IPOL) = 
     &            RD(I,ITXG,K,IPOL) + AD(I,ITXG,ICST,IPOL)
                  RT(I,ITXG,K,IPOL) = 
     &            RT(I,ITXG,K,IPOL) + AT(I,ITXG,ICST,IPOL)
               END DO  
            END DO  
         END DO  
      END DO  

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      IF( ITCHECK .NE. 0 ) THEN
         EBOT = -1
         ETOP = E(NEF) + DCORMAX + 10

         DO IPOL=1,NPOL
C#AXISTXT( EBOT,ETOP,  ETEXT, LETEXT,
C#0.0, TOP,   NTEXT, LNTEXT, S)
            CALL PLOTCURVES( EBOT,ETOP, 0.0,TOP,
     &                       NEMAX,NTMAX,NCMAX,NSPMAX,  RD,E,TT,
     &                       NE, ITCHECK,ITCHECK, 1,NKTYP, IPOL,IPOL )
         END DO
      END IF
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      IF( NKTYP .GT. 1 ) THEN
         DO K=1,NKTYP
            SUMK(K) = 0.0
         END DO

         DO ITXG = 1,NTXRSGRP
            DO K    = 1,NKTYP
               DO I    = 2,NE    
                  SUMK(K) = SUMK(K) + 0.5*(E(I)-E(I-1))*
     &                        (RD(I,ITXG,K,NPOL) - RD(I,ITXG,K,1))
               END DO  
            END DO  
         END DO  
         WRITE(*,'(A,3F10.4,A)') ' L2/L3-RATIO   ',SUMK(1),
     &                       SUMK(2),SUMK(2)/SUMK(1)
      END IF
C ======================================================================
      
      NKTYPEFF = NKTYP
      IF( MERGE .EQ. 1 ) THEN 
         NKTYPEFF = 1
         DO ITXG = 1,NTXRSGRP
            DO IPOL = 1,NPOL
               DO I = 1,NE    
                  RD(I,ITXG,1,IPOL) = RD(I,ITXG,1,IPOL) 
     &                              + RD(I,ITXG,2,IPOL)
                  RT(I,ITXG,1,IPOL) = RT(I,ITXG,1,IPOL)
     &                              + RT(I,ITXG,2,IPOL)
               END DO  
            END DO  
         END DO  
      END IF

      IF( EXPDATAVL ) THEN
         IF( NCEXP .GT. NKTYPEFF ) THEN 
            WRITE(6,*) 'WARNING: '
            WRITE(6,*) 'NCEXP=',NCEXP,'  > NKTYPEFF=',NKTYPEFF
         END IF
         IF( NCEXP .LT. NKTYPEFF ) THEN 
            WRITE(6,*) 'WARNING: '
            WRITE(6,*) 'NCEXP=',NCEXP,'  < NKTYPEFF=',NKTYPEFF
         END IF
      END IF
      DO ITXG = 1,NTXRSGRP 
         NLT(ITXG) = NKTYPEFF  
      END DO  
      write(*,*)  ' NTXRSGRP ',NTXRSGRP
      write(*,*)  ' NKTYPEFF ',NKTYPEFF   
      write(*,*)  ' NPOL     ',NPOL   
      write(*,*)  ' NE       ',NE     , emin,emax   

      CALL FILLUP( NLT,RD,AD,NEMAX,NTMAX,NCMAX,NSPMAX,
     &             NE,NTXRSGRP,NPOL,CONC,NQT )
      CALL FILLUP( NLT,RT,AT,NEMAX,NTMAX,NCMAX,NSPMAX,
     &             NE,NTXRSGRP,NPOL,CONC,NQT )
      
      CALL FINDEXTR( NLT,RD,NEMAX,NTMAX,NCMAX,NSPMAX,
     &               NE,NTXRSGRP,0,NPOL, NMIN,NMAX )

      X = 100.0/NMAX(0)
      WRITE(*,'(A,E12.4)') ' skalierungsfaktor fuer MAX=100: ', X
      IF( SCLFAC .EQ. 0.0D0 ) THEN
         SCLFAC = X
      ELSE
         WRITE(*,'(A,E12.4)') ' skalierungsfaktor eingelesen:   ',SCLFAC
      END IF

      CALL SCALEN( NLT,RD,AD,NMIN,NMAX, NEMAX,NTMAX,NCMAX,NSPMAX,
     &            NE,NTXRSGRP,NPOL, SCLFAC )
     
c undo second scaling for extrema  NMIN and NMAX
      DO ITXG=0,NTXRSGRP
         NMIN(ITXG) = NMIN(ITXG) / SCLFAC
         NMAX(ITXG) = NMAX(ITXG) / SCLFAC
      END DO  

      CALL SCALEN( NLT,RT,AT,NMIN,NMAX, NEMAX,NTMAX,NCMAX,NSPMAX,
     &            NE,NTXRSGRP,NPOL, SCLFAC )

C ======================================================================
C                 PLOT SPECTRA AND COMPARE WITH EXPERIMENT 
C ======================================================================
      DO K=1,NKTYPEFF

         IT = 1 
         NC = NCXRAY(IT)
         LC = LCXRAY(IT)
         IF( NC .EQ. 1 ) THEN
            EXT='   '
            LHEADER= 1
         ELSE
            EXT=               CHAR(ICHAR('0')+LC+ABS(KTYP(K)))//'  '
            LHEADER= 2
         END IF
         IF( MERGE .EQ. 1 ) THEN 
            EXT=EXT(1:1)//','//CHAR(ICHAR('0')+LC+ABS(KTYP(K))+1)
            LHEADER= 4
         END IF
         HEADER = SHELL(NC)//EXT
         IF( (80 - (LHEADER + 1 + 3 + 12 + 2 + 4) ) .LT. LSYSTEM ) THEN
             LL = 80 - (LHEADER + 1 + 3 + 12 + 2 + 4) 
         ELSE 
             LL = LSYSTEM
         END IF
         HEADER = HEADER(1:LHEADER)//'-'//SPEC//' spectra of '//
     &           TXTT(IT)(1:2)//' in '//SYSTEM(1:LL)
         LHEADER = LHEADER + 1 + 3 + 12 + 2 + 4 + LL
         
c ---------------------------------------- polarisation averaged spectra
C#AXISTXT( EMIN,    EMAX,    ETEXT, LETEXT, 
C#0.0,     NMAX(0), NTEXT, LNTEXT, S)
         CALL PLOTLCCURVES( EMIN,EMAX,0.0,NMAX(0),NEMAX, 
     &                      +0.5,RD(1,0,K,   1), 
     &                      +0.5,RD(1,0,K,NPOL), TT,E, NE )
      
         IF( EXPDATAVL ) THEN
            DO IE=1,NEEXP(K)
               TE(IE) = 0.5*IEXP(IE,K,1) + 0.5*IEXP(IE,K,NPOLEXP)
            END DO  
  
            CALL PLOTEXP( EEXP(1,K), TE, NEEXP(K), 
     &                    ESEXP(K),BLSEXP(K),SCLEXP(K),E,TT,NE ) 
         END IF

c --------------------------------------------------- difference spectra         
         DMIN= +1E+10
         DMAX= -1E+10
         DO IE = 1,NE
            DEL =          RD(IE,0,K,NPOL)-RD(IE,0,K,1)
            DMIN= MIN( DMIN, DEL )
            DMAX= MAX( DMAX, DEL )
         END DO  
         DMIN= 1.03 * DMIN / 2.0                             
         DMAX= 1.03 * DMAX / 2.0 

C#AXISTXT( EMIN, EMAX, ETEXT, LETEXT, 
C#DMIN, DMAX, NTEXT, LNTEXT, S)
         CALL PLOTLCCURVES( EMIN,EMAX,DMIN,DMAX,NEMAX, 
     &                      -0.5,RD(1,0,K,   1), 
     &                      +0.5,RD(1,0,K,NPOL), TT,E, NE )

         IF( EXPDATAVL ) THEN
            CALL PLOTLCCURVES( EMIN,EMAX,DMIN,DMAX,NEMAX, 
     &                         +0.5,IEXP(1,K,      1), 
     &                         -0.5,IEXP(1,K,NPOLEXP),
     &                         TT,EEXP(1,K), NEEXP(K) )
         END IF

c ------------------------------------------ relative difference spectra         
         
         RMIN= +1E+10
         RMAX= -1E+10
         DO IE = 1,NE
            DEL =            RD(IE,0,K,NPOL)-RD(IE,0,K,1)
            RAT = DEL / MAX( RD(IE,0,K,NPOL)+RD(IE,0,K,1), 1.0E-10 )  
            TD(IE) = RAT
            IF( E(IE) .GT. -4.0 ) THEN
               RMIN= MIN( RMIN, RAT )
               RMAX= MAX( RMAX, RAT )
            END IF
         END DO  

C#AXISTXT( EMIN, EMAX, ETEXT, LETEXT, 
C#RMIN, RMAX, NTEXT, LNTEXT, S)
         CALL PLOTLCCURVES( EMIN,EMAX,RMIN,RMAX,NEMAX, 
     &                      1.0,TD,0.0,TD, TT,E, NE )

         IF( EXPDATAVL ) THEN
            DO IE = 1,NEEXP(K)
               TD(IE) = (IEXP(IE,K,NPOLEXP)-IEXP(IE,K,1)) /
     &                  (IEXP(IE,K,NPOLEXP)+IEXP(IE,K,1)) 
               td(ie) = - td(ie)
            END DO  

            CALL PLOTLCCURVES( EMIN,EMAX,RMIN,RMAX,NEMAX, 
     &                         1.0,TD,0.0,TD, 
     &                         TT,EEXP(1,K), NEEXP(K) )
         END IF

c ----------------------------------------------------------------------
c                      tabulate spectra if required
c ----------------------------------------------------------------------
         IF( KTABLE ) THEN

            IF( NKTYPEFF .GT. 1 ) THEN
               STR10 = 'xas_'//TXTT(IT)(1:2)//'_'//CHAR(ICHAR('1')+K-1)
            ELSE
               STR10 = 'xas_'//TXTT(IT)(1:2)
            END IF
            OPEN (90, FILE=STR10 )
            IF( MERGE .EQ. 1 ) THEN
               DO IE=1,NE
                  DO KK=1,NKTYP
                     RAV(KK) = +0.5*RD(IE,0,KK,1)+0.5*RD(IE,0,KK,NPOL)
                     RDF(KK) = -0.5*RD(IE,0,KK,1)+0.5*RD(IE,0,KK,NPOL)
                  END DO
                  WRITE(90,'(20F15.8)') E(IE),
     &                RAV(1),RDF(1)
c    &                RAV(1),RDF(1), RAV(1)-RAV(2), RDF(1)-RDF(2),
c    &                RAV(2),RDF(2)
               END DO
            ELSE
               DO IE=1,NE 
                  WRITE(90,'(20F15.8)') E(IE),
     &                          +0.5*RD(IE,0,K,1)+0.5*RD(IE,0,K,NPOL),
     &                          -0.5*RD(IE,0,K,1)+0.5*RD(IE,0,K,NPOL)
               END DO
            END IF
            CLOSE (90)
            WRITE(*,*) '  '
            WRITE(*,*) '   SPECTRUM written to file  ', STR10,
     &                 '   for   IT = ',IT,'   ',TXTT(IT)
            WRITE(*,*) '  '

         END IF
c ----------------------------------------------------------------------


      END DO  
C ======================================================================




      IF( NTXRSGRP .EQ. 1 ) RETURN
      W0  = 0.0
      DO ITXG=1,NTXRSGRP
         W0 = W0 + NQT(ITXG)*CONC(ITXG)
      END DO
      W0 = MAX( 1.0, W0 )
C ======================================================================
C              COMPARE SPECTRA OF THE VARIOUS ATOM TYPES IT 
C ======================================================================
      DO K=1,NKTYPEFF
         IT = 1           
         NC = NCXRAY(IT)
         LC = LCXRAY(IT)
         IF( NC .EQ. 1 ) THEN
            EXT='   '
            LHEADER= 1
         ELSE
            EXT=               CHAR(ICHAR('0')+LC+ABS(KTYP(K)))//'  '
            LHEADER= 2
         END IF
         IF( MERGE .EQ. 1 ) THEN 
            EXT=EXT(1:1)//','//CHAR(ICHAR('0')+LC+ABS(KTYP(K))+1)
            LHEADER= 4
         END IF
         HEADER = SHELL(NC)//EXT
         HEADER = HEADER(1:LHEADER)//'-'//SPEC//' spectra of '//
     &           TXTT(IT)(1:2)//' in '//SYSTEM(1:LSYSTEM)
         LHEADER = LHEADER + 1 + 3 + 12 + 2 + 4 + LSYSTEM
         
         YMAX = 0.0
         DMIN= +1E+10
         DMAX= -1E+10
         RMIN= +1E+10
         RMAX= -1E+10
         DO ITXG=1,NTXRSGRP
            DO IE = 1,NE
               SUM = RD(IE,ITXG,K,NPOL)+RD(IE,ITXG,K,1)
               DEL = RD(IE,ITXG,K,NPOL)-RD(IE,ITXG,K,1)
               RAT = DEL / MAX( SUM, 1.0E-10 )  
               YMAX= MAX( YMAX, SUM )
               DMIN= MIN( DMIN, DEL )
               DMAX= MAX( DMAX, DEL )
               IF( E(IE) .GT. -4.0 ) THEN
                  RMIN= MIN( RMIN, RAT )
                  RMAX= MAX( RMAX, RAT )
               END IF
               AD(IE,ITXG,1,1) = RAT
            END DO
         END DO  
         YMAX= 1.03 * YMAX / 2.0
         DMIN= 1.03 * DMIN / 2.0                             
         DMAX= 1.03 * DMAX / 2.0 
         RMIN= 1.10 * RMIN 
         RMAX= 1.10 * RMAX

c ---------------------------------------- polarisation averaged spectra
C#AXISTXT( EMIN,    EMAX,    ETEXT, LETEXT, 
C#0.0,     YMAX,    NTEXT, LNTEXT, S)
         WGT = 0.5/W0
         DO IE=1,NE
            TD(IE) = WGT*(RD(IE,0,K,1)+RD(IE,0,K,NPOL)) 
         END DO
         CALL PLOTCURVES( EMIN,EMAX,0.0,YMAX,
     &                    NEMAX,0,0,0, 
     &                    TD, E, TT,NE, 0,0, 0,0, 0,0 )
         WGT = 0.5

         DO ITXG=1,NTXRSGRP
            IF( ITXG .EQ. 0 ) THEN 
               WGT = 0.5/W0
            ELSE
               WGT = 0.5
            END IF

            CALL PLOTLCCURVES( EMIN,EMAX,0.0,YMAX,NEMAX, 
     &                         +WGT,RD(1,ITXG,K,   1), 
     &                         +WGT,RD(1,ITXG,K,NPOL), TT,E, NE )
         END DO  
      
c --------------------------------------------------- difference spectra         
C#AXISTXT( EMIN, EMAX, ETEXT, LETEXT, 
C#DMIN, DMAX, NTEXT, LNTEXT, S)
         DO ITXG=0,NTXRSGRP
            IF( ITXG .EQ. 0 ) THEN 
               WGT = 0.5/W0
            ELSE
               WGT = 0.5
            END IF

            CALL PLOTLCCURVES( EMIN,EMAX,DMIN,DMAX,NEMAX, 
     &                         -WGT,RD(1,ITXG,K,   1), 
     &                         +WGT,RD(1,ITXG,K,NPOL), TT,E, NE )
         END DO  

c ------------------------------------------ relative difference spectra         
      END DO  

      RETURN
      END  
