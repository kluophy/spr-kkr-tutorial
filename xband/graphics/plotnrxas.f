      SUBROUTINE PLOTNRXAS( IFIL, N,NW,
     &                    NQMAX,NTMAX,NEMAX,NCMAX,NSPMAX,NLMAX,
     &                    SPEC,
     &                    NLQ, NLT, NMIN,NMAX, WMIN,WMAX,
     &                    IQOFT,NQT,CONC,TXTT,LTXTT, INFO,LINFO, 
     &                    HEADER,LHEADER, SYSTEM,LSYSTEM, E, Y,
     &                    NT,NS,NE,
     &                    EXRS,IXRS,BXRS,WXRS,NVMAX,NEXRSMAX,WGTGAUTAB,
     &                    EEXP, IEXP, NCEXPMAX,NPOLEXPMAX, 
     &                    NCXRAY, LCXRAY, ITXRSGRP, ECORE,NCSTMAX )
c **********************************************************************
c *                                                                    *
c *  plot XAS/XES - spectra                                            *
c *                                                                    *
c **********************************************************************
      REAL         EEXP(NEMAX,NCEXPMAX), IEXP(NEMAX,NCEXPMAX,NPOLEXPMAX)
      REAL         N(NEMAX,0:NTMAX,0:NCMAX,0:NSPMAX), E(NEMAX), Y(NEMAX)
      REAL         NW(NEMAX,0:NTMAX,0:NCMAX,0:NSPMAX)
      REAL         WXRS(NEXRSMAX,0:NTMAX,0:NCMAX,0:NSPMAX), 
     &             EXRS(NEXRSMAX)
      REAL         IXRS(NVMAX,NEMAX), BXRS(NVMAX,NEMAX)
      REAL         WGTGAUTAB(0:NEMAX)
      CHARACTER*80 HEADER,SYSTEM, NTEXT, INFO
      INTEGER      LHEADER, LSYSTEM
      INTEGER      NLQ(NQMAX), NLT(NTMAX)
      INTEGER      IQOFT(NQMAX,NTMAX), NQT(NTMAX), LTXTT(NTMAX)
      REAL         CONC(NTMAX) 
      REAL         NMIN(0:NTMAX), NMAX(0:NTMAX)
      REAL         WMIN(0:NTMAX), WMAX(0:NTMAX)
      INTEGER      NCXRAY(NTMAX), LCXRAY(NTMAX)
      INTEGER      ITXRSGRP(NTMAX)
      REAL         ECORE(NTMAX,NCSTMAX)

      CHARACTER    SPEC*3
      CHARACTER*10 TXTT(NTMAX)
      CHARACTER*10 KW

c ----------------------------------------------------------------------
      COMMON /PLOPAR/ EMIN,EMAX, MERGE
      CHARACTER EUNIT*2
      COMMON /ENERGY/ RYOVEV, EREFER,EUNIT
      CHARACTER FMTEXP*80
c ----------------------------------------------------------------------

      NTEXT = 'I_'//SPEC//'(E) (arb.u.)'
      LNTEXT = 17
      READ(IFIL,9020) NTXRSGRP

      DO 3600 IX=1,NTXRSGRP
c ----------------------------------------------------------------------
         READ(IFIL,9050) KW 
         READ(IFIL,9020) IT
         READ(IFIL,9020) NCXRAY(IT)
         READ(IFIL,9020) LCXRAY(IT)
         READ(IFIL,9070) (ECORE(IT,IS),IS=1,NS)

         ITXRSGRP(IX) = IT

         READ(IFIL,9050) KW
         READ(IFIL,9020) IT
         READ(IFIL,9020) NEXRS
         READ(IFIL,9050) KW

         DO 2340 IEXRS=1,NEXRS
            READ(IFIL,9060) EXRS(IEXRS),((WXRS(IEXRS,IT,IC,IS)
     &                     ,IC=1,NLT(IT)),IS=1,NS)
 2340    CONTINUE
 2330 CONTINUE

c ----------------------------------------------------------------------
         READ(IFIL,9050,ERR=9999) KW
         READ(IFIL,9020,ERR=9999) IT
         READ(IFIL,9050) KW
         DO 40 IE=1,NE
            READ(IFIL,9060) E(IE),
     &                    ((N(IE,IT,IC,IS),IC=1,NLT(IT)),IS=1,NS)
   40    CONTINUE
 3600 CONTINUE 

c ----------------------------------------------------------------------
c                      READING IN FINISHED
c ----------------------------------------------------------------------
      IF( EUNIT .EQ. 'eV' ) THEN
         DO 46 IEXRS=1,NEXRS
            EXRS(IEXRS) = EXRS(IEXRS) * RYOVEV
   46    CONTINUE
         DO 47 IE=1,NE
            E(IE) = E(IE) * RYOVEV
   47    CONTINUE
      END IF
      DO 48 IEXRS=1,NEXRS
         EXRS(IEXRS) = EXRS(IEXRS) - EREFER
   48 CONTINUE
      DO 49 IE=1,NE
         E(IE) = E(IE) - EREFER
   49 CONTINUE
      IF( ABS(EMIN-9999.) .LT. 1.0 ) THEN
         EMIN = E(1)
      END IF
      IF( ABS(EMAX-9999.) .LT. 1.0 ) THEN
         EMAX = E(NE)
      END IF
c ----------------------------------------------------------------------
c                     PLOT MATRIX ELEMENTS
c ----------------------------------------------------------------------

      CALL FINDEXTR( NLT,WXRS,NEXRSMAX,NTMAX,NCMAX,NSPMAX,
     &               NEXRS,NT,1,NS, WMIN,WMAX )

      DO 180 IT=1,NT
C#AXISTXT( EMIN,     EMAX,     ETEXT, LETEXT,
C#WMIN(IT), WMAX(IT), NTEXT, LNTEXT, S)

      CALL PLOTCURVES( EMIN,EMAX,NMIN(0),NMAX(0),
     &                 NEMAX,NTMAX,NCMAX,NSPMAX,
     &                 NW,E,Y, NE, 1,NT, 0,0, 1,NS )

C#CAPTNIT( PY,A,B,TXTT,NT )
 180  CONTINUE

c ----------------------------------------------------------------------
      DO 80 IT=1,NT
C#AXISTXT( EMIN,     EMAX,     ETEXT, LETEXT, 
C#NMIN(IT), NMAX(IT), NTEXT, LNTEXT, S)

         CALL PLOTCURVES( EMIN,EMAX,NMIN(IT),NMAX(IT),
     &                    NEMAX,NTMAX,NCMAX,NSPMAX,
     &                    N,E,Y, NE, IT,IT, 0,NLT(IT), 1,NS )

   80 CONTINUE

c ----------------------------------------------------------------------
      RETURN
 9999 STOP 'ERROR READING DATAFILE'

 9010 FORMAT(10X,A80)
 9020 FORMAT(10X,I10)  
 9030 FORMAT(10X,F10.5)
 9040 FORMAT(I5,1X,A10,F10.5,I5,10I3,:,(41X,10I3))
 9050 FORMAT(10X,A10)
 9060 FORMAT(8F10.4)
 9070 FORMAT(A10,2F15.6)
      END
