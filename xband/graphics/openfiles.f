      SUBROUTINE OPENFILES( FILEINFO, KW, LKW, 
     &                      IFILTAB,FILNAMTAB,NFILTAB)
C   ********************************************************************
C   *                                                                  *
C   *   reads file-specifications from file   FILEINFO                 *
C   *   and opens the  files accordingly                               *
C   *                                                                  *
C   *   if FILEINFO is empty <OPENFILES> looks for the file 'filelist' *
C   *   and reads the specifications assuming that these start after   *
C   *   a line beginning with a keyword   KW   and an additional       *
C   *   comment line                                                   *
C   *                                                                  *
C   *   if  'filelist'  is not available  <OPENFILES>  tries  to find  *
C   *   the keyword   KW   and specifications in file  5               *
C   *   in this case one has to be sure that a new file 5 should be    *
C   *   the last one to open                                           *
C   *                                                                  *
C   *   reading and opening is ended if a blank line or  EOF  is       *
C   *   encountered in  FILEINFO, 'filelist'  or  file 5               *
C   *                                                                  *
C   *   NFILTAB filenames are passed in array  FILNAMTAB  to the       *
C   *   calling routine for the channel numbers in  IFILTAB            *
C   *                                                                  *
C   ********************************************************************
      PARAMETER ( LNGFAC = 8 )
C
      INTEGER       FUNIT, FRECL
      CHARACTER*10  KW
      CHARACTER*3   C3STAT, C3ACC, C3FORM
      CHARACTER*12  FSTAT, FACC, FFORM  
      CHARACTER*40  FILEINFO
      CHARACTER*80  FFILE
      CHARACTER*80  FILNAMTAB(NFILTAB)
      INTEGER       IFILTAB(NFILTAB)
      LOGICAL       AVAILABLE
        
      INQUIRE( FILE=FILEINFO, EXIST=AVAILABLE )
      IFI = 88    
      IF( AVAILABLE ) THEN 
         OPEN( UNIT= 88, FILE=FILEINFO, STATUS='UNKNOWN')
      ELSE
         INQUIRE( FILE='filelist', EXIST=AVAILABLE )
         IF( AVAILABLE ) THEN 
            OPEN( UNIT= 88, FILE='filelist', STATUS='UNKNOWN')
         ELSE
            IFI = 5
         END IF
         CALL KWPOSFIL( IFI, KW, LKW, 1, 0, IPOS )
      END IF

      READ(IFI,FMT='(A3)') C3STAT

      WRITE(6,FMT='(//,26H UNIT  STATUS      ACCESS ,
     &              5X,28HFORM         RECL   FILENAME)' )

  888 READ(IFI,FMT='(I5,4X,A3,4X,A3,4X,A3,I7,3X,A)',END=886)
     &             FUNIT,C3STAT,C3ACC,C3FORM,FRECL,FFILE 
      IF( FUNIT .EQ. 0 )  GOTO 886  

      DO 10 I=1,NFILTAB
         IF( FUNIT .EQ. IFILTAB(I) ) FILNAMTAB(I) = FFILE
   10 CONTINUE

                               FSTAT = 'UNKNOWN'
      IF( C3STAT .EQ. 'OLD' )  FSTAT = 'OLD'
      IF( C3STAT .EQ. 'NEW' )  FSTAT = 'NEW'
      IF( C3STAT .EQ. 'SCR' )  FSTAT = 'SCRATCH'
                               FACC  = 'SEQUENTIAL'
      IF( C3ACC  .EQ. 'DIR' )  FACC  = 'DIRECT'
      IF( C3ACC  .EQ. 'SEQ' )  FACC  = 'SEQUENTIAL'
                               FFORM = 'FORMATTED'
      IF( C3FORM .EQ. 'FOR' )  FFORM = 'FORMATTED'
      IF( C3FORM .EQ. 'UNF' )  FFORM = 'UNFORMATTED'

      WRITE(6,FMT='(I5,2X,3A12,I5,3X,A80)') 
     &        FUNIT, FSTAT, FACC, FFORM, FRECL, FFILE
      IF( FSTAT .EQ. 'SCRATCH' ) THEN 
         IF( FRECL .GT. 0 ) THEN
            OPEN( UNIT= FUNIT, STATUS= FSTAT, ACCESS= FACC,
     &            FORM= FFORM )
c    _            FORM= FFORM, RECL= FRECL )
         ELSE
            OPEN( UNIT= FUNIT, STATUS= FSTAT, ACCESS= FACC,
     &            FORM= FFORM )
         END IF                  
      ELSE
         IF( FRECL .GT. 0 ) THEN
            OPEN( UNIT= FUNIT, STATUS= FSTAT, ACCESS= FACC,
     &            FORM= FFORM, RECL= FRECL, FILE= FFILE )
c    &            FORM= FFORM, RECL= FRECL, FILE= FFILE )
         ELSE
            OPEN( UNIT= FUNIT, STATUS= FSTAT, ACCESS= FACC,
     &            FORM= FFORM,                FILE= FFILE )
         END IF               
      END IF

      IF( FUNIT .EQ. 5 .AND. IFI .EQ. 5 ) 
     & WRITE(*,*) 'warning from <OPENFILES>: ',
     &            ' using AND reopening file 5 ',
     &            ' -- this has be the last one in the filelist !'
      GOTO 888

886   CONTINUE
      IF( IFI .EQ. 88 ) CLOSE( 88 )

      RETURN
      END
