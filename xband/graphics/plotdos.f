C*==plotdos.f    processed by SPAG 6.05Rc at 21:29 on 30 Oct 2001
      SUBROUTINE PLOTDOS(IFIL,N,NW,DATASET,LDATASET,LSTRMAX,NQMAX,NTMAX,
     &                   NEMAX,NCMAX,NSPMAX,NLMAX,NLQ,NLT,NMIN,NMAX,
     &                   IQOFT,NQT,CONC,TXTT,LTXTT,INFO,LINFO,HEADER,
     &                   LHEADER,SYSTEM,LSYSTEM,E,Y,RYOVEV,EREFER,EUNIT,
     &                   EMIN,EMAX,DE,DY,NQ,NT,NE,IREL,EF,KJDOS,
     &                   KTABLE,KNOHEADER,JDOS)
C **********************************************************************
C *                                                                    *
C *  plot density of states                                            *
C *                                                                    *
C **********************************************************************
C
C*** Start of declarations rewritten by SPAG
C
C PARAMETER definitions
C
      REAL EQUTOL
      PARAMETER (EQUTOL=1E-6)
      INTEGER NLEGMAX
      PARAMETER (NLEGMAX=50)
C
C Dummy arguments
C
      CHARACTER*(*) DATASET
      REAL DE,DY,EF,EMAX,EMIN,EREFER,RYOVEV
      CHARACTER*2 EUNIT
      CHARACTER*80 HEADER,INFO,SYSTEM
      INTEGER IFIL,IREL,LDATASET,LHEADER,LINFO,LSTRMAX,LSYSTEM,NCMAX,NE,
     &        NEMAX,NLMAX,NQ,NQMAX,NS,NSPMAX,NT,NTMAX
      LOGICAL KJDOS,KNOHEADER,KTABLE
      REAL CONC(NTMAX),E(NEMAX),JDOS(NEMAX,0:NTMAX,0:NSPMAX),
     &     N(NEMAX,0:NTMAX,0:NCMAX,0:NSPMAX),NMAX(0:NTMAX),NMIN(0:NTMAX)
     &     ,NW(NEMAX,0:NTMAX,0:NCMAX,0:NSPMAX),Y(NEMAX)
      INTEGER IQOFT(NQMAX,NTMAX),LTXTT(NTMAX),NLQ(NQMAX),NLT(NTMAX),
     &        NQT(NTMAX)
      CHARACTER*10 TXTT(NTMAX)
C
C Local variables
C
      CHARACTER CHAR
      REAL DOM,OMMAX,W1,W10,W2,W20,WA,WB,WGT,WSUM,WT,XMAX,XMIN,YMAX
      CHARACTER*80 FILNAM,FMT1,YTXT1,YTXT2
      INTEGER I1,I2,IC,ICA,ICB,ICB1,ICB2,IE,IEA,IEB,IFLAG,IL,IOM,IS,IT,
     &        ITP,IU,LFILNAM,LS,LYTXT1,LYTXT2,NCURVES,NEF,NEIN,NLTMAX,
     &        NOM
      INTEGER ICHAR,NINT
      CHARACTER*10 KW,STR10
      CHARACTER*20 LEG(NLEGMAX),STR20
C
C*** End of declarations rewritten by SPAG
C
      NS = 2
      READ (IFIL,99007,ERR=300) KW
      IF ( KW.EQ.'STANDARD  ' ) THEN
         DO IT = 1,NT
            READ (IFIL,99007,ERR=300) KW
            READ (IFIL,99004,ERR=300) ITP
            READ (IFIL,99003) FMT1
            DO IE = 1,NE
               READ (IFIL,99008) E(IE),
     &                           ((N(IE,IT,IC,IS),IC=1,NLT(IT)),IS=1,NS)
            END DO
         END DO
      ELSE IF ( KW.EQ.'OLD-SPRKKR' ) THEN
         DO IE = 1,NE
            READ (IFIL,99001,END=50) E(IE),Y(IE),
     &                               (((N(IE,IT,IC,IS),IC=1,NLT(IT)),
     &                               IS=1,NS),IT=1,NT)
            write(*,*) IE,NE,E(IE),Y(IE),N(IE,1,1,1)
            NEIN = IE
         END DO
 50      CONTINUE
         NE = NEIN
         IF ( ABS(Y(1)-Y(2)).GT.EQUTOL ) WRITE (*,*) ' ARC !!!! '
      ELSE
         WRITE (6,99007) KW
         STOP 'KEYWORD not allowed '
      END IF
      WRITE (*,'(A,I10)') ' number of energies read:  ',NE
      WRITE (*,'(A,2F10.4)') ' boundaries             :  ',E(1),E(NE)
C
C ----------------------------------------------------------------------
C                      update  NS  if necessary
C ----------------------------------------------------------------------
      IF ( (NS.EQ.2) .AND. (KW.EQ.'OLD-SPRKKR') ) THEN
         IFLAG = 0
         DO IT = 1,NT
            DO IC = 1,NLT(IT)
               DO IE = 1,NE
                  IF ( ABS(N(IE,IT,IC,1)-N(IE,IT,IC,2)).GT.0.01 ) THEN
                     IFLAG = 1
                     GOTO 100
                  END IF
               END DO
            END DO
         END DO
 100     CONTINUE
         IF ( IFLAG.EQ.0 ) THEN
            WRITE (6,99002)
            NS = 1
         END IF
      END IF
C
C ----------------------------------------------------------------------
C                      READING IN FINISHED
C ----------------------------------------------------------------------
C
      IF ( EUNIT.EQ.'eV' ) THEN
         DO IT = 1,NT
            DO IC = 1,NLT(IT)
               DO IS = 1,NS
                  DO IE = 1,NE
                     N(IE,IT,IC,IS) = N(IE,IT,IC,IS)/RYOVEV
                  END DO
               END DO
            END DO
         END DO
         DO IE = 1,NE
            E(IE) = E(IE)*RYOVEV
         END DO
      END IF
      DO IE = 1,NE
         E(IE) = E(IE) - EREFER
      END DO
C ----------------------------------------------------------------------
C
      CALL FILLUP(NLT,N,NW,NEMAX,NTMAX,NCMAX,NSPMAX,NE,NT,NS,CONC,NQT)
C
      CALL FINDEXTR(NLT,N,NEMAX,NTMAX,NCMAX,NSPMAX,NE,NT,0,NS,NMIN,NMAX)
C
C ----------------------------------------------------------------------
C                      tabulate DOS if required
C ----------------------------------------------------------------------
      IF ( KTABLE ) THEN
C
         DO IT = 1,NT
            STR10 = 'dos_'//TXTT(IT)(1:LTXTT(IT))
            OPEN (90,FILE=STR10)
            DO IE = 1,NE
               WRITE (90,'(20E12.4)') E(IE),
     &                                ((N(IE,IT,IC,IS),IC=0,NLT(IT)),
     &                                IS=1,NS)
            END DO
            CLOSE (90)
            WRITE (*,*) '  '
            WRITE (*,*) '   DOS written to file  ',STR10,
     &                  '   for   IT = ',IT,'   ',TXTT(IT)
         END DO
C
      END IF
C
C ----------------------------------------------------------------------
C                      calculate JDOS if required
C ----------------------------------------------------------------------
      IF ( KJDOS ) THEN
         DO IT = 0,NTMAX
            DO IS = 0,NSPMAX
               DO IE = 1,NEMAX
                  JDOS(IE,IT,IS) = 0.0
               END DO
            END DO
         END DO
C
         OMMAX = 15.0
         DOM = E(2) - E(1)
         NOM = NINT(OMMAX/DOM)
         DO IE = 1,NE
            IF ( E(IE).LT.0.0 ) NEF = IE + 1
         END DO
         WRITE (*,*) '   D-OMEGA ',DOM
         WRITE (*,*) '   NEF     ',NEF
         WRITE (*,*) '   NOM     ',NOM
         WRITE (*,*) '   E       ',E(NEF-1),E(NEF)
         WSUM = 0.0
         DO IT = 1,NT
            WT = NQT(IT)*CONC(IT)
            WSUM = WSUM + WT
            DO ICA = 1,NLT(IT)
               DO IS = 1,NS
                  DO IEA = 1,NEF - 2
                     WA = N(IEA,IT,ICA,IS) + N(IEA+1,IT,ICA,IS)
                     IF ( ICA.EQ.1 ) THEN
                        ICB1 = 2
                     ELSE
                        ICB1 = ICA - 1
                     END IF
                     IF ( ICA.EQ.NLT(IT) ) THEN
                        ICB2 = ICA - 1
                     ELSE
                        ICB2 = ICA + 1
                     END IF
                     DO ICB = ICB1,ICB2
                        DO IEB = NEF,NE - 1,2
                           WB = N(IEB,IT,ICB,IS) + N(IEB+1,IT,ICB,IS)
                           IOM = IEB - IEA
                           IF ( IOM.LE.NOM ) THEN
                              JDOS(IOM,IT,IS) = JDOS(IOM,IT,IS) + WA*WB
                              JDOS(IOM,IT,0) = JDOS(IOM,IT,0) + WA*WB
                              JDOS(IOM,0,IS) = JDOS(IOM,0,IS) + WA*WB*WT
                              JDOS(IOM,0,0) = JDOS(IOM,0,0) + WA*WB*WT
                           END IF
                        END DO
                     END DO
                  END DO
               END DO
            END DO
         END DO
         DO IS = 0,NS
            DO IOM = 1,NOM
               JDOS(IOM,0,IS) = JDOS(IOM,0,IS)/WSUM
            END DO
         END DO
         OPEN (90,FILE='jdos')
         DO IOM = 1,NOM
            WRITE (90,'(20F10.3)') IOM*DOM,
     &                             ((JDOS(IOM,IT,IS),IS=0,NS),IT=0,NT)
         END DO
         CLOSE (90)
         WRITE (*,*) '   JDOS written to file    jdos '
         WRITE (*,*) '   ((JDOS(IOM,IT,IS),IS=0,NS),IT=0,NT) '
         WRITE (*,*) '   FOR   NT = ',NT
         WRITE (*,*) '   AND   NS = ',NS
      END IF
C
C ======================================================================
C                              XMGRACE - output
C ======================================================================
      IF ( ABS(EMIN-9999.).LT.0.01 ) THEN
         XMIN = E(1)
      ELSE
         XMIN = EMIN
      END IF
      IF ( ABS(EMAX-9999.).LT.0.01 ) THEN
         XMAX = E(NE)
      ELSE
         XMAX = EMAX
      END IF
C ----------------------------------------------------------------------
      IFIL = 90
C
C ----------------------------------------------------------------------
C
      IF ( NT.GT.1 ) THEN
C
         YMAX = NMAX(0)
C
         IF ( NS.EQ.1 ) THEN
            YTXT1 = 'n!stot!N(E) (sts./eV)'
            LYTXT1 = 21
            YTXT2 = ' '
            LYTXT2 = 1
         ELSE
            YTXT1 = 'n!m{1}!S!UP!M{1}!N!stot!N(E) (sts./eV)'
            LYTXT1 = 38
            YTXT2 = 'n!m{1}!S!DN!M{1}!N!stot!N(E) (sts./eV)'
            LYTXT2 = 38
         END IF
C
         CALL XMGR4HEAD(DATASET,LDATASET,'dos',3,' ',0,FILNAM,80,
     &                  LFILNAM,IFIL,NS,XMIN,1,XMAX,1,0.0,0,YMAX,1,0.0,
     &                  0,YMAX,1,'energy (eV)',11,YTXT1,LYTXT1,YTXT2,
     &                  LYTXT2,HEADER,LHEADER,'total DOS of '//
     &                  SYSTEM(1:LSYSTEM),(13+LSYSTEM),KNOHEADER)
C
         NCURVES = MIN(NLEGMAX,NT+1)
C
         CALL XMGRCURVES(IFIL,NS,NCURVES,NCURVES,2,1,0)
C
         LEG(1) = 'tot'
         DO IT = 1,NT
            LEG(IT+1) = TXTT(IT)(1:LTXTT(IT))
         END DO
C
         CALL XMGRLEGEND(IFIL,NS,NCURVES,NCURVES,LEG,LEG)
C
         DO IS = 1,NS
C
            DO IT = 0,NT
               IF ( IT.EQ.0 ) THEN
                  WGT = 1.0
               ELSE
                  WGT = CONC(IT)
               END IF
C
               CALL XMGR4TABLE((IS-1),IT,E,N(1,IT,0,IS),WGT,NE,IFIL)
C
            END DO
C
         END DO
C
         WRITE (6,*) '  '
         WRITE (6,*) '   DOS written to file  ',FILNAM(1:LFILNAM)
         CLOSE (IFIL)
C
      END IF
C
C ----------------------------------------------------------------------
      DO IT = 1,NT
C
         YMAX = NMAX(IT)
C
         STR20 = TXTT(IT)(1:LTXTT(IT))//'!N(E) (sts./eV)'
         LS = LTXTT(IT) + 15
         IF ( NS.EQ.1 ) THEN
            YTXT1 = 'n!s'//STR20(1:LS)
            LYTXT1 = 3 + LS
            YTXT2 = ' '
            LYTXT2 = 1
         ELSE
            YTXT1 = 'n!m{1}!S!UP!M{1}!N!s'//STR20(1:LS)
            LYTXT1 = 20 + LS
            YTXT2 = 'n!m{1}!S!DN!M{1}!N!s'//STR20(1:LS)
            LYTXT2 = 20 + LS
         END IF
C
         CALL XMGR4HEAD(DATASET,LDATASET,'dos',3,TXTT(IT),LTXTT(IT),
     &                  FILNAM,80,LFILNAM,IFIL,NS,XMIN,1,XMAX,1,0.0,0,
     &                  YMAX,1,0.0,0,YMAX,1,'energy (eV)',11,YTXT1,
     &                  LYTXT1,YTXT2,LYTXT2,HEADER,LHEADER,
     &                  'DOS of '//TXTT(IT)(1:LTXTT(IT))
     &                  //' in '//SYSTEM(1:LSYSTEM),
     &                  (7+LTXTT(IT)+4+LSYSTEM),KNOHEADER)
C
         NCURVES = MIN(NLEGMAX,NLT(IT)+1)
C
         LEG(1) = 'tot'
         LEG(2) = 's'
         LEG(3) = 'p'
         LEG(4) = 'd'
         DO IC = 5,NCURVES
            LEG(IC) = CHAR(ICHAR('f')+IC-5)
         END DO
C
         CALL XMGRLEGEND(IFIL,NS,NCURVES,NCURVES,LEG,LEG)
C
         CALL XMGRCURVES(IFIL,NS,NCURVES,NCURVES,2,1,0)
C
         DO IS = 1,NS
C
            WGT = 1.0
            DO IL = 0,NLT(IT)
               CALL XMGR4TABLE((IS-1),IL,E,N(1,IT,IL,IS),WGT,NE,IFIL)
            END DO
C
         END DO
C
         WRITE (6,*) '  '
         WRITE (6,*) '   DOS written to file  ',FILNAM(1:LFILNAM)
         CLOSE (IFIL)
C
      END DO
C
C ======================================================================
C                               print dos(EF)
C ======================================================================
C
      WRITE (6,*) '  '
      WRITE (*,*) ' EREFER ',EREFER
C
      EF = 0.0
C
      I2 = 0
      DO IE = 1,NE
         IF ( (E(IE)-EF).GT.0.0 ) THEN
            I1 = IE - 1
            I2 = IE
            GOTO 200
         END IF
      END DO
 200  CONTINUE
      NLTMAX = 0
      DO IT = 1,NT
         NLTMAX = MAX(NLTMAX,NLT(IT))
      END DO
      IF ( I2.EQ.1 ) THEN
         WRITE (*,*) ' WARNING   I2 = 1 i.e.  E_F < E(1) '
         I1 = 1
         I2 = 2
      END IF
      IF ( I2.LE.0 ) THEN
         WRITE (*,*) ' no E-meshpoints found around E_F !!!???'
      ELSE
         W10 = (E(I2)-EF)/(E(I2)-E(I1))
         W20 = (EF-E(I1))/(E(I2)-E(I1))
         DO IU = 1,2
            WRITE (*,*) ' '
            IF ( IU.EQ.1 ) THEN
               WRITE (*,*) ' DOS(EF)  (STATES/RY*ATOM*SPIN) '
               IF ( EUNIT.EQ.'eV' ) THEN
                  W1 = W10*RYOVEV
                  W2 = W20*RYOVEV
               ELSE
                  W1 = W10
                  W2 = W20
               END IF
            ELSE
               WRITE (*,*) ' DOS(EF)  (STATES/EV*ATOM*SPIN) '
               IF ( EUNIT.EQ.'eV' ) THEN
                  W1 = W10
                  W2 = W20
               ELSE
                  W1 = W10/RYOVEV
                  W2 = W20/RYOVEV
               END IF
            END IF
            WRITE (*,*) ' ============================== '
            WRITE (*,*) '       (by interpolation)       '
            WRITE (6,99011) (IC,IC=1,NLTMAX)
            DO IT = 1,NT
               DO IS = 1,NS
                  WRITE (6,99009) IT,IS,
     &                            ((W1*N(I1,IT,IC,IS)+W2*N(I2,IT,IC,IS))
     &                            ,IC=0,NLT(IT))
               END DO
            END DO
C
            DO IS = 1,NS
               WRITE (6,99010) IS,W1*N(I1,0,0,IS) + W2*N(I2,0,0,IS)
            END DO
         END DO
      END IF
C
C ----------------------------------------------------------------------
      RETURN
 300  CONTINUE
      STOP 'ERROR READING DATAFILE'
C
99001 FORMAT (8E10.4,:,/,(10X,7E10.4))
99002 FORMAT (/,10X,51('*'),/,10X,
     &        'spin-polarized DOS supplied for paramagnetic system'/,
     &        10X,'              NS has been reduced to 1'/,10X,51('*'),
     &        /)
99003 FORMAT (10X,A80)
99004 FORMAT (10X,I10)
99005 FORMAT (10X,F10.5)
99006 FORMAT (I5,1X,A10,F10.5,I5,10I3,:,(41X,10I3))
99007 FORMAT (10X,A10)
99008 FORMAT (8F10.4)
99009 FORMAT (2I3,5F8.4)
99010 FORMAT (3X,I3,F8.4)
99011 FORMAT (' IT IS   TOT IC= ',10(I1,:,7X))
      END
