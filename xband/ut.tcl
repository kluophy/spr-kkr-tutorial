# Allgemeine Prozeduren f�r xband_menu
#
# Copyright (C) 1994  G. Lamprecht, W. Lotz, R. Weibezahn; LRW c/o Uni Bremen
# Copyright (C) 1996  G. Lamprecht, W. Lotz, R. Weibezahn; IWD, Bremen University

#-----------------------------------------------------------------
#       get the date in a standard format independent from country

proc standard_date {} {

set    job [open "zzzzzz_get_date" w]
puts  $job "export LANG=C"
puts  $job "date"
close $job
set res [catch "exec chmod 755 zzzzzz_get_date" message]
set res [catch "exec /bin/sh ./zzzzzz_get_date" result]
set res [catch "exec rm      ./zzzzzz_get_date" message]

# puts ">>>>>>>> STANDARD DATE: $result"

return $result

}

proc decr {v} {upvar 1 $v n; incr n -1}; # decrement v by -1

# liefert Uhrzeit als Return-Wert
#
proc datime {} {
    return [clock format [clock seconds] -format %H:%M:%S]
}


proc cat_file {dnam f} {# Anlisten der Datei dnam im Fenster f

  $f configure -state normal
  set fid [open "$dnam" r]; $f insert end "[read $fid]@\n"; close $fid
  $f configure -state disabled
} 


proc cat_file0 {dnam f} {# Anlisten der Datei dnam im Fenster f
#			   nach vorherigem L�schen des bisherigen Fensterinhalts
  global anl0 

  set fid [open "$dnam" r]
  if {($anl0==1)&&($f==".d.tt")} {# Anlisten "mit Vorsicht" (derzeit l�uft ein Programm)
    $f insert end "\n\n***** start inserting man page\n[read $fid]\n***** end   inserting man pages\n\n"
  } else {# Anlisten normal, es l�uft derzeit kein Programm
    $f configure -state normal
    $f delete 0.0 end;  $f insert end "[read $fid]@\n";  $f yview 0
    $f configure -state disabled
  }
  close $fid
} 


proc cathfile0 {d f} {global hlp_dir; cat_file0 $hlp_dir$d.hlp $f.d.tt;# driver for cat_file0}


proc clearscr {f} {# L�schen des Textfensters f
#
  $f configure -state normal;  $f delete 0.0 end;  $f configure -state disabled
}


proc testfilename {fname win suf} {# test filename (only letters, numerals, some others)
  set f $fname
  if {$f==""} {writescr $win "error: empty file name!\n"; $win yview 0; return 1}
  
  if {[regexp {^[/a-zA-Z0-9_-]+$} $f]&&![regexp {^[-].*$} $f]} {return 0} 
  set r 1
  writescr $win "warning: file name \"$f\" contains special characters\n         we recommend that you only use letters and digits"
  if {$suf!=""} {writescr $win ", possibly followed by a suffix \"$suf\""}
  writescr $win "\n"
  if {[string match "* *" $f]} {writescr $win "\n***** warning: you used spaces in the file name!!!\n"; set r 2} 
  if {[regexp {^[-].*$} $f]} {writescr $win "\n***** error: the 1st character is the \"-\" character!!!\n"} 
  $win yview 0; return $r
}


proc mtest {str var} {# Looks for the Variable $var in the personal .vst-files;
#                       if found: sets it to the assigned value
  upvar 1 $var v
  set pat $var
  if {[string match "$pat*" [string trim $str]]} {
	set z "[string trim $str] "; set p [string first " " $z]
	set v [string trim [string range $z $p end]] 
  }
}


proc progback {prog back} {# Testen, ob Programm $prog im Hintergrund 
#                            aufgerufen werden soll,
#                            Setzen von $back und korrigieren von $prog
  upvar 1 $prog programm
  upvar 1 $back background

  set bpos [string last & "$programm"]
  if {$bpos>0} {
    set programm [string trim [string range $programm 0 [expr {$bpos - 1}]]]
    set background & 
  } else {
    set background ""
  }
  set programm [string trim $programm]
}


proc writescr {w args} {# Ausgabe einer variablen Anzahl von Strings 
#                        (aus $args) im Textfenster $w

  $w configure -state normal
  foreach i $args {$w insert end "$i"}
  $w yview -pickplace end
  $w configure -state disabled
  update idletasks
} 


proc writescrd {w PRC message} {
# print message in textwindow    w
# and write debug info on procedure  PRC
#
    global DEBUG 

    writescr $w "\n$message"

    if {$DEBUG == 1} {
	puts stdout "<$PRC>:  $message"
    }
    
} 


proc writescr0 {w args} {# Ausgabe einer variablen Anzahl von Strings 
#                         (aus $args) im Textfenster $w
#			  nach vorherigem L�schen des bisherigen Fensterinhalts
  $w configure -state normal
  $w delete 0.0 end
  foreach i $args {$w insert end "$i"}
  $w yview -pickplace end
  $w configure -state disabled
  update idletasks
} 


proc writescr2 {w args} {# Ausgabe einer variablen Anzahl von Strings ohne update
#                        (aus $args) im Textfenster $w
  $w configure -state normal
  foreach i $args {$w insert end "$i"}
  $w yview -pickplace end
  $w configure -state disabled
} 


proc getscl {foid e} {# filter for call of gets, but line breaks by "\" allowed
#
  upvar 1 $e ein

  set ret [gets $foid z];  set z [string trimright $z]
  set ein "$z"
  set lm1 [expr [string length $z]-1]
  while {($lm1>=0)&&([string last \\ $z]==$lm1)} { 
    set ein [string range $ein 0 [expr [string length $ein]-2]]
    set ret [gets $foid z];  set z [string trimright $z]
    set lm1 [expr [string length $z]-1]
    set ein "$ein$z"
  }
  if {$ret>=0} {set ret [string length "$ein"]};  return $ret
}


proc getscl0 {foid e} {# like getscl, but blanks at begin-of-continuation-line deleted
#                
  upvar 1 $e ein

  set ret [gets $foid z];  set z [string trimright $z]
  set ein "$z"
  set lm1 [expr [string length $z]-1]
  while {($lm1>=0)&&([string last \\ $z]==$lm1)} { 
    set ein [string range $ein 0 [expr [string length $ein]-2]]
    set ret [gets $foid z];  set z [string trimright $z]
    set lm1 [expr [string length $z]-1]
    set ein "$ein[string trimleft $z]"
  }
  if {$ret>=0} {set ret [string length "$ein"]};  return $ret
}


proc fillbox {w dat} {# fill listbox $w from .vst-file $dat, if file exists
#
  if {$dat!=""} {set f [open_vst $dat]; while {[getscl $f e]>0} {$w insert end $e}; close $f}
}


proc fillboxsep {w dat} {# listbox $w f�llen aus .vst-Datei $dat und Separator
#                                     bestimmen (1. Zeichen ungleich Blank)
  set f [open_vst $dat]
  getscl $f e;  set sep [string index [string trim $e] 0]
  while {[getscl $f e]>0} {$w insert end $e}
  close $f
  return $sep
}


proc TestNotEmpty {string message} {# write message if string empty and close
 if {$string!=""} return
 global Werr;  set Werr .xbanderror
 puts stdout "\n$message\n"; flush stdout
 toplevel_init $Werr abc 0 0
 button $Werr.l -text "$message --> close" -command {destroy $Werr; exit} -height 20
 pack configure $Werr.l -in $Werr -side left; update
 bind $Werr <Button-1> {destroy $Werr; exit}
}


proc GetSep {d f} {# read separator for vst-file (read separator line only)
 getscl $f e; set s [string index [string trim $e] 0]
 TestNotEmpty "$s" "Error in file $d (empty first line!)\n inform your system administrator/TeX administrator!"
 return $s
}


proc getsep {d} {# get separator for vst-file $d (open, read-separator, close)
 set f [open_vst $d]; set s [GetSep $d $f]; close $f
 return $s
}


proc getlnum {d} {# get number of lines in file $d (open, read-lines, close)
  set f [open_vst $d]; set n 0; while {[getscl $f e]>0} {incr n}; close $f; return $n
}


proc alternative {dv t1 t2} {# Hilfsprozedur f�r alternative Ausgabe 
  if {$dv==""} {return $t1} else {return $t2}
}


proc stripdir {file} {return [crange $file [string last / $file]+1 end];# strip dir from file}


proc request {t frage1 frage2 aus abb helpdatei w} {# Abfrage-Men� (Fragetext und zwei
#                                                            Antwort-Buttons)
  global  hlp_dir geom
  wm title $t "confirm"
  if [info exists geom(ut_rp)] {wm geometry $t $geom(ut_rp)}
  bind $t <Button-3> "cat_file0 ${hlp_dir}${helpdatei} $w"
  label $t.dv1 -width 80 -anchor w  -text "$frage1"
  pack configure $t.dv1 -in $t -padx 37 -pady 13
  bind $t.dv1 <Button-3> "cat_file0 ${hlp_dir}${helpdatei} $w"
  if {$frage2!=""} {
    label $t.dv2 -width 80 -anchor w  -text "$frage2"
    pack configure $t.dv2 -in $t -padx 37 -pady 13
    bind $t.dv2 <Button-3> "cat_file0 ${hlp_dir}${helpdatei} $w"
  }
  frame $t.bv -relief raised -borderwidth 0
  pack configure $t.bv -in $t -padx 13 -pady 13
  button $t.bv.aus -text "$aus"
  button $t.bv.abb -text "$abb"
  pack configure $t.bv.aus $t.bv.abb -in $t.bv -side left -anchor n -padx 13 -pady 3
  bind $t.bv.aus <Button-3> "cat_file0 ${hlp_dir}${helpdatei} $w"
  bind $t.bv.abb <Button-3> "cat_file0 ${hlp_dir}${helpdatei} $w"
}

proc destror w {global geom
  set g [wm geometry $w]; set geom(ut_rp) [crange $g [string first + $g] end]
  destroy $w; update
}

proc destros w {global geom; set geom($w) [wm geometry $w];  destroy $w; update}

proc toplevel_init {w wname wmin wmax} { ; # initialisations for toplevel window
global geom
global VISUAL W_screen H_screen 
  
  if [winfo exists $w] {destros $w}
  toplevel $w -visual $VISUAL 
  wm title $w $wname
  wm iconname $w $wname
  wm minsize $w $wmin $wmax
  wm maxsize $w [expr int(0.2*$W_screen)] [expr int(0.2*$H_screen)]
  wm maxsize $w $W_screen $H_screen
###  if [info exists geom($w)] {wm geometry $w $geom($w)}
}

proc toplevel_init_SB {w wname wmin wmax} { ; # initialisations for toplevel window
global geom
global VISUAL W_screen H_screen 
  
  if [winfo exists $w] {destros $w}
  toplevel $w -visual $VISUAL 
  wm title $w $wname
  wm iconname $w $wname
  wm minsize $w $wmin $wmax
  wm maxsize $w [expr int(0.2*$W_screen)] [expr int(0.2*$H_screen)]
  wm maxsize $w $W_screen $H_screen
###  if [info exists geom($w)] {wm geometry $w $geom($w)}
}



proc getvalue {zeile n separator} {# aus der Zeile den n-ten Wert zur�ckgeben

    set tmp $zeile
    set count 0
    while {$count<$n} {
	set pos [string first $separator $tmp]
	if {$pos>=0} {set tmp [string range $tmp [expr $pos+1] end]} else {set tmp ""}
	incr count
    }
    
    set pos [string first $separator $tmp]
    if {$pos>=0} {set tmp [string range $tmp 0 [expr $pos-1]]}
    incr count

    return [string trim $tmp]
}


proc vstr_readinst1 {varname} {global $varname
 if {[info exists env(XBANDINSTALL)]} {set f [open "$env(XBANDINSTALL)" r]} \
	else {set f [open_vst install.vst]}
 while {[getscl $f z]>0} \
   {set [string trim [string range $z 0 18]] [string trim [string range $z 19 end]]}
 close $f
}

proc vstr_editor {} {
 global We editor edtext edxterm edoptions evesel_v
 set f [open_vst editor.vst]; set s [GetSep editor.vst $f]; getscl $f e; close $f
 set evesel_v "$e"; set edtext [getvalue $e 0 $s]
 set edxterm   [getvalue $e 1 $s]; set editor [getvalue $e 2 $s]
 set edoptions [getvalue $e 3 $s]
 vstr_readinst1 edsyntaxhelp
 if [winfo exists $We.c.f.f.li] {selection clear $We.c.f.f.li}
}


proc vstr_cleanup {} {
 global aufmax aufsuff auftoggle
 set f [open_vst cleanup.vst]; set s [GetSep cleanup.vst $f]
 set aufsuff ""; set auftoggle ""
 while {[getscl $f e]>0} \
   {set aufsuff "$aufsuff [getvalue $e 0 $s]"; set auftoggle "$auftoggle [getvalue $e 1 $s]"}
 close $f
}


proc vstr_prgdir {} {
 global prgdirlist
 set f [open_vst cleanup.vst]; set s [GetSep cleanup.vst $f]
 set prgdirlist  ""
 while {[getscl $f e]>0} \
   {set prgdirlist "$prgdirlist [getvalue $e 0 $s]"}
 close $f
 puts stdout " prgdirlist"
}

proc vst_getvalues {w vstdat} {# Voreinstellungen lesen - Werte aus Datei lesen
#
    global vst_dir version_vst 
    global editor edtext edxterm edoptions edsyntaxhelp edback
    global index 
    global NPACKAGE PACKAGE_NAME
    global PROG_PATH0 PROG_DIRECT PROG_OPTION
    global PRINTPS RASMOL XMGR SYMMETRY DEBUG
    global DATAEXPLORER 
# -> automatic setting  global PLOTPROG SYMMETRY 
    global GREP1 GREP2 GREP3 
    global COLOR key_COLOR key_GEOMETRY
    global aufmax aufsuff auftoggle
    global prgdirlist prgdirlistmaxindex
    global sysdirlist sysdirlistmaxindex
    global exedirlist exedirlistmaxindex
    global geom
    global PROGS_NAME_LIST_TAB PROGS_VERS_LIST_TAB    

    if {$vstdat==""} {
	vstr_editor
	vstr_cleanup 
    } else {
	if {$w != ""} {writescr0 $w "settings loaded from: $vstdat\n\n"}
	set f [open $vstdat r]
	while {[getscl0 $f z]>0} {
	    set z "[string trim $z] "
            set p [string first " " $z]
	    set [crange $z 0 $p-1] [string trim [crange $z $p end]]
            puts "[crange $z 0 $p-1]      <<<   [string trim [crange $z $p end]]"

	}
	close $f
    }
    progback  editor  edback
}

proc vst_read {w vst_file} {# Voreinstellungen lesen
#
    global  version_vst main_file dir env vstdat
    global  editor edback 
    global  edxterm edoptions version_incompatible 
    global  vst_dir
    
    set version_vst 0.00
    if {$vst_file=="default.vst"}  {# "Grund-Voreinst."
	set vstdat ""
    } else                            {# "Start/eigene Voreinst."
	if {($main_file=="")&&[file exists .lastxband]} {
	    set f [open ".lastxband" r]
	    while {[getscl $f e]>0} {mtest $e dir; mtest $e main_file}
	    close $f
	}
	if {($main_file!="")&&[file exists "${main_file}.vst"] \
		&&($vst_file!=".xband.vst")} {
	    set vstdat ${main_file}.vst
	} elseif {[file exists "$env(HOME)/.xband.vst"]} {
	    set vstdat "$env(HOME)/.xband.vst"
	} else {
	    set vstdat ""
	}
    }
    vst_getvalues $w $vstdat
    if {$w!=""} {disp_prefs1 $w}
    set r 0
    if {[string range $version_vst 0 3]<=$version_incompatible} {
##      if {$vstdat!=""} {rename $vstdat $vstdat.bak; set r $vstdat}
	if {$vstdat!=""} {exec cp $vstdat $vstdat.bak; set r $vstdat}
    }
    if {$vstdat==""} {set vstdat $vst_dir/*.vst}
    return $r
}

proc lastxband_write {} {
    global dir main_file 
    set f [open ".lastxband" w]
    puts $f "dir                $dir"
    puts $f "main_file          $main_file"
    close $f
}


proc vst_write {win} {# Voreinstellungen sichern
#
  global  Wq main_file env 

  if {[string length $main_file]>0} {
    vst_write_file "${main_file}.vst"
    lastxband_write
    writescr0 $win "the following values are stored in ${main_file}.vst \n\n"
    set frage "[eval list The settings have been stored connected with  ``$main_file''!]"
    if {[winfo exists $Wq]} {destroy $Wq}; toplevel $Wq
    set frage1 "[eval list The settings have been stored connected with  ``$main_file''!]"
    set frage2 "[eval list Are these values also to be stored as your base settings?]"
    request $Wq "$frage1" "$frage2" "yes" "no"  ut_saven.hlp .d.tt
    bind $Wq.bv.abb <Button-1> {destror $Wq; Bend; # no save in $HOME}
    bind $Wq.bv.aus <Button-1> {destror $Wq;       # save in $HOME
      vst_write_file "$env(HOME)/.xband.vst"
      writescr0 .d.tt "the following values are stored in $env(HOME)/.xband.vst \n\n"
      disp_prefs1 .d.tt
      Bend
    }
    
  } else {
    vst_write_file "$env(HOME)/.xband.vst"
    writescr0 $win "the following values are stored in $env(HOME)/.xband.vst \n\n"
  }
  disp_prefs1 $win
}


proc vst_write_file {vst_file} {# Voreinstellungen in vst-Datei schreiben
#
    global version dir main_file edback env
    global editor edtext edxterm edoptions edsyntaxhelp 
    global index 
    global aufmax aufsuff auftoggle
    global sysdirlist sysdirlistmaxindex
    global prgdirlist prgdirlistmaxindex
    global exedirlist exedirlistmaxindex
    global geom
    global PRINTPS RASMOL XMGR DEBUG
    global DATAEXPLORER 
# -> automatic setting  global PLOTPROG SYMMETRY 
    global COLOR key_COLOR key_GEOMETRY
    global NPACKAGE PACKAGE_NAME PROGS_NAME_LIST_TAB PROGS_VERS_LIST_TAB
    global PROG_PATH0 PROG_DIRECT PROG_OPTION
    global GREP1 GREP2 GREP3 

    set geom(xband) [wm geometry .]
    set f [open "$vst_file" w]
    puts $f "version_vst  $version"
    if {"$vst_file" != "$env(HOME)/.xband.vst"} {
	puts $f "dir  $dir"
	puts $f "main_file  $main_file"
    }
    puts $f "editor  $editor$edback"; puts $f "edtext  $edtext"
    puts $f "edxterm  $edxterm";  puts $f "edoptions  $edoptions"
    puts $f "edsyntaxhelp  $edsyntaxhelp"
    puts $f "index     $index"
    puts $f "PRINTPS   $PRINTPS"
    puts $f "RASMOL    $RASMOL"
    puts $f "XMGR      $XMGR"
    puts $f "DATAEXPLORER $DATAEXPLORER"
# -> automatic setting    puts $f "PLOTPROG  $PLOTPROG"
# -> automatic setting    puts $f "SYMMETRY  $SYMMETRY"
    puts $f "PROG_PATH0   $PROG_PATH0"  
    puts $f "NPACKAGE    $NPACKAGE"
    for {set i 1} {$i<=$NPACKAGE} {incr i} {
	set PACKAGE $PACKAGE_NAME($i)
	puts $f "PACKAGE_NAME($i) $PACKAGE"
	puts $f "PROG_DIRECT($PACKAGE)  $PROG_DIRECT($PACKAGE)"  
	puts $f "PROG_OPTION($PACKAGE)  $PROG_OPTION($PACKAGE)"  
	puts $f "PROGS_NAME_LIST_TAB($PACKAGE) $PROGS_NAME_LIST_TAB($PACKAGE)"
 	puts $f "PROGS_VERS_LIST_TAB($PACKAGE) $PROGS_VERS_LIST_TAB($PACKAGE)"
    }
    puts $f "GREP1     $GREP1"
    puts $f "GREP2     $GREP2"
    puts $f "GREP3     $GREP3"
    puts $f "key_COLOR  $key_COLOR"
    puts $f "key_GEOMETRY  $key_GEOMETRY"
    puts $f "DEBUG         $DEBUG"
    puts $f "aufmax  $aufmax"
    puts $f "aufsuff  $aufsuff"
    puts $f "auftoggle  $auftoggle"
    puts $f "prgdirlistmaxindex   $prgdirlistmaxindex" 
    puts $f "prgdirlist   $prgdirlist" 
    puts $f "exedirlistmaxindex   $exedirlistmaxindex" 
    puts $f "exedirlist   $exedirlist" 
    puts $f "sysdirlistmaxindex  $sysdirlistmaxindex" 
    puts $f "sysdirlist  $sysdirlist" 
    if [info exists geom] {foreach k [array names geom] {puts $f "geom($k) $geom($k)"}}
    close $f
    
}


proc disp_prefs1 {win} {# aktuelle Einstellungen anzeigen (ohne Dateien)
#
  global  editor edback edtext 
  global  PRINTPS index 

  writescr .d.tt " editor $editor$edback  $edtext" \
	[alternative $edback "\n" "   (Background call)\n"] \
	" printps command       : $PRINTPS\n"
}


proc disp_prefs2 {win} {# aktuelle Einstellungen anzeigen (mit Dateien)
#
  global  main_file

  disp_prefs1 $win
}


proc LockButtons {args} {foreach i $args {$i configure -state disabled}; update idletasks;# locks the given buttons}


proc UnlockButtons {args} {foreach i $args {$i configure -state normal}; update idletasks;# unlocks the given buttons}


proc ConfigText {w t} {if [winfo exists $w] {$w configure -text $t}}


proc lock {} {# Verriegeln aller Buttons des Hauptmenues
#
  global gesliste anl0

  set anl0 1
  set nmax [llength $gesliste]
  while {$nmax>0} {incr nmax -1;  [lindex $gesliste $nmax] configure -state disabled}
  update idletasks
}


proc unlock {args} {# Entriegeln der angegebenen Buttons (bel. viele)
#
  global sub anl0

  foreach i $args {
    set k [llength $i]
    while {$k>0} {incr k -1; set ll [lindex $i $k]; $ll configure -state normal}
  }
  set sub 0
  set anl0 0
}


proc unlock_list {} {# Entriegeln der Buttons der globalen Liste
#
  global liste edsubback 

  if {([edactiv]==0)&&($edsubback>0)} {
    set edsubback 0
    if {[lsearch $liste .c.1.ed]<0} {set liste [linsert $liste 1 .c.1.ed]}
  }

  unlock $liste
}


proc edactiv {} {#  Kontrolle, wieviele Editor-Prozesse im Background aktiv
#
  global edliste

  set l [llength $edliste]
  while {$l>0} {
   incr l -1
   set p [lindex $edliste $l]
   if {[process $p]!=0} {set edliste [lreplace $edliste $l $l]};# Prozess $p nicht mehr aktiv!
  }
  return [llength $edliste]
}



proc kill_list {prozessliste} {# kill processes
  
  if {[llength $prozessliste]!=0} {foreach pi $prozessliste {killprocess $pi}; exec sleep 1}

}


proc open_vst {filename} {# open for a .vst-file

  global xband_path vst_dir env 

  if {[info exists env(XBANDVSTDIR)] && [file exists $env(XBANDVSTDIR)/$filename]} {
      set f [open "$env(XBANDVSTDIR)/$filename" r]
      set vst_dir $env(XBANDVSTDIR)
  } else {
      set f [open "$xband_path/locals/$filename" r]
      set vst_dir $xband_path/locals
  }

  return $f

}


proc insert_textframe {w tyh} {# frame for help/information text
  global hlp_dir

  frame $w.ditf; pack configure $w.ditf -in [alternative $w . $w] -pady 3
  frame $w.d -relief raised -borderwidth 2
  pack configure $w.d -in [alternative $w . $w] -anchor sw -fill x -expand yes

  scrollbar $w.d.sb -command "$w.d.tt yview"
  bind      $w.d.sb <Button-3>  "cat_file0 ${hlp_dir}z_textfeld.hlp $w.d.tt"
  text      $w.d.tt -yscrollcommand "$w.d.sb set" -height $tyh
  bind      $w.d.tt <Button-3> "cat_file0 ${hlp_dir}z_textfeld.hlp $w.d.tt"
  pack configure $w.d.sb -in $w.d -side right -fill y
  pack configure $w.d.tt -in $w.d -side right -fill both -expand yes
  $w.d.tt configure -state disabled
}


proc insert_topbuttons {w h args} {# inserts 3 top buttons: close, help, clear -> $w.a
  global hlp_dir

  if {$args==""} {set wh $w.d.tt} {set wh $args}
  frame $w.a -relief raised -borderwidth 1; pack configure $w.a -in $w -pady 3 -anchor nw
 
  button $w.a.e -text "close"
  bind   $w.a.e <Button-3> "cat_file0 ${hlp_dir}z_vstquit.hlp $wh"
  button $w.a.h -text "help" 
  bind   $w.a.h <Any-Button> "cat_file0 ${hlp_dir}$h $wh"
  button $w.a.l -text "clear text field" -command "clearscr $wh"
  bind   $w.a.l <Button-3> "cat_file0 ${hlp_dir}z_loeschetf.hlp $wh"
  pack configure $w.a.e $w.a.h $w.a.l -in $w.a -side left -padx 3 -pady 3
}


proc vst2list {file skip sep text var} {# read help-file, return as list
  global [set var]
  if ![info exists [set var]] {set [set var] ""}
  set f [open_vst $file]
  for {set s 1} {$s<=$skip} {incr s} {getscl $f e}
  set l ""
  while {[getscl $f e]>0} {
    lappend l $e
     if {$text==[getvalue $e 0 $sep]} {eval set [set var] \"\$e\"}
		# do not evaluate $e here 
  }
  close $f
  return "$l"
}


proc CreateLSBox {w text s wi he bw hf hw sep list com sel} {
# creates frame w.f and title for selection, fills with list
# creates selection box + scrollbar or radiobuttons; width wi, height he; help-text hf in hw
#
  global hlp_dir [set com]_v lastclick

  frame $w.f; pack configure $w.f -in $w -anchor w -padx 4
  if {$text!=""} {
    label $w.f.l -text $text; pack configure $w.f.l -side $s -in $w.f -anchor w
    if {$hf!=""} \
	{bind $w.f.l <Button-3> "writescr0 $hw \"left mouse click:\n\"; cat_file $hlp_dir$hf $hw"}
  }
  frame $w.f.f -relief raised -borderwidth $bw ; pack configure $w.f.f -in $w.f -anchor w
  if {[regexp "sel0*" "$sel"]||([llength $list]>$he)} {
    scrollbar $w.f.f.sb -command "$w.f.f.li yview"
    if {$sel=="sel0"} {set sc ""} {set sc "$w.f.f.sb set"}
    listbox $w.f.f.li -yscrollcommand "$sc" -width $wi -height $he
    pack configure $w.f.f.sb $w.f.f.li -in $w.f.f -side right -fill y
    if {$list!=""} then {foreach e $list {$w.f.f.li insert end $e}}
    if {$hf!=""} {
      bind $w.f.f.sb <Button-3> "writescr0 $hw \"left mouse click:\n\"; cat_file $hlp_dir$hf $hw"
      bind $w.f.f.li <Button-3> "writescr0 $hw \"left mouse click:\n\"; cat_file $hlp_dir$hf $hw"
    }
    if {$sel=="sel0"} {destroy $w.f.f.sb}
    bind $w.f.f.li <Leave> Bend
    bind $w.f.f.li <Double-Button-1> Bend
    bind $w.f.f.li <Triple-Button-1> Bend
    eval bind $w.f.f.li <Button-1> \
			\{set lastclick $w.f.f.li\; [set com] \[Selektion %W %y\]\; Bend\}
  } else {
    set i 0
    foreach e $list {
      incr i
      radiobutton $w.f.f.$i -width $wi -anchor w -text [getvalue $e 0 $sep] \
	-variable [set com]_v -value "$e" -command "set lastclick $w.f.f.$i; [set com] \{$e\}"
      pack configure $w.f.f.$i -in $w.f.f -anchor nw
      if {$hf!=""} {bind $w.f.f.$i <Button-3> \
			"writescr0 $hw \"left mouse click:\n\"; cat_file $hlp_dir$hf $hw"}
    }
  }
}


proc CreateLSBox2 {w text s wi he bw hf hw sep list com com2 sel} {
# creates frame w.f and title for selection, fills with list
# creates selection box + scrollbar or radiobuttons; width wi, height he; help-text hf in hw
# allow commoand com2 for button 2
#
  global hlp_dir [set com]_v lastclick

  frame $w.f; pack configure $w.f -in $w -anchor w -padx 4
  if {$text!=""} {
    label $w.f.l -text $text; pack configure $w.f.l -side $s -in $w.f -anchor w
    if {$hf!=""} \
	{bind $w.f.l <Button-3> "writescr0 $hw \"left mouse click:\n\"; cat_file $hlp_dir$hf $hw"}
  }
  frame $w.f.f -relief raised -borderwidth $bw ; pack configure $w.f.f -in $w.f -anchor w
  if {[regexp "sel0*" "$sel"]||([llength $list]>$he)} {
    scrollbar $w.f.f.sb -command "$w.f.f.li yview"
    if {$sel=="sel0"} {set sc ""} {set sc "$w.f.f.sb set"}
    listbox $w.f.f.li -yscrollcommand "$sc" -width $wi -height $he
    pack configure $w.f.f.sb $w.f.f.li -in $w.f.f -side right -fill y
    if {$list!=""} then {foreach e $list {$w.f.f.li insert end $e}}
    if {$hf!=""} {
      bind $w.f.f.sb <Button-3> "writescr0 $hw \"left mouse click:\n\"; cat_file $hlp_dir$hf $hw"
      bind $w.f.f.li <Button-3> "writescr0 $hw \"left mouse click:\n\"; cat_file $hlp_dir$hf $hw"
    }
    if {$sel=="sel0"} {destroy $w.f.f.sb}
    bind $w.f.f.li <Leave> Bend
    bind $w.f.f.li <Double-Button-1> Bend
    bind $w.f.f.li <Triple-Button-1> Bend
    eval bind $w.f.f.li <Button-1> \
			\{set lastclick $w.f.f.li\; [set com] \[Selektion %W %y\]\; Bend\}
    eval bind $w.f.f.li <Button-2> \
                        \{set lastclick $w.f.f.li\; [set com2] \[Selektion %W %y\]\; Bend\}                                        
  } else {
    set i 0
    foreach e $list {
      incr i
      radiobutton $w.f.f.$i -width $wi -anchor w -text [getvalue $e 0 $sep] \
	-variable [set com]_v -value "$e" -command "set lastclick $w.f.f.$i; [set com] \{$e\}"
      pack configure $w.f.f.$i -in $w.f.f -anchor nw
      if {$hf!=""} {bind $w.f.f.$i <Button-3> \
			"writescr0 $hw \"left mouse click:\n\"; cat_file $hlp_dir$hf $hw"}
    }
  }
}




proc CreateLSBoxSBx {w text s wi he bw hf hw sep list com sel} {
# creates frame w.f and title for selection, fills with list
# creates selection box + scrollbar or radiobuttons; width wi, height he; help-text hf in hw
#  CreateLSBox + scroll bar x
#
  global hlp_dir [set com]_v lastclick

  frame $w.f; pack configure $w.f -in $w -anchor w -padx 4
  if {$text!=""} {
    label $w.f.l -text $text; pack configure $w.f.l -side $s -in $w.f -anchor w
    if {$hf!=""} \
	{bind $w.f.l <Button-3> "writescr0 $hw \"left mouse click:\n\"; cat_file $hlp_dir$hf $hw"}
  }
  frame $w.f.f -relief raised -borderwidth $bw ; pack configure $w.f.f -in $w.f -anchor w
  if {[regexp "sel0*" "$sel"]||([llength $list]>$he)} {
    scrollbar $w.f.f.sby -command "$w.f.f.li yview" -orient vertical  
    scrollbar $w.f.f.sbx -command "$w.f.f.li xview" -orient horizontal
    if {$sel=="sel0"} {set scy ""} {set scy "$w.f.f.sby set"}
    if {$sel=="sel0"} {set scx ""} {set scx "$w.f.f.sbx set"}
    listbox $w.f.f.li -yscrollcommand "$scy"  -xscrollcommand "$scx" -width $wi -height $he
    pack configure $w.f.f.sby $w.f.f.li -in $w.f.f -side right  -fill y -expand 0
    pack configure $w.f.f.sbx $w.f.f.li -in $w.f.f -side bottom -fill x -expand 0
    if {$list!=""} then {foreach e $list {$w.f.f.li insert end $e}}
    if {$hf!=""} {
      bind $w.f.f.sby <Button-3> "writescr0 $hw \"left mouse click:\n\"; cat_file $hlp_dir$hf $hw"
      bind $w.f.f.sbx <Button-3> "writescr0 $hw \"left mouse click:\n\"; cat_file $hlp_dir$hf $hw"
      bind $w.f.f.li  <Button-3> "writescr0 $hw \"left mouse click:\n\"; cat_file $hlp_dir$hf $hw"
    }
    if {$sel=="sel0"} {destroy $w.f.f.sby}
    if {$sel=="sel0"} {destroy $w.f.f.sbx}
    bind $w.f.f.li <Leave> Bend
    bind $w.f.f.li <Double-Button-1> Bend
    bind $w.f.f.li <Triple-Button-1> Bend
    eval bind $w.f.f.li <Button-1> \
			\{set lastclick $w.f.f.li\; [set com] \[Selektion %W %y\]\; Bend\}
  } else {
    set i 0
    foreach e $list {
      incr i
      radiobutton $w.f.f.$i -width $wi -anchor w -text [getvalue $e 0 $sep] \
	-variable [set com]_v -value "$e" -command "set lastclick $w.f.f.$i; [set com] \{$e\}"
      pack configure $w.f.f.$i -in $w.f.f -anchor nw
      if {$hf!=""} {bind $w.f.f.$i <Button-3> \
			"writescr0 $hw \"left mouse click:\n\"; cat_file $hlp_dir$hf $hw"}
    }
  }
}


proc CreateLSBoxSBx2 {w text s wi he bw hf hw sep list com com2 sel} {
# creates frame w.f and title for selection, fills with list
# creates selection box + scrollbar or radiobuttons; width wi, height he; help-text hf in hw
# allow commoand com2 for button 2
#  CreateLSBox2 + scroll bar x
#
  global hlp_dir [set com]_v lastclick

  frame $w.f; pack configure $w.f -in $w -anchor w -padx 4
  if {$text!=""} {
    label $w.f.l -text $text; pack configure $w.f.l -side $s -in $w.f -anchor w
    if {$hf!=""} \
	{bind $w.f.l <Button-3> "writescr0 $hw \"left mouse click:\n\"; cat_file $hlp_dir$hf $hw"}
  }
  frame $w.f.f -relief raised -borderwidth $bw ; pack configure $w.f.f -in $w.f -anchor w
  if {[regexp "sel0*" "$sel"]||([llength $list]>$he)} {
    scrollbar $w.f.f.sby -command "$w.f.f.li yview" -orient vertical
    scrollbar $w.f.f.sbx -command "$w.f.f.li xview" -orient horizontal
    if {$sel=="sel0"} {set scy ""} {set scy "$w.f.f.sby set"}
    if {$sel=="sel0"} {set scx ""} {set scx "$w.f.f.sbx set"}
    listbox $w.f.f.li -yscrollcommand "$scy"  -xscrollcommand "$scx" -width $wi -height $he
    pack configure $w.f.f.sby $w.f.f.li -in $w.f.f -side right  -fill y -expand 0
    pack configure $w.f.f.sbx $w.f.f.li -in $w.f.f -side bottom -fill x -expand 0
    if {$list!=""} then {foreach e $list {$w.f.f.li insert end $e}}
    if {$hf!=""} {
      bind $w.f.f.sby <Button-3> "writescr0 $hw \"left mouse click:\n\"; cat_file $hlp_dir$hf $hw"
      bind $w.f.f.sbx <Button-3> "writescr0 $hw \"left mouse click:\n\"; cat_file $hlp_dir$hf $hw"
      bind $w.f.f.li <Button-3> "writescr0 $hw \"left mouse click:\n\"; cat_file $hlp_dir$hf $hw"
    }
    if {$sel=="sel0"} {destroy $w.f.f.sby}
    if {$sel=="sel0"} {destroy $w.f.f.sbx}
    bind $w.f.f.li <Leave> Bend
    bind $w.f.f.li <Double-Button-1> Bend
    bind $w.f.f.li <Triple-Button-1> Bend
    eval bind $w.f.f.li <Button-1> \
			\{set lastclick $w.f.f.li\; [set com] \[Selektion %W %y\]\; Bend\}
    eval bind $w.f.f.li <Button-2> \
                        \{set lastclick $w.f.f.li\; [set com2] \[Selektion %W %y\]\; Bend\}                                        
  } else {
    set i 0
    foreach e $list {
      incr i
      radiobutton $w.f.f.$i -width $wi -anchor w -text [getvalue $e 0 $sep] \
	-variable [set com]_v -value "$e" -command "set lastclick $w.f.f.$i; [set com] \{$e\}"
      pack configure $w.f.f.$i -in $w.f.f -anchor nw
      if {$hf!=""} {bind $w.f.f.$i <Button-3> \
			"writescr0 $hw \"left mouse click:\n\"; cat_file $hlp_dir$hf $hw"}
    }
  }
}


proc dir_file {} {# if filename is prefixed by directory: cd if possible
  global Wf Wq main_file dirprefix dir Wf fs_d fs_f

  set dirprefix ""
  if {[string last / $main_file]>=0} {# filename is prefixed by directory
    set d [crange $main_file 0 [string last / $main_file]-1]
    set f [crange $main_file [string last / $main_file]+1 end]
    if {[file isdirectory $d]&&[file readable $d]&&[file writable $d]&&[file executable $d]} {
      cd $d; set dir [pwd]; if [winfo exists $Wf] {dirselected $dir $f}
      set main_file $f
      if [winfo exists $Wf] {fsanzeige $Wf}
    } elseif {($d!="")&&![file exists $d]} {
      set fs_d "$d"; set fs_f "$f"; mkdcdfill
    } else {
      set dirprefix $d
    }
  }
}


proc mkdcdfill {} {
  global Wf Wq dir fs_d fs_f

  if [winfo exists $Wf.d.tt] {set w $Wf.d.tt} else {set w .d.tt}
  if [string match "* *" $fs_d] {writescr0 $w "directory name includes space(s): directory not created!"; return}
  regsub -all "//+" "$fs_d" "/" fs_d;  set fs_d [string trimright $fs_d /]

  if {[winfo exists $Wq]} {destroy $Wq}; toplevel $Wq
  set frage1 "directory \"$fs_d\" does not exist!   create directory?"; set frage2 ""
  request $Wq "$frage1" "$frage2" "yes" "no" ut_createdir.hlp $w
  bind $Wq.bv.abb <Button-1> {destror $Wq; Bend; # no directory create}
  bind $Wq.bv.aus <Button-1> {destror $Wq; # directory creation
    set res [catch "mkdir -path $fs_d" mes]
    if {$res==1} {
      if [winfo exists $Wf.d.tt] {set w $Wf.d.tt} else {set w .d.tt}
      writescr $w "\n*** error: file directory not created; return code:\n $mes\n "
    } else {
      dirselected $fs_d $fs_f
    }
    Bend
  }
}


proc which_silent {command} {

    global env

    foreach dir [split $env(PATH) :] { 
	set path [file join $dir $command] 
	if {[file isfile $path] && [file executable $path]} {
	    return $path
	}
    }
    
    return ""
}

proc which {command} {
    global env whichcheck
    
    if {$whichcheck=="yes"} {
	
	set path [which_silent $command]
	if {$path != ""} {
	    return $path
	} else {
	    return -code error \
		    -errorinfo "$command not found in [split $env(PATH) :]" "$command not found"
	}
    }
    
}

proc Selektion {W y} {# selection for list boxes (y=B1 (y coordinate) within widget W)
  set i [$W nearest $y]
  $W selection clear 0 end;  $W selection set $i
  return [$W get $i]
}


proc Bend {} {return -code break; # procedure to break resp. nullify bindings}


proc XhostTest {f str} {# procedure to test, whether send command is supposed to work here
  if [catch "exec xhost" xhostres] {
    if {$f!=""} {writescr $f "\n$str  xhost command not found \n\n"};  
    return -1
  } else {
    if {[string first \n $xhostres]>0} {set nl 9999} else {set nl 1}
    set beg [lrange $xhostres 0 7]
    if {([string first enabled $beg]>0)&&($nl>1)} \
	    {writescr $f "\n$str \"X server not secure\" (\"xauth-style authorization\" must be used); \n"
         writescr $f "$nd.4 (send) probably will not run --- see \"xband_texmenu.README\"  \n"
         writescr $f "and \"xhost\" command !\n\n"}
  }
}


proc AnzCharIn {c string} {
 set n 0
 if {$c=="z"} {set s "${string}x"} else {set s "${string}z"}
 while {[string first $c $s]>=0} {incr n; set s [crange $s [string first $c $s]+1 end]}
 return $n
}


proc CheckNumInt {num} {# checks, if num is integer number: return 0 if not; else ...
  set n "x$num"
  if {($n=="x0") ||[regexp {^x[1-9]+[0-9]*$} $n]}     {return  1};# >=0 without sign
  if {($n=="x+0")||[regexp {^x[+]+[1-9]+[0-9]*$} $n]} {return  2};# >=0 with sign
  if {[regexp {^x[-]+[1-9]+[0-9]*$} $n]}              {return -1};# <0
  if {($n=="x-0")}                                    {return -2};# -0 (0 with sign!)
  return 0
}


proc CheckNumReal {num} {# checks, if num is real number: return 0 if not; else ...
 set n $num
 if {([string first + $n]==0)&&([string length $n]>1)} {set s +;set n [crange $n 1 end]} \
 elseif {([string first - $n]==0)&&([string length $n]>1)} {set s -;set n [crange $n 1 end]} \
 elseif {([string length $n]>0)} {set s ""} \
 else {return 0}
 if {(![regexp {^[0-9.]+$} $n])||([AnzCharIn "." $n]>1)} {return 0}
 catch "if {($n==.)} {return 0}"
 # now we know: $num is number, n contains only digits an at most 1 point, $s contains sign
 return ${s}1
}


proc TestPut {l s} {
}


proc getBefore {str pos} {# returns all characters from string before position or empty string
  if {$pos<=0} {return ""} else {return [crange $str 0 $pos-1]}
}


proc getAfter {str pos} {# returns all characters from string after position or empty string
  if {$pos>=[string length $str]} {return ""} else {return [crange $str $pos+1 end]}
}



proc CommentIncl {string} {# returns 1 if comment starts elsewhere in string, 0 otherwise
  set rest "$string"; set commInc 0
  while {$rest!=""} {
    set fbpr [string first {\%} "$rest"]; set fpro [string first {%} "$rest"]
    if {($fbpr>=0)&&($fbpr<$fpro)} {set rest [getAfter "$rest" [expr $fbpr+1]]; # \% first} \
    elseif {$fpro>0} {set rest ""; set commInc 1; # \% first}
  }
  return $commInc
}



proc crange {s i1 i2} {

  set l [string length $s]

  if {$i2=="end"} {
     set top [expr $l - 1]
  } else {
     set top [expr $i2]
  }

  if {$l>0} {
     set result [string range $s $i1 $top]
  } else {
     set result ""
  }
}


# execute additional programs:  $utcmd $utoptions  with file  $ut_file
#                                 in background if  $utback = "&"  
#                                 with backup-file if  $utbackup = "1"  
#
# Copyright (C) 1994  G. Lamprecht, W. Lotz, R. Weibezahn; LRW c/o Uni Bremen
# Copyright (C) 1996  G. Lamprecht, W. Lotz, R. Weibezahn; IWD, Bremen University

proc utility {f austext} {

  upvar 1 $austext a

  global  utcmd utoptions ut_file utback utbackup

  set a "";  set ab "";  set tmpfilnam .utility.tmp

  writescr0 $f "[datime] execute program $utcmd$utback $utoptions $ut_file"

  if {($utbackup==1)&&($ut_file!="")} {

    set dateiliste [lsort [glob -nocomplain -- *]]
    set i 1;  while {[string match "*$ut_file-$i.bak*" "$dateiliste"]!=0} {incr i}
    set backupdateiname "$ut_file-$i.bak"
    set ab "\n original file ---> $backupdateiname"
    exec cp $ut_file $backupdateiname; lock
    set ab "${ab}\n recoded  file ---> $ut_file"

  }

  if {($utcmd=="changecode")&&($utoptions!="-analyse")} {# changecode with file modification

    exec mv $ut_file .tmp-$ut_file; lock
    eval mkCmd_wait $f $utcmd [list "$utoptions .tmp-$ut_file $ut_file"]
    if {[file isfile $ut_file]&&([file size $ut_file]>0)} {unlink -nocomplain .tmp-$ut_file} \
    else {exec mv .tmp-$ut_file $ut_file; lock}
    set ab "${ab}\n[datime] finished!"

  } elseif {$utback=="&"} {# program call in background

    eval set res [catch "exec $utcmd $utoptions $ut_file $utback" message]
    set a "${a}\n$message"
    if {$res!=0} {set a "${a}\n finished with return value=$res"} else {set a "${a}\n started in background"}

  } else {# program call in foreground

    eval mkCmd_wait $f $utcmd [list "$utoptions $ut_file"]
    set ab "${ab}\n[datime] finished!"

  }

  set a  "${a}$ab\n"

}


########################################################################################
# laux  returns 0 if str has length 0 else the result is 1
proc laux {str} {
    if { [string length [string trim $str]]==0 } {
        return 0
    } else {
        return 1 
    }
}

########################################################################################
# faux   returns str formatted with length n according to key - words for empty strings
proc faux {str key n} {
    if { [laux $str] == 0 } {
        return [format "%${n}s"    " "]
    } elseif { $key == "f" } {        
        return [format "%${n}.3f" $str]
    } elseif { $key == "i" } {        
        return [format "%${n}d"   $str]
    } elseif { $key == "e" } {        
        return [format "%${n}.3e" $str]
    }
}

########################################################################################
# freal_m_n returns   %m.nf   formatted   real number
proc freal_m_n {f m n} {
    return [format "%${m}.${n}f" $f]
}                                                                                        


########################################################################################
proc debug {PRC Message} {
#
#----------------------------------- to suppress debug info  set DEBUG 0
#
global DEBUG 

if {$DEBUG != 1} {return}

if {$Message=="WRHEAD"} {
   puts stdout " "
   puts stdout "************************************************************************"
   puts stdout "************************************************************************"
   puts stdout "     entering proc  <$PRC> "
   puts stdout "************************************************************************"
   puts stdout "************************************************************************"
   puts stdout " "
} else {
   puts stdout "<$PRC>:  $Message"
}

}

#
# read lines from locals/batchjobheader.vst
# and replace the following macrostrings with corresponding
# values:
# 
# [whoami]
# [hostname]
# [date]
# [jobfile]
#
proc prepare_batchjob_header {jobname} {

    global xband_path 
    
    set fname [file join $xband_path locals batchjobheader.vst]
    set fp [open $fname]
    set header [read $fp]
    close $fp
    
    if 0 { 
	set header ""
	append header {#!/bin/csh}
	append header {#$ -l class.ge.1.and.mem.gt.100}
	append header {#$ -M [WHOAMI]@[HOSTNAME]}
	append header {#$ -m bae}
	append header {#$ -cwd}
	append header {#$ -N [JOBNAME]}
	append header {#}
	append header {# jobfile created by   xband   [DATE]}
	append header {#}
    }	

    # expand macro keywords (replace them with values)
    set whoami [exec whoami]
    regsub -all -nocase {\[whoami\]} $header $whoami header
    
    set hostname [exec hostname]
    regsub -all -nocase {\[hostname\]} $header $hostname header
    
    set date [standard_date]
    regsub -all -nocase {\[date\]} $header $date header
    
    regsub -all -nocase {\[jobname\]} $header $jobname header

    
    regsub {\n$} $header {} header   ; # remove last newline, will be added from puts
    return $header

}

#
# reads the line from file "fname" starting with "keyword" 
# returns whole line including keyword
# or silently "" if file or keyword is not available 
#
proc read_keyed_line {fname keyword} {
    
    if {[file exists $fname] && [file readable $fname]} {
	
	if {![catch {set fp [open $fname r]}]} {
	    
	    while {[gets $fp line] > -1} {
		if {[lindex $line 0] == $keyword} {
		    return $line
		}
	    }
	    close $fp
	    
	}
    } 
    
    return "";
    
}

#
# returns value in column column from fname in line with keyword   
# (keyword is column 0)
#
# remember there is a proc getvalue
#
proc read_keyed_value {fname keyword {column 1}} {
    
    set line [read_keyed_line $fname $keyword]
    return [lindex $line $column]
    
}



#
# tcl only replacement for tclx command
#
# check if list is empty or not
#
proc lempty {l} {
    if {[llength $l] == 0} {
	return 1
    } else { 
	return 0
    }
}

#
# tcl only replacement for tclx command
#
# assign list entries one by one to provided varlist
#
proc lassign {values args} {
    set vlen [llength $values]
    set alen [llength $args]
    # Make lists equal length
    for {set i $vlen} {$i < $alen} {incr i} { 
	lappend values {}
    }
    uplevel 1 [list foreach $args $values break]
    return [lrange $values $alen end]
}

#
# tcl only replacement for tclx command
#
# Determine if the element is a list element of list. If the
# element is contained in the list, 1 is returned, otherwise,
# 0 is returned.
#
proc lcontain {list elem} {

    if {[lsearch -exact $list $elem] == -1} {
	return 0
    } else {
	return 1
    }
    
}


#
# return the name of the calling proc 
#
proc myname {} {
    return [lindex [info level -1] 0]
}

