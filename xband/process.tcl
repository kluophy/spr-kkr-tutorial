# Checks the state of the process with PID $pid
# - process is still active              : return 0
# - process terminated since last check  : return 1
# - process terminated before last check : return 2
#
# Here the unix SYSV-command  ps  has been used.
# If modification is necessary, please inform the authors.
#
# Copyright (C) 1994  G. Lamprecht, W. Lotz, R. Weibezahn; LRW c/o Uni Bremen
# Copyright (C) 1996  G. Lamprecht, W. Lotz, R. Weibezahn; IWD, Bremen University

proc process {pid} {


  set res [catch "wait -nohang $pid" message]
  if { ($res == 0) && ($message == "") } {return 0}
  if { ($res == 0) } {return 1} else {return 2}

}
