########################################################################################
#                              give a message on the screen 
########################################################################################
set w .
set num  "[lindex $argv 0]"    
if {$num=="1" } {                                                     
   set text "HELLO  \n\nyour interactive run has been done"
} else {
   return
    
   set PRC " "
  
   debug $PRC "from message.tcl:  no message for num=$num" 
}


wm title     . "MESSAGE"
wm iconname  . "MESSAGE"

wm geometry  . +10+50 

button .close -text $text  -command "destroy ."  -height 20 -width 40 -bg tomato1 \
        -font "-adobe-courier-bold-r-normal--34-240-100-100-m-200-iso8859-1"   
pack   .close -side top -expand y             

