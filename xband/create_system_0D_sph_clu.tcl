#
# setting up the structure  for   0D systems
#
#
#   NQ       total number of lattice sites in unit cell (independent of occupation)
#   NCL      number of inequivalent lattice sites
#   NQCL     number of equivalent sites IQ for class ICL with ICL = 1,..,NCL  
#   NT       number of atom types 
#   NAT      number of equivalent sites IQ occupied by atom type IT with  IT = 1,..,NT
#   CONC     concentration of atom type IT (on a site)
#
#                            NQCL(ICL)  == NAT(IT)
#                            CONC(IT)   == 1
#
#
#
#
#
# Copyright (C) 2004 H. Ebert
#
########################################################################################



proc create_system_0D_sph_clu {} {

    global sysfile syssuffix COLOR FONT HEIGHT Wclua
    global system cluster_type 
    global W_basvec IQCNTR NQCLU clu_box_dist clu_box_show
    
#
#------------------------------------------------------------------------------------ ND
#
    global structure_window_calls
    global sysfile syssuffix 
    global TABCHSYM iprint irel Wcsys
    global system NQ NT ZT RQX RQY RQZ NOQ NLQ CONC TXTT
    global ITOQ IQAT RWS
    global RBASX RBASY RBASZ
    global NM IMT IMQ RWSM NAT
    global NQCL ICLQ
#
#------------------------------------------------------------------------------------ 2D
#
    global RBASX_L RBASY_L RBASZ_L RBASX_R RBASY_R RBASZ_R
    global NQ_bulk_L NQ_bulk_R 
    
    set clu_box_show 1
    set clu_box_dist 0.5
    run_geometry cr_sph_cluster 
    
    set Wclu_sph .w_clu_sph 
    if {[winfo exists $Wclu_sph]} {destroy $Wclu_sph } 
    
    toplevel_init $Wclu_sph "create $cluster_type spherical cluster" 0 0
    
    wm sizefrom $Wclu_sph ""
    wm minsize  $Wclu_sph 100 100
    
    
#=======================================================================================

    frame $Wclu_sph.head
    pack $Wclu_sph.head -fill both -expand yes
    
    button $Wclu_sph.head.reset -text "RESET" \
	    -width 25 -height $HEIGHT(BUT1) -bg tomato  \
	    -command "create_system_0D ; destroy $Wclu_sph  " 
    button $Wclu_sph.head.cancel -text "CLOSE" \
	    -width 25 -height $HEIGHT(BUT1) -bg $COLOR(CLOSE)  \
	    -command "read_sysfile ; destroy $Wclu_sph" 
    
    pack $Wclu_sph.head.reset $Wclu_sph.head.cancel \
	    -side left -expand yes -pady 5
    
    #=======================================================================================

    set BG1 linen                
    set BG2 SteelBlue1 
    set BG3 ivory1
    
    global BGFIL ; set BGFIL  LightPink1
    global BGTSK ; set BGTSK  LightSalmon1
    global BGCPA ; set BGCPA  wheat
    global BGTAU ; set BGTAU  orange1 
    global BGWR  ; set BGWR   lavender
    global BGCLU ; set BGCLU  LightSkyBlue 
    global BGE   ; set BGE    moccasin 
    global BGP   ; set BGP    ivory1
    global BGX   ; set BGX    cornsilk1  
    global BGSCF ; set BGSCF  LemonChiffon1
    global BGCRL ; set BGCRL  orange1 

    set BGSCL orange     
    set BG_A khaki1
    set BG_a gold1
    set BG_x yellow1
    set BG_b goldenrod1
    set we_old 6
    set we_new 8
    set we_atoms 30
    
    #=======================================================================================


    label $Wclu_sph.info1 -text "spherical cluster created around site IQCNTR = $IQCNTR"  
    label $Wclu_sph.info2 -text "number of sites  $NQCLU"  
    
    pack $Wclu_sph.info1 $Wclu_sph.info2 -in $Wclu_sph -side top  -expand yes -pady 5
    
    #=======================================================================================
    
    frame $Wclu_sph.show
    pack $Wclu_sph.show  -in $Wclu_sph -side top -expand yes
    
    button $Wclu_sph.show.rasmol -text "show cluster" -bg green1  \
	    -command "exec_rasmol" 
    
    proc exec_rasmol {} {

	global RASMOL
	
	# settings for  clu_box_show  or clu_box_dist  might have been changed - rerun 
	run_geometry cr_sph_cluster 
	
	eval set res [catch "exec $RASMOL -script cluster_data.ras &" message]
	writescr .d.tt "$message \n"
    }	    
    
    checkbutton $Wclu_sph.show.box -text "show surrounding box with distance" \
	    -variable clu_box_show
    $Wclu_sph.show.box select
    
    # clu_box_show_dist
    
    scale $Wclu_sph.show.dist -from 0 -orient horizontal -resolution 0.1 \
	    -sliderlength 20 -to 2.0 -highlightthickness 0 -variable clu_box_dist  
    $Wclu_sph.show.dist set 0.5
    
    pack $Wclu_sph.show.rasmol $Wclu_sph.show.box  $Wclu_sph.show.dist -in $Wclu_sph.show   \
	    -side left -anchor w  -fill x -padx 5
    
}

