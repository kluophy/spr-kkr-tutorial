      subroutine defmtr(W,nrad1,av,bv,cv,is,BAS,
     .     RO,r0,dpas,alat,natom,nsort,Z,smt,imt,nradmax,wsrest)
      implicit double precision (a-h,p-z),integer (o)
c      include 'AA2.FI'
      integer w(1)
c input  :
      dimension av(3),bv(3),cv(3),RO(NRADMAX,*),Z(*),r0(*)
      dimension is(*),bas(3,*),wsrest(*)
c output :
      dimension  smt(*)
c locals :
      integer ndel(3),ltmax(3),imt
      double precision origin(3)
      dimension bound(3,3),plat(3,3)
      save
c$$$
c$$$      write(28,*)'av=',av
c$$$      write(28,*)'bv=',bv
c$$$      write(28,*)'cv=',cv
c$$$      write(28,*)'is',(is(i),i=1,natom)
c$$$      write(28,*)'BAS'
c$$$      do i=1,natom
c$$$        write(28,*)(bas(j,i),j=1,3)
c$$$      enddo
c$$$      do ist=1,nsort
c$$$        write(28,*)'R0S',R0(ist),' ZZ',z(ist)!,wsrest(ist)
c$$$      enddo
c$$$      write(28,*)'dpas,alat,natom,nsort:',dpas,alat,natom,nsort,imt
c$$$     $     ,nradmax
c$$$      do i=1,nsort
c$$$        write(28,*)' =============', i
c$$$        write(28,'(8F10.3)')(RO(ir,i),ir=1,nradmax)
c$$$      enddo
 

      pi=dpi()
      nrmax=nrad1+1
      call defdr(oa,nsort)
      call defdr(opot,nsort*nrmax)
      call defi(onr,nsort)
      call defdr(or,nrmax)
      call defdr(odv,nsort*nrmax)
      call hpot(W(oa),r0,nsort,W(onr),nrad1,W(opot),Z,RO,dpas,
     .     W(or),W(odv),nradmax)
      call rlse(or)

      volcel=
     >     Av(1)*(Bv(2)*Cv(3)-Bv(3)*Cv(2))+
     >     Av(2)*(Bv(3)*Cv(1)-Bv(1)*Cv(3))+
     >     Av(3)*(Bv(1)*Cv(2)-Bv(2)*Cv(1))
      volcel=abs(volcel)
C    average radii:
      Avw=(volcel/NATOM/(4.d0*PI)*3.d0)**(1.d0/3.d0)*alat
      plat(1,1)=av(1)
      plat(1,2)=bv(1)
      plat(1,3)=cv(1)
      plat(2,1)=av(2)
      plat(2,2)=bv(2)
      plat(2,3)=cv(2)
      plat(3,1)=av(3)
      plat(3,2)=bv(3)
      plat(3,3)=cv(3)

      DO I=1,3
        DO J=1,3
          BOUND(I,J)=PLAT(I,J)
        ENDDO
        LTMAX(I)=2
        NDEL(I)=0
        ORIGIN(I)=0
      ENDDO
      call defdr(obas,natom*3)
c      call defdr(owsrest,natom)
      call hrtree(w,alat,avw,bas,bound,
     .     is,ltmax,natom,nsort,ndel,
     .     origin,plat,smt,z,wsrest,
     .     W(oA),R0,W(onR),W(oPOT),NRMAX,imt)
      
      end
c
      subroutine hpot(a,b,nclass,nr,nrad1,pot,z,RO,dpas,R,dv,nradmax)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C- Calculate H. potential
C ----------------------------------------------------------------------
Ci Inputs:
Ci   clabl :name of the different inequivalent atom
Ci   nclass:number of classes, atoms in same class are symmetry-related
Ci   nrmax :maximum number of mesh points
Ci   nsp   :=1 spin degenerate, =2 non-degenerate
Ci   z     :nuclear charge
Co Outputs:
Co   a     :the mesh points are given by rofi(i) = b [e^(a(i-1)) -1]
Co   b     :                 -//-
Co   ierr  :0 if no error occurs, ic if read error for class ic
Co   nr    :number of mesh points
Co   pot   :spherical Hartree potential
Cw   v     :spherical potential (electronic contribution)
Cr Remarks:
C ----------------------------------------------------------------------
C Passed parameters:
      integer nclass,nr(*),nrmax
      double precision a(*),b(*),pot(nrad1+1,*),z(*),ro(nradmax,*)
C Local parameters:
      parameter(huge=1.d6)
      dimension r(nrad1),dv(nrad1)     ! - radial mesh
      save
c
      nrmax=nrad1+1
      do icl=1,nclass
        do  i=1,nrad1
          r(i)=b(icl)*exp(dble(i-1)*dpas)
        enddo
        np=nrad1
        call pots(pot(2,icl),ro(1,icl),dv,r,dpas,z(icl),np)
        zz=2*z(icl)
        a(icl)=dpas
        nr(icl)=nrmax
        do i=2,nrmax
          pot(i,icl)=pot(i,icl)-zz/r(i-1)
        enddo
        pot(1,icl)=-huge
      enddo
      END

      subroutine pots(dv,d,dp,dr,dpas,z,np)
C
C iHTEgPiPOBAHiE pOTEHciAlA pO 4 TO~KAM
C DV - POTENTIAL   D CH.DENCITY  DP WORK ARRAY  DR RADIAL MESH
C DPAS EXP. PASS
C Z ATOM NUMBER     NP - NUMBER OF POINTS
C **********************************************************************
      implicit double precision(a-h,o-z)
c$$$      double precision z
      dimension dv(*),d(*),dp(*),dr(*)
      save

      das=dpas/24.d0
      do i=1,np
        dv(i)=d(i)*dr(i)
      enddo
      dlo=exp(dpas)
      dlo2=dlo*dlo
      dp(2)=dr(1)*(d(2)-d(1)*dlo2)/(12.d0*(dlo-1.d0))
      dp(1)=dv(1)/3.d0-dp(2)/dlo2
      dp(2)=dv(2)/3.d0-dp(2)*dlo2
      j=np-1
      do i=3,j
        dp(i)=dp(i-1)+das*(13.d0*(dv(i)+dv(i-1))-(dv(i-2)+dv(i+1)))
      enddo
      dp(np)=dp(j)
      dv(j)=dp(j)
      dv(np)=dp(j)
      do i=3,j
        k=np+1-i
        dv(k)=dv(k+1)/dlo+das*(13.d0*(dp(k+1)/dlo+dp(k))-(dp(k+2)/dlo2
     $       +dp(k-1)*dlo))
      enddo
      dv(1)=dv(3)/dlo2+dpas*(dp(1)+4.d0*dp(2)/dlo+dp(3)/dlo2)/3.d0
      do i=1,np
        dv(i)=dv(i)/dr(i)*2
      enddo
      end

      double precision function fmesh(a,b,f,nr,r)
C- Computes the value of f at r for given values on a mesh
C ----------------------------------------------------------------------
Ci Inputs:
Ci   a     :the mesh points are given by rofi(i) = b [e^(a(i-1)) -1]
Ci   b     :                 -//-
Ci   f     :function defined on a mesh
Ci   nr    :number of mesh points
Ci   r     :radial distance
Co Outputs:
Co   fmesh :interpolated value of f at r
Cr Remarks:
Cr   if r is insite the mesh a quadratic fit is used
Cr   if r is outside the mesh an exponential fit is used
C ----------------------------------------------------------------------
      implicit none
C Passed parameters:
      integer nr
      double precision a,b,f(*),r
C Local parameters:
      integer is,nstart,np
      double precision delsqf,di3int,xx
      parameter(np=4)
C External calls:
      external  di3int,delsqf
C Intrinsic functions
      intrinsic log,idnint,max,min
      save

*      xx = 1.d0 + log(r/b+1.d0)/a
      if(r.ge.b)then
       xx=log(r/b)/a+2
       is = idnint(xx) - 1
      else
       fmesh=f(2)
       return
      endif
      if (is.le.nr-2) then
        is = max0(1,is)
        fmesh=di3int(is,f(is),xx)
      else
        nstart=nr-np+1
        fmesh=delsqf(np,nstart,f(nstart),xx)
      endif
      end
c
      integer function iprint()
C- Gets last integer of print priority stack
C ----------------------------------------------------------------------
Co Outputs:
Co   iprint:defines the verbosity level
Cr Remarks:
Cr   verbosity:   0  nearly nothing is printed
Cr               10  very terse
Cr               20  terse
Cr               30  normal
Cr               40  verbose
Cr               50  very verbose
Cr               60  highest verbosity
Cr              100  low-level debugging
Cr              110  intermediate-level debugging
Cr              120  high-level debugging
C ----------------------------------------------------------------------
      implicit none
C Local parameters:
c$$$      integer nvstck
c$$$      parameter (nvstck=5)
c$$$      integer vstack(0:nvstck-1),vstp,ip
c$$$      common /iprnt/ ip
c$$$
c$$$*      iprint = vstack(vstp)
      IPRINT=30
      return
      end
c
      subroutine potxn(a,alat,b,bas,iax,iclass,npr,nr,nrmax,plat,
     .                 pot,potl,xn)
C-Calculates Hartree potential at point xn
C ----------------------------------------------------------------------
Ci Inputs:
Ci   a     :the mesh points are given by rofi(i) = b [e^(a(i-1)) -1]
Ci   alat  :length scale
Ci   b     :                 -//-
Ci   bas   :basis vectors (scaled by alat)
Ci   iax   :information about positions around a specified pair of atom
Ci   iclass:the jth atom belongs to class iclass(j)
Ci   npr   :number of neighbors
Ci   nr    :number of mesh points
Ci   nrmax :maximum number of mesh points
Ci   plat  :primitive lattice vectors (scaled by alat)
Ci   plat  :primitive translation vectors in real space
Ci   pot   :spherical Hartree potential
Ci   xn    :cartesian coordinates where potential is calculated
Co Outputs:
Ci   potl  :ovelapping Hartree potential
Cr Remarks:
C ----------------------------------------------------------------------
      implicit none
C Passed parameters:
      integer iax(5,*),iclass(*),npr,nr(*),nrmax
      double precision a(*),alat,b(*),bas(3,*),plat(3,3),
     .                 pot(nrmax,*),potl,xn(3)
C Local parameters:
      integer jbas,jc,jpr,k
      double precision d(3),dnrm23,r,fmesh
C External calls:
      external dnrm23,fmesh
C Intrinsic functions
      intrinsic sqrt
      save

      potl=0.d0
      do jpr=1,npr
        jbas=iax(2,jpr)
        jc  =iclass(jbas)
        do k=1,3
          d(k)=bas(k,jbas)-xn(k)+plat(k,1)*iax(3,jpr)
     .     +plat(k,2)*iax(4,jpr)+plat(k,3)*iax(5,jpr)
        enddo
        r = sqrt(dnrm23(d))*alat
        potl=potl+fmesh(a(jc),b(jc),pot(1,jc),nr(jc),r)
      enddo

      end
c
      integer function lunit(i)
C- Returns OUTPUT for i=1, ERR for i=2
C ----------------------------------------------------------------------
Ci Inputs:
Ci   i    :1 or 2
Co Outputs:
Co   lunit:output file for i=1, error message unit for i=2
C ----------------------------------------------------------------------
      implicit none
C Passed parameters:
      integer i
C Common block
c$$$      common /unit/ iun
      LUNIT=6
      return
      end
c
      double precision function di3int(ix,ya,x)
C- Interpolates y = f(x) for given xa and ya=f(xa)
C ----------------------------------------------------------------------
Ci Inputs:
Ci   ix    :xa(1) is integer and equals ix
Ci          and xa(1),xa(2) and xa(3) differ exactly by 1.d0
Ci   ya    :value of f at xa
Ci   x     :x-value at which f is interpolated
Co Outputs:
Co   di3int:interpolated value
Cr Remarks:
C ----------------------------------------------------------------------
      implicit none
C Passed parameters:
      integer ix
      double precision x,ya(3)
C Local parameters:
      double precision xa(3)

      xa(1)=dble(ix)
      xa(2)=dble(ix+1)
      xa(3)=dble(ix+2)

      di3int=0.5d0*(x-xa(2))*((x-xa(3))*ya(1)+(x-xa(1))*ya(3))
     .            -(x-xa(1))* (x-xa(3))*ya(2)
      end
c
      subroutine i_shell(m,n,iarray)
C- shell sort of a array of integer vectors
C ----------------------------------------------------------------------
Ci Inputs:
Ci   m     :number of components in iarray
Ci   n     :number of elements in iarray
Ci   iarray:array to be sorted
Co Outputs:
Co   iarray:array to be sorted
C ----------------------------------------------------------------------
      implicit none
C Passed parameters:
      integer m,n,iarray(m,0:n-1)
C Local parameters:
      integer lognb2,i,j,k,l,n2,nn,it,mm,mmm
C Intrinsic functions
      intrinsic float,int,log

      lognb2 = int(log(float(n+1))*1.4426950)
      n2 = n
      do nn = 1, lognb2
        n2 = n2/2
        k = n - n2
        do  11  j = 1, k
          i = j - 1
    3     continue
          l = i + n2
          do  15  mm = 1, m
            if (iarray(mm,l) - iarray(mm,i)) 16,15,11
   16       continue
            do mmm = 1, m
              it = iarray(mmm,i)
              iarray(mmm,i) = iarray(mmm,l)
              iarray(mmm,l) = it
            enddo
            i = i - n2
            if (i .ge. 0) goto 3
            goto 11
   15     continue
   11   continue
      enddo

      end

      subroutine hrtree(w,alat,avw,bas,bound,iclass,
     .     ltmax,nbas,nclass,ndel,origin,plat,
     .     wsr,z,wsrest,A,B,NR,POT,NRMAX,imt)
C- Calculate Hartree potential on a mesh and determine muffin-tin radia
C ----------------------------------------------------------------------
Ci Inputs:
Ci   alat  :length scale
Ci   avw   :average Wigner Seitz radius
Ci   bas   :basis vectors (scaled by alat)
Ci   bound :two vectors spanning the plane (scaled by alat)
Ci   clabl :name of the different inequivalent atom
Ci   iclass:the jth atom belongs to class iclass(j)
Ci   ltmax :ltmax(i)= limit in i-dirction for unit cells considered
Ci   nbas  :number of atoms in the basis
Ci   nclass:number of classes, atoms in same class are symmetry-related
Ci   ndel  :ndel(i)=number of mesh points along the bound(i) vector
Ci   nrclas:number of atoms in the i-th class
Ci   origin:origin of the plane (scaled by alat)
Ci   plat  :primitive lattice vectors (scaled by alat)
Ci   z     :nuclear charge
Co Outputs:
Cio  wsr   :Wigner-Seitz sphere radius (in atomic units)
Cr Remarks:
C ----------------------------------------------------------------------
      implicit none
C Passed parameters:
      integer iclass(*),ltmax(3),nbas,nclass,ndel(*)
      double precision alat,avw,bas(3,*),bound(3,3),
     $     origin(*),plat(3,3),wsr(*),z(*)
C Local parameters:
      integer i,ic,idamax,iprint,j,lunit,
     .        neighm,nrmax,oiax1,onpr1,
     .        odscl,olock,NR(*),imt
      double precision facn1,wmax,wsrest(*) ,A(*),B(*),POT(nrmax,*)
      parameter(facn1=1.5d0)
C Heap allocation
      integer w(1)
C External calls:
      external defdr,defi,errmsg,defwsr,iprint,lunit,nghbr1,
     .         potmax,potsum,rlse
C Intrinsic functions
      intrinsic abs,idnint,max0
      save

      if(iprint().gt.90)then
      print*,'******************************************'
111   format(20f10.4)
112   format(20i10)
      print*,'alat',alat,'  avw',avw
      print*,' === bas. v. ==='
      do j=1,nbas
      print111,(bas(i,j),i=1,3)
      enddo
      print*,' === bound ==='
      do j=1,3
      print111,(bound(i,j),i=1,3)
      enddo
      print*,' === ltmax ==='
      print112,ltmax
      print*,' === ndel ==='
      print112,(ndel(i),i=1,3)
      print*,' === origin ==='
      print111,(origin(i),i=1,3)
      print*,' === plat ==='
      do j=1,3
      print111,(plat(i,j),i=1,3)
      enddo
      print*,'******************************************'
      endif
C --- Calculate Hartree potential on a mesh
      if (ndel(1)*ndel(2).ne.0)
     .  call potsum(a,alat,b,bas,bound,iclass,ltmax,nbas,ndel,
     .              nr,nrmax,origin,plat,pot)
C --- Determine maximum of Hartree potential
      do ic=1,nclass
        call defwsr(wsrest(ic),z(ic))
        wsr(ic)=wsrest(ic)
      enddo
      wmax=wsrest(idamax(nclass,wsrest,1))/avw
      I=2*int((2.d0*facn1*wmax+1.d0)**3)
      neighm = max(2*int((2.d0*facn1*wmax+1.d0)**3),50)
      call defi(oiax1,5*neighm*nclass)
      call defi(onpr1  ,nclass)
      call nghbr1(w,alat,bas,facn1,w(oiax1),iclass,nbas,nclass,
     .     neighm,w(onpr1),plat,wsrest)
      
      call potmax(w,a,alat,b,bas,w(oiax1),iclass,nbas,
     .     nclass,w(onpr1),nr,nrmax,plat,pot,wmax,wsr,
     .     wsrest,z)
      call defdr(odscl,nclass*(nclass+2))
      call defdr(olock,nclass)
      call blowup(alat,bas,1.d0,0.d0,w(oiax1),iclass,nbas,
     $           nclass,w(onpr1),0.d0,0.d0,plat,wsr,W(odscl),W(olock))
      if(imt.ne.0)call blowup(alat,bas,1.d0,0.d0,w(oiax1),iclass,nbas,
     $         nclass,w(onpr1),0.4d0,0.8d0,plat,wsr,W(odscl),W(olock))

      call rlse(oiax1)
400   format(' HRTREE: set BEGATOM=T for a new atomic calculation.$')

      end
c
      subroutine potsum(a,alat,b,bas,bound,iclass,ltmax,nbas,ndel,nr,
     .                  nrmax,origin,plat,pot)
C- Calculates the overlapping Hartree potential
C ----------------------------------------------------------------------
Ci Inputs:
Ci   a     :the mesh points are given by rofi(i) = b [e^(a(i-1)) -1]
Ci   b     :                 -//-
Ci   alat  :length scale
Ci   bas   :basis vectors (scaled by alat)
Ci   bound :two vectors spanning the plane (scaled by alat)
Ci   iclass:the jth atom belongs to class iclass(j)
Ci   ltmax :ltmax(i)= limit in i-dirction for unit cells considered
Ci   nbas  :number of atoms in the basis
Ci   ndel  :ndel(i)=number of mesh points along the bound(i) vector
Ci   nr    :number of mesh points
Ci   nrmax :maximum number of mesh points
Ci   origin:origin of the plane (scaled by alat)
Ci   plat  :primitive lattice vectors (scaled by alat)
Ci   pot   :spherical Hartree potential
Co Output written to file POT
Cr Remarks:
C ----------------------------------------------------------------------
      implicit none
C Passed parameters:
      integer nbas,nr(*),nrmax,iclass(*),ndel(*),ltmax(3)
      double precision alat,a(*),b(*),bas(3,*),bound(3,3),
     .                 origin(3),plat(3,3),pot(nrmax,*)
C Local parameters:
      integer i,ibas,ic,i1,i2,j,j1,j2,j3,lunit
      double precision d01,d02,d03,d11,d12,d13,d21,d22,d23,d31,
     .                d32,d33,dn1,dn2,fac1,fac2,potl,
     .                 fmesh,rad,xn1,xn2,xn3
C External calls:
*      external fclose,fopn,fmesh,lunit
C Intrinsic functions
      intrinsic log,sqrt,max,min
      save

      if (ndel(1).ne.1) fac1=1.d0/dble(ndel(1)-1)
      if (ndel(2).ne.1) fac2=1.d0/dble(ndel(2)-1)
      write(lunit(1),33)origin,((bound(i,j),i=1,3),j=1,2)
      write(lunit(1),34)

      do i1=0,ndel(1)-1
        dn1 = i1*fac1
        do i2=0,ndel(2)-1
          dn2 = i2*fac2
          xn1= origin(1)+bound(1,1)*dn1+bound(1,2)*dn2
          xn2= origin(2)+bound(2,1)*dn1+bound(2,2)*dn2
          xn3= origin(3)+bound(3,1)*dn1+bound(3,2)*dn2
          potl=0.d0
          do ibas=1,nbas
            ic=iclass(ibas)
C --------  ltmax(i) lattice translations in i-direction considered
            d01=bas(1,ibas)-xn1
            d02=bas(2,ibas)-xn2
            d03=bas(3,ibas)-xn3
            do j1=-ltmax(1),ltmax(1)
              d11=d01+plat(1,1)*j1
              d12=d02+plat(2,1)*j1
              d13=d03+plat(3,1)*j1
              do j2=-ltmax(2),ltmax(2)
                d21=d11+plat(1,2)*j2
                d22=d12+plat(2,2)*j2
                d23=d13+plat(3,2)*j2
                do j3=-ltmax(3),ltmax(3)
                  d31=d21+plat(1,3)*j3
                  d32=d22+plat(2,3)*j3
                  d33=d23+plat(3,3)*j3
                  rad = sqrt(d31*d31+d32*d32+d33*d33)*alat
                  potl=potl+fmesh(a(ic),b(ic),pot(1,ic),nr(ic),rad)
                enddo
              enddo
            enddo
          enddo
        enddo
      enddo

  33  format(//25('-')//,'make plot for plane',//,'ORIGIN:',3f10.5/
     .       'R1    :',3f10.5,/'R2    :',3f10.5//,25('-'))
  34  format('Begin to make POT ...')
 200  format (1x,2f10.6,e17.8)
      end
c
      subroutine nghbr1(w,alat,bas,facn1,iax,iclass,nbas,nclass,
     .                  neighm,npr,plat,wsr)
C- Create a table of all neighbors within facn1*(wsr(1)+wsr(2))
C ----------------------------------------------------------------------
Ci Inputs:
Ci   alat  :length scale
Ci   bas   :basis vectors (scaled by alat)
Ci   clabl :name of the different inequivalent atom
Ci   facn1 :see remarks
Ci   iclass:the jth atom belongs to class iclass(j)
Ci   nbas  :number of atoms in the basis
Ci   nclass:number of classes, atoms in same class are symmetry-related
Ci   neighm:maximum number of neighbors
Ci   plat  :primitive lattice vectors (scaled by alat)
Ci   wsr   :Wigner-Seitz sphere radius (in atomic units)
Co Outputs:
Ci   iax   :see remarks
Ci   npr   :number of neighbors around each atom
Cr Remarks:
Cr   Creates a neighour list for a specified atom, generating iax
Cr   which contains all neigbors which fulfill:
Cr
Cr        distance(i,j) <= (wsr(i)+wsr(j))*facn1
Cr
Cr    iax(1): not used
Cr    iax(2): ibas = atom in cluster
Cr    iax(3): i
Cr    iax(4): j
Cr    iax(5): k
Cr   To be sure that at least one pair is found, fac is increased
Cr   by 1.2 until a neighbor is found.
C ----------------------------------------------------------------------
      implicit none
C Passed parameters:
      integer nbas,nclass,iax(5,nclass,*),iclass(*),npr(*)
      double precision alat,facn1,plat(3,3),bas(3,*),wsr(*)
C Local parameters:
      integer i,i1,i2,i3,iclbas,ipr,iprint,ibas,ipr5,j,jbas,
     .        jc,k,lunit,neighm,oiwk
      double precision d2,drr2,dr(3),fac,wi,wj,wjpwi,wjpwi2
      character *72 messg
C Heap
      integer w(1)
      save

      call defi(oiwk,neighm)
      do jc = 1,nclass
        jbas=iclbas(jc,iclass,nbas,1)
        wj=wsr(jc)
        ipr=0
        fac=facn1
        do while (ipr.lt.2)
          ipr=0
          do  ibas = 1, nbas
            wi=wsr(iclass(ibas))
            wjpwi=(wj+wi)/alat*fac
            call latlim(plat,wjpwi,i1,i2,i3)
            wjpwi2=wjpwi*wjpwi
C --------- Sweep lattice translations to find all neighbors
C --------- within wjpwi
            do i = -i1, i1
              do j = -i2, i2
                do k = -i3, i3
                  d2 = drr2(plat,bas(1,jbas),bas(1,ibas),i,j,k,dr)
                  if (d2 .le. wjpwi2) then
                    if (ipr .ge. neighm) then
                      write (messg,400)neighm,i1,i2,i3
                      call errmsg(messg,4)
                    endif
                    ipr5=ipr*5
                    w(oiwk+ipr5+0) = 10000*d2
                    w(oiwk+ipr5+1) = ibas
                    w(oiwk+ipr5+2) = i
                    w(oiwk+ipr5+3) = j
                    w(oiwk+ipr5+4) = k
                    ipr=ipr+1
                  endif
                enddo
              enddo
            enddo
          enddo
          fac=fac*1.2d0
        enddo

        call i_shell(5,ipr,w(oiwk))
        do i=1,ipr
          call incopy(5,w(oiwk+5*(i-1)),1,iax(1,jc,i),1)
          iax(1,jc,i)=jbas
        enddo
        npr(jc) = ipr


C ----- Printout
        if (iprint() .ge. 80) then
*          write(lunit(1),300) clabl(jc),jbas,(bas(m,jbas),m=1,3)
          do ipr = 1, npr(jc)
            ibas=iax(2,jc,ipr)
            i  =iax(3,jc,ipr)
            j  =iax(4,jc,ipr)
            k  =iax(5,jc,ipr)
            d2 = drr2(plat,bas(1,jbas),bas(1,ibas),i,j,k,dr)
*            write(lunit(1),301)ipr,d2,ibas,clabl(ic),i,j,k,dr
          enddo
          write(lunit(1),*)
        endif

      enddo
      if(iprint().ge.70) write(lunit(1),302)neighm,(npr(jc),jc=1,nclass)

300   format(/' NGHBR1: neighbors around ',a4,'; IBAS=',i3,
     .       ', POS=',3f8.4,
     .       //,' IPR    D**2    ATOM  --x PLAT---',8x,'----TAU----')
301   format(i3,f9.4,i4,1x,a4,1x,3i3,3f9.4)
302   format(/' NGHBR1: neighm=',i4,'> npr=',50(11i4,/26x))
400   format(' NGHBR1: too many pairs, neighm=',
     .       i3,6x,'i1,i2,i3 =',3i3,'$')
      end
c
      subroutine potmax(w,a,alat,b,bas,iax1,iclass,nbas,nclass,
     . 			npr1,nr,nrmax,plat,pot,wmax,wsr,wsrest,z)
C- Calculates the overlapping Hartree potential and finds it's maximum
C ----------------------------------------------------------------------
Ci Inputs:
Ci   a     :the mesh points are given by rofi(i) = b [e^(a(i-1)) -1]
Ci   alat  :length scale
Ci   b     :the mesh points are given by rofi(i) = b [e^(a(i-1)) -1]
Ci   bas   :basis vectors (scaled by alat)
Ci   clabl :name of the different inequivalent atom
Ci   iax1  :information about positions around a specified atom
Ci   iclass:the jth atom belongs to class iclass(j)
Ci   nbas  :number of atoms in the basis
Ci   nclass:number of classes, atoms in same class are symmetry-related
Ci   npr1  :number of neighbors
Ci   nr    :number of mesh points
Ci   nrmax :maximum number of mesh points
Ci   plat  :primitive lattice vectors (scaled by alat)
Ci   pot   :spherical Hartree potential
Ci   wmax  :largest wsr/avw
Ci   wsrest:estimation of Wigner-Seitz sphere radius (in atomic units)
Ci   z     :nuclear charge
Co Outputs:
Co   wsr   :Wigner-Seitz sphere radius (in atomic units)
Cr Remarks:
C ----------------------------------------------------------------------
      implicit none
C Passed parameters:
      integer nbas,nclass,iax1(5,nclass,*),npr1(*),
     .        nr(*),nrmax,iclass(*)
      double precision a(*),alat,b(*),bas(3,*),plat(3,3),
     .                 pot(nrmax,*),wmax,wsr(*),wsrest(*),z(*)

C Local parameters:
      integer i,ibas,ic,iclbas,imin,iprint,istep,
     .        k,k1,k2,k3,kbas,kc,kco,kpr,lunit,
     .        neighm,npr2,nstep,oiax2
      parameter (nstep=20)
      double precision facn2,dlamch,drr2,dr(3),potl,rik,riko,
     .                 rpmx,step,tiny,tol,x(:),xn(3),y(:)
      parameter(facn2=3.d0,tiny=1.d-5, tol=1.d-2)
C Heap allocation
      integer w(1)
C External calls:
      external  dlamch,defi,drr2,dscal,iclbas,iprint,lunit,nghbr2,potxn
C Intrinsic functions
      intrinsic abs,dble,max,sqrt
      allocatable x,y
      save
      allocate(x(0:nstep),y(0:nstep))
      neighm = max0(int(4.d0*(facn2*wmax+1.d0)**3),50)
      call defi(oiax2,5*neighm)

      do ic=1,nclass
      if (idnint(z(ic)).ne.0) then
        if (iprint().ge.40) write(lunit(1),300)
        ibas=iclbas(ic,iclass,nbas,1)

        wsr(ic)=dlamch('o')
        riko=-1.d0
        kco=-1
        do kpr=2,npr1(ic)
          kbas=iax1(2,ic,kpr)
          k1=  iax1(3,ic,kpr)
          k2=  iax1(4,ic,kpr)
          k3=  iax1(5,ic,kpr)
          kc= iclass(kbas)
          rik=sqrt(drr2(plat,bas(1,ibas),bas(1,kbas),k1,k2,k3,dr))*alat
          if (idnint(z(kc)).ne.0.and.
     .       (abs(rik-riko).gt.tiny.or.kco.ne.kc)) then

            riko=rik
            kco=kc
            step=rik/nstep
            call nghbr2(alat,bas,facn2,w(oiax2),ibas,iclass,k1,k2,
     .                  k3,kbas,nbas,neighm,npr2,plat,wsrest)
            call dscal(3,1.d0/dfloat(nstep),dr,1)
C --------  Find aproximate position of first maximun
            do istep=0,nstep
              do k=1,3
                xn(k)=bas(k,ibas)+istep*dr(k)
              enddo
              call potxn(a,alat,b,bas,w(oiax2),iclass,npr2,nr,nrmax,
     .                   plat,pot,potl,xn)
              x(istep)=dble(istep)*step
              y(istep)=max(potl,-1.d5)
              if (istep.ne.0) then
              if (istep.ge.2.and.y(istep).lt.y(istep-1)) goto 10
              end if
            enddo
            call errmsg('bug in potmax.$',4)

C --------- Now a single maximum is supposed between istep-2 and istep
10          do k=1,3
              x(k)=x(istep-3+k)
              y(k)=y(istep-3+k)
            enddo
            rpmx=0.d0
            imin=(istep-2)*2
            do while (x(3)-x(1).gt.tol)
              x(5)=x(3)
              x(3)=x(2)
              y(5)=y(3)
              y(3)=y(2)
              step=step*0.5d0
              call dscal(3,0.5d0,dr,1)
              do i=1,3,2
                do k=1,3
                  xn(k)=bas(k,ibas)+(imin+i)*dr(k)
                enddo
                call potxn(a,alat,b,bas,w(oiax2),iclass,npr2,nr,nrmax,
     .                     plat,pot,y(i+1),xn)
                x(i+1)=(imin+i)*step
              enddo
              do i=2,4
                if (y(i+1).lt.y(i)) then
                  do k=1,3
                    y(k)=y(i-2+k)
                    x(k)=x(i-2+k)
                  enddo
                  goto 20
                endif
              enddo
20            imin=2*(imin+i-2)
            enddo
            rpmx=0.5d0*(y(1)*(x(2)+x(3))-2.d0*y(2)*
     .           (x(1)+x(3))+y(3)*(x(1)+x(2)))/(y(1)-y(2)-y(2)+y(3))
            wsr(ic)=min(wsr(ic),rpmx)
*            if (iprint().ge.40)
*     .        write(lunit(1),301)labl(ic),clabl(kc),rik,y(2),rpmx
          endif
        enddo
        if (iprint().ge.40) write(lunit(1),302)
c        if (iprint().ge.20) write(lunit(1),303)clabl(ic),wsr(ic)
      endif
      enddo

300   format(/' POTMAX: ATOM1   ATOM2   DIST      VMAX   R(VMAX)',
     .       /,8x,42('-'))
301   format(11x,a4,3x,a4,1x,f7.3,2x,f7.3,2x,f7.3)
302   format(8x,42('-'))
303   format( ' POTMAX:  -> CLASS: ',a4,'; WSR=',f7.3)

      end
c
      subroutine nghbr2(alat,bas,facn2,iax,ibas,iclass,k1,k2,k3,
     .                  kbas,nbas,neighm,npr,plat,wsr)
C- Create a table of all neighbors around two atoms
C ----------------------------------------------------------------------
Ci Inputs:
Ci   alat  :length scale
Ci   bas   :basis vectors (scaled by alat)
Ci   clabl :name of the different inequivalent atom
Ci   facn2 :see remarks
Ci   ibas  :index to first atom
Ci   iclass:the jth atom belongs to class iclass(j)
Ci   k1-3  :second atom is translated by k1*plat1+k2*plat2+p3*plat3
Ci   kbas  :index to second atom
Ci   nbas  :number of atoms in the basis
Ci   neighm:maximum number of neighbors
Ci   plat  :primitive lattice vectors (scaled by alat)
Ci   wsr   :Wigner-Seitz sphere radius (in atomic units)
Co Outputs:
Ci   iax   :see remarks
Ci   npr   :number of neighbors
Cr Remarks:
Cr   Creates a neighour list for a pair of atoms ibas and kbas,
Cr   generating iax which contains all neigbors jbas within
Cr   facn2*wsr(jbas) around ibas or kbas , i.e. those which fulfill:
Cr
Cr        distance(i,j) <= wsr(j)*facn2
Cr    or  distance(k,j) <= wsr(j)*facn2
Cr
Cr    iax(1): not used
Cr    iax(2): ibas = atom in cluster
Cr    iax(3): i
Cr    iax(4): j
Cr    iax(5): k
C ----------------------------------------------------------------------
      implicit none
C Passed parameters:
      integer iax(5,*),ibas,iclass(*),k1,k2,k3,kbas,nbas,neighm,npr
      double precision alat,bas(3,*),facn2,plat(3,3),wsr(*)

C Local parameters:
      integer ic,i,i1,i2,i3,ii,iprint,ipr,j,jbas,jc,k,kc,lunit
      double precision dr11,dr12,dr13,dr21,dr22,dr23,dr31,dr32,dr33,
     .                 db11,db21,db31,db12,db22,db32,
     .                 drb11,drb21,drb31,drb12,drb22,drb32,
     .                 b(3,2),d1,d2,wjf,wjf2
      character *72 messg

C External calls:
      external  dcopy,iprint,latlim,lunit
      save

      ic=iclass(ibas)
      kc=iclass(kbas)
*      if (iprint() .ge. 80) write(lunit(1),300)clabl(ic),clabl(kc)
      call dcopy(3,bas(1,ibas),1,b(1,1),1)
      b(1,2)=bas(1,kbas)+k1*plat(1,1)+k2*plat(1,2)+k3*plat(1,3)
      b(2,2)=bas(2,kbas)+k1*plat(2,1)+k2*plat(2,2)+k3*plat(2,3)
      b(3,2)=bas(3,kbas)+k1*plat(3,1)+k2*plat(3,2)+k3*plat(3,3)
      ipr=0
      do  jbas = 1, nbas
        jc=iclass(jbas)
        wjf=wsr(jc)/alat*facn2
        call latlim(plat,wjf,i1,i2,i3)
        wjf2=wjf*wjf
        db11=bas(1,jbas)-b(1,1)
        db21=bas(2,jbas)-b(2,1)
        db31=bas(3,jbas)-b(3,1)
        db12=bas(1,jbas)-b(1,2)
        db22=bas(2,jbas)-b(2,2)
        db32=bas(3,jbas)-b(3,2)
        do  i =-i1, i1
          dr11=plat(1,1)*i
          dr21=plat(2,1)*i
          dr31=plat(3,1)*i
          do j =-i2, i2
            dr12=dr11+plat(1,2)*j
            dr22=dr21+plat(2,2)*j
            dr32=dr31+plat(3,2)*j
            do k =-i3, i3
              dr13=dr12+plat(1,3)*k
              dr23=dr22+plat(2,3)*k
              dr33=dr32+plat(3,3)*k
              drb11=dr13+db11
              drb21=dr23+db21
              drb31=dr33+db31
              drb12=dr13+db12
              drb22=dr23+db22
              drb32=dr33+db32
              d1=drb11*drb11+drb21*drb21+drb31*drb31
              d2=drb12*drb12+drb22*drb22+drb32*drb32
              if (d1.le.wjf2 .or. d2.le.wjf2) then
                ipr=ipr+1
                if (ipr .ge. 2*neighm) then
                  write (messg,400)2*neighm,i1,i2,i3
                  call errmsg(messg,4)
                endif
                iax(2,ipr) = jbas
                iax(3,ipr) = i
                iax(4,ipr) = j
                iax(5,ipr) = k
                ii = 0
                if (d1.le.wjf2) ii = ii + 1
                if (d2.le.wjf2) ii = ii + 2
C ------------- Printout
                if(iprint().ge.80)
     .            write(lunit(1),301)ipr,d1,d2,jbas,i,j,k,ii
              endif
            enddo
          enddo
        enddo
      enddo

      npr = ipr

  300 format(/' NGHBR2: neighbors around atoms ',a4,'and ',a4,':',
     .       /,' IPR',5x,'D1**2',6x,'D2**2',3x,'ATOM',2x,'--x PLAT---',
     .        2x,'FLAG',/,1x,49('-'))
  301 format(i3,2f10.4,4i5,2x,i3)
  400 format(' NGHBR2: too many pairs, neighm=',
     .       i3,6x,'i1,i2,i3 =',3i3,'$')
      end
c
      subroutine blowup(alat,bas,facvol,gamma,iax1,iclass,nbas,
     .                  nclass,npr1,ommax1,ommax2,plat,wsr,dscl,lock)
C- Blows up the spheres
C ----------------------------------------------------------------------
Ci Inputs:
Ci   alat  :length scale
Ci   bas   :basis vectors (scaled by alat)
Ci   clabl :name of the different inequivalent atom
Ci   iax1  :information about positions around a specified atom
Ci   iclass:the jth atom belongs to class iclass(j)
Ci   nbas  :number of atoms in the basis
Ci   nclass:number of classes, atoms in same class are symmetry-related
Ci   npr1  :number of neighbors
Ci   facvol:sum of sphere volumes = facvol * cell volume
Ci   gamma :scaling is r -> a(r+b) with a*b=gamma*(a-1)*avw
Ci   ommax1:maximum overlap divided by distance (s1+s2-d)/d  < ommax1
Ci   ommax2:maximum overlap divided by  radius  (s1+s2-d)/s1 < ommax2
Ci   plat  :primitive lattice vectors (scaled by alat)
Cio  wsr   :Wigner-Seitz sphere radius (in atomic units)
Co Outputs:
Cio  wsr   :Wigner-Seitz sphere radius (in atomic units)
Cr Remarks:
C ----------------------------------------------------------------------
      implicit none
C Passed parameters:
      integer nbas,nclass,iax1(5,nclass,*),npr1(*),iclass(*)
      double precision alat,bas(3,*),facvol,gamma,ommax1,ommax2,
     .                 plat(3,3),wsr(*)
C Local parameters:
      integer ibas,ic,iclbas,icmax,icmin,iloop,iprint,k1,k2,k3,
     .        kbas,kc,kco,kpr,line,lunit,nloop
      double precision a,amax,amax1,amax2,amax3,avw,b,bmax,d,dlamch,
     .                 ddet33,dm(0:3),dscl(nclass,0:nclass+1),
     .                dsclmx,drr2,dr(3),fpi3,gw,opo1,omo2,p,q,r,
     .                 rik,riko,s,t,tiny,tmo2,u,v,vol,vola,volb,
     .                 volsph,wsri,wsrk,x
      logical fin,lock(*)
      character*72 messg
      parameter(fpi3=4.18879020478639053d0,tiny=1.d-4)
C External calls:
      external  dlamch,dcopy,drr2,errmsg,iclbas,iprint,lunit
C Intrinsic functions
      intrinsic abs,min,sqrt
      save
c
c     print *,'blowup input'
c     print*,' gamma :',gamma
c     print1133,' npr1:',(npr1(i),i=1,nclass)
c     print1133,' iax1:',(((iax1(i,n,k),i=1,5),k=1,npr1(n)),n=1,nclass)
c133  format(a/(5i5))

      opo1=1.d0+ommax1
      omo2=1.d0-ommax2
      tmo2=2.d0-ommax2
      vol=abs(ddet33(plat))*alat**3
      avw = (vol/fpi3/nbas)**(1.d0/3.d0)
      gw=avw*gamma

      volsph = 0.d0
      do  ibas = 1, nbas
        volsph = volsph + fpi3 * wsr(iclass(ibas))**3
      enddo
      if (iprint().ge.30) write(lunit(1),308) vol,volsph

      call dcopy(nclass,1.d0,0,dscl(1,0),1)

      do nloop=1,nclass+1
        amax=dlamch('o')
C ----- lock radii of those spheres whith maximum allowed overlap
        call linit(lock,nclass)
        do ic=1,nclass
          ibas=iclbas(ic,iclass,nbas,1)
          wsri=wsr(ic)
          do kpr=2,npr1(ic)
            kbas=iax1(2,ic,kpr)
            k1=  iax1(3,ic,kpr)
            k2=  iax1(4,ic,kpr)
            k3=  iax1(5,ic,kpr)
            kc= iclass(kbas)
            wsrk=wsr(kc)
            rik=sqrt(drr2(plat,bas(1,ibas),bas(1,kbas),k1,k2,k3,dr))
            rik=rik*alat
            if (abs(opo1*rik-wsri-wsrk).lt.tiny) lock(ic)=.true.
            if (abs(rik-omo2*wsri-wsrk).lt.tiny) lock(ic)=.true.
            if (abs(rik-wsri-omo2*wsrk).lt.tiny) lock(ic)=.true.
          enddo
        enddo

        do ic=1,nclass
          if (.not.lock(ic)) then
            ibas=iclbas(ic,iclass,nbas,1)
            riko=-1.d0
            kco=-1
            wsri=wsr(ic)
            do kpr=2,npr1(ic)
              kbas=iax1(2,ic,kpr)
              k1=  iax1(3,ic,kpr)
              k2=  iax1(4,ic,kpr)
              k3=  iax1(5,ic,kpr)
              kc= iclass(kbas)
              rik=sqrt(drr2(plat,bas(1,ibas),bas(1,kbas),k1,k2,k3,dr))
              rik=rik*alat
              if (abs(rik-riko).gt.tiny.or.kco.ne.kc) then
                wsrk=wsr(kc)
                riko=rik
                kco=kc
                if (lock(kc)) then
                  amax1=(opo1*rik-wsrk+gw)/(wsri+gw)
                  if (omo2.gt.0.d0)
     .            amax2=(rik-wsrk+omo2*gw)/(wsri+gw)/omo2
                  amax3=(rik-omo2*wsrk+gw)/(wsri+gw)
                else
                  amax1=(opo1*rik+gw+gw)/(wsri+wsrk+gw+gw)
                  if (wsrk+omo2*wsri+tmo2*gw.gt.0.d0)
     .            amax2=(rik+tmo2*gw)/(wsrk+omo2*wsri+tmo2*gw)
                  if (wsri+omo2*wsrk+tmo2*gw.gt.0.d0)
     .            amax3=(rik+tmo2*gw)/(wsri+omo2*wsrk+tmo2*gw)
                endif
                amax=min(amax,amax1,amax2,amax3)
              endif
            enddo
          endif
        enddo
        bmax=(1.d0-1.d0/amax)*gw

        vola = 0.d0
        volb = 0.d0
        do  ibas = 1, nbas
          ic = iclass(ibas)
          if (.not.lock(ic)) then
            volb = volb + (amax*wsr(ic)+bmax)**3
          else
            vola = vola + wsr(ic)**3
          endif
        enddo
        vola = vola * fpi3
        volb = volb * fpi3

        if (vol*facvol .lt. vola+volb) then
          call dinit(dm,4)
          do  ibas = 1, nbas
            ic = iclass(ibas)
            if (.not.lock(ic)) then
              a=wsr(ic)+gw
C ----------- for numerical reasons distinguish cases
              if (abs(gamma).gt.1.d0) then
                b=wsr(ic)
              else
                b=-gw
              endif
              dm(0) = dm(0) +        b*b*b
              dm(1) = dm(1) + 3.d0 * a*b*b
              dm(2) = dm(2) + 3.d0 * a*a*b
              dm(3) = dm(3) +        a*a*a
            endif
          enddo
          if (abs(dm(3)).gt.tiny) then
            r =   dm(2) / dm(3)
            s =   dm(1) / dm(3)
            t = ( dm(0)-(vol*facvol-vola)/fpi3)/dm(3)
            p = s - r*r/3.d0
            q=2.d0*r*r*r/27.d0-r*s/3.d0+t
            d=p*p*p/27.d0+q*q/4.d0
            u=(sqrt(d)-q/2.d0)**(1.d0/3.d0)
            v=-p/u/3.d0
            x=u+v-r/3.d0
            if (abs(gamma).gt.1.d0) then
              amax=x+1.d0
              bmax=x*gw/amax
            else
              amax=x
              bmax=(1.d0-1.d0/amax)*gw
            endif
            if (iprint().ge.100) then
              write(lunit(1),307)'R S T',r,s,t
              write(lunit(1),307)'P Q  ',p,q
              write(lunit(1),307)'  D  ',d
              write(lunit(1),307)' U V ',u,v
              write(lunit(1),307)' AMAX',amax
              write(lunit(1),307)' BMAX',bmax
              write(lunit(1),307)' -------------------------'
            endif
          endif
        endif

        fin=.true.
        do ic=1,nclass
          dscl(ic,nloop)=1.d0
          if (.not.lock(ic)) then
            dsclmx=amax+bmax/wsr(ic)
            wsr(ic)       =dsclmx*wsr(ic)
            dscl(ic,0)    =dsclmx*dscl(ic,0)
            dscl(ic,nloop)=dsclmx
            fin=.false.
          endif
        enddo
        fin=fin.or.abs(dsclmx-1.d0).lt.tiny/256

        volsph = 0.d0
        do  ibas = 1, nbas
          volsph = volsph + fpi3 * wsr(iclass(ibas))**3
        enddo


        if (fin.or.nloop.eq.nclass+1) then
          if (iprint().ge.30) then
            write(lunit(1),300)ommax1,ommax2
            do line=0,(nclass-1)/6
              icmin=1+6*line
              icmax=min0(nclass,6+6*line)
*              write(lunit(1),301)(clabl(ic),ic=icmin,icmax)
              write(lunit(1),306)('============',ic=icmin,icmax+1)
              write(lunit(1),302)(wsr(ic)/dscl(ic,0),ic=icmin,icmax)
              write(lunit(1),306)('------------',ic=icmin,icmax+1)
              do iloop=1,nloop
                write(lunit(1),303)iloop,(dscl(ic,iloop),ic=icmin,icmax)
              enddo
              write(lunit(1),306)('------------',ic=icmin,icmax+1)
              write(lunit(1),305)(wsr (ic  ),ic=icmin,icmax)
              write(lunit(1),304)(dscl(ic,0),ic=icmin,icmax)
              write(lunit(1),309)(wsr(ic)-wsr(ic)/dscl(ic,0),
     .                            ic=icmin,icmax)
              write(lunit(1),306)('------------',ic=icmin,icmax+1)
            enddo
            write(lunit(1),308) vol,volsph
          endif
          if (abs(ommax1*ommax2).gt.tiny.and.
     .        abs(volsph-vol*facvol).gt.tiny) then
            write(messg,400)
            call errmsg(messg,0)
          endif
          return
        endif
      enddo

300   format(/' BLOWUP: scale radii;   OMMAX1=',f5.3,', OMMAX2=',f5.3)
301   format(/6x,'   ATOM:    ',6(5x,a4))
302   format(6x,'OLD WSR:    ',6f9.5)
303   format(6x,'LOOP:',i3,'  * ',6f9.5)
305   format(6x,'NEW WSR:    ',6f9.5)
304   format(6x,'W_NEW/W_OLD:',6f9.5)
309   format(6x,'W_NEW-W_OLD:',6f9.5)
306   format(6x,a12,6a9)
307   format(6x,a,3f13.7)
308   format(/' CELL VOLUME=',f12.5,'   SUM OF SPHERE VOLUMES=',f12.5)
400   format(' BLOWUP: impossible to reach VOL, increase OMMAX.$')

      end
c
      double precision function delsqf(ndata,ix,ya,x)
C- exponential least square fit (  a * exp(bx) )
C ----------------------------------------------------------------------
Ci Inputs:
Ci   ndata :number of data points
Ci   ix    :xa(1) is integer and equals ix
Ci          and xa(1),xa(2),xa(3),... differ exactly by 1.d0
Ci   ya    :value of f at xa
Ci   x     :x-value at which f is interpolated
Co Outputs:
Co   delsqf:interpolated value
Cr Remarks:
C ----------------------------------------------------------------------
      implicit none
C Passed parameters:
      integer ix,ndata
      double precision x,ya(*)
C Local parameters:
      integer idata
      double precision a,b,ss,sx,sy,sxy,sxx,xx,yy
C Intrinsic functions:
      intrinsic  exp,log,dble
      save

      sx =0.d0
      sy =0.d0
      sxy=0.d0
      sxx=0.d0

      do idata = 1, ndata
        xx = dble(ix+idata-1)
        if (ya(idata).le.0.d0) then
          delsqf=0.d0
          return
        endif
        yy = log(ya(idata))
        sx =sx  + xx
        sy =sy  + yy
        sxx=sxx + xx * xx
        sxy=sxy + xx * yy
      enddo
      ss=dble(ndata)

      b=(sxy-sx*sy/ss)/(sxx-sx*sx/ss)
      a=exp((sy-b*sx)/ss)

      delsqf = a * exp(b*x)

      end
c
      double precision function drr2(plat,bas1,bas2,i,j,k,dr)
C- Calculates the vector connecting two sites in a solid
C ----------------------------------------------------------------
Ci Inputs:
Ci   plat  :primitive lattice vectors
Ci   bas1  :basis vector of first site
Ci   bas2  :basis vector of second site
Ci   i,j,k :the number of primitive lattice vectors separating sites
Co Outputs:
Co   dr    :connecting vector bas2 - bas1
Co   drr2  :square of the length of this vector
Cr Remarks:
Cr   Using the TB package and a table of indices iax, the connecting
Cr   vector and the square of the distance is obtained by:
Cr   rsqr=drr2(plat,bas(1,iax(1)),bas(1,iax(2)),iax(3),iax(4),iax(5),dr)
C ----------------------------------------------------------------
      implicit none
C Passed parameters:
      integer i,j,k
      double precision dr(3),plat(3,3),bas1(3),bas2(3)
C Local parameters:
      integer ix

      drr2 = 0.d0
      do ix = 1, 3
        dr(ix)=bas2(ix)-bas1(ix)+plat(ix,1)*i+plat(ix,2)*j+plat(ix,3)*k
        drr2 = drr2 + dr(ix)*dr(ix)
      enddo
      end
c
      integer function iclbas(ic,iclass,nbas,nrbas)
C- Returns an index to iclbas atom in basis given class
C ----------------------------------------------------------------------
Ci Inputs:
Ci   ic    :class label
Ci   iclass:the jth atom belongs to class iclass(j)
Ci   nbas  :number of atoms in the basis
Ci   nrbas :the nrbas-th basis atom of class ic is seeked
Co Outputs:
Co   iclbas:the iclbas-th atom belongs to class ic
C ----------------------------------------------------------------------
      implicit none
C Passed parameters
      integer ic,iclass(*),nbas,nrbas
C Local parameters
      integer ibas,n,lunit
      character*72 messg
C External calls
      external errmsg,lunit
      save

      iclbas=1
      n = 0
      do  ibas = 1,nbas
        if (iclass(ibas).eq.ic) n = n + 1
        if (n.eq.nrbas) then
          iclbas = ibas
          return
        endif
      enddo
      write(messg,400) ic,nrbas,n
      call errmsg(messg,4)

400   format(' ICLBAS: class',i3,', nrbas=',i3,', but only',i3,
     .       ' basis atoms exist.$')
      end
c
      subroutine latlim(pqlat,rmaxs,i1,i2,i3)
C- Set limits in X Y Z direction
C ----------------------------------------------------------------------
Ci Inputs:
Ci   pqlat :primitive lattice vectors (real or reciprocal space)
Ci   rmaxs :maximum length of connecting vector
Co Outputs:
Co   i1,i2,i3:all connecting vectors lie within these multiples of the
Co            lattice vectors.
Cr Remarks:
Cr   This routine only returns the integer part
C ----------------------------------------------------------------------
       implicit none
C Passed parameters:
      integer i1,i2,i3
      double precision pqlat(3,3),rmaxs
C Local parameters:
      integer iprint,lunit
      double precision dlamch,qplat(3,3),det,dnrm23,x1,x2,x3
C External calls
      external dlamch,dinv33,dnrm23,iprint,lunit
C Intrinsic functions
      intrinsic int,sqrt
      save

      call dinv33(pqlat,1,qplat,det)
      x1=rmaxs*sqrt(dnrm23(qplat(1,1)))
      x2=rmaxs*sqrt(dnrm23(qplat(1,2)))
      x3=rmaxs*sqrt(dnrm23(qplat(1,3)))

      i1 = 1 + int(x1 - dlamch('p'))
      i2 = 1 + int(x2 - dlamch('p'))
      i3 = 1 + int(x3 - dlamch('p'))
      if (iprint().ge.80)
     .  write(lunit(1),300)rmaxs,x1,x2,x3,i1,i2,i3

300   format(/' LATLIM: rmaxs=',f6.4,', x1,x2,x3=',3f7.4,
     .        ', i1,i2,i3=',3i2)
      end
c
      subroutine defwsr(wsr,z)
C- Returns default value of radii for given nuclear charge
C ----------------------------------------------------------------------
Ci Inputs:
Ci   z     :nuclear charge
Co Outputs:
Co   wsr   :Wigner-Seitz sphere radius (in atomic units)
Cr Remarks:
Cr These are equilibrium average Wigner-Seitz radii for closed-packed
Cr structure of pure element.
C ----------------------------------------------------------------------
      implicit none
C Passed variables:
      double precision wsr,z
C Local variables:
      integer iz
      double precision cpwsr(0:100)
      character*72 messg
C External calls:
      external errmsg
C Intrinsic functions:
      intrinsic idnint
      save
C Data statements:
      data cpwsr/2.00,
     .       1.39, 2.55, 3.04, 2.27, 1.96, 1.66, 1.90, 1.90, 2.17, 2.89,
     .       3.76, 3.25, 2.95, 2.63, 2.56, 2.70, 2.85, 3.71, 4.66, 3.88,
     .       3.31, 2.99, 2.76, 2.64, 2.57, 2.52, 2.52, 2.55, 2.62, 2.78,
     .       2.75, 2.79, 2.83, 2.94, 3.13, 4.32, 4.95, 4.22, 3.61, 3.28,
     .       3.03, 2.91, 2.82, 2.77, 2.78, 2.84, 2.95, 3.14, 3.30, 3.45,
     .       3.30, 3.31, 3.50, 4.31, 5.30, 4.20, 3.91, 3.80, 3.75, 3.70,
     .       3.65, 3.60, 3.55, 3.52, 3.61, 3.67, 3.70, 3.73, 3.75, 3.56,
     .       3.44, 3.23, 3.04, 2.93, 2.86, 2.82, 2.83, 2.88, 2.98, 3.27,
     .       3.57, 3.62, 3.37, 3.46, 3.63, 4.44, 5.81, 4.30, 3.84, 3.52,
     .       3.32, 3.13, 3.02, 2.96, 2.93, 2.93, 2.95, 2.99, 3.05, 3.17/
c     data cpwsr/2d0,1.693d0, 2.561d0, 3.108d0, 2.622d0, 2.107d0,
c    .      2.253d0, 2.272d0, 2.293d0, 2.401d0, 2.885d0,
c    .      3.763d0, 3.268d0, 2.955d0, 2.886d0, 2.922d0,
c    .      2.976d0, 3.145d0, 3.709d0, 4.666d0, 3.890d0,
c    .      3.310d0, 2.995d0, 2.724d0, 2.661d0, 2.575d0,
c    .      2.533d0, 2.528d0, 2.558d0, 2.625d0, 2.806d0,
c    .      3.066d0, 3.126d0, 3.144d0, 3.214d0, 3.400d0,
c    .      3.998d0, 4.967d0, 4.212d0, 3.602d0, 3.265d0,
c    .      3.067d0, 2.923d0, 2.833d0, 2.784d0, 2.785d0,
c    .      2.843d0, 2.954d0, 3.166d0, 3.426d0, 3.476d0,
c    .      3.481d0, 3.528d0, 3.698d0, 4.329d0, 5.329d0,
c    .      4.284d0, 3.548d0, 3.294d0, 3.208d0, 3.145d0,
c    .      3.099d0, 3.073d0, 3.083d0, 3.105d0, 3.144d0,
c    .      3.197d0, 3.250d0, 3.308d0, 3.534d0, 3.600d0,
c    .      3.431d0, 3.228d0, 3.070d0, 2.953d0, 2.870d0,
c    .      2.827d0, 2.830d0, 2.886d0, 2.992d0, 3.260d0,
c    .      3.592d0, 3.623d0, 3.642d0, 3.694d0, 3.843d0,
c    .      4.441d0, 5.800d0, 4.298d0, 3.857d0, 3.522d0,
c    .      3.321d0, 3.182d0, 3.080d0, 3.013d0, 2.982d0,
c    .      2.979d0, 2.990d0, 3.025d0, 3.082d0, 3.170d0/

      iz=idnint(z)
      if (iz.lt.0.or.iz.gt.100) then
        write(messg,400)iz
        call errmsg(messg,0)
        wsr=cpwsr(100)
      else
        wsr=cpwsr(iz)
      endif
 400  format('DEFWSR: bad nuclear charge, Z=',i3)
      end
