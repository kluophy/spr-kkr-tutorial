C*==xband_geometry.f    processed by SPAG 6.70Rc at 18:37 on  3 Mar 2013
      PROGRAM XBAND_GEOMETRY
C
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C PARAMETER definitions
C
      INTEGER IOTMP,IWSCR,IWDAT
      PARAMETER (IOTMP=80,IWSCR=10,IWDAT=11)
C
C Local variables
C
      REAL*8 A5BAS(3,5),ABAS(3,3),ABAS_BOX(3,3),ABAS_I(3,3),ABAS_L(3,3),
     &       ABAS_R(3,3),ABAS_RLX(3,3),ADAINV(3,3),ADAINV_I(3,3),
     &       ADAINV_L(3,3),ADAINV_R(3,3),ADAMAT(3,3),ADAMAT_L(3,3),
     &       ADAMAT_R(3,3),ADAMAT_S(3,3),ALAT,AUX,BBAS(3,3),BDBINV(3,3),
     &       BDBMAT(3,3),BOA,BVEC(3),CLURAD,CLURADSQ_IQBOX(:),
     &       CLURAD_IQ(:),CLU_BOX_DIST,COA,CONC(:),D,DQCLU(:),DQCLU_TMP,
     &       DSQIJ,DSQIJMAX,D_CG(3),EXT_MAX(3),EXT_MIN(3),LATANG(3),
     &       LATPAR(3),L_BOX(3),L_RLX(3),NDOTQ(:),NVEC_LAY(3),PA,PB,
     &       PN_ILAY(:),PN_IQCLU(:),PN_TMP,QBAS(:,:),RCNTR_BOX(3),
     &       RCNTR_RLX(3),RQBOX(:,:),RQCLU(:,:),RQCLU_TMP(3),RQRLX(:,:),
     &       RSCL,RSUM,RVEC(3),RWSQ(:),R_CG(3),VOLUC,VUC,WRK3X3(3,3),X1,
     &       X2
      CHARACTER*15 ANCHOR_MODE,STR15
      INTEGER BRAVAIS,CLU_BOX_SHOW,I,I1,I2,IA,IA_ERR,IBRCLU(:,:),
     &        IBRCLU_TMP(3),ICL,ICLQ(:),IDUM,IE,IERROR,IFLAG,ILAY,
     &        ILAYCNTR,INFIL,IOCCTYPE_IQ(:),IOCCTYPE_IQBOX(:),
     &        IOCCTYPE_IQRLX(:),IOFFSET,IPRINT,IQ,IQAT(:,:),IQBOX,
     &        IQBOX1_ILAY(:),IQBOX2_ILAY(:),IQBOXIN,IQBOX_IQRLX(:),
     &        IQBOX_ORD,IQCLU,IQCLU1_ILAY(:),IQCLU2_ILAY(:),IQCNTR,
     &        IQCNTR_TMP,IQECL(:,:),IQRLX,IQRLX1_ILAY(:),IQRLX2_ILAY(:),
     &        IQRLX_INEW(:),IQRLX_IQBOX(:),IQRLX_ORD,IQ_IQBOX(:),
     &        IQ_IQCLU(:),IQ_IQCLU_TMP,IQ_IQRLX(:),IQ_ORDERED(:),IT,
     &        ITOQ(:,:),IWR,J,JCL,JQ,JQBOX,JQCLU,JQRLX,JT,KQCLU,
     &        MILLER_INDICES(3),N5VEC_CG(5),N5VEC_IQBOX(:,:),
     &        N5VEC_IQCLU(:,:),N5VEC_IQRLX(:,:),NAT(:),NCHK1,NCL,
     &        NLAYBOX,NLAYCLU,NLAYRLX,NLQ(:),NOQ(:),NQ,NQADD,NQBOX,
     &        NQBOXMAX,NQBOX_L,NQBOX_R,NQBOX_S,NQBOX_SHIFTED
      CHARACTER*3 BULK_L_EQ_BULK_R,STRUC_L_EQ_STRUC_R
      LOGICAL CHANGES_DONE,FOUND,INSIDE,MATCH,N_EQ_0,OUTSIDE,
     &        USE_CRY_PRIM_VECS
      REAL*8 DDOT
      CHARACTER*10 DIMENSION,SU_NAME(10)
      CHARACTER*80 LATTICE_TYPE,LINE,OCCUPATION_OF_LAYERS,
     &             STRUCTURE_TYPE,SYSFILE,ZRANGE_TYPE
      INTEGER NQCL(:),NQCLU,NQCLUMAX,NQCLU_L,NQCLU_R,NQCLU_S,NQIN,NQMAX,
     &        NQNEW,NQRLX,NQRLX0,NQRLXMAX,NQRLX_L,NQRLX_R,NQRLX_S,NQSEL,
     &        NQSEL0,NQSHLCLU(:),NQ_BULK_L,NQ_BULK_R,NSHLCLU,NSHLCLU0,
     &        NSUBSYS,NT,NTMAX,NVEC(3),N_REP_SU_BULK_L,N_REP_SU_BULK_R,
     &        N_SU,SEL0_IQRLX(:),SEL_IQBOX(:),SEL_IQRLX(:),SPACEGROUP,
     &        SPACEGROUP_AP,SU_IQ(:),ZT(:)
      CHARACTER*40 TASK
      CHARACTER*4 TXTT(:)
      CHARACTER*1 WYCKOFFCL(:),WYCKOFFQ(:)
C
C*** End of declarations rewritten by SPAG
C
      DATA IA_ERR/0/
      DATA ABAS/9*0D0/,ABAS_L/9*0D0/,ABAS_I/9*0D0/,ABAS_R/9*0D0/
      DATA RSCL/3D0/
C
      ALLOCATABLE NAT,NLQ,NOQ,CONC,QBAS,RWSQ,ICLQ,IQAT,IQECL
      ALLOCATABLE NQCL,SU_IQ,ZT,TXTT,WYCKOFFCL,WYCKOFFQ,ITOQ
      ALLOCATABLE RQCLU,DQCLU,IQ_IQCLU,NQSHLCLU,IBRCLU
      ALLOCATABLE RQBOX,IQ_IQBOX,N5VEC_IQBOX,IQ_ORDERED,SEL_IQBOX
      ALLOCATABLE IQCLU1_ILAY,IQCLU2_ILAY,SEL0_IQRLX,N5VEC_IQCLU
      ALLOCATABLE IQBOX1_ILAY,IQBOX2_ILAY,PN_ILAY
      ALLOCATABLE NDOTQ,PN_IQCLU ,IQRLX1_ILAY,IQRLX2_ILAY
      ALLOCATABLE IQBOX_IQRLX,CLURADSQ_IQBOX,IQRLX_INEW
      ALLOCATABLE RQRLX,IQ_IQRLX,N5VEC_IQRLX,IQRLX_IQBOX,SEL_IQRLX
      ALLOCATABLE IOCCTYPE_IQ,IOCCTYPE_IQBOX,IOCCTYPE_IQRLX,CLURAD_IQ
C
      IPRINT = 5
      IERROR = 0
C
      OPEN (1,FILE='xband_geometry.inp')
C
      IWR = 3
      OPEN (IWR,FILE='xband_geometry.out')
C
      READ (1,*)
      READ (1,*) SYSFILE
      WRITE (6,*) 'system file: ',SYSFILE
      READ (1,*) LINE
      READ (1,*) TASK
C
C=======================================================================
      IF ( TASK.EQ.'optimisation_of_basis_vectors_3D' ) THEN
C=======================================================================

         READ (1,*)
         DO J = 1,3
            READ (1,*) (ABAS(I,J),I=1,3)
            WRITE (6,'(''input ABAS  '',I4,3F10.5)') J,(ABAS(I,J),I=1,3)
         END DO
         READ (1,*)
         READ (1,*) NQ
         NQMAX = NQ
         ALLOCATE (QBAS(3,NQ))
         WRITE (6,'(''input NQ    '',I4)') NQ
         DO IQ = 1,NQ
            READ (1,*) IDUM,(QBAS(I,IQ),I=1,3)
            WRITE (6,'(''input QBAS  '',I4,3F10.5)') IQ,
     &             (QBAS(I,IQ),I=1,3)
         END DO
C
         CALL OPTIMIZE_BASIS_3D(ABAS,QBAS,NQ,NQMAX,IFLAG)
C
         WRITE (IWR,99008) 
     &      'GEOMETRY:  all done for optimisation_of_basis_vectors_3D'
         WRITE (IWR,99008) 'ERROR FLAG ',IFLAG

         IF( IFLAG .GE. 0 ) THEN 
            IF ( IFLAG .EQ. 1 ) THEN
               WRITE (IWR,99008) 'CHANGES MADE'
            ELSE
               WRITE (IWR,99008) 'NO CHANGES'
            END IF
            DO IQ = 1,NQ
               WRITE (IWR,'(3F20.15)') (QBAS(I,IQ),I=1,3)
            END DO
         END IF
C
         CLOSE (IWR)
C
         STOP
C=======================================================================
      ELSE IF ( TASK.EQ.'optimisation_of_basis_vectors_2D' ) THEN
C=======================================================================
C
C          TO BE DONE
C
      END IF
C
C
C=======================================================================
C=======================================================================
C                        tasks using system file
C=======================================================================
C=======================================================================
C
      INFIL = 2
      OPEN (INFIL,FILE=SYSFILE)
C
 100  CONTINUE
      READ (INFIL,'(A)',END=400) LINE
      IF ( LINE(1:18).NE.'number of sites NQ' ) GOTO 100
      IF ( LINE(1:28).EQ.'number of sites NQ for bulk(' ) GOTO 100
      READ (INFIL,*) NQ
 200  CONTINUE
      READ (INFIL,'(A)',END=400) LINE
      IF ( LINE(1:23).NE.'number of atom types NT' ) GOTO 200
      READ (INFIL,*) NT
C
      NQMAX = NQ
      NTMAX = NT
C
      ALLOCATE (CONC(NTMAX),RWSQ(NQMAX))
      ALLOCATE (ICLQ(NQMAX),IQAT(NQMAX,NTMAX),IQECL(NQMAX,NQMAX))
      ALLOCATE (ITOQ(NTMAX,NQMAX),NAT(NTMAX),NLQ(NQMAX),NOQ(NQMAX))
      ALLOCATE (NQCL(NQMAX),SU_IQ(NQMAX),ZT(NTMAX),TXTT(NTMAX))
      ALLOCATE (WYCKOFFCL(NQMAX),WYCKOFFQ(NQMAX),QBAS(3,NQMAX))
C
      REWIND INFIL
C
      CALL READ_SYSFILE(INFIL,ALAT,BOA,ABAS,ABAS_L,ABAS_I,ABAS_R,COA,
     &                  CONC,LATANG,LATPAR,QBAS,RWSQ,BRAVAIS,I,IA,ICL,
     &                  ICLQ,IE,IQ,IQAT,IQECL,IT,ITOQ,JCL,JQ,JT,NAT,NCL,
     &                  NLQ,NOQ,NQ,NQCL,NQ_BULK_L,NQ_BULK_R,NSUBSYS,NT,
     &                  N_REP_SU_BULK_L,N_REP_SU_BULK_R,N_SU,SPACEGROUP,
     &                  SPACEGROUP_AP,ZT,BULK_L_EQ_BULK_R,
     &                  STRUC_L_EQ_STRUC_R,DIMENSION,SU_NAME,SU_IQ,
     &                  LATTICE_TYPE,LINE,OCCUPATION_OF_LAYERS,
     &                  STRUCTURE_TYPE,ZRANGE_TYPE,TXTT,WYCKOFFQ,
     &                  WYCKOFFCL,NQMAX,NTMAX)
C
      DO I = 1,3
         DO J = 1,3
            ADAMAT(I,J) = DDOT(3,ABAS(1,I),1,ABAS(1,J),1)
         END DO
      END DO
C
      CALL RINVGJ(ADAINV,ADAMAT,3,3)
C
      CALL RVECSPAT(ABAS(1,1),ABAS(1,2),ABAS(1,3),VUC,1)
C
      IF ( DIMENSION(1:2).EQ.'2D' ) THEN
         DO I = 1,3
            DO J = 1,3
               ADAMAT_L(I,J) = DDOT(3,ABAS_L(1,I),1,ABAS_L(1,J),1)
            END DO
         END DO
         CALL RINVGJ(ADAINV_L,ADAMAT_L,3,3)
C
         DO I = 1,3
            DO J = 1,3
               ADAMAT_S(I,J) = DDOT(3,ABAS_I(1,I),1,ABAS_I(1,J),1)
            END DO
         END DO
         CALL RINVGJ(ADAINV_I,ADAMAT_S,3,3)
C
         DO I = 1,3
            DO J = 1,3
               ADAMAT_R(I,J) = DDOT(3,ABAS_R(1,I),1,ABAS_R(1,J),1)
            END DO
         END DO
         CALL RINVGJ(ADAINV_R,ADAMAT_R,3,3)
C
      END IF
C
C--------------------------------------------- auxilary 5-fold basis set
C
      IF ( DIMENSION(1:2).EQ.'2D' ) THEN
C
         A5BAS(1:3,1:2) = ABAS_L(1:3,1:2)
         A5BAS(1:3,3) = ABAS_I(1:3,3)
         A5BAS(1:3,4) = ABAS_L(1:3,3)
         A5BAS(1:3,5) = ABAS_R(1:3,3)
C
      ELSE
C
         A5BAS(1:3,1:3) = ABAS(1:3,1:3)
         A5BAS(1:3,4:5) = 0D0
C
      END IF
C
C-------------------------- primitive vectors (BBAS) of reciprocal space
C
      DO I = 1,3
         I1 = 1 + MOD(I,3)
         I2 = 1 + MOD(I1,3)
         BBAS(1,I) = ABAS(2,I1)*ABAS(3,I2) - ABAS(3,I1)*ABAS(2,I2)
         BBAS(2,I) = ABAS(3,I1)*ABAS(1,I2) - ABAS(1,I1)*ABAS(3,I2)
         BBAS(3,I) = ABAS(1,I1)*ABAS(2,I2) - ABAS(2,I1)*ABAS(1,I2)
      END DO
      VOLUC = DABS(ABAS(1,1)*BBAS(1,1)+ABAS(2,1)*BBAS(2,1)+ABAS(3,1)
     &        *BBAS(3,1))
C
      DO I = 1,3
         BBAS(1,I) = BBAS(1,I)/VOLUC
         BBAS(2,I) = BBAS(2,I)/VOLUC
         BBAS(3,I) = BBAS(3,I)/VOLUC
      END DO
C
      DO I = 1,3
         DO J = 1,3
            BDBMAT(I,J) = DDOT(3,BBAS(1,I),1,BBAS(1,J),1)
         END DO
      END DO
C
      WRK3X3(1:3,1:3) = BDBMAT(1:3,1:3)
C
      CALL RMATINV(3,3,WRK3X3,BDBINV)
C-----------------------------------------------------------------------
C
C
      WRITE (6,99014) 'TASK         ',TASK
C
C=======================================================================
      IF ( TASK.EQ.'IQ_NLAY_order' ) THEN
C=======================================================================
C
         READ (1,*) LINE
         WRITE (6,*) LINE
         READ (1,*) NVEC_LAY
         WRITE (6,*) NVEC_LAY
C
         ALLOCATE (IQ_ORDERED(NQMAX),NDOTQ(NQMAX))
C
         CALL IQ_NLAY_ORDER(NQ,QBAS,IQ_ORDERED,NDOTQ,NVEC_LAY,NQMAX)
C
         WRITE (6,99009) ' NVEC_LAY = ',NVEC_LAY
C
         WRITE (IWR,99008) 'GEOMETRY:  ordering of sites along NVEC_LAY'
         WRITE (IWR,99008) 'NQ       = ',NQ
         WRITE (IWR,99009) 'NVEC_LAY = ',NVEC_LAY
         WRITE (IWR,99008) '   IQ   JQ     QX(JQ)    QY(JQ)    QZ(JQ)'
         DO IQ = 1,NQ
            JQ = IQ_ORDERED(IQ)
            WRITE (IWR,'(2I5,1X,3F10.3)') IQ,JQ,(QBAS(I,JQ),I=1,3)
         END DO
C
C=======================================================================
      ELSE IF ( TASK.EQ.'cr_sph_regime' ) THEN
C=======================================================================
C
         READ (1,*) LINE
         READ (1,*) IQCNTR
         WRITE (6,99011) LINE(1:30),IQCNTR
         READ (1,'(A)') LINE
         IF ( LINE(1:7).EQ.'NSHLCLU' ) THEN
            READ (1,*) NSHLCLU
            WRITE (6,99011) LINE(1:30),NSHLCLU
            CLURAD = 0D0
         ELSE
            READ (1,*) CLURAD
            WRITE (6,99010) LINE(1:30),CLURAD
            NSHLCLU = 0
         END IF
C
         READ (1,'(A)') LINE
         READ (1,*) NVEC_LAY
         CALL RVECNORM(3,NVEC_LAY)
         WRITE (6,99010) LINE(1:30),NVEC_LAY
C
         READ (1,*) LINE
         READ (1,*) CLU_BOX_SHOW
         WRITE (6,99011) LINE(1:30),CLU_BOX_SHOW
         READ (1,*) LINE
         READ (1,*) CLU_BOX_DIST
         WRITE (6,99010) LINE(1:30),CLU_BOX_DIST
C
         CALL SPH_CLUSTER_SITES(IOTMP,DIMENSION,LATTICE_TYPE,ABAS,
     &                          ABAS_L,ABAS_I,ABAS_R,ADAINV_L,ADAINV_I,
     &                          ADAINV_R,QBAS,CLURAD,IQCNTR,NQCLU,
     &                          NQCLU_L,NQCLU_S,NQCLU_R,NSHLCLU,NQ,
     &                          NQ_BULK_L,NQ_BULK_R,NQMAX)
         NQCLUMAX = NQCLU
C
         WRITE (6,99015)
         WRITE (6,99016) 'cluster sites    ','NQCLU   ',NQCLU
         WRITE (6,99016) 'cluster shells   ','NSHLCLU ',NSHLCLU
C
         ALLOCATE (RQCLU(3,NQCLU),DQCLU(NQCLU),IBRCLU(3,NQCLU))
         ALLOCATE (IQ_IQCLU(NQCLU),NQSHLCLU(NSHLCLU),STAT=IA_ERR)
         IF ( IA_ERR.NE.0 ) STOP 'ALLOC:CLUSTER -> IQCLUS'
C
         READ (IOTMP) ((RQCLU(J,I),J=1,3),DQCLU(I),IQ_IQCLU(I),I=1,
     &                NQCLU),(NQSHLCLU(I),I=1,NSHLCLU)
         READ (IOTMP) ((IBRCLU(J,I),J=1,3),I=1,NQCLU)
         CLOSE (IOTMP)
C
C-----------------------------------------------------------------------
         ALLOCATE (PN_IQCLU(NQCLU))
         DO IQCLU = 1,NQCLU
            PN_IQCLU(IQCLU) = DDOT(3,NVEC_LAY,1,RQCLU(1,IQCLU),1)
         END DO
C
         DO IQCLU = 2,NQCLU
            DO JQCLU = 1,IQCLU - 1
               IF ( PN_IQCLU(IQCLU).LT.PN_IQCLU(JQCLU) ) THEN
C
                  PN_TMP = PN_IQCLU(IQCLU)
                  RQCLU_TMP(1:3) = RQCLU(1:3,IQCLU)
                  DQCLU_TMP = DQCLU(IQCLU)
                  IQ_IQCLU_TMP = IQ_IQCLU(IQCLU)
                  IBRCLU_TMP(1:3) = IBRCLU(1:3,IQCLU)
C
                  DO KQCLU = IQCLU - 1,JQCLU, - 1
                     PN_IQCLU(KQCLU+1) = PN_IQCLU(KQCLU)
                     RQCLU(1:3,KQCLU+1) = RQCLU(1:3,KQCLU)
                     DQCLU(KQCLU+1) = DQCLU(KQCLU)
                     IQ_IQCLU(KQCLU+1) = IQ_IQCLU(KQCLU)
                     IBRCLU(1:3,KQCLU+1) = IBRCLU(1:3,KQCLU)
                  END DO
C
                  PN_IQCLU(JQCLU) = PN_TMP
                  RQCLU(1:3,JQCLU) = RQCLU_TMP(1:3)
                  DQCLU(JQCLU) = DQCLU_TMP
                  IQ_IQCLU(JQCLU) = IQ_IQCLU_TMP
                  IBRCLU(1:3,JQCLU) = IBRCLU_TMP(1:3)
C
                  EXIT
               END IF
            END DO
         END DO
C
C
         NQCLUMAX = NQCLU
         ALLOCATE (IQCLU1_ILAY(NQCLUMAX),IQCLU2_ILAY(NQCLUMAX))
         ALLOCATE (PN_ILAY(NQCLUMAX),N5VEC_IQCLU(5,NQCLUMAX))
C
         PN_TMP = -1D10
C
         NLAYCLU = 0
         DO IQCLU = 1,NQCLU
            IF ( ABS(PN_IQCLU(IQCLU)-PN_TMP).GT.1D-6 ) THEN
               NLAYCLU = NLAYCLU + 1
               PN_ILAY(NLAYCLU) = PN_IQCLU(IQCLU)
               PN_TMP = PN_IQCLU(IQCLU)
               IQCLU1_ILAY(NLAYCLU) = IQCLU
            END IF
            IQCLU2_ILAY(NLAYCLU) = IQCLU
C
            IF ( ABS(PN_TMP).LT.1D-6 ) ILAYCNTR = NLAYCLU
C
         END DO
C
C---------------------- recover cluster site position in global frame 2D
C
         IF ( DIMENSION(1:2).EQ.'2D' ) THEN
C
            DO IQCLU = 1,NQCLU
C
               N5VEC_IQCLU(1:5,IQCLU) = 0
C
               IQ = IQ_IQCLU(IQCLU)
C
               RVEC(1:3) = RQCLU(1:3,IQCLU) + QBAS(1:3,IQCNTR)
     &                     - QBAS(1:3,IQ)
C
C................................................................ L BULK
               IF ( IQ.LE.NQCLU_L ) THEN
C
                  DO I = 1,3
                     BVEC(I) = DDOT(3,ABAS_L(1,I),1,RVEC(1),1)
                  END DO
C
                  DO I = 1,3
                     RSUM = 0D0
                     DO J = 1,3
                        RSUM = RSUM + ADAINV_L(I,J)*BVEC(J)
                     END DO
                     NVEC(I) = NINT(RSUM)
                     IF ( ABS(NVEC(I)-RSUM).GT.1D-6 ) THEN
                        IERROR = 1
                        WRITE (6,*) '**** L   non-integer N ',IQCLU,I,
     &                              NVEC(I),RSUM
                     END IF
                  END DO
C
                  N5VEC_IQCLU(1:2,IQCLU) = NVEC(1:2)
C
                  N5VEC_IQCLU(4,IQCLU) = NVEC(3)
C
C...................................................... INTERACTION ZONE
               ELSE IF ( IQ.LE.NQCLU-NQCLU_R ) THEN
C
                  DO I = 1,3
                     BVEC(I) = DDOT(3,ABAS_I(1,I),1,RVEC(1),1)
                  END DO
C
                  DO I = 1,3
                     RSUM = 0D0
                     DO J = 1,3
                        RSUM = RSUM + ADAINV_I(I,J)*BVEC(J)
                     END DO
                     NVEC(I) = NINT(RSUM)
                     IF ( ABS(NVEC(I)-RSUM).GT.1D-6 ) THEN
                        IERROR = 1
                        WRITE (6,*) '**** I   non-integer N ',IQCLU,I,
     &                              NVEC(I),RSUM
                     END IF
                  END DO
C
                  N5VEC_IQCLU(1:2,IQCLU) = NVEC(1:2)
C
                  N5VEC_IQCLU(3,IQCLU) = NVEC(3)
                  IF ( NVEC(3).NE.0 ) THEN
                     IERROR = 1
                     WRITE (6,*) '**** I   non-zero NVEC(3) ',NVEC(3),
     &                           IQCLU
                  END IF
C
C................................................................ R BULK
               ELSE
C
                  DO I = 1,3
                     BVEC(I) = DDOT(3,ABAS_R(1,I),1,RVEC(1),1)
                  END DO
C
                  DO I = 1,3
                     RSUM = 0D0
                     DO J = 1,3
                        RSUM = RSUM + ADAINV_R(I,J)*BVEC(J)
                     END DO
                     NVEC(I) = NINT(RSUM)
                     IF ( ABS(NVEC(I)-RSUM).GT.1D-6 ) THEN
                        IERROR = 1
                        WRITE (6,*) '**** R   non-integer N ',IQCLU,I,
     &                              NVEC(I),RSUM
                     END IF
                  END DO
C
                  N5VEC_IQCLU(1:2,IQCLU) = NVEC(1:2)
C
                  N5VEC_IQCLU(5,IQCLU) = NVEC(3)
C
               END IF
C
            END DO
C
C---------------------- recover cluster site position in global frame 3D
C
         ELSE
C
            DO IQCLU = 1,NQCLU
C
               N5VEC_IQCLU(1:5,IQCLU) = 0
C
               IQ = IQ_IQCLU(IQCLU)
C
               RVEC(1:3) = RQCLU(1:3,IQCLU) + QBAS(1:3,IQCNTR)
     &                     - QBAS(1:3,IQ)
C
               DO I = 1,3
                  BVEC(I) = DDOT(3,ABAS(1,I),1,RVEC(1),1)
               END DO
C
               DO I = 1,3
                  RSUM = 0D0
                  DO J = 1,3
                     RSUM = RSUM + ADAINV(I,J)*BVEC(J)
                  END DO
                  NVEC(I) = NINT(RSUM)
                  IF ( ABS(NVEC(I)-RSUM).GT.1D-6 ) THEN
                     IERROR = 1
                     WRITE (6,*) '**** B   non-integer N ',IQCLU,I,
     &                           NVEC(I),RSUM
                  END IF
               END DO
C
               N5VEC_IQCLU(1:3,IQCLU) = NVEC(1:3)
C
            END DO
C
         END IF
C
C----------------------------- check expansion coefficients  N5VEC_IQCLU
C
         DO IQCLU = 1,NQCLU
C
            IQ = IQ_IQCLU(IQCLU)
C
            RVEC(1:3) = 0D0
            DO I = 1,5
               RVEC(1:3) = RVEC(1:3) + A5BAS(1:3,I)*N5VEC_IQCLU(I,IQCLU)
            END DO
C
            RVEC(1:3) = RVEC(1:3) + QBAS(1:3,IQ) - QBAS(1:3,IQCNTR)
C
            DO I = 1,3
               IF ( ABS(RVEC(I)-RQCLU(I,IQCLU)).GT.1D-6 ) THEN
                  IERROR = 1
                  WRITE (6,*) '**** RQCLU not recovered ',IQCLU,I,
     &                        RVEC(I),RQCLU(I,IQCLU)
               END IF
            END DO
C
         END DO
C
C-----------------------------------------------------------------------
C
         ANCHOR_MODE = 'xyz-centered'
C
         WRITE (IWR,'(A)') 'GEOMETRY:  spherical cluster for '
         WRITE (IWR,'(A,I10)') 'IQCNTR      = ',IQCNTR
         WRITE (IWR,'(A,A)') 'ANCHOR_MODE = ',ANCHOR_MODE
         WRITE (IWR,'(A,3F10.3)') 'NVEC_LAY    = ',NVEC_LAY
         WRITE (IWR,'(A,I10)') 'NQCLU       = ',NQCLU
         WRITE (IWR,'(A,I10)') 'NLAYCLU     = ',NLAYCLU
         WRITE (IWR,'(A,I10)') 'ILAYCNTR    = ',ILAYCNTR
         WRITE (IWR,'('' ILAY  IQ1  IQ2     PROJECT'')')
         DO ILAY = 1,NLAYCLU
            WRITE (IWR,'(3I5,F15.10)') ILAY,IQCLU1_ILAY(ILAY),
     &                                 IQCLU2_ILAY(ILAY),PN_ILAY(ILAY)
         END DO
C
         WRITE (IWR,99006)
         DO IQCLU = 1,NQCLU
            WRITE (IWR,99005) IQCLU,(RQCLU(I,IQCLU),I=1,3),
     &                        (N5VEC_IQCLU(I,IQCLU),I=1,5),
     &                        IQ_IQCLU(IQCLU)
         END DO
C
C-----------------------------------------------------------------------
C
         IF ( LATTICE_TYPE.EQ.'heterogeneous' .AND. NSHLCLU.NE.0 ) THEN
            NSHLCLU = 0
            CLURAD = 1.5D0
            WRITE (IWR,'(A)') 'LATTICE_TYPE = heterogeneous'
            WRITE (IWR,'(A)') 'setting  NSHLCLU  in input not allowed'
            WRITE (IWR,'(A)') 'specify  CLURAD   instead '
            WRITE (IWR,'(A)') 'modified settings'
            WRITE (IWR,'(A,I10)') 'NSHLCLU  = ',NSHLCLU
            WRITE (IWR,'(A,3F10.3)') 'CLURAD   = ',CLURAD
         END IF
C
         OPEN (IWDAT,FILE='cluster_data.pdb')
C
         IOFFSET = 0
         IF ( CLU_BOX_SHOW.NE.0 ) THEN
C
            CALL RVECCOP(3,QBAS(1,IQCNTR),RCNTR_BOX)
C
            CALL RINIT(9,ABAS_BOX)
            DO I = 1,3
               ABAS_BOX(I,I) = 1D0
               L_BOX(I) = 0D0
            END DO
            DO IQCLU = 1,NQCLU
               DO I = 1,3
                  L_BOX(I) = MAX(L_BOX(I),ABS(RQCLU(I,IQCLU))*2D0)
               END DO
            END DO
C
            CALL RECANGBOX(IOTMP,IWDAT,DIMENSION,ABAS,ABAS_L,ABAS_I,
     &                     ABAS_R,ADAINV,ADAINV_L,ADAINV_I,ADAINV_R,VUC,
     &                     NQ,NQ_BULK_L,NQ_BULK_R,QBAS,ABAS_BOX,
     &                     RCNTR_BOX,L_BOX,RSCL,IOFFSET,NQBOX,NQBOX_L,
     &                     NQBOX_S,NQBOX_R,NQMAX)
C
         ELSE
C
            NQBOX = 0
C
         END IF
C
         DO IQCLU = 1,NQCLU
            I = IQCLU + NQBOX
            J = I
            CALL WRITE_COORD(IWDAT,'a',I,J,RSCL*RQCLU(1,IQCLU),
     &                       RSCL*RQCLU(2,IQCLU),RSCL*RQCLU(3,IQCLU),
     &                       1D0,'txtt',1,1)
         END DO
C
         OPEN (IWSCR,FILE='cluster_data.ras')
         WRITE (IWSCR,'(A)') 'load "cluster_data.pdb"'
         WRITE (IWSCR,'(A)') 'color temperature'
         WRITE (IWSCR,'(A)') 'background white'
         WRITE (IWSCR,'(A)') 'label false'
         WRITE (IWSCR,'(A)') 'set fontsize 20'
C
         DO IQBOX = 1,NQBOX
            WRITE (IWSCR,'(A,I5)') 'select ',IOFFSET + IQBOX
            WRITE (IWSCR,'(A,I5)') 'cpk  ',30
         END DO
         DO IQCLU = 1,NQCLU
            WRITE (IWSCR,'(A,I5)') 'select ',IQCLU + NQBOX
            WRITE (IWSCR,'(A,I5)') 'cpk  ',150
         END DO
C
C=======================================================================
      ELSE IF ( TASK.EQ.'cr_rec_regime' ) THEN
C=======================================================================
C
         READ (1,*) LINE
         READ (1,*) IQCNTR
         WRITE (6,99011) LINE(1:30),IQCNTR
C
         READ (1,'(A)') LINE
         READ (1,*) ANCHOR_MODE
         WRITE (6,99014) LINE(1:30),ANCHOR_MODE
C
         READ (1,'(A)') LINE
         READ (1,*) NVEC_LAY
         CALL RVECNORM(3,NVEC_LAY)
         WRITE (6,99010) LINE(1:30),NVEC_LAY
C
         READ (1,'(A)') LINE
         READ (1,*) L_BOX
         WRITE (6,99010) LINE(1:30),L_BOX
C
         CALL LOCAL_BASIS(NVEC_LAY,ABAS_BOX)
C
         WRITE (6,'(3f10.5)') ABAS_BOX
C
         IF ( ANCHOR_MODE(1:6).EQ.'anchor' ) THEN
            DO I = 1,3
               D = 0D0
               DO J = 1,3
                  D = D + ABAS_BOX(I,J)*L_BOX(J)
               END DO
               RCNTR_BOX(I) = QBAS(I,IQCNTR) + D/2D0
            END DO
         ELSE IF ( ANCHOR_MODE(1:11).EQ.'xy-centered' ) THEN
            DO I = 1,3
               RCNTR_BOX(I) = QBAS(I,IQCNTR) + ABAS_BOX(I,3)*L_BOX(3)
     &                        /2D0
            END DO
         ELSE IF ( ANCHOR_MODE(1:12).EQ.'xyz-centered' ) THEN
            CALL RVECCOP(3,QBAS(1,IQCNTR),RCNTR_BOX)
         ELSE
            STOP 'inproper input for anchor_mode'
         END IF
C
         WRITE (6,'(3f10.5)') RCNTR_BOX
C
         OPEN (IWDAT,FILE='cluster_data.pdb')
C
         IOFFSET = 0
C
         CALL RECANGBOX(IOTMP,IWDAT,DIMENSION,ABAS,ABAS_L,ABAS_I,ABAS_R,
     &                  ADAINV,ADAINV_L,ADAINV_I,ADAINV_R,VUC,NQ,
     &                  NQ_BULK_L,NQ_BULK_R,QBAS,ABAS_BOX,RCNTR_BOX,
     &                  L_BOX,RSCL,IOFFSET,NQBOX,NQBOX_L,NQBOX_S,
     &                  NQBOX_R,NQMAX)
C
         REWIND (IOTMP)
         READ (IOTMP) NQBOX
         NQBOXMAX = NQBOX
         ALLOCATE (RQBOX(3,NQBOXMAX))
         ALLOCATE (IQ_IQBOX(NQBOXMAX),N5VEC_IQBOX(5,NQBOXMAX))
         READ (IOTMP) ((RQBOX(I,IQBOX),I=1,3),IQ_IQBOX(IQBOX),
     &                (N5VEC_IQBOX(I,IQBOX),I=1,5),IQBOX=1,NQBOX)
         CLOSE (IOTMP)
C
         ALLOCATE (IQ_ORDERED(NQBOXMAX),NDOTQ(NQBOXMAX))
C
         CALL IQ_NLAY_ORDER(NQBOX,RQBOX,IQ_ORDERED,NDOTQ,NVEC_LAY,
     &                      NQBOXMAX)
C
         ALLOCATE (IQBOX1_ILAY(NQBOXMAX),IQBOX2_ILAY(NQBOXMAX))
         ALLOCATE (PN_ILAY(NQBOXMAX))
C
         ILAYCNTR = 0
         NLAYBOX = 1
         IQBOX1_ILAY(NLAYBOX) = 1
         IQBOX_ORD = IQ_ORDERED(1)
         PA = NDOTQ(IQBOX_ORD)
         PN_ILAY(NLAYBOX) = PA
         N_EQ_0 = .TRUE.
         DO I = 1,5
            N_EQ_0 = N_EQ_0 .AND. N5VEC_IQBOX(I,IQBOX_ORD).EQ.0
         END DO
         IF ( N_EQ_0 .AND. IQ_IQBOX(IQBOX_ORD).EQ.IQCNTR )
     &        ILAYCNTR = NLAYBOX
C
         DO IQBOX = 2,NQBOX
            IQBOX_ORD = IQ_ORDERED(IQBOX)
            N_EQ_0 = .TRUE.
            DO I = 1,5
               N_EQ_0 = N_EQ_0 .AND. N5VEC_IQBOX(I,IQBOX_ORD).EQ.0
            END DO
            IF ( N_EQ_0 .AND. IQ_IQBOX(IQBOX_ORD).EQ.IQCNTR )
     &           ILAYCNTR = NLAYBOX
C
            PB = NDOTQ(IQBOX_ORD)
            IF ( ABS(PA-PB).GT.1D-8 ) THEN
               IQBOX2_ILAY(NLAYBOX) = IQBOX - 1
               PA = PB
               NLAYBOX = NLAYBOX + 1
               IQBOX1_ILAY(NLAYBOX) = IQBOX
               PN_ILAY(NLAYBOX) = PA
            END IF
         END DO
         IQBOX2_ILAY(NLAYBOX) = NQBOX
C
         IF ( ILAYCNTR.EQ.0 ) STOP 'ILAYCNTR = 0 !!!!!!!!!!!!!!!!!!!!!'
C
         WRITE (IWR,'(A)') 'GEOMETRY:  rectangular box for '
         WRITE (IWR,'(A,I10)') 'IQCNTR      = ',IQCNTR
         WRITE (IWR,'(A,A)') 'ANCHOR_MODE = ',ANCHOR_MODE
         WRITE (IWR,'(A,3F10.3)') 'NVEC_LAY    = ',NVEC_LAY
         WRITE (IWR,'(A,I10)') 'NQBOX       = ',NQBOX
         WRITE (IWR,'(A,I10)') 'NLAYBOX     = ',NLAYBOX
         WRITE (IWR,'(A,I10)') 'ILAYCNTR    = ',ILAYCNTR
         WRITE (IWR,'('' ILAY  IQ1  IQ2     PROJECT'')')
         DO ILAY = 1,NLAYBOX
            WRITE (IWR,'(3I5,F15.10)') ILAY,IQBOX1_ILAY(ILAY),
     &                                 IQBOX2_ILAY(ILAY),PN_ILAY(ILAY)
         END DO
C
         WRITE (IWR,99006)
         DO IQBOX = 1,NQBOX
            IQBOX_ORD = IQ_ORDERED(IQBOX)
            WRITE (IWR,99005) IQBOX,(RQBOX(I,IQBOX_ORD),I=1,3),
     &                        (N5VEC_IQBOX(I,IQBOX_ORD),I=1,5),
     &                        IQ_IQBOX(IQBOX_ORD)
         END DO
C
         OPEN (IWSCR,FILE='cluster_data.ras')
         WRITE (IWSCR,'(A)') 'load "cluster_data.pdb"'
         WRITE (IWSCR,'(A)') 'color temperature'
         WRITE (IWSCR,'(A)') 'background white'
         WRITE (IWSCR,'(A)') 'label false'
         WRITE (IWSCR,'(A)') 'set fontsize 20'
C
         DO IQBOX = 1,NQBOX
            WRITE (IWSCR,'(A,I5)') 'select ',IQBOX
            WRITE (IWSCR,'(A,I5)') 'cpk  ',30
         END DO
C
C
C=======================================================================
      ELSE IF ( TASK.EQ.'cr_relax_zone' .OR. TASK.EQ.'center_cluster' )
     &          THEN
C=======================================================================
C
C--------------------------------------------------- read in present BOX
C------------------------ that may be too small to cover relaxation zone
C
         READ (1,*) LINE
         READ (1,*) IQCNTR
         WRITE (6,99011) LINE(1:30),IQCNTR
C
         READ (1,'(A)') LINE
         READ (1,*) NVEC_LAY
         CALL RVECNORM(3,NVEC_LAY)
         WRITE (6,99010) LINE(1:30),NVEC_LAY
C
         READ (1,'(A)') LINE
         IF ( LINE(1:9).EQ.'NSHLRELAX' ) THEN
            READ (1,*) NSHLCLU
            WRITE (6,99011) LINE(1:30),NSHLCLU
            CLURAD = 0D0
         ELSE
            READ (1,*) CLURAD
            WRITE (6,99010) LINE(1:30),CLURAD
            NSHLCLU = 0
         END IF
C
         READ (1,'(A)') LINE
         READ (1,*) NQIN
         WRITE (6,99011) LINE(1:30),NQIN
         IF ( NQIN.NE.NQ ) THEN
            IERROR = 1
            GOTO 300
         END IF
C
         ALLOCATE (IOCCTYPE_IQ(NQ))
C
         READ (1,'(A)') LINE
         DO IQ = 1,NQ
            READ (1,*) I,IOCCTYPE_IQ(IQ)
         END DO
C
         READ (1,'(A)') LINE
         READ (1,*) NQBOX
         WRITE (6,99011) LINE(1:30),NQBOX
         NQBOXMAX = NQBOX
         ALLOCATE (RQBOX(3,NQBOXMAX),SEL_IQBOX(NQBOXMAX))
         ALLOCATE (IQ_IQBOX(NQBOXMAX),N5VEC_IQBOX(5,NQBOXMAX))
         ALLOCATE (CLURADSQ_IQBOX(NQBOXMAX),IOCCTYPE_IQBOX(NQBOXMAX))
C
         READ (1,'(A)') LINE
         NQSEL = 0
         DO IQBOX = 1,NQBOX
            READ (1,*) IQBOXIN,(RQBOX(I,IQBOX),I=1,3),
     &                 (N5VEC_IQBOX(I,IQBOX),I=1,5),IQ_IQBOX(IQBOX),
     &                 SEL_IQBOX(IQBOX),IOCCTYPE_IQBOX(IQBOX)
            WRITE (6,'(I3,3F8.3,10I4)') IQBOXIN,(RQBOX(I,IQBOX),I=1,3),
     &                                  (N5VEC_IQBOX(I,IQBOX),I=1,5),
     &                                  IQ_IQBOX(IQBOX),SEL_IQBOX(IQBOX)
     &                                  ,IOCCTYPE_IQBOX(IQBOX)
            IF ( IQBOXIN.NE.IQBOX ) IERROR = 1
            IF ( SEL_IQBOX(IQBOX).NE.0 ) NQSEL = NQSEL + 1
         END DO
C
         IF ( TASK.EQ.'center_cluster' ) THEN
C
C-------------------------------------------------------- center cluster
C               shift all cluster atoms to have center of
C               gravity in unit cell at the origin
C
            R_CG(1:3) = 0D0
            N5VEC_CG(1:5) = 0
            DO IQBOX = 1,NQBOX
               IF ( SEL_IQBOX(IQBOX).NE.0 ) THEN
                  R_CG(1:3) = R_CG(1:3) + RQBOX(1:3,IQBOX)
                  N5VEC_CG(1:5) = N5VEC_CG(1:5) + N5VEC_IQBOX(1:5,IQBOX)
               END IF
            END DO
            R_CG(1:3) = R_CG(1:3)/DBLE(NQSEL)
            DO I = 1,5
               N5VEC_CG(I) = NINT(DBLE(N5VEC_CG(I))/DBLE(NQSEL))
            END DO
C
            D_CG(1:3) = 0D0
            DO I = 1,5
               D_CG(1:3) = D_CG(1:3) + A5BAS(1:3,I)*N5VEC_CG(I)
            END DO
            WRITE (6,99012) 'center of gravity for cluster'
            WRITE (6,99012) 'N5VEC_CG                     ',N5VEC_CG
            WRITE (6,99009) 'R_CG (arbitrary)             ',R_CG
            WRITE (6,99009) 'D_CG (lattice vector)        ',D_CG
C
C---------------------------------------------  find EXT_MAX and EXT_MIN
C
            EXT_MAX(1:3) = 0D0
            EXT_MIN(1:3) = 0D0
            DO IQBOX = 1,NQBOX
               DO I = 1,3
                  EXT_MAX(I) = MAX(EXT_MAX(I),RQBOX(I,IQBOX))
                  EXT_MIN(I) = MIN(EXT_MIN(I),RQBOX(I,IQBOX))
               END DO
            END DO
C
            WRITE (6,99010) 
     &                     '-------------------------------------------'
            WRITE (6,99010) 'EXT_MAX:  ',EXT_MAX(1:3)
            WRITE (6,99010) 'EXT_MIN:  ',EXT_MIN(1:3)
            DO I = 1,3
               EXT_MAX(I) = (EXT_MAX(I)-EXT_MIN(I))/2D0
               EXT_MIN(I) = -EXT_MAX(I)
            END DO
            WRITE (6,99010) 
     &                     '-------------------------------------------'
            WRITE (6,99010) 'EXT_MAX:  ',EXT_MAX(1:3)
            WRITE (6,99010) 'EXT_MIN:  ',EXT_MIN(1:3)
C
C-----------------------------------------------------------------------
C shift the position of a cluster atom when it was selected or when
C it would get into the cental box otherwise remove it from list
C
            NQBOX_SHIFTED = 0
            DO IQBOX = 1,NQBOX
C
               RVEC(1:3) = RQBOX(1:3,IQBOX) - D_CG(1:3)
C
               OUTSIDE = .FALSE.
               IF ( SEL_IQBOX(IQBOX).EQ.0 ) THEN
                  DO I = 1,3
                     IF ( RVEC(I).LT.EXT_MIN(I) .OR. RVEC(I)
     &                    .GE.EXT_MAX(I) ) OUTSIDE = .TRUE.
                  END DO
               END IF
C
               IF ( (SEL_IQBOX(IQBOX).EQ.1) .OR. (.NOT.OUTSIDE) ) THEN
                  NQBOX_SHIFTED = NQBOX_SHIFTED + 1
                  RQBOX(1:3,NQBOX_SHIFTED) = RVEC(1:3)
                  N5VEC_IQBOX(1:5,NQBOX_SHIFTED)
     &               = N5VEC_IQBOX(1:5,IQBOX) - N5VEC_CG(1:5)
                  IQ_IQBOX(NQBOX_SHIFTED) = IQ_IQBOX(IQBOX)
                  SEL_IQBOX(NQBOX_SHIFTED) = SEL_IQBOX(IQBOX)
                  IOCCTYPE_IQBOX(NQBOX_SHIFTED) = IOCCTYPE_IQBOX(IQBOX)
               END IF
            END DO
C
            WRITE (6,99012) 'number of sites shifted      ',
     &                      NQBOX_SHIFTED
C
            NQBOX = NQBOX_SHIFTED
C
         END IF
C
C---------------------------------------------  find EXT_MAX and EXT_MIN
C
         EXT_MAX(1:3) = 0D0
         EXT_MIN(1:3) = 0D0
         L_BOX(1:3) = 0D0
         DO IQBOX = 1,NQBOX
            DO I = 1,3
               EXT_MAX(I) = MAX(EXT_MAX(I),RQBOX(I,IQBOX))
               EXT_MIN(I) = MIN(EXT_MIN(I),RQBOX(I,IQBOX))
            END DO
            IF ( SEL_IQBOX(IQBOX).NE.0 ) THEN
               DO I = 1,3
                  L_BOX(I) = MAX(L_BOX(I),ABS(RQBOX(I,IQBOX)))
               END DO
            END IF
         END DO
C
         NQSEL0 = NQSEL
         WRITE (6,99012) 'NQSEL0:                      ',NQSEL0
C
C-------------------------- find extension to box due to relaxation zone
C
         IF ( NSHLCLU.NE.0 ) THEN
C
            DSQIJMAX = 0D0
            ALLOCATE (CLURAD_IQ(NQ))
            CLURAD_IQ(1:NQ) = 0D0
C
            DO IQBOX = 1,NQBOX
               IF ( SEL_IQBOX(IQBOX).NE.0 ) THEN
C
                  IQ = IQ_IQBOX(IQBOX)
C
                  IF ( CLURAD_IQ(IQ).LT.1D-8 ) THEN
C
                     IQCNTR_TMP = IQ
C
                     NSHLCLU0 = NSHLCLU
C
                     CALL SPH_CLUSTER_SITES(IOTMP,DIMENSION,
     &                  LATTICE_TYPE,ABAS,ABAS_L,ABAS_I,ABAS_R,ADAINV_L,
     &                  ADAINV_I,ADAINV_R,QBAS,CLURAD,IQCNTR_TMP,NQCLU,
     &                  NQCLU_L,NQCLU_S,NQCLU_R,NSHLCLU,NQ,NQ_BULK_L,
     &                  NQ_BULK_R,NQMAX)
C
                     NSHLCLU = NSHLCLU0
C
C
                     CLURAD_IQ(IQ) = CLURAD
C
                  ELSE
C
                     CLURAD = CLURAD_IQ(IQ)
C
                  END IF
C
                  CLURADSQ_IQBOX(IQBOX) = CLURAD*CLURAD
C
                  DSQIJMAX = MAX(DSQIJMAX,CLURADSQ_IQBOX(IQBOX))
C
                  WRITE (6,99001) IQBOX,NSHLCLU,
     &                            SQRT(CLURADSQ_IQBOX(IQBOX))
C
               END IF
            END DO
C
            CLURAD = SQRT(DSQIJMAX)
C
         ELSE
C
            CLURADSQ_IQBOX(1:NQBOX) = CLURAD*CLURAD
C
         END IF
C
         WRITE (6,99010) '-------------------------------------------'
         WRITE (6,99010) 'EXT_MAX:  ',EXT_MAX(1:3)
         WRITE (6,99010) 'EXT_MIN:  ',EXT_MIN(1:3)
         WRITE (6,99010) 'L_BOX:    ',L_BOX
C
         DO I = 1,3
            X1 = MAX(ABS(EXT_MAX(I)),ABS(EXT_MIN(I)))
            X2 = L_BOX(I) + CLURAD*1.05D0
            L_RLX(I) = MAX(X1,X2)
         END DO
         WRITE (6,99010) 'CLURAD:   ',CLURAD
         WRITE (6,99010) 'L_RLX:    ',L_RLX
         L_RLX(1:3) = L_RLX(1:3)*2
         WRITE (6,99010) 'L_RLX:    ',L_RLX
         WRITE (6,99010) '-------------------------------------------'
C
C-------------------- create an extended BOX that covers relaxation zone
C
         CALL LOCAL_BASIS(NVEC_LAY,ABAS_RLX)
C
         WRITE (6,'(3f10.5)') ABAS_RLX
C
         ANCHOR_MODE = 'xyz-centered'
         RCNTR_RLX(1:3) = QBAS(1:3,IQCNTR)
C
         WRITE (6,'(3f10.5)') RCNTR_RLX
C
         OPEN (IWDAT,FILE='cluster_data.pdb')
C
         IOFFSET = 0
C
         CALL RECANGBOX(IOTMP,IWDAT,DIMENSION,ABAS,ABAS_L,ABAS_I,ABAS_R,
     &                  ADAINV,ADAINV_L,ADAINV_I,ADAINV_R,VUC,NQ,
     &                  NQ_BULK_L,NQ_BULK_R,QBAS,ABAS_RLX,RCNTR_RLX,
     &                  L_RLX,RSCL,IOFFSET,NQRLX,NQRLX_L,NQRLX_S,
     &                  NQRLX_R,NQMAX)
C
         REWIND (IOTMP)
         READ (IOTMP) NQRLX
         NQRLXMAX = NQRLX
         ALLOCATE (RQRLX(3,NQRLXMAX))
         ALLOCATE (IQ_IQRLX(NQRLXMAX),N5VEC_IQRLX(5,NQRLXMAX))
         READ (IOTMP) ((RQRLX(I,IQRLX),I=1,3),IQ_IQRLX(IQRLX),
     &                (N5VEC_IQRLX(I,IQRLX),I=1,5),IQRLX=1,NQRLX)
         CLOSE (IOTMP)
C
C------------------------------------- find mapping of BOX on RLX regime
C
         ALLOCATE (IQRLX_IQBOX(NQBOXMAX),SEL_IQRLX(NQRLXMAX))
         ALLOCATE (IQBOX_IQRLX(NQRLXMAX),SEL0_IQRLX(NQRLXMAX))
         ALLOCATE (IOCCTYPE_IQRLX(NQRLXMAX))
C
         DO IQRLX = 1,NQRLX
            IQ = IQ_IQRLX(IQRLX)
            SEL_IQRLX(IQRLX) = 0
            IQBOX_IQRLX(IQRLX) = 0
            IOCCTYPE_IQRLX(IQRLX) = IOCCTYPE_IQ(IQ)
         END DO
C
         WRITE (6,*) 'relaxation box --  initial state'
         DO IQRLX = 1,NQRLX
            WRITE (6,'(I3,3F8.3,10I4)') IQRLX,(RQRLX(I,IQRLX),I=1,3),
     &                                  (N5VEC_IQRLX(I,IQRLX),I=1,5),
     &                                  IQ_IQRLX(IQRLX),
     &                                  IQBOX_IQRLX(IQRLX),
     &                                  IOCCTYPE_IQRLX(IQRLX)
         END DO
C
         DO IQBOX = 1,NQBOX
            FOUND = .FALSE.
            DO IQRLX = 1,NQRLX
               IF ( IQBOX_IQRLX(IQRLX).EQ.0 ) THEN
C
                  CALL MATCH_BOX_RLX(IQBOX,RQBOX,IQ_IQBOX,N5VEC_IQBOX,
     &                               IQRLX,RQRLX,IQ_IQRLX,N5VEC_IQRLX,
     &                               MATCH)
C
                  IF ( MATCH ) THEN
                     FOUND = .TRUE.
                     IQBOX_IQRLX(IQRLX) = IQBOX
                     IQRLX_IQBOX(IQBOX) = IQRLX
                     SEL_IQRLX(IQRLX) = SEL_IQBOX(IQBOX)
                     IOCCTYPE_IQRLX(IQRLX) = IOCCTYPE_IQBOX(IQBOX)
                     WRITE (6,99002) IQBOX,IQRLX
                     EXIT
                  END IF
               END IF
            END DO
C
            IF ( .NOT.FOUND ) WRITE (6,99003) IQBOX
         END DO
C
C--------------------------------------------------- add relaxation zone
C
         NQNEW = 0
         NQADD = 0
         ALLOCATE (IQRLX_INEW(NQRLX))
C
         SEL0_IQRLX(1:NQRLX) = SEL_IQRLX(1:NQRLX)
C
         DO IQRLX = 1,NQRLX
            IF ( SEL0_IQRLX(IQRLX).NE.0 ) THEN
               IQBOX = IQBOX_IQRLX(IQRLX)
               WRITE (6,99004) IQRLX,IQBOX,SQRT(CLURADSQ_IQBOX(IQBOX))
               DO JQRLX = 1,NQRLX
                  IF ( IQRLX.NE.JQRLX .AND. SEL_IQRLX(JQRLX).EQ.0 ) THEN
                     DSQIJ = 0D0
                     DO I = 1,3
                        AUX = RQRLX(I,IQRLX) - RQRLX(I,JQRLX)
                        DSQIJ = DSQIJ + AUX*AUX
                     END DO
C
                     IF ( (DSQIJ-1D-6).LT.CLURADSQ_IQBOX(IQBOX) ) THEN
C
                        JQBOX = IQBOX_IQRLX(JQRLX)
C
                        NQADD = NQADD + 1
C
                        IF ( JQBOX.NE.0 ) THEN
                           SEL_IQRLX(JQRLX) = 1
                           SEL_IQBOX(JQBOX) = 1
                           WRITE (6,*) 'relax site JQBOX',JQBOX
                        ELSE
                           NQNEW = NQNEW + 1
                           IQRLX_INEW(NQNEW) = JQRLX
                           SEL_IQRLX(JQRLX) = 1
                           IQ = IQ_IQRLX(JQRLX)
                           IOCCTYPE_IQRLX(JQRLX) = IOCCTYPE_IQ(IQ)
                           WRITE (6,*) 'relax site INEW ',NQNEW,
     &                                 ' NEW !!!!!!!!'
                        END IF
C
                     END IF
C
                  END IF
               END DO
            END IF
         END DO
C
         NCHK1 = 0
         DO IQRLX = 1,NQRLX
            IF ( SEL_IQRLX(IQRLX).NE.0 ) NCHK1 = NCHK1 + 1
         END DO
C
         WRITE (6,99011) 'NQNEW:   ',NQNEW
         WRITE (6,99011) 'NQSEL0:  ',NQSEL0
         WRITE (6,99011) 'NQADD:   ',NQADD
         NQSEL = NQSEL + NQADD
         WRITE (6,99011) 'NQSEL:   ',NQSEL
         IF ( NCHK1.NE.NQSEL ) THEN
            STOP '<NCHK1  .NE. NQSEL> '
         ELSE
            WRITE (6,99011) 'NCHK1:  ',NCHK1
         END IF
C
C----------------------------------------- fix limits for display regime
C
         DO IQRLX = 1,NQRLX
            IF ( SEL_IQRLX(IQRLX).NE.0 ) THEN
               DO I = 1,3
                  EXT_MAX(I) = MAX(EXT_MAX(I),RQRLX(I,IQRLX))
                  EXT_MIN(I) = MIN(EXT_MIN(I),RQRLX(I,IQRLX))
               END DO
            END IF
         END DO
C
         EXT_MAX(1:3) = EXT_MAX(1:3) + 1D-6
         EXT_MIN(1:3) = EXT_MIN(1:3) - 1D-6
         WRITE (6,99010) 'EXT_MAX:  ',EXT_MAX(1:3)
         WRITE (6,99010) 'EXT_MIN:  ',EXT_MIN(1:3)
C
C----------------------------------- contract relaxation box for display
C
         JQRLX = 0
         NQRLX0 = NQRLX
         DO IQRLX = 1,NQRLX
            INSIDE = .TRUE.
            DO I = 1,3
               IF ( RQRLX(I,IQRLX).GT.EXT_MAX(I) .OR. RQRLX(I,IQRLX)
     &              .LT.EXT_MIN(I) ) THEN
C                    INSIDE = .FALSE.
               END IF
            END DO
C
            IF ( INSIDE ) THEN
               JQRLX = JQRLX + 1
C
               RQRLX(1:3,JQRLX) = RQRLX(1:3,IQRLX)
               IQ_IQRLX(JQRLX) = IQ_IQRLX(IQRLX)
               N5VEC_IQRLX(1:5,JQRLX) = N5VEC_IQRLX(1:5,IQRLX)
               SEL_IQRLX(JQRLX) = SEL_IQRLX(IQRLX)
               IOCCTYPE_IQRLX(JQRLX) = IOCCTYPE_IQRLX(IQRLX)
            END IF
C
         END DO
C
         WRITE (6,99011) 'NQRLX (old) ',NQRLX0
         WRITE (6,99011) 'NQRLX (new) ',JQRLX
         NQRLX = JQRLX
C
C------------------------------------------ sort into layers for display
C
         ALLOCATE (IQ_ORDERED(NQRLXMAX),NDOTQ(NQRLXMAX))
C
         CALL IQ_NLAY_ORDER(NQRLX,RQRLX,IQ_ORDERED,NDOTQ,NVEC_LAY,
     &                      NQRLXMAX)
C
         ALLOCATE (IQRLX1_ILAY(NQRLXMAX),IQRLX2_ILAY(NQRLXMAX))
         ALLOCATE (PN_ILAY(NQRLXMAX))
C
         ILAYCNTR = 0
         NLAYRLX = 1
         IQRLX1_ILAY(NLAYRLX) = 1
         IQRLX_ORD = IQ_ORDERED(1)
         PA = NDOTQ(IQRLX_ORD)
         PN_ILAY(NLAYRLX) = PA
         N_EQ_0 = .TRUE.
         DO I = 1,5
            N_EQ_0 = N_EQ_0 .AND. N5VEC_IQRLX(I,IQRLX_ORD).EQ.0
         END DO
         IF ( N_EQ_0 .AND. IQ_IQRLX(IQRLX_ORD).EQ.IQCNTR )
     &        ILAYCNTR = NLAYRLX
C
         DO IQRLX = 2,NQRLX
            IQRLX_ORD = IQ_ORDERED(IQRLX)
            N_EQ_0 = .TRUE.
            DO I = 1,5
               N_EQ_0 = N_EQ_0 .AND. N5VEC_IQRLX(I,IQRLX_ORD).EQ.0
            END DO
            IF ( N_EQ_0 .AND. IQ_IQRLX(IQRLX_ORD).EQ.IQCNTR )
     &           ILAYCNTR = NLAYRLX
C
            PB = NDOTQ(IQRLX_ORD)
            IF ( ABS(PA-PB).GT.1D-8 ) THEN
               IQRLX2_ILAY(NLAYRLX) = IQRLX - 1
               PA = PB
               NLAYRLX = NLAYRLX + 1
               IQRLX1_ILAY(NLAYRLX) = IQRLX
               PN_ILAY(NLAYRLX) = PA
            END IF
         END DO
         IQRLX2_ILAY(NLAYRLX) = NQRLX
C
         IF ( ILAYCNTR.EQ.0 ) STOP 'ILAYCNTR = 0 !!!!!!!!!!!!!!!!!!!!!'
C
         WRITE (IWR,'(A)') 'GEOMETRY:  rectangular box for '
         WRITE (IWR,'(A,I10)') 'IQCNTR      = ',IQCNTR
         WRITE (IWR,'(A,A)') 'ANCHOR_MODE = ',ANCHOR_MODE
         WRITE (IWR,'(A,3F10.3)') 'NVEC_LAY    = ',NVEC_LAY
         WRITE (IWR,'(A,I10)') 'NQRLX       = ',NQRLX
         WRITE (IWR,'(A,I10)') 'NLAYRLX     = ',NLAYRLX
         WRITE (IWR,'(A,I10)') 'ILAYCNTR    = ',ILAYCNTR
         WRITE (IWR,'('' ILAY  IQ1  IQ2     PROJECT'')')
         DO ILAY = 1,NLAYRLX
            WRITE (IWR,'(3I5,F15.10)') ILAY,IQRLX1_ILAY(ILAY),
     &                                 IQRLX2_ILAY(ILAY),PN_ILAY(ILAY)
         END DO
C
         WRITE (IWR,99007)
         NCHK1 = 0
         DO IQRLX = 1,NQRLX
            IQRLX_ORD = IQ_ORDERED(IQRLX)
            WRITE (IWR,99005) IQRLX,(RQRLX(I,IQRLX_ORD),I=1,3),
     &                        (N5VEC_IQRLX(I,IQRLX_ORD),I=1,5),
     &                        IQ_IQRLX(IQRLX_ORD),SEL_IQRLX(IQRLX_ORD),
     &                        IOCCTYPE_IQRLX(IQRLX_ORD)
            IF ( SEL_IQRLX(IQRLX_ORD).NE.0 ) NCHK1 = NCHK1 + 1
         END DO
         IF ( NCHK1.NE.NQSEL ) THEN
            STOP '<NCHK1  .NE. NQSEL> '
         ELSE
            WRITE (6,99011) 'NQSEL:   ',NQSEL
            WRITE (6,99011) 'NCHK1:  ',NCHK1
         END IF
C
         OPEN (IWSCR,FILE='cluster_data.ras')
         WRITE (IWSCR,'(A)') 'load "cluster_data.pdb"'
         WRITE (IWSCR,'(A)') 'color temperature'
         WRITE (IWSCR,'(A)') 'background white'
         WRITE (IWSCR,'(A)') 'label false'
         WRITE (IWSCR,'(A)') 'set fontsize 20'
C
         DO IQRLX = 1,NQRLX
            WRITE (IWSCR,'(A,I5)') 'select ',IQRLX
            WRITE (IWSCR,'(A,I5)') 'cpk  ',30
         END DO
C
C
C=======================================================================
      ELSE IF ( TASK.EQ.'cr_3D_surface' ) THEN
C=======================================================================
C
         READ (1,'(A80)') LINE
         READ (1,*) IQCNTR
         WRITE (6,99011) LINE(1:30),IQCNTR
C
         READ (1,'(A80)') LINE
         READ (1,*) MILLER_INDICES(1:3)
         WRITE (6,99011) LINE(1:30),MILLER_INDICES
C
         READ (1,'(A80)') LINE
         READ (1,*) STR15
         WRITE (6,99014) LINE(1:30),STR15
         USE_CRY_PRIM_VECS = STR15(1:5).EQ.'cryst'
C
         CALL CREATE_3D_SURFACE(IWR,BRAVAIS,USE_CRY_PRIM_VECS,
     &                          MILLER_INDICES,ABAS,BBAS,BDBINV,NQ,NT,
     &                          QBAS,NOQ,NAT,IQAT,ITOQ,VOLUC,IERROR,
     &                          NQMAX,NTMAX)
C
C=======================================================================
      ELSE
C
         STOP ' >>>>>>>>>>  TASK not found/implemented'
C
      END IF
C
 300  CONTINUE
      WRITE (6,*) 'run completed -- ERROR FLAG = ',IERROR
      STOP
 400  CONTINUE
      STOP 'end of system file reached !!!!!!!!!!!!!!???????????'
99001 FORMAT (3X,'IQBOX =',I4,3X,'NSHLCLU =',I4,3X,' CLURAD =',F8.4)
99002 FORMAT ('matching  IQBOX =',I4,'  ---  IQRLX =',I4)
99003 FORMAT ('matching  IQBOX =',I4,'  ---  NOT FOUND !!!!')
99004 FORMAT ('**** selected IQRLX = ',I4,'   ==  IQBOX =',I4,3X,
     &        'with R =',F8.4)
99005 FORMAT (I5,3F20.15,5I4,3I5)
99006 FORMAT (' IQBOX     RQBOX X         RQBOX Y         RQBOX Z',
     &        '     N1  N2  N3 N3L N3R   IQ')
99007 FORMAT (' IQBOX     RQBOX X         RQBOX Y         RQBOX Z',
     &        '     N1  N2  N3 N3L N3R   IQ  SEL OCCTYPE')
99008 FORMAT (A,3I10,F10.3)
99009 FORMAT (A,4F10.3)
99010 FORMAT (A30,10F10.6)
99011 FORMAT (A30,10I5)
99012 FORMAT (A,10I5)
99013 FORMAT (A,L5,I5)
99014 FORMAT (A,A)
99015 FORMAT (/,1X,79('*'))
99016 FORMAT (10X,A,9X,A,I10,:,7X,'(',I7,')')
      END
C*==sph_cluster_sites.f    processed by SPAG 6.70Rc at 18:37 on  3 Mar 2013
      SUBROUTINE SPH_CLUSTER_SITES(IOTMP,DIMENSION,LATTICE_TYPE,ABAS,
     &                             ABAS_L,ABAS_I,ABAS_R,ADAINV_L,
     &                             ADAINV_I,ADAINV_R,QBAS,CLURAD,IQCNTR,
     &                             NQCLU,NQCLU_L,NQCLU_S,NQCLU_R,
     &                             NSHLCLU,NQ,NQ_BULK_L,NQ_BULK_R,NQMAX)
C   ********************************************************************
C   *                                                                  *
C   * find the configuration of a cluster around a central site IQCNTR *
C   * either specified by the cluster of radius  CLURAD                *
C   * or the number of atomic shells             NSHLCLU               *
C   *                                                                  *
C   * ---------------------------------------------------------------- *
C   *                                                                  *
C   * the subroutine determines the cluster parameters:                *
C   *                                                                  *
C   * RQCLU,DQCLU,IQ_IQCLU,NQSHLCLU                                    *
C   *                                                                  *
C   * - a rough estimate is made first for the corresponding           *
C   *   array sizes   NQCLUMAX  and  NSHLCLUMAX                        *
C   *   using these the storage is allocated                           *
C   * - the actual array sizes   NQCLU  and  NSHLCLU  are passed       *
C   *   to the calling subroutine via the argument list                *
C   * - the data are passed via the temporary file  IOTMP              *
C   *                                                                  *
C   ********************************************************************
C
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      REAL*8 CLURAD
      CHARACTER*10 DIMENSION
      INTEGER IOTMP,IQCNTR,NQ,NQCLU,NQCLU_L,NQCLU_R,NQCLU_S,NQMAX,
     &        NQ_BULK_L,NQ_BULK_R,NSHLCLU
      CHARACTER*80 LATTICE_TYPE
      REAL*8 ABAS(3,3),ABAS_I(3,3),ABAS_L(3,3),ABAS_R(3,3),ADAINV_I(3,3)
     &       ,ADAINV_L(3,3),ADAINV_R(3,3),QBAS(3,NQMAX)
C
C Local variables
C
      REAL*8 BON(3,3),BR(3),CEXT,DC(3),DQ(3),DQCLU(:),DR,DSH(:),DTMP,
     &       D_C,D_L,D_R,D_S,RCOF(3),RCOR(3),RQCLU(:,:),TOL
      REAL*8 DDOT,DNRM2
      INTEGER I,I1,I2,I3,IA_ERR,IBRCLU(:,:),IERROR,IEXT,IQ,IQ1,IQ2,
     &        IQ_IQCLU(:),ISH,IX,IY,IZ,J,JQ,JSH,KSH,NBR(3),NMAX(3),
     &        NMAX_L(3),NMAX_R(3),NMAX_S(3),NMIN(3),NMIN_L(3),NMIN_R(3),
     &        NMIN_S(3),NQCLUMAX,NQSHLCLU(:),NSH,NSHLCLUMAX,NTMP
C
C*** End of declarations rewritten by SPAG
C
      DATA IA_ERR/0/
C
      ALLOCATABLE DSH,RQCLU,DQCLU,IQ_IQCLU,NQSHLCLU,IBRCLU
C
      WRITE (6,99003)
C
      IQ1 = 1
      IQ2 = NQ
C
      DO I = 1,3
         BR(I) = DNRM2(3,ABAS(1,I),1)
         NBR(I) = INT(CLURAD/BR(I)) + 2
      END DO
C
C ----------------------------------------------------------------------
C       if NSHLCLU is given: find first corresponding radius CLURAD
C ----------------------------------------------------------------------
C
      TOL = 1.0D-4
      IF ( NSHLCLU.NE.0 ) THEN
C
         IF ( LATTICE_TYPE.EQ.'heterogeneous' ) THEN
            WRITE (6,'(A)') 'LATTICE_TYPE = heterogeneous'
            WRITE (6,'(A)') 'setting  NSHLCLU  in input not allowed'
            WRITE (6,'(A)') 'specify  CLURAD   instead '
            STOP
         END IF
C
         ALLOCATE (DSH(2*NSHLCLU),STAT=IA_ERR)
         IF ( IA_ERR.NE.0 ) STOP 'alloc:SPH_CLUSTER_SITES -> DSH'
C
         CLURAD = 3D0
 50      CONTINUE
         CLURAD = CLURAD*1.05D0
C
         DO I = 1,3
            NBR(I) = INT(CLURAD/BR(I)) + 2
         END DO
C
         NSH = 0
         DSH(1) = 1D10
C
         DO JQ = 1,NQ
C
            DO I = 1,3
               DQ(I) = QBAS(I,JQ) - QBAS(I,IQCNTR)
            END DO
            DO I1 = -NBR(1), + NBR(1)
               DO I2 = -NBR(2), + NBR(2)
                  DO I3 = -NBR(3), + NBR(3)
C
                     DO I = 1,3
                        DC(I) = I1*ABAS(I,1) + I2*ABAS(I,2)
     &                          + I3*ABAS(I,3) + DQ(I)
                     END DO
                     DR = DNRM2(3,DC,1)
C
                     IF ( DR.GT.TOL ) THEN
                        DO I = 1,NSH
                           IF ( ABS(DR-DSH(I)).LT.TOL ) GOTO 55
                           IF ( DR.LT.(DSH(I)-TOL) ) THEN
                              DO J = MIN(NSH,NSHLCLU-1),I, - 1
                                 DSH(J+1) = DSH(J)
                              END DO
                              DSH(I) = DR
                              GOTO 55
                           END IF
                        END DO
                        IF ( NSH.LT.NSHLCLU ) THEN
                           NSH = NSH + 1
                           DSH(NSH) = DR
                        END IF
                     END IF
C
 55               END DO
               END DO
            END DO
C
         END DO
C
         IF ( NSH.LT.NSHLCLU ) GOTO 50
         CLURAD = DSH(NSH) + 1D-6
C
      END IF
C
C ----------------------------------------------------------------------
C                   allocate temporary arrays
C ----------------------------------------------------------------------
C
      NQCLUMAX = NQ
      DO I = 1,3
         NQCLUMAX = NQCLUMAX*(INT(2*CLURAD/BR(I))+1)
      END DO
      NSHLCLUMAX = NQCLUMAX
C
      ALLOCATE (RQCLU(3,NQCLUMAX),DQCLU(NQCLUMAX),IBRCLU(3,NQCLUMAX))
      ALLOCATE (IQ_IQCLU(NQCLUMAX),NQSHLCLU(NSHLCLUMAX),STAT=IA_ERR)
      IF ( IA_ERR.NE.0 ) STOP 'ALLOC:SPH_CLUSTER_SITES -> IQCLUS'
C
      IF ( .NOT.ALLOCATED(DSH) ) ALLOCATE (DSH(NSHLCLUMAX),STAT=IA_ERR)
      IF ( IA_ERR.NE.0 ) STOP 'alloc:SPH_CLUSTER_SITES -> DSH'
      GOTO 200
C
 100  CONTINUE
      DEALLOCATE (RQCLU,DQCLU,IBRCLU,IQ_IQCLU,NQSHLCLU,DSH)
      NQCLUMAX = NINT(NQCLUMAX*1.2)
      NSHLCLUMAX = NINT(NSHLCLUMAX*1.2)
      ALLOCATE (RQCLU(3,NQCLUMAX),DQCLU(NQCLUMAX),IBRCLU(3,NQCLUMAX))
      ALLOCATE (IQ_IQCLU(NQCLUMAX),NQSHLCLU(NSHLCLUMAX),STAT=IA_ERR)
      ALLOCATE (DSH(NSHLCLUMAX),STAT=IA_ERR)
C
 200  CONTINUE
      IF ( DIMENSION(1:2).EQ.'3D' ) THEN
C=======================================================================
C
         DO I = 1,3
            NMIN(I) = -NBR(I)
            NMAX(I) = NBR(I)
         END DO
         WRITE (6,99001) ' NMIN     = ',NMIN
         WRITE (6,99001) ' NMAX     = ',NMAX
C
         NQCLU = 0
C
         IQ1 = 1
         IQ2 = NQ
         CALL SPHCLUADD(IERROR,CLURAD,NMIN,NMAX,ABAS,DQ,QBAS,IQ1,IQ2,
     &                  IQCNTR,RQCLU,DQCLU,IBRCLU,IQ_IQCLU,NQCLU,
     &                  NQCLUMAX,NQMAX)
         IF ( IERROR.EQ.1 ) GOTO 100
C
      ELSE
C=======================================================================
C
         DO I = 1,3
            NMIN_L(I) = 0
            NMAX_L(I) = 0
            NMIN_S(I) = 0
            NMAX_S(I) = 0
            NMIN_R(I) = 0
            NMAX_R(I) = 0
         END DO
C
         CALL RVECORTHO(ABAS,BON)
C
         CEXT = CLURAD*1.001D0
C
         DO IX = -1,1,2
            DO IY = -1,1,2
               DO IZ = -1,1,2
                  RCOR(1) = QBAS(1,IQCNTR) + IX*CEXT
                  RCOR(2) = QBAS(2,IQCNTR) + IY*CEXT
                  RCOR(3) = QBAS(3,IQCNTR) + IZ*CEXT
C
                  CALL RVECEXPD(RCOR,ABAS_L,ADAINV_L,RCOF)
C
                  DO I = 1,3
                     IF ( RCOF(I).GT.0D0 ) THEN
                        IEXT = INT(RCOF(I)) + 2
                     ELSE
                        IEXT = INT(RCOF(I)) - 1
                     END IF
                     NMIN_L(I) = MIN(NMIN_L(I),IEXT)
                     NMAX_L(I) = MAX(NMAX_L(I),IEXT)
                  END DO
C
                  DO I = 1,3
                     RCOR(I) = RCOR(I) - ABAS_L(I,3)
                  END DO
C
                  CALL RVECEXPD(RCOR,ABAS_I,ADAINV_I,RCOF)
C
                  DO I = 1,3
                     IF ( RCOF(I).GT.0D0 ) THEN
                        IEXT = INT(RCOF(I)) + 2
                     ELSE
                        IEXT = INT(RCOF(I)) - 1
                     END IF
                     NMIN_S(I) = MIN(NMIN_S(I),IEXT)
                     NMAX_S(I) = MAX(NMAX_S(I),IEXT)
                  END DO
C
                  DO I = 1,3
                     RCOR(I) = RCOR(I) - ABAS_I(I,3)
                  END DO
C
                  CALL RVECEXPD(RCOR,ABAS_R,ADAINV_R,RCOF)
C
                  DO I = 1,3
                     IF ( RCOF(I).GT.0D0 ) THEN
                        IEXT = INT(RCOF(I)) + 2
                     ELSE
                        IEXT = INT(RCOF(I)) - 1
                     END IF
                     NMIN_R(I) = MIN(NMIN_R(I),IEXT)
                     NMAX_R(I) = MAX(NMAX_R(I),IEXT)
                  END DO
               END DO
            END DO
         END DO
C
         D_C = ABS(DDOT(3,QBAS(1,IQCNTR),1,BON(1,3),1))
         D_L = ABS(DDOT(3,ABAS_L(1,3),1,BON(1,3),1))
         D_S = ABS(DDOT(3,ABAS_I(1,3),1,BON(1,3),1))
         D_R = ABS(DDOT(3,ABAS_R(1,3),1,BON(1,3),1))
C
         NMIN_L(3) = -INT((CLURAD-D_C)/D_L) - 1
         NMAX_L(3) = 0
         NMIN_S(3) = 0
         NMAX_S(3) = 0
         NMIN_R(3) = 0
         NMAX_R(3) = INT((CLURAD-D_L-D_S+D_C)/D_R) + 1
C
         WRITE (6,99002) ' CLURAD   = ',CLURAD
         WRITE (6,99002) ' QCTR     = ',(QBAS(1,IQCNTR),I=1,3),D_C
         WRITE (6,99001) ' NMIN (L) = ',NMIN_L,D_L
         WRITE (6,99001) ' NMAX (L) = ',NMAX_L
         WRITE (6,99001) ' NMIN (S) = ',NMIN_S,D_S
         WRITE (6,99001) ' NMAX (S) = ',NMAX_S
         WRITE (6,99001) ' NMIN (R) = ',NMIN_R,D_R
         WRITE (6,99001) ' NMAX (R) = ',NMAX_R
C
C ----------------------------------------------------------------------
C
         NQCLU = 0
C
         IQ1 = 1
         IQ2 = NQ_BULK_L
         CALL SPHCLUADD(IERROR,CLURAD,NMIN_L,NMAX_L,ABAS_L,DQ,QBAS,IQ1,
     &                  IQ2,IQCNTR,RQCLU,DQCLU,IBRCLU,IQ_IQCLU,NQCLU,
     &                  NQCLUMAX,NQMAX)
         IF ( IERROR.EQ.1 ) GOTO 100
         NQCLU_L = NQCLU
C
         IQ1 = NQ_BULK_L + 1
         IQ2 = NQ - NQ_BULK_R
         CALL SPHCLUADD(IERROR,CLURAD,NMIN_S,NMAX_S,ABAS_I,DQ,QBAS,IQ1,
     &                  IQ2,IQCNTR,RQCLU,DQCLU,IBRCLU,IQ_IQCLU,NQCLU,
     &                  NQCLUMAX,NQMAX)
         IF ( IERROR.EQ.1 ) GOTO 100
         NQCLU_S = NQCLU - NQCLU_L
C
         IQ1 = NQ - NQ_BULK_R + 1
         IQ2 = NQ
         CALL SPHCLUADD(IERROR,CLURAD,NMIN_R,NMAX_R,ABAS_R,DQ,QBAS,IQ1,
     &                  IQ2,IQCNTR,RQCLU,DQCLU,IBRCLU,IQ_IQCLU,NQCLU,
     &                  NQCLUMAX,NQMAX)
         IF ( IERROR.EQ.1 ) GOTO 100
         NQCLU_R = NQCLU - NQCLU_L - NQCLU_S
C
      END IF
C=======================================================================
C
      NSHLCLU = 0
      NQSHLCLU(1) = 1
C
      DO IQ = 1,NQCLU
C
         DO ISH = 1,NSHLCLU
            IF ( ABS(DQCLU(IQ)-DSH(ISH)).LT.TOL ) THEN
               NQSHLCLU(ISH) = NQSHLCLU(ISH) + 1
               GOTO 300
            END IF
         END DO
C
         NSHLCLU = NSHLCLU + 1
         DSH(NSHLCLU) = DQCLU(IQ)
         NQSHLCLU(NSHLCLU) = 1
C
C
 300  END DO
C
      DO ISH = 2,NSHLCLU
         DO JSH = 1,(ISH-1)
            IF ( DSH(ISH).LT.DSH(JSH) ) THEN
               NTMP = NQSHLCLU(ISH)
               DTMP = DSH(ISH)
               DO KSH = (ISH-1),JSH, - 1
                  NQSHLCLU(KSH+1) = NQSHLCLU(KSH)
                  DSH(KSH+1) = DSH(KSH)
               END DO
               NQSHLCLU(JSH) = NTMP
               DSH(JSH) = DTMP
               EXIT
            END IF
         END DO
      END DO
C
      DO ISH = 1,NSHLCLU
         WRITE (6,99004) ISH,NQSHLCLU(ISH),DSH(ISH)
      END DO
      WRITE (6,99005) CLURAD,IQCNTR,NSHLCLU
      IF ( DIMENSION(1:2).NE.'3D' ) WRITE (6,99007) NQCLU_L,NQCLU_S,
     &     NQCLU_R
      WRITE (6,99006) NQCLU
C
C ----------------------------------------------------------------------
C  write results to temporary file for data transfer to calling routine
C ----------------------------------------------------------------------
C
      OPEN (UNIT=IOTMP,STATUS='SCRATCH',FORM='UNFORMATTED')
      WRITE (IOTMP) ((RQCLU(J,I),J=1,3),DQCLU(I),IQ_IQCLU(I),I=1,NQCLU),
     &              (NQSHLCLU(I),I=1,NSHLCLU)
      WRITE (IOTMP) ((IBRCLU(J,I),J=1,3),I=1,NQCLU)
      REWIND IOTMP
C
C ----------------------------------------------------------------------
      IF ( ALLOCATED(DSH) ) DEALLOCATE (DSH,STAT=IA_ERR)
      IF ( IA_ERR.NE.0 ) STOP 'dealloc:SPH_CLUSTER_SITES -> DSH'
C
      DEALLOCATE (RQCLU,DQCLU,IQ_IQCLU,NQSHLCLU,STAT=IA_ERR)
      IF ( IA_ERR.NE.0 ) STOP 'dealloc:SPH_CLUSTER_SITES -> RSCLUS'
C
C ----------------------------------------------------------------------
99001 FORMAT (A,3I10,F10.3)
99002 FORMAT (A,4F10.3)
99003 FORMAT (/,1X,79('*'),/,21X,'<SPH_CLUSTER_SITES>',/,1X,79('*'),/)
99004 FORMAT (10X,'shell ',I3,' with ',I3,' sites ',' at distance ',
     &        F10.4)
99005 FORMAT (/,10X,'cluster of radius  ',F7.3,' a ',
     &        '  around central site  IQ=',I5,:,/,10X,
     &        'number of shells in cluster      NSHLCLU  = ',I5)
99006 FORMAT (10X,'total number of sites in cluster NQCLU    = ',I5,/,
     &        10X,'INCLUDING central site !',/)
99007 FORMAT (10X,'number of cluster sites  NQCLU  L | S | R = ',3I5)
      END
C*==sphcluadd.f    processed by SPAG 6.70Rc at 18:37 on  3 Mar 2013
      SUBROUTINE SPHCLUADD(IERROR,CLURAD,NMIN,NMAX,ABAS,DQ,QBAS,IQ1,IQ2,
     &                     IQCNTR,RQCLU,DQCLU,IBRCLU,IQ_IQCLU,NQCLU,
     &                     NQCLUMAX,NQMAX)
C   ********************************************************************
C   *                                                                  *
C   *                                                                  *
C   ********************************************************************
C
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      REAL*8 CLURAD
      INTEGER IERROR,IQ1,IQ2,IQCNTR,NQCLU,NQCLUMAX,NQMAX
      REAL*8 ABAS(3,3),DQ(3),DQCLU(NQCLUMAX),QBAS(3,NQMAX),
     &       RQCLU(3,NQCLUMAX)
      INTEGER IBRCLU(3,NQCLUMAX),IQ_IQCLU(NQCLUMAX),NMAX(3),NMIN(3)
C
C Local variables
C
      REAL*8 DC(3),DR
      REAL*8 DNRM2
      INTEGER I,IQ,L,M,N
C
C*** End of declarations rewritten by SPAG
C
      IERROR = 0
C
      DO IQ = IQ1,IQ2
C
         DO I = 1,3
            DQ(I) = QBAS(I,IQ) - QBAS(I,IQCNTR)
         END DO
C
         DO N = NMIN(3),NMAX(3)
            DO M = NMIN(2),NMAX(2)
               DO L = NMIN(1),NMAX(1)
C
                  DO I = 1,3
                     DC(I) = L*ABAS(I,1) + M*ABAS(I,2) + N*ABAS(I,3)
     &                       + DQ(I)
                  END DO
                  DR = DNRM2(3,DC,1)
                  IF ( DR.LE.CLURAD ) THEN
                     NQCLU = NQCLU + 1
                     IF ( NQCLU.LE.NQCLUMAX ) THEN
                        DO I = 1,3
                           RQCLU(I,NQCLU) = DC(I) + QBAS(I,IQCNTR)
                        END DO
                        DQCLU(NQCLU) = DR
                        IQ_IQCLU(NQCLU) = IQ
                        IBRCLU(1,NQCLU) = L
                        IBRCLU(2,NQCLU) = M
                        IBRCLU(3,NQCLU) = N
                     ELSE
                        WRITE (6,99001) NQCLUMAX
                        IERROR = 1
                        RETURN
                     END IF
                  END IF
               END DO
            END DO
         END DO
C
      END DO
C
99001 FORMAT (/,5X,'WARNING: NQCLUMAX too small for  NQCLU=',I8,/,5X,
     &        'WARNING: NQCLUMAX will be increased')
      END
C*==recangbox.f    processed by SPAG 6.70Rc at 18:37 on  3 Mar 2013
      SUBROUTINE RECANGBOX(IOTMP,IWDAT,DIMENSION,ABAS,ABAS_L,ABAS_I,
     &                     ABAS_R,ADAINV,ADAINV_L,ADAINV_I,ADAINV_R,VUC,
     &                     NQ,NQ_BULK_L,NQ_BULK_R,QBAS,ABAS_BOX,
     &                     RCNTR_BOX,L_BOX,RSCL,IOFFSET,NQBOX,NQBOX_L,
     &                     NQBOX_S,NQBOX_R,NQMAX)
C   ********************************************************************
C   *                                                                  *
C   *                                                                  *
C   ********************************************************************
C
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      CHARACTER*10 DIMENSION
      INTEGER IOFFSET,IOTMP,IWDAT,NQ,NQBOX,NQBOX_L,NQBOX_R,NQBOX_S,
     &        NQMAX,NQ_BULK_L,NQ_BULK_R
      REAL*8 RSCL,VUC
      REAL*8 ABAS(3,3),ABAS_BOX(3,3),ABAS_I(3,3),ABAS_L(3,3),ABAS_R(3,3)
     &       ,ADAINV(3,3),ADAINV_I(3,3),ADAINV_L(3,3),ADAINV_R(3,3),
     &       L_BOX(3),QBAS(3,NQMAX),RCNTR_BOX(3)
C
C Local variables
C
      REAL*8 DC,DQ(3),RCOF(3),RCOR(3),RQBOX(:,:),V_BOX
      INTEGER I,I1,I2,I3,IA_ERR,IBV3,IERROR,IEXT,II(3),IQ1,IQ2,IQBOX,
     &        IQ_IQBOX(:),J,N5VEC_IQBOX(:,:),NMAX(3),NMAX_L(3),NMAX_R(3)
     &        ,NMAX_S(3),NMIN(3),NMIN_L(3),NMIN_R(3),NMIN_S(3),NQBOXMAX
C
C*** End of declarations rewritten by SPAG
C
      DATA IA_ERR/0/
C
      ALLOCATABLE RQBOX,IQ_IQBOX,N5VEC_IQBOX
C
      V_BOX = L_BOX(1)*L_BOX(2)*L_BOX(3)
C
      NQBOXMAX = (NINT(2D0*V_BOX/VUC)+1)*NQ
      ALLOCATE (RQBOX(3,NQBOXMAX))
      ALLOCATE (IQ_IQBOX(NQBOXMAX),N5VEC_IQBOX(5,NQBOXMAX))
      GOTO 200
 100  CONTINUE
      NQBOXMAX = NINT(NQBOXMAX*1.1D0)
      DEALLOCATE (RQBOX,IQ_IQBOX,N5VEC_IQBOX)
      ALLOCATE (RQBOX(3,NQBOXMAX))
      ALLOCATE (IQ_IQBOX(NQBOXMAX),N5VEC_IQBOX(5,NQBOXMAX))
C
 200  CONTINUE
      DO I = 1,5
         DO IQBOX = 1,NQBOXMAX
            N5VEC_IQBOX(I,IQBOX) = 0
         END DO
      END DO
      NQBOX = 0
      IQ1 = 1
      IQ2 = NQ
C
      DO I = 1,3
         NMIN(I) = 0
         NMAX(I) = 0
         NMIN_L(I) = 0
         NMAX_L(I) = 0
         NMIN_S(I) = 0
         NMAX_S(I) = 0
         NMIN_R(I) = 0
         NMAX_R(I) = 0
      END DO
C
      IF ( DIMENSION(1:2).EQ.'3D' ) THEN
C=======================================================================
C
         DO I1 = -1,1,2
            II(1) = I1
            DO I2 = -1,1,2
               II(2) = I2
               DO I3 = -1,1,2
                  II(3) = I3
C
                  DO I = 1,3
                     DC = 0D0
                     DO J = 1,3
                        DC = DC + II(J)*ABAS_BOX(I,J)*L_BOX(J)/2D0
                     END DO
                     RCOR(I) = RCNTR_BOX(I) + DC
                  END DO
C
                  CALL RVECEXPD(RCOR,ABAS,ADAINV,RCOF)
C
                  DO I = 1,3
                     IF ( RCOF(I).GT.0D0 ) THEN
                        IEXT = INT(RCOF(I)) + 2
                     ELSE
                        IEXT = INT(RCOF(I)) - 1
                     END IF
                     NMIN(I) = MIN(NMIN(I),IEXT)
                     NMAX(I) = MAX(NMAX(I),IEXT)
                  END DO
               END DO
            END DO
         END DO
         WRITE (6,99002) ' QCTR     = ',(RCNTR_BOX(1),I=1,3)
         WRITE (6,99001) ' NMIN     = ',NMIN
         WRITE (6,99001) ' NMAX     = ',NMAX
C
         IBV3 = 3
         CALL RECANGBOXADD(IERROR,NMIN,NMAX,ABAS,DQ,QBAS,IQ1,IQ2,
     &                     ABAS_BOX,RCNTR_BOX,L_BOX,RQBOX,IQ_IQBOX,
     &                     N5VEC_IQBOX,IBV3,NQBOX,NQBOXMAX,NQMAX)
         IF ( IERROR.EQ.1 ) GOTO 100
C
      ELSE
C=======================================================================
C
         DO I1 = -1,1,2
            II(1) = I1
            DO I2 = -1,1,2
               II(2) = I2
               DO I3 = -1,1,2
                  II(3) = I3
C
                  DO I = 1,3
                     DC = 0D0
                     DO J = 1,3
                        DC = DC + II(J)*ABAS_BOX(I,J)*L_BOX(J)/2D0
                     END DO
                     RCOR(I) = RCNTR_BOX(I) + DC
                  END DO
C
                  CALL RVECEXPD(RCOR,ABAS_L,ADAINV_L,RCOF)
C
                  DO I = 1,3
                     IF ( RCOF(I).GT.0D0 ) THEN
                        IEXT = INT(RCOF(I)) + 2
                     ELSE
                        IEXT = INT(RCOF(I)) - 1
                     END IF
                     NMIN_L(I) = MIN(NMIN_L(I),IEXT)
                     NMAX_L(I) = MAX(NMAX_L(I),IEXT)
                  END DO
C
                  DO I = 1,3
                     RCOR(I) = RCOR(I) - ABAS_L(I,3)
                  END DO
C
                  CALL RVECEXPD(RCOR,ABAS_I,ADAINV_I,RCOF)
C
                  DO I = 1,3
                     IF ( RCOF(I).GT.0D0 ) THEN
                        IEXT = INT(RCOF(I)) + 2
                     ELSE
                        IEXT = INT(RCOF(I)) - 1
                     END IF
                     NMIN_S(I) = MIN(NMIN_S(I),IEXT)
                     NMAX_S(I) = MAX(NMAX_S(I),IEXT)
                  END DO
C
                  DO I = 1,3
                     RCOR(I) = RCOR(I) - ABAS_I(I,3)
                  END DO
C
                  CALL RVECEXPD(RCOR,ABAS_R,ADAINV_R,RCOF)
C
                  DO I = 1,3
                     IF ( RCOF(I).GT.0D0 ) THEN
                        IEXT = INT(RCOF(I)) + 2
                     ELSE
                        IEXT = INT(RCOF(I)) - 1
                     END IF
                     NMIN_R(I) = MIN(NMIN_R(I),IEXT)
                     NMAX_R(I) = MAX(NMAX_R(I),IEXT)
                  END DO
               END DO
            END DO
         END DO
C
         NMAX_L(3) = 0
         NMIN_S(3) = 0
         NMAX_S(3) = 0
         NMIN_R(3) = 0
C
         WRITE (6,99002) ' QCTR     = ',(RCNTR_BOX(I),I=1,3)
         WRITE (6,99001) ' NMIN (L) = ',NMIN_L
         WRITE (6,99001) ' NMAX (L) = ',NMAX_L
         WRITE (6,99001) ' NMIN (S) = ',NMIN_S
         WRITE (6,99001) ' NMAX (S) = ',NMAX_S
         WRITE (6,99001) ' NMIN (R) = ',NMIN_R
         WRITE (6,99001) ' NMAX (R) = ',NMAX_R
C
C................................................................ L BULK
         IQ1 = 1
         IQ2 = NQ_BULK_L
         IBV3 = 4
         write(*,'(A,/,(3f10.4))') 'basis vectors L-BULK ',ABAS_L
         CALL RECANGBOXADD(IERROR,NMIN_L,NMAX_L,ABAS_L,DQ,QBAS,IQ1,IQ2,
     &                     ABAS_BOX,RCNTR_BOX,L_BOX,RQBOX,IQ_IQBOX,
     &                     N5VEC_IQBOX,IBV3,NQBOX,NQBOXMAX,NQMAX)
         IF ( IERROR.EQ.1 ) GOTO 100
C
         NQBOX_L = NQBOX
C
C...................................................... INTERACTION ZONE
         IQ1 = NQ_BULK_L + 1
         IQ2 = NQ - NQ_BULK_R
         IBV3 = 3
         write(*,'(A,/,(3f10.4))') 'basis vectors I-ZONE ',ABAS_I
         CALL RECANGBOXADD(IERROR,NMIN_S,NMAX_S,ABAS_I,DQ,QBAS,IQ1,IQ2,
     &                     ABAS_BOX,RCNTR_BOX,L_BOX,RQBOX,IQ_IQBOX,
     &                     N5VEC_IQBOX,IBV3,NQBOX,NQBOXMAX,NQMAX)
         IF ( IERROR.EQ.1 ) GOTO 100
C
         NQBOX_S = NQBOX - NQBOX_L
C
C................................................................ R BULK
         IQ1 = NQ - NQ_BULK_R + 1
         IQ2 = NQ
         IBV3 = 5
         write(*,'(A,/,(3f10.4))') 'basis vectors R-BULK ',ABAS_R
         CALL RECANGBOXADD(IERROR,NMIN_R,NMAX_R,ABAS_R,DQ,QBAS,IQ1,IQ2,
     &                     ABAS_BOX,RCNTR_BOX,L_BOX,RQBOX,IQ_IQBOX,
     &                     N5VEC_IQBOX,IBV3,NQBOX,NQBOXMAX,NQMAX)
         IF ( IERROR.EQ.1 ) GOTO 100
C
         NQBOX_R = NQBOX - NQBOX_L - NQBOX_S
C
C=======================================================================
      END IF
C
      WRITE (6,'(A, 2I10  )') ' NQBOX    = ',NQBOX,NQBOXMAX
C
      OPEN (UNIT=IOTMP,STATUS='SCRATCH',FORM='UNFORMATTED')
      WRITE (IOTMP) NQBOX
      WRITE (IOTMP) ((RQBOX(I,IQBOX),I=1,3),IQ_IQBOX(IQBOX),
     &              (N5VEC_IQBOX(I,IQBOX),I=1,5),IQBOX=1,NQBOX)
C
      DO IQBOX = 1,NQBOX
c        WRITE (6,'(i5,1x,3F10.3)') IQBOX,(RQBOX(J,IQBOX),J=1,3)
         I = IOFFSET + IQBOX
         J = I
         CALL WRITE_COORD(IWDAT,'a',I,J,RSCL*RQBOX(1,IQBOX),
     &                    RSCL*RQBOX(2,IQBOX),RSCL*RQBOX(3,IQBOX),1D0,
     &                    'txtt',1,1)
C
      END DO
C
99001 FORMAT (A,3I10,F10.3)
99002 FORMAT (A,4F10.3)
      END
C*==recangboxadd.f    processed by SPAG 6.70Rc at 18:37 on  3 Mar 2013
      SUBROUTINE RECANGBOXADD(IERROR,NMIN,NMAX,ABAS,DQ,QBAS,IQ1,IQ2,
     &                        ABAS_BOX,RCNTR_BOX,L_BOX,RQBOX,IQ_IQBOX,
     &                        N5VEC_IQBOX,IBV3,NQBOX,NQBOXMAX,NQMAX)
C   ********************************************************************
C   *                                                                  *
C   *                                                                  *
C   ********************************************************************
C
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      INTEGER IBV3,IERROR,IQ1,IQ2,NQBOX,NQBOXMAX,NQMAX
      REAL*8 ABAS(3,3),ABAS_BOX(3,3),DQ(3),L_BOX(3),QBAS(3,NQMAX),
     &       RCNTR_BOX(3),RQBOX(3,NQBOXMAX)
      INTEGER IQ_IQBOX(NQBOXMAX),N5VEC_IQBOX(5,NQBOXMAX),NMAX(3),NMIN(3)
C
C Local variables
C
      REAL*8 DB(3),DC(3),PJ
      REAL*8 DDOT
      INTEGER I,INSIDE,IQ,J,L,M,N
C
C*** End of declarations rewritten by SPAG
C
      IERROR = 0
C
      DO IQ = IQ1,IQ2
C
         DO I = 1,3
            DQ(I) = QBAS(I,IQ)
         END DO
C
         DO N = NMIN(3),NMAX(3)
            DO M = NMIN(2),NMAX(2)
               DO L = NMIN(1),NMAX(1)
C
                  INSIDE = 1
                  DO I = 1,3
                     DC(I) = L*ABAS(I,1) + M*ABAS(I,2) + N*ABAS(I,3)
     &                       + DQ(I)
                     DB(I) = DC(I) - RCNTR_BOX(I)
                  END DO
C
                  DO J = 1,3
                     PJ = DDOT(3,DB,1,ABAS_BOX(1,J),1)
C
                     IF ( ABS(PJ).GT.L_BOX(J)/2D0 ) INSIDE = 0
                  END DO
C
                  IF ( INSIDE.EQ.1 ) THEN
                     NQBOX = NQBOX + 1
                     IF ( NQBOX.LE.NQBOXMAX ) THEN
                        DO I = 1,3
                           RQBOX(I,NQBOX) = DC(I)
                        END DO
                        N5VEC_IQBOX(1,NQBOX) = L
                        N5VEC_IQBOX(2,NQBOX) = M
                        N5VEC_IQBOX(IBV3,NQBOX) = N
                        IQ_IQBOX(NQBOX) = IQ
                     ELSE
                        WRITE (6,99001) NQBOX
                        IERROR = 1
                        RETURN
                     END IF
C
                  END IF
               END DO
            END DO
         END DO
C
      END DO
99001 FORMAT (/,5X,'WARNING: NQBOXMAX too small for  NQBOX=',I8,/,5X,
     &        'WARNING: NQBOXMAX will be increased')
      END
C*==read_sysfile.f    processed by SPAG 6.70Rc at 18:37 on  3 Mar 2013
      SUBROUTINE READ_SYSFILE(INFIL,ALAT,BOA,ABAS,ABAS_L,ABAS_I,ABAS_R,
     &                        COA,CONC,LATANG,LATPAR,QBAS,RWSQ,BRAVAIS,
     &                        I,IA,ICL,ICLQ,IE,IQ,IQAT,IQECL,IT,ITOQ,
     &                        JCL,JQ,JT,NAT,NCL,NLQ,NOQ,NQ,NQCL,
     &                        NQ_BULK_L,NQ_BULK_R,NSUBSYS,NT,
     &                        N_REP_SU_BULK_L,N_REP_SU_BULK_R,N_SU,
     &                        SPACEGROUP,SPACEGROUP_AP,ZT,
     &                        BULK_L_EQ_BULK_R,STRUC_L_EQ_STRUC_R,
     &                        DIMENSION,SU_NAME,SU_IQ,LATTICE_TYPE,LINE,
     &                        OCCUPATION_OF_LAYERS,STRUCTURE_TYPE,
     &                        ZRANGE_TYPE,TXTT,WYCKOFFQ,WYCKOFFCL,NQMAX,
     &                        NTMAX)
C   ********************************************************************
C   *                                                                  *
C   *                                                                  *
C   *                                                                  *
C   ********************************************************************
C
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      REAL*8 ALAT,BOA,COA
      INTEGER BRAVAIS,I,IA,ICL,IE,INFIL,IQ,IT,JCL,JQ,JT,NCL,NQ,NQMAX,
     &        NQ_BULK_L,NQ_BULK_R,NSUBSYS,NT,NTMAX,N_REP_SU_BULK_L,
     &        N_REP_SU_BULK_R,N_SU,SPACEGROUP,SPACEGROUP_AP
      CHARACTER*3 BULK_L_EQ_BULK_R,STRUC_L_EQ_STRUC_R
      CHARACTER*10 DIMENSION
      CHARACTER*80 LATTICE_TYPE,LINE,OCCUPATION_OF_LAYERS,
     &             STRUCTURE_TYPE,ZRANGE_TYPE
      REAL*8 ABAS(3,3),ABAS_I(3,3),ABAS_L(3,3),ABAS_R(3,3),CONC(NTMAX),
     &       LATANG(3),LATPAR(3),QBAS(3,NQMAX),RWSQ(NQMAX)
      INTEGER ICLQ(NQMAX),IQAT(NQMAX,NTMAX),IQECL(NQMAX,NQMAX),
     &        ITOQ(NTMAX,NQMAX),NAT(NTMAX),NLQ(NQMAX),NOQ(NQMAX),
     &        NQCL(NQMAX),SU_IQ(NQMAX),ZT(NTMAX)
      CHARACTER*10 SU_NAME(10)
      CHARACTER*4 TXTT(NTMAX)
      CHARACTER*1 WYCKOFFCL(NQMAX),WYCKOFFQ(NQMAX)
C
C Local variables
C
      INTEGER J
      CHARACTER*80 SYSFIL_INP,TEXT_LINE
      REAL*8 VERSION
C
C*** End of declarations rewritten by SPAG
C
C=======================================================================
C                        read   system file
C=======================================================================
C
      READ (INFIL,*) TEXT_LINE
      WRITE (6,'(A)') TEXT_LINE
      READ (INFIL,*) SYSFIL_INP
      WRITE (6,'(A)') SYSFIL_INP
C
      READ (INFIL,*) TEXT_LINE
      WRITE (6,'(A)') TEXT_LINE
      READ (INFIL,*) VERSION
      WRITE (6,*) VERSION
C
      READ (INFIL,*) TEXT_LINE
      WRITE (6,'(A)') TEXT_LINE
      READ (INFIL,'(A)') DIMENSION
      WRITE (6,'(A)') DIMENSION
C
      IF ( DIMENSION(1:2).NE.'3D' ) THEN
         SPACEGROUP = 0
         SPACEGROUP_AP = 0
      END IF
C
      READ (INFIL,'(A)') TEXT_LINE
      READ (INFIL,*) BRAVAIS
      WRITE (6,99003) TEXT_LINE(1:30),BRAVAIS
C
      READ (INFIL,'(A)') TEXT_LINE
      READ (INFIL,*) SPACEGROUP,SPACEGROUP_AP
      WRITE (6,99003) TEXT_LINE(1:30),SPACEGROUP,SPACEGROUP_AP
C
      READ (INFIL,'(A)') TEXT_LINE
      READ (INFIL,*) STRUCTURE_TYPE
      WRITE (6,99004) TEXT_LINE(1:30),STRUCTURE_TYPE
C
      READ (INFIL,'(A)') TEXT_LINE
      READ (INFIL,*) ALAT
      WRITE (6,'(A,5f10.3)') TEXT_LINE(1:30),ALAT
C
      READ (INFIL,'(A)') TEXT_LINE
      READ (INFIL,*) BOA,COA
      WRITE (6,'(A,5f10.3)') TEXT_LINE(1:30),BOA,COA
C
      READ (INFIL,'(A)') TEXT_LINE
      READ (INFIL,*) (LATPAR(I),I=1,3)
      WRITE (6,'(A,5f10.3)') TEXT_LINE(1:30),(LATPAR(I),I=1,3)
C
      READ (INFIL,'(A)') TEXT_LINE
      READ (INFIL,*) (LATANG(I),I=1,3)
      WRITE (6,'(A,5f10.3)') TEXT_LINE(1:30),(LATANG(I),I=1,3)
C
      READ (INFIL,'(A)') TEXT_LINE
      DO J = 1,3
         READ (INFIL,*) (ABAS(I,J),I=1,3)
         WRITE (6,'(A,5f10.3)') TEXT_LINE(1:30),(ABAS(I,J),I=1,3)
      END DO
C
C-------------------------------------------------------------------- 2D
C
      IF ( DIMENSION(1:2).EQ.'2D' ) THEN
Ccc         READ (INFIL,'(A)') TEXT_LINE
Ccc         READ (INFIL,*) LATTICE_TYPE
Ccc         WRITE (6,99004) TEXT_LINE(1:30),LATTICE_TYPE
C
         READ (INFIL,'(A)') TEXT_LINE
         READ (INFIL,*) ZRANGE_TYPE
         WRITE (6,99004) TEXT_LINE(1:30),ZRANGE_TYPE
C
Ccc         READ (INFIL,'(A)') TEXT_LINE
Ccc         READ (INFIL,*) OCCUPATION_OF_LAYERS
Ccc         WRITE (6,99004) TEXT_LINE(1:30),OCCUPATION_OF_LAYERS
C
Ccc         READ (INFIL,'(A)') TEXT_LINE
Ccc         READ (INFIL,*) NSUBSYS
Ccc         WRITE (6,99003) TEXT_LINE(1:30),NSUBSYS
C
Ccc         READ (INFIL,'(A)') TEXT_LINE
Ccc         READ (INFIL,*) N_SU
Ccc         WRITE (6,99003) TEXT_LINE(1:30),N_SU
C
Ccc         READ (INFIL,'(A)') TEXT_LINE
Ccc         DO I = 1,N_SU
Ccc            READ (INFIL,*) SU_NAME(I)
Ccc            WRITE (6,99004) TEXT_LINE(1:30),SU_NAME(I)
Ccc         END DO
C
         IF ( ZRANGE_TYPE(1:8).EQ.'extended' ) THEN
Ccc            READ (INFIL,'(A)') TEXT_LINE
Ccc            READ (INFIL,*) BULK_L_EQ_BULK_R
Ccc            WRITE (6,99004) TEXT_LINE(1:30),BULK_L_EQ_BULK_R
C
Ccc            READ (INFIL,'(A)') TEXT_LINE
Ccc            READ (INFIL,*) STRUC_L_EQ_STRUC_R
Ccc            WRITE (6,99004) TEXT_LINE(1:30),STRUC_L_EQ_STRUC_R
C
            READ (INFIL,'(A)') TEXT_LINE
            READ (INFIL,*) NQ_BULK_L
            WRITE (6,99003) TEXT_LINE(1:30),NQ_BULK_L
C
Ccc            READ (INFIL,'(A)') TEXT_LINE
Ccc            READ (INFIL,*) N_REP_SU_BULK_L
Ccc            WRITE (6,99003) TEXT_LINE(1:30),N_REP_SU_BULK_L
C
            READ (INFIL,'(A)') TEXT_LINE
            DO J = 1,3
               READ (INFIL,*) (ABAS_L(I,J),I=1,3)
               WRITE (6,'(A,5f10.3)') TEXT_LINE(1:30),
     &                                (ABAS_L(I,J),I=1,3)
            END DO
C
            READ (INFIL,'(A)') TEXT_LINE
            READ (INFIL,*) NQ_BULK_R
            WRITE (6,99003) TEXT_LINE(1:30),NQ_BULK_L
C
Ccc            READ (INFIL,'(A)') TEXT_LINE
Ccc            READ (INFIL,*) N_REP_SU_BULK_R
Ccc            WRITE (6,99003) TEXT_LINE(1:30),NQ_BULK_L
C
            READ (INFIL,'(A)') TEXT_LINE
            DO J = 1,3
               READ (INFIL,*) (ABAS_R(I,J),I=1,3)
               WRITE (6,'(A,5f10.3)') TEXT_LINE(1:30),
     &                                (ABAS_R(I,J),I=1,3)
            END DO
C
         END IF
C
C                                                              2D I-ZONE
         ABAS_I(1:3,1:2) = ABAS(1:3,1:2)
         ABAS_I(1:3,3) = ABAS(1:3,3) - ABAS_L(1:3,3) - ABAS_R(1:3,3)
C
      END IF
C-------------------------------------------------------------------- 2D
C
C
C
C----------------------------------------------------------------- sites
C
      READ (INFIL,'(A)') TEXT_LINE
      READ (INFIL,*) NQ
      WRITE (6,99003) TEXT_LINE(1:30),NQ
C
      READ (INFIL,'(A)') TEXT_LINE
      WRITE (6,*) TEXT_LINE
C
      IF ( DIMENSION.EQ.'3D' ) THEN
C
         DO IQ = 1,NQ
            READ (INFIL,*) JQ,ICLQ(IQ),(QBAS(I,IQ),I=1,3),RWSQ(IQ),
     &                     NLQ(IQ),NOQ(IQ),(ITOQ(IA,IQ),IA=1,NOQ(IQ))
            WRITE (6,99001) JQ,ICLQ(IQ),(QBAS(I,IQ),I=1,3),RWSQ(IQ),
     &                      NLQ(IQ),NOQ(IQ),(ITOQ(IA,IQ),IA=1,NOQ(IQ))
         END DO
C
      ELSE IF ( DIMENSION.EQ.'2D' ) THEN
C
         DO IQ = 1,NQ
            READ (INFIL,*) JQ,ICLQ(IQ),SU_IQ(IQ),WYCKOFFQ(IQ),
     &                     (QBAS(I,IQ),I=1,3),RWSQ(IQ),NLQ(IQ),NOQ(IQ),
     &                     (ITOQ(IA,IQ),IA=1,NOQ(IQ))
            WRITE (6,99002) JQ,ICLQ(IQ),SU_IQ(IQ),WYCKOFFQ(IQ),
     &                      (QBAS(I,IQ),I=1,3),RWSQ(IQ),NLQ(IQ),NOQ(IQ),
     &                      (ITOQ(IA,IQ),IA=1,NOQ(IQ))
         END DO
C
      END IF
C
C---------------------------------------------------------- site classes
C
      READ (INFIL,'(A)') TEXT_LINE
      READ (INFIL,*) NCL
      WRITE (6,99003) TEXT_LINE(1:30),NCL
C
      READ (INFIL,'(A)') TEXT_LINE
      WRITE (6,'(A)') TEXT_LINE
      DO ICL = 1,NCL
         READ (INFIL,99006) JCL,WYCKOFFCL(ICL),NQCL(ICL),
     &                      (IQECL(IE,ICL),IE=1,NQCL(ICL))
         WRITE (6,99006) JCL,WYCKOFFCL(ICL),NQCL(ICL),
     &                   (IQECL(IE,ICL),IE=1,NQCL(ICL))
      END DO
C
C----------------------------------------------------------------- types
C
      READ (INFIL,'(A)') TEXT_LINE
      READ (INFIL,*) NT
      WRITE (6,99003) TEXT_LINE(1:30),NT
C
      READ (INFIL,'(A)') TEXT_LINE
      DO IT = 1,NT
         READ (INFIL,99007) JT,ZT(IT),TXTT(IT),NAT(IT),CONC(IT),
     &                      (IQAT(IA,IT),IA=1,NAT(IT))
         WRITE (6,99007) JT,ZT(IT),TXTT(IT),NAT(IT),CONC(IT),
     &                   (IQAT(IA,IT),IA=1,NAT(IT))
      END DO
C
      WRITE (6,99005)
99001 FORMAT (i3,i4,3F18.12,2x,f18.12,i4,i5,10I3)
99002 FORMAT (i3,i4,i3,2x,a1,3F18.12,2x,f18.12,i4,i5,10I3)
99003 FORMAT (A30,10I5)
99004 FORMAT (A30,a)
99005 FORMAT (/,' ************* system file read in ************* ',//)
99006 FORMAT (I3,3X,A,I5,100I3)
99007 FORMAT (I3,I4,2X,A8,I5,F6.3,100I3)
C
Cset NCPA 0
Cfor (set IQ 1) (IQ <= NQ) (incr IQ) ( if (NOQ(IQ) != 1) ( set NCPA 1 )
C
C------------------------------------------------------- find mesh table
C
Cset NM 0
Cfor (set IQ 1) (IQ <= NQ) (incr IQ) (
C  set IMQ(IQ) 0
C  for (set IM 1) (IM <= NM) (incr IM) (
C    if (RWS(IQ)==RWSM(IM)) (set IMQ(IQ) IM)
C
C
C  if (IMQ(IQ)==0) (
C     incr NM
C     set RWSM(NM) RWS(IQ)
C     set IMQ(IQ) NM
C  )
C
C  for (set IO 1) (IO <= NOQ(IQ)) (incr IO) (
C     set IT ITOQ(IO,IQ)
C     set IMT(IT) IMQ(IQ)
C)
C
C------------------------------------------------------ init QMTET QMPHI
C
Cfor (set IQ 1) (IQ <= NQ) (incr IQ) (
C   set QMTET(IQ) 0.0
C   set QMPHI(IQ) 0.0
C
      END
C*==iq_nlay_order.f    processed by SPAG 6.70Rc at 18:37 on  3 Mar 2013
      SUBROUTINE IQ_NLAY_ORDER(NQ,QBAS,IQ_ORDERED,NDOTQ,NVEC_LAY,NQMAX)
C   ********************************************************************
C   *                                                                  *
C   *  order the sites along the normal vector   NVEC_LAY              *
C   *  using the pointer  IQ_ORDERED                                   *
C   *                                                                  *
C   ********************************************************************
C
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      INTEGER NQ,NQMAX
      INTEGER IQ_ORDERED(NQMAX)
      REAL*8 NDOTQ(NQMAX),NVEC_LAY(3),QBAS(3,NQMAX)
C
C Local variables
C
      REAL*8 DDOT
      INTEGER INSERT,IORD,IQ,JORD,JQ,NORD
C
C*** End of declarations rewritten by SPAG
C
      DO IQ = 1,NQ
         NDOTQ(IQ) = DDOT(3,NVEC_LAY,1,QBAS(1,IQ),1)
      END DO
C
      IQ_ORDERED(1) = 1
      NORD = 1
C
      DO IQ = 2,NQ
C
         DO IORD = 1,NORD
            JQ = IQ_ORDERED(IORD)
            INSERT = 0
            IF ( NDOTQ(JQ).GT.NDOTQ(IQ) ) THEN
               INSERT = 1
            ELSE IF ( ABS(NDOTQ(JQ)-NDOTQ(IQ)).LT.1D-6 ) THEN
               IF ( QBAS(3,JQ).GT.QBAS(3,IQ) ) THEN
                  INSERT = 1
               ELSE IF ( ABS(QBAS(3,JQ)-QBAS(3,IQ)).LT.1D-6 ) THEN
                  IF ( QBAS(2,JQ).GT.QBAS(2,IQ) ) THEN
                     INSERT = 1
                  ELSE IF ( ABS(QBAS(2,JQ)-QBAS(2,IQ)).LT.1D-6 .AND. 
     &                      QBAS(1,JQ).GT.QBAS(1,IQ) ) THEN
                     INSERT = 1
                  END IF
               END IF
            END IF
C
            IF ( INSERT.EQ.1 ) THEN
               DO JORD = NORD,IORD, - 1
                  IQ_ORDERED(JORD+1) = IQ_ORDERED(JORD)
               END DO
               IQ_ORDERED(IORD) = IQ
               NORD = NORD + 1
               GOTO 100
            END IF
C
         END DO
         NORD = NORD + 1
         IQ_ORDERED(IQ) = IQ
C
 100  END DO
C
      END
C*==rinvgj.f    processed by SPAG 6.70Rc at 18:37 on  3 Mar 2013
      SUBROUTINE RINVGJ(AINV,A,ARRAYDIM,N)
C   ********************************************************************
C   *                                                                  *
C   *                      AINV = A**(-1)                              *
C   *                                                                  *
C   *  invert A using the GAUSS-JORDAN - algorithm                     *
C   *  the 1- matrix is not set up and use is made of its structure    *
C   *                                                                  *
C   *                    REAL*8 VERSION                                *
C   *                                                                  *
C   ********************************************************************
C
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      INTEGER ARRAYDIM,N
      REAL*8 A(ARRAYDIM,ARRAYDIM),AINV(ARRAYDIM,ARRAYDIM)
C
C Local variables
C
      INTEGER ICOL,L,LL
      REAL*8 T,T1
C
C*** End of declarations rewritten by SPAG
C
      AINV(1,1) = 0D0
C                                                        scan columns
      DO ICOL = 1,N
C
C                                               make A(ICOL,ICOL) = 1
         T1 = 1.0D0/A(ICOL,ICOL)
         DO L = (ICOL+1),N
            A(ICOL,L) = A(ICOL,L)*T1
         END DO
C
         DO L = 1,(ICOL-1)
            AINV(ICOL,L) = AINV(ICOL,L)*T1
         END DO
         AINV(ICOL,ICOL) = T1
C
C                                    make A(LL,ICOL) = 0 for LL<>ICOL
         DO LL = 1,N
            IF ( LL.NE.ICOL ) THEN
               T = A(LL,ICOL)
               DO L = (ICOL+1),N
                  A(LL,L) = A(LL,L) - A(ICOL,L)*T
               END DO
C
               DO L = 1,(ICOL-1)
                  AINV(LL,L) = AINV(LL,L) - AINV(ICOL,L)*T
               END DO
               AINV(LL,ICOL) = -T1*T
            END IF
         END DO
      END DO
C
      END
C*==write_coord.f    processed by SPAG 6.70Rc at 18:37 on  3 Mar 2013
      SUBROUTINE WRITE_COORD(F,S,I,J,X,Y,Z,T,TXTT,IQ,IT)
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      INTEGER F,I,IQ,IT,J
      CHARACTER*1 S
      REAL*8 T,X,Y,Z
      CHARACTER*4 TXTT
C
C Local variables
C
      CHARACTER*6 ATOM
C
C*** End of declarations rewritten by SPAG
C
      IF ( S.EQ.'a' ) THEN
         ATOM = 'ATOM  '
      ELSE
         ATOM = 'HETATM'
      END IF
C
      WRITE (F,99001) ATOM,I,J,X,Y,Z,T,TXTT,IQ,IT
C
99001 FORMAT (A6,I5,11X,I4,4X,3F8.3,'  0.00',F8.3,3X,A4,' IQ ',I4,
     &        ' IT ',I4)
      END
C*==rvecexpd.f    processed by SPAG 6.70Rc at 18:37 on  3 Mar 2013
      SUBROUTINE RVECEXPD(A,B,BBINV,C)
C   ********************************************************************
C   *                                                                  *
C   *   expand A with respect to basis vectors B_i                     *
C   *                                                                  *
C   *                   A = sum(i) c_i * B_i                           *
C   *                                                                  *
C   *   with          c_i =  sum(j)  BBINV(i,j) * (A*B_j)              *
C   *                                                                  *
C   *   A     real vector of dimension 3                               *
C   *   B     real 3x3-matrix containing the basis vectors             *
C   *   BBINV real 3x3-matrix with                                     *
C   *         BBINV = BB^(-1)  and  BB(i,j) = (B_i*B_j)                *
C   *   C     real vector of dimension 3 with expansion coefficients   *
C   *                                                                  *
C   ********************************************************************
C
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      REAL*8 A(3),B(3,3),BBINV(3,3),C(3)
C
C Local variables
C
      REAL*8 ADOTB(3)
      REAL*8 DDOT
      INTEGER I,J
C
C*** End of declarations rewritten by SPAG
C
      DO I = 1,3
         ADOTB(I) = DDOT(3,A,1,B(1,I),1)
      END DO
C
      DO I = 1,3
         C(I) = 0D0
         DO J = 1,3
            C(I) = C(I) + BBINV(I,J)*ADOTB(J)
         END DO
      END DO
C
      END
C*==rvecortho.f    processed by SPAG 6.70Rc at 18:37 on  3 Mar 2013
      SUBROUTINE RVECORTHO(A,B)
C   ********************************************************************
C   *                                                                  *
C   *   A     real 3x3-matrix containing the original basis vectors    *
C   *   B     real 3x3-matrix containing the   NEW    basis vectors    *
C   *         ortho-normal vectors obained via Schmidt-s method        *
C   *                                                                  *
C   ********************************************************************
C
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      REAL*8 A(3,3),B(3,3)
C
C Local variables
C
      REAL*8 DDOT,DNRM2
      REAL*8 DOT21,DOT31,DOT32,NORM
      INTEGER I,J
C
C*** End of declarations rewritten by SPAG
C
      CALL RVECCOP(9,A,B)
C
      NORM = 1D0/DNRM2(3,B(1,1),1)
      CALL DSCAL(3,NORM,B(1,1),1)
C
C
      DOT21 = DDOT(3,B(1,2),1,B(1,1),1)
      DO I = 1,3
         B(I,2) = B(I,2) - DOT21*B(I,1)
      END DO
C
      NORM = 1D0/DNRM2(3,B(1,2),1)
      CALL DSCAL(3,NORM,B(1,2),1)
C
C
      DOT31 = DDOT(3,B(1,3),1,B(1,1),1)
      DOT32 = DDOT(3,B(1,3),1,B(1,2),1)
      DO I = 1,3
         B(I,3) = B(I,3) - DOT31*B(I,1) - DOT32*B(I,2)
      END DO
C
      NORM = 1D0/DNRM2(3,B(1,3),1)
      CALL DSCAL(3,NORM,B(1,3),1)
C
      DO I = 1,3
         DO J = 1,3
            WRITE (6,*) ' ORTHO ',I,J,DDOT(3,B(1,I),1,B(1,J),1)
         END DO
      END DO
C
      END
C*==rveccop.f    processed by SPAG 6.70Rc at 18:37 on  3 Mar 2013
      SUBROUTINE RVECCOP(N,A,B)
C   ********************************************************************
C   *                                                                  *
C   *   copy  REAL   array  A  onto  B                    B = A        *
C   *                                                                  *
C   *   A       REAL  array                                            *
C   *   N       dimension of A                                         *
C   *                                                                  *
C   ********************************************************************
C
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      INTEGER N
      REAL*8 A(N),B(N)
C
C Local variables
C
      INTEGER I
C
C*** End of declarations rewritten by SPAG
C
      DO I = 1,N
         B(I) = A(I)
      END DO
C
      END
C*==rvecspat.f    processed by SPAG 6.70Rc at 18:37 on  3 Mar 2013
      SUBROUTINE RVECSPAT(A,B,C,V,IFLAG)
C   ********************************************************************
C   *                                                                  *
C   *   spatial product                          V = A * ( B x C )     *
C   *                                                                  *
C   *   A,B,C   real vectors of dimension 3                            *
C   *                                                                  *
C   *   for  IFLAG = 1:                          V ->  |V|             *
C   *                                                                  *
C   *                                                                  *
C   *   NOTE:  the input is copied to local arrays to avoid            *
C   *          warnings when checking with ftnchek                     *
C   *                                                                  *
C   ********************************************************************
C
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      INTEGER IFLAG
      REAL*8 V
      REAL*8 A(3),B(3),C(3)
C
C Local variables
C
      REAL*8 AA(3),BB(3),CC(3),DD(3)
      REAL*8 DDOT
      INTEGER I
C
C*** End of declarations rewritten by SPAG
C
      DO I = 1,3
         AA(I) = A(I)
         BB(I) = B(I)
         CC(I) = C(I)
      END DO
C
      CALL RVECCROSS(BB,CC,DD)
C
      V = DDOT(3,AA,1,DD,1)
C
      IF ( IFLAG.EQ.1 ) V = ABS(V)
C
      END
C*==rinit.f    processed by SPAG 6.70Rc at 18:37 on  3 Mar 2013
      SUBROUTINE RINIT(N,A)
C   ********************************************************************
C   *                                                                  *
C   *   initialize  REAL   array  A                       A = 0        *
C   *                                                                  *
C   *   A       complex array                                          *
C   *   N       dimension of A                                         *
C   *                                                                  *
C   ********************************************************************
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C PARAMETER definitions
C
      REAL*8 R0
      PARAMETER (R0=0.0D0)
C
C Dummy arguments
C
      INTEGER N
      REAL*8 A(N)
C
C Local variables
C
      INTEGER I
C
C*** End of declarations rewritten by SPAG
C
      DO I = 1,N
         A(I) = R0
      END DO
C
      END
C*==local_basis.f    processed by SPAG 6.70Rc at 18:37 on  3 Mar 2013
      SUBROUTINE LOCAL_BASIS(NVEC,ABAS)
C   ********************************************************************
C   *                                                                  *
C   *   find local basis  ABAS                                         *
C   *                                                                  *
C   ********************************************************************
C
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      REAL*8 ABAS(3,3),NVEC(3)
C
C Local variables
C
      REAL*8 NXY
C
C*** End of declarations rewritten by SPAG
C
      CALL RINIT(9,ABAS)
C
      CALL RVECCOP(3,NVEC,ABAS(1,3))
C
C------------------------------------------------------------- ->n = ->z
      IF ( ABS(NVEC(3)-1D0).LT.1D-8 ) THEN
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
         ABAS(1,1) = 1D0
C-------------------------------------------------------------- n_y != 0
      ELSE IF ( ABS(NVEC(2)).GT.1D-8 ) THEN
         NXY = NVEC(1)/NVEC(2)
         ABAS(1,1) = 1D0/SQRT(1D0+NXY*NXY)
         ABAS(2,1) = -NXY*ABAS(1,1)
C--------------------------------------------------------------- n_y = 0
      ELSE
         ABAS(2,1) = 1D0
      END IF
C
C----------------------------------------------- ->y_l = ->z_l  x  ->x_l
C
      CALL RVECCROSS(ABAS(1,3),ABAS(1,1),ABAS(1,2))
C
      END
C*==rvecnorm.f    processed by SPAG 6.70Rc at 18:37 on  3 Mar 2013
      SUBROUTINE RVECNORM(N,A)
C   ********************************************************************
C   *                                                                  *
C   *   normalize REAL vector to 1                     A = A / |A|     *
C   *                                                                  *
C   ********************************************************************
C
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      INTEGER N
      REAL*8 A(N)
C
C Local variables
C
      REAL*8 DNRM2
      REAL*8 NORM
C
C*** End of declarations rewritten by SPAG
C
      NORM = 1D0/DNRM2(N,A,1)
      CALL DSCAL(N,NORM,A,1)
C
      END
C*==rveccross.f    processed by SPAG 6.70Rc at 18:37 on  3 Mar 2013
      SUBROUTINE RVECCROSS(A,B,C)
C   ********************************************************************
C   *                                                                  *
C   *   vector cross product                           C = A x B       *
C   *                                                                  *
C   *   A,B,C   real vectors of dimension 3                            *
C   *                                                                  *
C   ********************************************************************
C
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      REAL*8 A(3),B(3),C(3)
C
C*** End of declarations rewritten by SPAG
C
      C(1) = A(2)*B(3) - A(3)*B(2)
      C(2) = A(3)*B(1) - A(1)*B(3)
      C(3) = A(1)*B(2) - A(2)*B(1)
C
      END
C*==dnrm2.f    processed by SPAG 6.70Rc at 18:37 on  3 Mar 2013
      FUNCTION DNRM2(N,X,INCX)
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C PARAMETER definitions
C
      REAL*8 ONE,ZERO
      PARAMETER (ONE=1.0D+0,ZERO=0.0D+0)
C
C Dummy arguments
C
      INTEGER INCX,N
      REAL*8 DNRM2
      REAL*8 X(N)
C
C Local variables
C
      REAL*8 ABSXI,NORM,SCL,SSQ
      INTEGER IX
C
C*** End of declarations rewritten by SPAG
C
C  DNRM2 returns the euclidean norm of a vector via the function
C  name, so that
C
C     DNRM2 := sqrt( x'*x )
C
C
C
C  -- This version written on 25-October-1982.
C     Modified on 14-October-1993 to inline the call to DLASSQ.
C     Sven Hammarling, Nag Ltd.
C
      IF ( N.LT.1 .OR. INCX.LT.1 ) THEN
         NORM = ZERO
      ELSE IF ( N.EQ.1 ) THEN
         NORM = ABS(X(1))
      ELSE
         SCL = ZERO
         SSQ = ONE
C        The following loop is equivalent to this call to the LAPACK
C        auxiliary routine:
C        CALL DLASSQ( N, X, INCX, SCL, SSQ )
C
         DO IX = 1,1 + (N-1)*INCX,INCX
            IF ( X(IX).NE.ZERO ) THEN
               ABSXI = ABS(X(IX))
               IF ( SCL.LT.ABSXI ) THEN
                  SSQ = ONE + SSQ*(SCL/ABSXI)**2
                  SCL = ABSXI
               ELSE
                  SSQ = SSQ + (ABSXI/SCL)**2
               END IF
            END IF
         END DO
         NORM = SCL*SQRT(SSQ)
      END IF
C
      DNRM2 = NORM
C
C     End of DNRM2.
C
      END
C*==ddot.f    processed by SPAG 6.70Rc at 18:37 on  3 Mar 2013
      FUNCTION DDOT(N,DX,INCX,DY,INCY)
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      INTEGER INCX,INCY,N
      REAL*8 DDOT
      REAL*8 DX(N),DY(N)
C
C Local variables
C
      REAL*8 DTEMP
      INTEGER I,IX,IY,M,MP1
C
C*** End of declarations rewritten by SPAG
C
C
C     forms the dot product of two vectors.
C     uses unrolled loops for increments equal to one.
C     jack dongarra, linpack, 3/11/78.
C     modified 12/3/93, array(1) declarations changed to array(6)
C
C
      DDOT = 0.0D0
      DTEMP = 0.0D0
      IF ( N.LE.0 ) RETURN
      IF ( INCX.EQ.1 .AND. INCY.EQ.1 ) THEN
C
C        code for both increments equal to 1
C
C
C        clean-up loop
C
         M = MOD(N,5)
         IF ( M.NE.0 ) THEN
            DO I = 1,M
               DTEMP = DTEMP + DX(I)*DY(I)
            END DO
            IF ( N.LT.5 ) THEN
               DDOT = DTEMP
               GOTO 99999
            END IF
         END IF
         MP1 = M + 1
         DO I = MP1,N,5
            DTEMP = DTEMP + DX(I)*DY(I) + DX(I+1)*DY(I+1) + DX(I+2)
     &              *DY(I+2) + DX(I+3)*DY(I+3) + DX(I+4)*DY(I+4)
         END DO
         DDOT = DTEMP
      ELSE
C
C        code for unequal increments or equal increments
C          not equal to 1
C
         IX = 1
         IY = 1
         IF ( INCX.LT.0 ) IX = (-N+1)*INCX + 1
         IF ( INCY.LT.0 ) IY = (-N+1)*INCY + 1
         DO I = 1,N
            DTEMP = DTEMP + DX(IX)*DY(IY)
            IX = IX + INCX
            IY = IY + INCY
         END DO
         DDOT = DTEMP
         RETURN
      END IF
99999 CONTINUE
      END
C*==dscal.f    processed by SPAG 6.70Rc at 18:37 on  3 Mar 2013
      SUBROUTINE DSCAL(N,DA,DX,INCX)
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      REAL*8 DA
      INTEGER INCX,N
      REAL*8 DX(N)
C
C Local variables
C
      INTEGER I,M,MP1,NINCX
C
C*** End of declarations rewritten by SPAG
C
C
C     scales a vector by a constant.
C     uses unrolled loops for increment equal to one.
C     jack dongarra, linpack, 3/11/78.
C     modified 3/93 to return if incx .le. 0.
C     modified 12/3/93, array(1) declarations changed to array(6)
C
C
      IF ( N.LE.0 .OR. INCX.LE.0 ) RETURN
      IF ( INCX.EQ.1 ) THEN
C
C        code for increment equal to 1
C
C
C        clean-up loop
C
         M = MOD(N,5)
         IF ( M.NE.0 ) THEN
            DO I = 1,M
               DX(I) = DA*DX(I)
            END DO
            IF ( N.LT.5 ) RETURN
         END IF
         MP1 = M + 1
         DO I = MP1,N,5
            DX(I) = DA*DX(I)
            DX(I+1) = DA*DX(I+1)
            DX(I+2) = DA*DX(I+2)
            DX(I+3) = DA*DX(I+3)
            DX(I+4) = DA*DX(I+4)
         END DO
      ELSE
C
C        code for increment not equal to 1
C
         NINCX = N*INCX
         DO I = 1,NINCX,INCX
            DX(I) = DA*DX(I)
         END DO
         RETURN
      END IF
      END
C*==match_n5vec_in_list.f    processed by SPAG 6.70Rc at 18:37 on  3 Mar 2013
      SUBROUTINE MATCH_N5VEC_IN_LIST(N5VEC,N5VEC_LIST,N_LIST,I_MATCH,
     &                               MATCH)
C   ********************************************************************
C   *                                                                  *
C   ********************************************************************
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      INTEGER I_MATCH,N_LIST
      LOGICAL MATCH
      INTEGER N5VEC(5),N5VEC_LIST(5,N_LIST)
C
C Local variables
C
      INTEGER I,I_LIST
C
C*** End of declarations rewritten by SPAG
C
      I_MATCH = 0
      MATCH = .FALSE.
C
      DO I_LIST = 1,N_LIST
         MATCH = .TRUE.
         DO I = 1,5
            IF ( N5VEC(I).NE.N5VEC_LIST(I,I_LIST) ) MATCH = .FALSE.
         END DO
         IF ( MATCH ) THEN
            I_MATCH = I_LIST
            RETURN
         END IF
      END DO
C
      END
C*==match_box_rlx.f    processed by SPAG 6.70Rc at 18:37 on  3 Mar 2013
      SUBROUTINE MATCH_BOX_RLX(IQBOX,RQBOX,IQ_IQBOX,N5VEC_IQBOX,IQRLX,
     &                         RQRLX,IQ_IQRLX,N5VEC_IQRLX,MATCH)
C   ********************************************************************
C   *                                                                  *
C   ********************************************************************
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      INTEGER IQBOX,IQRLX
      LOGICAL MATCH
      INTEGER IQ_IQBOX(IQBOX),IQ_IQRLX(IQRLX),N5VEC_IQBOX(5,IQBOX),
     &        N5VEC_IQRLX(5,IQRLX)
      REAL*8 RQBOX(3,IQBOX),RQRLX(3,IQRLX)
C
C Local variables
C
      INTEGER I
C
C*** End of declarations rewritten by SPAG
C
      MATCH = .FALSE.
C
      DO I = 1,5
         IF ( N5VEC_IQBOX(I,IQBOX).NE.N5VEC_IQRLX(I,IQRLX) ) RETURN
      END DO
C
      IF ( IQ_IQBOX(IQBOX).NE.IQ_IQRLX(IQRLX) ) RETURN
C
      DO I = 1,3
         IF ( ABS(RQBOX(I,IQBOX)-RQRLX(I,IQRLX)).GT.1D-8 ) RETURN
      END DO
C
      MATCH = .TRUE.
C
      END
C*==rveclcib.f    processed by SPAG 6.70Rc at 18:37 on  3 Mar 2013
      SUBROUTINE RVECLCIB(I,J,K,B,A)
C   ********************************************************************
C   *                                                                  *
C   *   perform  the scalar-vector operation    A = I*B1 + J*B2 +K*B3  *
C   *                                                                  *
C   *   A       real vectors of dimension 3                            *
C   *   I,J,K   integer weights                                        *
C   *   B       3 real basis vectors of dimension 3                    *
C   *           stored as 3x3 matrix                                   *
C   *                                                                  *
C   ********************************************************************
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      INTEGER I,J,K
      REAL*8 A(3),B(3,3)
C
C Local variables
C
      INTEGER IC
C
C*** End of declarations rewritten by SPAG
C
      DO IC = 1,3
         A(IC) = I*B(IC,1) + J*B(IC,2) + K*B(IC,3)
      END DO
C
      END
C*==rveclcrvb.f    processed by SPAG 6.70Rc at 18:37 on  3 Mar 2013
      SUBROUTINE RVECLCRVB(N,RV,B,A)
C   ********************************************************************
C   *                                                                  *
C   *   perform  the scalar-vector operation    A = SUM(i) R_i * B_i   *
C   *                                                                  *
C   *   N       number of coefficients in vector RV                    *
C   *   A       real vector of dimension 3                             *
C   *   RV      real weights                                           *
C   *   B       N real basis vectors of dimension 3                    *
C   *           stored as 3xN matrix                                   *
C   *                                                                  *
C   ********************************************************************
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      INTEGER N
      REAL*8 A(3),B(3,N),RV(N)
C
C Local variables
C
      INTEGER I,J
C
C*** End of declarations rewritten by SPAG
C
      A(1:3) = 0D0
C
      DO J = 1,N
         DO I = 1,3
            A(I) = A(I) + B(I,J)*RV(J)
         END DO
      END DO
C
      END
C*==rmatinv.f    processed by SPAG 6.70Rc at 18:37 on  3 Mar 2013
      SUBROUTINE RMATINV(N,M,A,B)
C   ********************************************************************
C   *                                                                  *
C   *   perform  the matrix-matrix operation           B = 1 / A       *
C   *                                                                  *
C   *   A,B     real  SQUARE  N x N - matrices                         *
C   *   N       dimension of A and B                                   *
C   *   M       array size of A, B    with M >= N                      *
C   *                                                                  *
C   *   on exit A contains the LU-decomposition of A                   *
C   *   NO PIVOTING is used                                            *
C   *                                                                  *
C   *   based on a subroutine by H. Akai                               *
C   *                                                                  *
C   ********************************************************************
C
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C Dummy arguments
C
      INTEGER M,N
      REAL*8 A(M,M),B(M,M)
C
C Local variables
C
      INTEGER I,J,L
      REAL*8 S
C
C*** End of declarations rewritten by SPAG
C
      DO I = 1,N - 1
         DO J = I + 1,N
            S = -A(J,I)/A(I,I)
C
            DO L = 1,N
               A(J,L) = A(J,L) + S*A(I,L)
            END DO
            A(J,I) = S
         END DO
      END DO
C
      DO I = N,1, - 1
C
         DO J = 1,I - 1
            B(I,J) = A(I,J)
            DO L = I + 1,N
               B(I,J) = B(I,J) - A(I,L)*B(L,J)
            END DO
         END DO
C
         DO J = I + 1,N
            B(I,J) = 0D0
            DO L = I + 1,N
               B(I,J) = B(I,J) - A(I,L)*B(L,J)
            END DO
         END DO
C
         B(I,I) = 1D0
         DO L = I + 1,N
            B(I,I) = B(I,I) - A(I,L)*B(L,I)
         END DO
C
         S = 1D0/A(I,I)
         DO J = 1,N
            B(I,J) = B(I,J)*S
         END DO
C
      END DO
C
      END
C*==create_3d_surface.f    processed by SPAG 6.70Rc at 18:37 on  3 Mar 2013
      SUBROUTINE CREATE_3D_SURFACE(IWR,BRAVAIS,USE_CRY_PRIM_VECS,
     &                             MILLER_INDICES,ABAS,BBAS,BDBINV,NQ,
     &                             NT,QBAS,NOQ,NAT,IQAT,ITOQ,VOLUC,
     &                             IERROR,NQMAX,NTMAX)
C   ********************************************************************
C   *                                                                  *
C   *  change the 3D basis vectors from the standard setting to a new  *
C   *  setting that has 2 basis vetors parallel to a given surface     *
C   *  specified by the Miller indices (h,k,l)                         *
C   *                                                                  *
C   ********************************************************************
      IMPLICIT NONE
C
C*** Start of declarations rewritten by SPAG
C
C PARAMETER definitions
C
      REAL*8 PI
      PARAMETER (PI=3.141592653589793238462643D0)
C
C Dummy arguments
C
      INTEGER BRAVAIS,IERROR,IWR,NQ,NQMAX,NT,NTMAX
      LOGICAL USE_CRY_PRIM_VECS
      REAL*8 VOLUC
      REAL*8 ABAS(3,3),BBAS(3,3),BDBINV(3,3),QBAS(3,NQ)
      INTEGER IQAT(NQMAX,NTMAX),ITOQ(NTMAX,NQMAX),MILLER_INDICES(3),
     &        NAT(NTMAX),NOQ(NQMAX)
C
C Local variables
C
      REAL*8 A1DOTV,A1_NEW,A1_P(3),A1_PHI,A1_PP(3),A1_PPP(3),A1_XY,
     &       ABAS_HKL(3,3),ABAS_NEW(3,3),ADAINV_NEW(3,3),ADAMAT_NEW(3,3)
     &       ,AIBJ,AP(3),AQVEC(3),BBAS_HKL(3,3),BBAS_NEW(3,3),
     &       BDBINV_HKL(3,3),BDBMAT_HKL(3,3),BP(3),CA,CB,CG,MA(3,3),
     &       MB(3,3),MBA(3,3),MG(3,3),MGBA(3,3),N_LNG,N_P(3),N_PHI,
     &       N_PP(3),N_PPP(3),N_TET,N_XY,PVEC(3),QBASNEW(:,:),
     &       QBASTMP(:,:),QVEC_NEW(3),QZ,SA,SB,SG,V,V23(3),VEC(3),
     &       VEC_SEL(:),VMIN,VNORM(:),VNORM_INP(:),VOL,VOLMIN,VOLUC_HKL,
     &       VOLUC_NEW,WRK3X3_HKL(3,3)
      LOGICAL CHECK
      REAL*8 DDOT,DNRM2
      INTEGER I,I1,I2,I3,IA,IMAX,IO,IQ,IQNEW,IQNEWAT(:,:),IQNEW_IQ(:),
     &        IQ_IQNEW(:),IT,ITOQNEW(:,:),J,JQ,K,KQ,
     &        MILLER_INDICES_INP(3),NABAS_NEW(3,3),NATNEW(:),NOQNEW(:),
     &        NVEC(3),NVEC_SEL(:)
      CHARACTER*38 TXTBRAVAIS(14)
C
C*** End of declarations rewritten by SPAG
C
      ALLOCATABLE QBASNEW,QBASTMP,IQNEW_IQ,NOQNEW,IQ_IQNEW
      ALLOCATABLE IQNEWAT,ITOQNEW,NATNEW
      ALLOCATABLE VEC_SEL,NVEC_SEL,VNORM,VNORM_INP
C
      DATA TXTBRAVAIS/'triclinic   primitive      -1     C_i ',
     &     'monoclinic  primitive      2/m    C_2h',
     &     'monoclinic  base centered  2/m    C_2h',
     &     'orthorombic primitive      mmm    D_2h',
     &     'orthorombic base-centered  mmm    D_2h',
     &     'orthorombic body-centered  mmm    D_2h',
     &     'orthorombic face-centered  mmm    D_2h',
     &     'tetragonal  primitive      4/mmm  D_4h',
     &     'tetragonal  body-centered  4/mmm  D_4h',
     &     'trigonal    primitive      -3m    D_3d',
     &     'hexagonal   primitive      6/mmm  D_6h',
     &     'cubic       primitive      m3m    O_h ',
     &     'cubic       face-centered  m3m    O_h ',
     &     'cubic       body-centered  m3m    O_h '/
C
C*** End of declarations rewritten by SPAG
C
      IMAX = 20
C
      CHECK = .FALSE.
      ALLOCATE (QBASTMP(3,NQMAX))
      ALLOCATE (QBASNEW(3,NQMAX),NOQNEW(NQMAX),NATNEW(NTMAX))
      ALLOCATE (IQNEW_IQ(NQMAX),IQ_IQNEW(NQMAX))
      ALLOCATE (IQNEWAT(NQMAX,NTMAX),ITOQNEW(NTMAX,NQMAX))
C
      ALLOCATE (VEC_SEL(3),NVEC_SEL(3),VNORM(3),VNORM_INP(3))
C
C-----------------------------------------------------------------------
      IF ( MILLER_INDICES(1).EQ.0 .AND. MILLER_INDICES(2).EQ.0 .AND. 
     &     +MILLER_INDICES(3).EQ.0 ) THEN
         WRITE (6,*) 'all Miller indices are 0 !!!!!!!!!!!!!!!'
         IERROR = 1
         RETURN
      END IF
C
C
      MILLER_INDICES_INP(1:3) = MILLER_INDICES(1:3)
C-----------------------------------------------------------------------
C
      ABAS_HKL(1:3,1:3) = ABAS(1:3,1:3)
      BBAS_HKL(1:3,1:3) = BBAS(1:3,1:3)
C
      IF ( USE_CRY_PRIM_VECS ) THEN
C
         SELECT CASE (BRAVAIS)
C----------------------------------------------------------- P primitive
         CASE (4,8,12)
C
            ABAS_HKL(1:3,1:3) = ABAS(1:3,1:3)
C
C------------------------------------------------------- I body centered
         CASE (6,9,14)
C
            ABAS_HKL(1:3,1) = ABAS(1:3,2) + ABAS(1:3,3)
            ABAS_HKL(1:3,2) = ABAS(1:3,1) + ABAS(1:3,3)
            ABAS_HKL(1:3,3) = ABAS(1:3,1) + ABAS(1:3,2)
C
C------------------------------------------------------- F face centered
         CASE (7,13)
C
            ABAS_HKL(1:3,1) = -ABAS(1:3,1) + ABAS(1:3,2) + ABAS(1:3,3)
            ABAS_HKL(1:3,2) = +ABAS(1:3,1) - ABAS(1:3,2) + ABAS(1:3,3)
            ABAS_HKL(1:3,3) = +ABAS(1:3,1) + ABAS(1:3,2) - ABAS(1:3,3)
C
C----------------------------------------------------- B,C base centered
         CASE (5)
C
            IF ( ABS(ABAS(1,2)).LT.1D-6 .AND. ABS(ABAS(3,2)).LT.1D-6 )
     &           THEN
C                                                            B base (AC)
C
               ABAS_HKL(1:3,1) = +ABAS(1:3,1) + ABAS(1:3,3)
               ABAS_HKL(1:3,2) = +ABAS(1:3,2)
               ABAS_HKL(1:3,3) = -ABAS(1:3,1) + ABAS(1:3,3)
C
            ELSE
C                                                            C base (AB)
C
               ABAS_HKL(1:3,1) = +ABAS(1:3,1) - ABAS(1:3,2)
               ABAS_HKL(1:3,2) = +ABAS(1:3,1) + ABAS(1:3,2)
               ABAS_HKL(1:3,3) = +ABAS(1:3,3)
C
            END IF
C
C------------------------------------------------------------------ else
         CASE (1,2,3,10,11)
C
            ABAS_HKL(1:3,1:3) = ABAS(1:3,1:3)
C
         CASE DEFAULT
C
            STOP 'Bravais lattice not set'
C
         END SELECT
C
         IF ( BRAVAIS.GE.12 .AND. BRAVAIS.LE.14 ) THEN
            ABAS_HKL(1:3,1:3) = 0D0
            DO I = 1,3
               ABAS_HKL(I,I) = 1D0
            END DO
         END IF
C
C---------------------- primitive vectors (BBAS_HKL) of reciprocal space
C
         DO I = 1,3
            I1 = 1 + MOD(I,3)
            I2 = 1 + MOD(I1,3)
            BBAS_HKL(1,I) = ABAS_HKL(2,I1)*ABAS_HKL(3,I2)
     &                      - ABAS_HKL(3,I1)*ABAS_HKL(2,I2)
            BBAS_HKL(2,I) = ABAS_HKL(3,I1)*ABAS_HKL(1,I2)
     &                      - ABAS_HKL(1,I1)*ABAS_HKL(3,I2)
            BBAS_HKL(3,I) = ABAS_HKL(1,I1)*ABAS_HKL(2,I2)
     &                      - ABAS_HKL(2,I1)*ABAS_HKL(1,I2)
         END DO
         VOLUC_HKL = DABS(ABAS_HKL(1,1)*BBAS_HKL(1,1)+ABAS_HKL(2,1)
     &               *BBAS_HKL(2,1)+ABAS_HKL(3,1)*BBAS_HKL(3,1))
C
         BBAS_HKL(1:3,1:3) = BBAS_HKL(1:3,1:3)/VOLUC_HKL
C
         DO I = 1,3
            DO J = 1,3
               BDBMAT_HKL(I,J) = DDOT(3,BBAS_HKL(1,I),1,BBAS_HKL(1,J),1)
            END DO
         END DO
C
         WRK3X3_HKL(1:3,1:3) = BDBMAT_HKL(1:3,1:3)
C
         CALL RMATINV(3,3,WRK3X3_HKL,BDBINV_HKL)
C
         MILLER_INDICES(1:3) = 0
C
         DO K = 1,3
C
            DO I = 1,3
               BP(I) = DDOT(3,BBAS_HKL(1,K),1,BBAS(1,I),1)
            END DO
C
            DO I = 1,3
               AP(I) = 0D0
               DO J = 1,3
                  AP(I) = AP(I) + BDBINV(I,J)*BP(J)
               END DO
               AP(I) = 2*AP(I)
               IF ( (NINT(AP(I))-AP(I)).GT.1D-6 ) WRITE (6,*) '***',K,I,
     &              AP(I)
            END DO
C
            DO I = 1,3
               MILLER_INDICES(K) = MILLER_INDICES(K)
     &                             + MILLER_INDICES_INP(I)*NINT(AP(I))
            END DO
C
         END DO
C
      END IF
C
      CALL RVECLCIB(MILLER_INDICES_INP(1),MILLER_INDICES_INP(2),
     &              MILLER_INDICES_INP(3),BBAS_HKL,VNORM_INP)
C
      CALL RVECLCIB(MILLER_INDICES(1),MILLER_INDICES(2),
     &              MILLER_INDICES(3),BBAS,VNORM)
C
C-----------------------------------------------------------------------
C                            get   A1_new
C-----------------------------------------------------------------------
      IF ( CHECK ) WRITE (6,*) '**************************************'
C
      VMIN = 1D+6
      VEC_SEL(1:3) = -1D+6
C
      DO I1 = -IMAX,IMAX
         NVEC(1) = I1
         DO I2 = -IMAX,IMAX
            NVEC(2) = I2
            DO I3 = -IMAX,IMAX
               NVEC(3) = I3
C
               IF ( (I1*MILLER_INDICES(1)+I2*MILLER_INDICES(2)+I3*
     &              MILLER_INDICES(3)).EQ.0 .AND. 
     &              .NOT.(I1.EQ.0 .AND. I2.EQ.0 .AND. I3.EQ.0) ) THEN
C
                  CALL RVECLCIB(I1,I2,I3,ABAS,VEC)
C
                  V = DNRM2(3,VEC,1)
C
                  IF ( ABS(V-VMIN).LT.1D-8 ) THEN
C
                     IF ( ABS(VEC(1)-VEC_SEL(1)).LT.1D-8 ) THEN
C
                        IF ( ABS(VEC(2)-VEC_SEL(2)).LT.1D-8 ) THEN
C
                           IF ( ABS(VEC(3)-VEC_SEL(3)).LT.1D-8 ) THEN
                              WRITE (6,*) 'for A1: vec = vec_sel !!!!'
                              IERROR = 1
                              RETURN
                           ELSE IF ( VEC(3).GT.VEC_SEL(3) ) THEN
                              NVEC_SEL = NVEC
                              VEC_SEL = VEC
                              VMIN = V
                           END IF
C
                        ELSE IF ( VEC(2).GT.VEC_SEL(2) ) THEN
                           NVEC_SEL = NVEC
                           VEC_SEL = VEC
                           VMIN = V
                        END IF
C
                     ELSE IF ( VEC(1).GT.VEC_SEL(1) ) THEN
                        NVEC_SEL = NVEC
                        VEC_SEL = VEC
                        VMIN = V
                     END IF
C
                  ELSE IF ( V.LT.VMIN ) THEN
                     NVEC_SEL = NVEC
                     VEC_SEL = VEC
                     VMIN = V
                  END IF
C
                  IF ( CHECK ) WRITE (6,99004) I1,I2,I3,VEC,V,VEC_SEL,
     &                                VMIN
C
               END IF
C
            END DO
         END DO
      END DO
C
      ABAS_NEW(1:3,1) = VEC_SEL(1:3)
      NABAS_NEW(1:3,1) = NVEC_SEL(1:3)
      A1_NEW = DNRM2(3,ABAS_NEW(1,1),1)
C
C-----------------------------------------------------------------------
C                            get   A2_new
C-----------------------------------------------------------------------
      IF ( CHECK ) WRITE (6,*) '**************************************'
C
      VMIN = 1D+6
      VEC_SEL(1:3) = -1D+6
C
      DO I1 = -IMAX,IMAX
         NVEC(1) = I1
         DO I2 = -IMAX,IMAX
            NVEC(2) = I2
            DO I3 = -IMAX,IMAX
               NVEC(3) = I3
C
               CALL RVECLCIB(I1,I2,I3,ABAS,VEC)
C
               V = DNRM2(3,VEC,1)
C
               A1DOTV = ABS(DDOT(3,VEC,1,ABAS_NEW(1,1),1))
C
               IF ( (I1*MILLER_INDICES(1)+I2*MILLER_INDICES(2)+I3*
     &              MILLER_INDICES(3)).EQ.0 .AND. ABS(A1DOTV-A1_NEW*V)
     &              .GT.1D-8 ) THEN
C
                  IF ( ABS(V-VMIN).LT.1D-8 ) THEN
C
                     IF ( ABS(VEC(1)-VEC_SEL(1)).LT.1D-8 ) THEN
C
                        IF ( ABS(VEC(2)-VEC_SEL(2)).LT.1D-8 ) THEN
C
                           IF ( ABS(VEC(3)-VEC_SEL(3)).LT.1D-8 ) THEN
                              WRITE (6,*) 'for A3: vec = vec_sel !!!!'
                              IERROR = 1
                              RETURN
                           ELSE IF ( VEC(3).GT.VEC_SEL(3) ) THEN
                              NVEC_SEL = NVEC
                              VEC_SEL = VEC
                              VMIN = V
                           END IF
C
                        ELSE IF ( VEC(2).GT.VEC_SEL(2) ) THEN
                           NVEC_SEL = NVEC
                           VEC_SEL = VEC
                           VMIN = V
                        END IF
C
                     ELSE IF ( VEC(1).GT.VEC_SEL(1) ) THEN
                        NVEC_SEL = NVEC
                        VEC_SEL = VEC
                        VMIN = V
                     END IF
C
                  ELSE IF ( V.LT.VMIN ) THEN
                     NVEC_SEL = NVEC
                     VEC_SEL = VEC
                     VMIN = V
                  END IF
C
                  IF ( CHECK ) WRITE (6,99004) I1,I2,I3,VEC,V,VEC_SEL,
     &                                VMIN
C
               END IF
C
            END DO
         END DO
      END DO
C
      ABAS_NEW(1:3,2) = VEC_SEL(1:3)
      NABAS_NEW(1:3,2) = NVEC_SEL(1:3)
C
C-----------------------------------------------------------------------
C                            get   A3_new
C-----------------------------------------------------------------------
      IF ( CHECK ) WRITE (6,*) '**************************************'
C
      VOLMIN = 1D+6
      VMIN = 1D+6
      VEC_SEL(1:3) = -1D+6
C
      DO I1 = -IMAX,IMAX
         NVEC(1) = I1
         DO I2 = -IMAX,IMAX
            NVEC(2) = I2
            DO I3 = -IMAX,IMAX
               NVEC(3) = I3
C
               IF ( (I1*MILLER_INDICES(1)+I2*MILLER_INDICES(2)+I3*
     &              MILLER_INDICES(3)).NE.0 ) THEN
C
                  CALL RVECLCIB(I1,I2,I3,ABAS,VEC)
C
                  V = DNRM2(3,VEC,1)
C
                  V23(1) = ABAS_NEW(2,2)*VEC(3) - ABAS_NEW(3,2)*VEC(2)
                  V23(2) = ABAS_NEW(3,2)*VEC(1) - ABAS_NEW(1,2)*VEC(3)
                  V23(3) = ABAS_NEW(1,2)*VEC(2) - ABAS_NEW(2,2)*VEC(1)
C
                  VOL = ABS(ABAS_NEW(1,1)*V23(1)+ABAS_NEW(2,1)*V23(2)
     &                  +ABAS_NEW(3,1)*V23(3))
C
                  IF ( ABS(VOL-VOLMIN).LT.1D-8 ) THEN
C
                     IF ( ABS(V-VMIN).LT.1D-8 ) THEN
C
                        IF ( ABS(VEC(3)-VEC_SEL(3)).LT.1D-8 ) THEN
C
                           IF ( ABS(VEC(2)-VEC_SEL(2)).LT.1D-8 ) THEN
C
                              IF ( ABS(VEC(1)-VEC_SEL(1)).LT.1D-8 ) THEN
                                 WRITE (6,*)
     &                                   'for A3: vec = vec_sel !!!!'
                                 IERROR = 1
                                 RETURN
                              ELSE IF ( VEC(1).GT.VEC_SEL(1) ) THEN
                                 NVEC_SEL = NVEC
                                 VEC_SEL = VEC
                                 VMIN = V
                                 VOLMIN = VOL
                              END IF
C
                           ELSE IF ( VEC(2).GT.VEC_SEL(2) ) THEN
                              NVEC_SEL = NVEC
                              VEC_SEL = VEC
                              VMIN = V
                              VOLMIN = VOL
                           END IF
C
                        ELSE IF ( VEC(3).LT.VEC_SEL(3) ) THEN
                           NVEC_SEL = NVEC
                           VEC_SEL = VEC
                           VMIN = V
                           VOLMIN = VOL
                        END IF
C
                     ELSE IF ( V.LT.VMIN ) THEN
                        NVEC_SEL = NVEC
                        VEC_SEL = VEC
                        VMIN = V
                        VOLMIN = VOL
                     END IF
C
                  ELSE IF ( VOL.LT.VOLMIN ) THEN
                     NVEC_SEL = NVEC
                     VEC_SEL = VEC
                     VMIN = V
                     VOLMIN = VOL
                  END IF
C
                  IF ( CHECK ) WRITE (6,99004) I1,I2,I3,VEC,V,VEC_SEL,
     &                                VMIN,VOL,VOLMIN
C
               END IF
C
            END DO
         END DO
      END DO
C
      IF ( DDOT(3,VEC_SEL,1,VNORM,1).LT.0D0 ) THEN
         ABAS_NEW(1:3,3) = -VEC_SEL(1:3)
         NABAS_NEW(1:3,3) = -NVEC_SEL(1:3)
      ELSE
         ABAS_NEW(1:3,3) = VEC_SEL(1:3)
         NABAS_NEW(1:3,3) = NVEC_SEL(1:3)
      END IF
C
C-----------------------------------------------------------------------
C                       adjust sign of A2_new
C-----------------------------------------------------------------------
C
      VEC(1) = ABAS_NEW(2,2)*ABAS_NEW(3,3) - ABAS_NEW(3,2)*ABAS_NEW(2,3)
      VEC(2) = ABAS_NEW(3,2)*ABAS_NEW(1,3) - ABAS_NEW(1,2)*ABAS_NEW(3,3)
      VEC(3) = ABAS_NEW(1,2)*ABAS_NEW(2,3) - ABAS_NEW(2,2)*ABAS_NEW(1,3)
C
      VOLUC_NEW = ABAS_NEW(1,1)*VEC(1) + ABAS_NEW(2,1)*VEC(2)
     &            + ABAS_NEW(3,1)*VEC(3)
C
      IF ( VOLUC_NEW.LT.0D0 ) THEN
         VOLUC_NEW = -VOLUC_NEW
         ABAS_NEW(1:3,2) = -ABAS_NEW(1:3,2)
         NABAS_NEW(1:3,2) = -NABAS_NEW(1:3,2)
      END IF
C
C-----------------------------------------------------------------------
      IF ( CHECK ) WRITE (6,*) '**************************************'
C
C
      WRITE (6,99005) 'old primitve vectors'
      WRITE (6,99006) 'A1    ',(ABAS(1:3,1)),DNRM2(3,ABAS(1,1),1)
      WRITE (6,99006) 'A2    ',(ABAS(1:3,2)),DNRM2(3,ABAS(1,2),1)
      WRITE (6,99006) 'A3    ',(ABAS(1:3,3)),DNRM2(3,ABAS(1,3),1)
C
      WRITE (6,99005) 'old reciprocal primitve vectors'
      WRITE (6,99006) 'B1    ',(BBAS(1:3,1)),DNRM2(3,BBAS(1,1),1)
      WRITE (6,99006) 'B2    ',(BBAS(1:3,2)),DNRM2(3,BBAS(1,2),1)
      WRITE (6,99006) 'B3    ',(BBAS(1:3,3)),DNRM2(3,BBAS(1,3),1)
C
      WRITE (6,99005) 'new primitve vectors'
      WRITE (6,99007) 'A1    ',(NABAS_NEW(1:3,1)),(ABAS_NEW(1:3,1)),
     &                DNRM2(3,ABAS_NEW(1,1),1)
      WRITE (6,99007) 'A2    ',(NABAS_NEW(1:3,2)),(ABAS_NEW(1:3,2)),
     &                DNRM2(3,ABAS_NEW(1,2),1)
      WRITE (6,99007) 'A3    ',(NABAS_NEW(1:3,3)),(ABAS_NEW(1:3,3)),
     &                DNRM2(3,ABAS_NEW(1,3),1)
C
      WRITE (6,*)
      WRITE (6,99008) MILLER_INDICES_INP,VNORM_INP,MILLER_INDICES,VNORM
C
      WRITE (6,99005) 'used old reciprocal primitve vectors'
      WRITE (6,99006) 'B1    ',(BBAS_HKL(1:3,1))
      WRITE (6,99006) 'B2    ',(BBAS_HKL(1:3,2))
      WRITE (6,99006) 'B3    ',(BBAS_HKL(1:3,3))
C
      WRITE (6,99009)
      WRITE (6,99006) 'A1 * N',DDOT(3,ABAS_NEW(1,1),1,VNORM,1)
      WRITE (6,99006) 'A2 * N',DDOT(3,ABAS_NEW(1,2),1,VNORM,1)
      WRITE (6,99006) 'A3 * N',DDOT(3,ABAS_NEW(1,3),1,VNORM,1)
C
      WRITE (6,99010) VOLUC,VOLUC_NEW
C
C--------------------------------------------------- check basis vectors
C
      DO I = 1,3
         DO J = 1,3
            AIBJ = DDOT(3,ABAS(1,I),1,BBAS(1,J),1)
            IF ( I.EQ.J ) THEN
               IF ( ABS(AIBJ-1D0).GT.1D-8 ) THEN
                  IERROR = 1
                  WRITE (6,'(2I3,F10.7)') I,J,AIBJ
               END IF
            ELSE IF ( ABS(AIBJ).GT.1D-8 ) THEN
               IERROR = 1
               WRITE (6,'(2i3,f10.7)') I,J,AIBJ
            END IF
         END DO
      END DO
C
C-----------------------------------------------------------------------
C                      find rotation matrix  MGBA
C-----------------------------------------------------------------------
C
      N_LNG = DNRM2(3,VNORM,1)
      N_TET = ACOS(VNORM(3)/N_LNG)
C
      N_XY = SQRT(VNORM(1)**2+VNORM(2)**2)
C
      IF ( ABS(N_XY).LT.1D-8 ) THEN
         N_PHI = 0D0
      ELSE IF ( VNORM(2).GE.0D0 ) THEN
         N_PHI = ACOS(VNORM(1)/N_XY)
      ELSE IF ( VNORM(1).LT.0D0 ) THEN
         N_PHI = PI + ACOS(-VNORM(1)/N_XY)
      ELSE
         N_PHI = 2*PI - ACOS(VNORM(1)/N_XY)
      END IF
C
      CA = COS(N_PHI)
      SA = SIN(N_PHI)
      CB = COS(N_TET)
      SB = SIN(N_TET)
C
      MA(1:3,1:3) = 0D0
      MA(1,1) = +CA
      MA(1,2) = +SA
      MA(2,1) = -SA
      MA(2,2) = +CA
      MA(3,3) = 1D0
C
      MB(1:3,1:3) = 0D0
      MB(1,1) = +CB
      MB(1,3) = -SB
      MB(3,1) = +SB
      MB(3,3) = +CB
      MB(2,2) = 1D0
C
      N_P(1:3) = MATMUL(MA(1:3,1:3),VNORM(1:3))
      A1_P(1:3) = MATMUL(MA(1:3,1:3),ABAS_NEW(1:3,1))
C
      WRITE (6,'(A,3F10.5)') '    '
      WRITE (6,'(A,3F10.5)') '  N     ',VNORM(1:3)
      WRITE (6,'(A,3F10.5)') ' A1     ',ABAS_NEW(1:3,1)
      WRITE (6,'(A,3F10.5)') '    '
      WRITE (6,'(A,3F10.5)') '  N_P   ',N_P
      WRITE (6,'(A,3F10.5)') ' A1_P   ',A1_P
C
      N_PP(1:3) = MATMUL(MB(1:3,1:3),N_P(1:3))
      A1_PP(1:3) = MATMUL(MB(1:3,1:3),A1_P(1:3))
C
      WRITE (6,'(A,3F10.5)') '    '
      WRITE (6,'(A,3F10.5)') '  N_PP  ',N_PP
      WRITE (6,'(A,3F10.5)') ' A1_PP  ',A1_PP
C
      A1_XY = SQRT(A1_PP(1)**2+A1_PP(2)**2)
C
      IF ( ABS(A1_XY).LT.1D-8 ) THEN
         A1_PHI = 0D0
      ELSE IF ( A1_PP(2).GE.0D0 ) THEN
         A1_PHI = ACOS(A1_PP(1)/A1_XY)
      ELSE IF ( A1_PP(1).LT.0D0 ) THEN
         A1_PHI = PI + ACOS(-A1_PP(1)/A1_XY)
      ELSE
         A1_PHI = 2*PI - ACOS(A1_PP(1)/A1_XY)
      END IF
C
      CG = COS(A1_PHI)
      SG = SIN(A1_PHI)
C
      MG(1:3,1:3) = 0D0
      MG(1,1) = +CG
      MG(1,2) = +SG
      MG(2,1) = -SG
      MG(2,2) = +CG
      MG(3,3) = 1D0
C
      N_PPP(1:3) = MATMUL(MG(1:3,1:3),N_PP(1:3))
      A1_PPP(1:3) = MATMUL(MG(1:3,1:3),A1_PP(1:3))
C
      WRITE (6,'(A,3F10.5)') '    '
      WRITE (6,'(A,3F10.5)') '  N_PPP ',N_PPP
      WRITE (6,'(A,3F10.5)') ' A1_PPP ',A1_PPP
C
      MBA(1:3,1:3) = MATMUL(MB(1:3,1:3),MA(1:3,1:3))
      MGBA(1:3,1:3) = MATMUL(MG(1:3,1:3),MBA(1:3,1:3))
C
      N_PPP(1:3) = MATMUL(MGBA(1:3,1:3),VNORM(1:3))
      A1_PPP(1:3) = MATMUL(MGBA(1:3,1:3),ABAS_NEW(1:3,1))
C
      WRITE (6,'(A,3F10.5)') ' ----------------------------------------'
      WRITE (6,'(A,3F10.5)') '  N_PPP ',N_PPP
      WRITE (6,'(A,3F10.5)') ' A1_PPP ',A1_PPP
C
C-----------------------------------------------------------------------
C                  rotate vectors to new coordinate system
C-----------------------------------------------------------------------
C
      DO I = 1,3
         VEC(1:3) = MATMUL(MGBA(1:3,1:3),ABAS_NEW(1:3,I))
         ABAS_NEW(1:3,I) = VEC(1:3)
      END DO
C
      WRITE (6,99005) 'new primitve vectors ---- rotated'
      WRITE (6,99007) 'A1    ',(NABAS_NEW(1:3,1)),(ABAS_NEW(1:3,1)),
     &                DNRM2(3,ABAS_NEW(1,1),1)
      WRITE (6,99007) 'A2    ',(NABAS_NEW(1:3,2)),(ABAS_NEW(1:3,2)),
     &                DNRM2(3,ABAS_NEW(1,2),1)
      WRITE (6,99007) 'A3    ',(NABAS_NEW(1:3,3)),(ABAS_NEW(1:3,3)),
     &                DNRM2(3,ABAS_NEW(1,3),1)
C
      DO I = 1,3
         DO J = 1,3
            ADAMAT_NEW(I,J) = DDOT(3,ABAS_NEW(1,I),1,ABAS_NEW(1,J),1)
         END DO
      END DO
C
      CALL RINVGJ(ADAINV_NEW,ADAMAT_NEW,3,3)
C
C---------------------- primitive vectors (BBAS_NEW) of reciprocal space
C
      DO I = 1,3
         I1 = 1 + MOD(I,3)
         I2 = 1 + MOD(I1,3)
         BBAS_NEW(1,I) = ABAS_NEW(2,I1)*ABAS_NEW(3,I2) - ABAS_NEW(3,I1)
     &                   *ABAS_NEW(2,I2)
         BBAS_NEW(2,I) = ABAS_NEW(3,I1)*ABAS_NEW(1,I2) - ABAS_NEW(1,I1)
     &                   *ABAS_NEW(3,I2)
         BBAS_NEW(3,I) = ABAS_NEW(1,I1)*ABAS_NEW(2,I2) - ABAS_NEW(2,I1)
     &                   *ABAS_NEW(1,I2)
      END DO
      VOLUC = DABS(ABAS_NEW(1,1)*BBAS_NEW(1,1)+ABAS_NEW(2,1)
     &        *BBAS_NEW(2,1)+ABAS_NEW(3,1)*BBAS_NEW(3,1))
C
      BBAS_NEW(1:3,1:3) = BBAS_NEW(1:3,1:3)/VOLUC
C
C
C------------------- basis vectors - rotate and shift into new unit cell
      DO I = 1,3
         WRITE (6,'(A,I3,A,3F10.5)') 'I ',I,'  ABAS   ',ABAS(1:3,I)
      END DO
C
      DO IQ = 1,NQ
C
         QVEC_NEW(1:3) = MATMUL(MGBA(1:3,1:3),QBAS(1:3,IQ))
         WRITE (6,'(A,I3,A,3F10.5)') 'IQ',IQ
         WRITE (6,'(A,I3,A,3F10.5)') 'IQ',IQ,'  Q_old  ',QBAS(1:3,IQ)
         WRITE (6,'(A,I3,A,3F10.5)') 'IQ',IQ,'  Q_rot  ',QVEC_NEW
C
         DO I = 1,3
            AQVEC(I) = DDOT(3,ABAS_NEW(1,I),1,QVEC_NEW,1)
         END DO
         WRITE (6,'(A,I3,A,3F10.5)') 'IQ',IQ,'  Q*A    ',AQVEC
C
         PVEC(1:3) = MATMUL(ADAINV_NEW(1:3,1:3),AQVEC(1:3))
         WRITE (6,'(A,I3,A,3F10.5)') 'IQ',IQ,'  P      ',PVEC
C
         DO I = 1,3
            PVEC(I) = PVEC(I) + 1000D0
            PVEC(I) = PVEC(I) - INT(PVEC(I))
            IF ( ABS(PVEC(I)-1D0).LT.1D-8 ) PVEC(I) = 0D0
         END DO
C
         QBASNEW(1:3,IQ) = MATMUL(ABAS_NEW(1:3,1:3),PVEC(1:3))
         WRITE (6,'(A,I3,A,3F10.5)') 'IQ',IQ,'  Q_new  ',QBASNEW(1:3,IQ)
C
      END DO
C
C-------------------- sort basis vectors QBASNEW with increasing z-value
C
      IQNEW_IQ(1) = 1
      DO IQ = 2,NQ
         QZ = QBASNEW(3,IQ)
         IF ( QZ.LT.QBASNEW(3,IQNEW_IQ(IQ-1)) ) THEN
            DO KQ = 1,(IQ-1)
               IF ( QZ.LT.QBASNEW(3,IQNEW_IQ(KQ)) ) THEN
                  JQ = IQ
                  DO J = KQ,(IQ-1)
                     IQNEW_IQ(JQ) = IQNEW_IQ(JQ-1)
                     JQ = JQ - 1
                  END DO
                  IQNEW_IQ(KQ) = IQ
                  GOTO 100
               END IF
            END DO
         END IF
C
         IQNEW_IQ(IQ) = IQ
 100  END DO
C
      QBASTMP(1:3,1:NQ) = QBASNEW(1:3,1:NQ)
C
      DO IQ = 1,NQ
         IQNEW = IQNEW_IQ(IQ)
         IQ_IQNEW(IQNEW) = IQ
C
         QBASNEW(1:3,IQNEW) = QBASTMP(1:3,IQ)
      END DO
C
      DO IQNEW = 1,NQ
         IQ = IQ_IQNEW(IQNEW)
         WRITE (*,*) 'QBASNEW ordered',IQNEW,IQ,QBASNEW(3,IQNEW)
      END DO
C
      DO IQNEW = 1,NQ
C
         IQ = IQ_IQNEW(IQNEW)
C
         NOQNEW(IQNEW) = NOQ(IQ)
         DO IO = 1,NOQ(IQ)
            ITOQNEW(IO,IQNEW) = ITOQ(IO,IQ)
         END DO
C
      END DO
C
      DO IT = 1,NT
         NATNEW(IT) = NAT(IT)
         DO IA = 1,NAT(IT)
            IQ = IQAT(IA,IT)
            IQNEWAT(IA,IT) = IQNEW_IQ(IQ)
         END DO
      END DO
C
C=======================================================================
C          write result to      xband_geometry.out
C=======================================================================
C
      WRITE (IWR,99002) 'GEOMETRY:  result for TASK = cr_3D_surface'
      WRITE (IWR,99002) 'IERROR   = ',IERROR
      WRITE (IWR,99002) '   ABAS (new)'
      DO J = 1,3
         WRITE (IWR,99003) J,(ABAS_NEW(I,J),I=1,3)
      END DO
      WRITE (IWR,99002) 'NQ       = ',NQ
      WRITE (IWR,99002) '   IQ     QX(IQ)    QY(IQ)    QZ(IQ)  new'
      DO IQ = 1,NQ
         WRITE (IWR,99003) IQ,(QBASNEW(I,IQ),I=1,3)
      END DO
C
      WRITE (IWR,99002) 'new occupation '
      DO IQ = 1,NQ
         WRITE (IWR,99001) IQ,NOQNEW(IQ)
         WRITE (IWR,99001) (ITOQNEW(IO,IQ),IO=1,NOQNEW(IQ))
      END DO
C
      WRITE (IWR,99002) 'IQ_3DBULK(old)  of  IQ_3DSURF(new) '
      DO IQNEW = 1,NQ
         WRITE (IWR,99001) IQNEW,IQ_IQNEW(IQNEW)
      END DO
C
C=======================================================================
C
99001 FORMAT (10I5)
99002 FORMAT (A,3I10,F10.3)
99003 FORMAT (I5,1X,3F18.12)
99004 FORMAT (3I3,3F8.3,2x,f8.3,'  >>> ',3F8.3,2x,3F8.3)
99005 FORMAT (/,10X,A,/)
99006 FORMAT (10X,A,9X,2X,3F12.6,2X,3F12.6)
99007 FORMAT (10X,A,3I3,2X,3F12.6,2X,3F12.6)
99008 FORMAT (10X,'NORM     h  k  l',/,10X,'inp    ',3I3,2X,3F12.6,/,
     &        10X,'old    ',3I3,2X,3F12.6)
99009 FORMAT (/,10X,'dot-products',/)
99010 FORMAT (/,10X,'volume of the unit cell ',/,10X,'old',f26.6,/,10X,
     &        'new',f26.6,/)
C
      END
