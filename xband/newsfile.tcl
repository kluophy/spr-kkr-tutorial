# display local news, edited in file $localnews   
#
# Copyright (C) 1994  G. Lamprecht, W. Lotz, R. Weibezahn; LRW c/o Uni Bremen
# Copyright (C) 1996  G. Lamprecht, W. Lotz, R. Weibezahn; IWD, Bremen University

proc newsfile {f austext} {

  upvar 1 $austext a

  global  vv localnews

  writescr0 $f ""
  set a ""

  writescr $f a "${a}[datime] $vv(ne1)  $localnews\n"



  if { [file exists $localnews] } { 

    if { [file isfile $localnews] } { # display the local news:

      cat_file $localnews $f       

    } else {                                 # $localnews is no regular file!

     set a "${a}***** $vv(aus5) \"$localnews\"  $vv(aus6)\n"

    }

  } else {                                 # $localnews doesn't exist!

   set a "${a}$vv(ne2)\n"

  }

}
