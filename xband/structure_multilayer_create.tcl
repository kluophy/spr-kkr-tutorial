########################################################################################
#                     procedure used to create  2D  and  3D  layered systems
########################################################################################
#                                                            structure_multilayer_create
proc structure_multilayer_create {dimension type case} {

global ML_NSU ML_NML ML_ALAT2to1 ML_SCALE_ISD ML_SUBSYS_REF ML_name
global ML_AX ML_AY ML_AZ ML_CX ML_CY ML_CZ ML_SCALE_C ML_SCALE_C1 ML_SCALE_C2
global ML_BX ML_BY ML_BZ ML_START_IL1 ML_START_IL2
global ML_NLAY ML_NSITES ML_U1 ML_V1 ML_W1
global ML_U2 ML_V2 ML_W2
global ML_Ntot ML_Na1 ML_Na2 ML_Nb1 ML_Nb2 ML_select

global Wcrml
global LATANG LATPAR ALAT BOA COA NQ NCL SPACEGROUP SPACEGROUP_AP
global NQCL WYCKOFFCL RCLU RCLV RCLW BRAVAIS STRUCTURE_TYPE
global RQX RQY RQZ RBASX RBASY RBASZ NQ BRAVAIS
global NQ_SU1 NQ_SU2  NSUBSYS SU_SUBSYS SU_IQ
global N_rep_SU_bulk_L N_rep_SU_bulk_R ZRANGE_TYPE
global IL_START_SUBSYS NLAY_SUBSYS SU_TOP_LAYER
global NQ_bulk_L NQ_bulk_R NLAY_bulk_L NLAY_bulk_R 
global N_rep_SU_a N_rep_SU_b N_SU SU_NAME
global RBASX_L RBASY_L RBASZ_L RBASX_R RBASY_R RBASZ_R

set tcl_precision 17
set PRC "structure_multilayer_create"

if {[string trim $ALAT] ==""} {give_warning "." \
              "WARNING \n\n specify lattice parameter   A  first \n\n " ; return}

set C     $type
set N_SU  $C
set ML    $ML_select

if {$C=="1"} { set SU_NAME(1) [lindex [string trim $ML_name($ML,$C)] 0]}

set STRUCTURE_TYPE [string trim $ML_name($ML,$C)]
if {$dimension=="2D"} {set STRUCTURE_TYPE "2D $STRUCTURE_TYPE"}


for {set su 1} {$su <= $N_SU } {incr su} {
   puts ">>>>>>>>>>>>>>SU_NAME     $SU_NAME($su) "
}

#=======================================================================================
#                              use general indices
#=======================================================================================
if {$dimension!="2D"||$C=="1"} {

set NSUBSYS $C

#
# --------------------------------------------------- fix start index for layer sequence
#
if {$C=="1"} {
   set IL_START_SUBSYS(1) 1
} else {
   if { $ML_NLAY($ML,$C,1)==2 && $ML_NLAY($ML,$C,2)==2 } {
      if {$case=="a"} {
         set IL_START_SUBSYS(1) 1
         set IL_START_SUBSYS(2) 1
      } else {
          set IL_START_SUBSYS(1) 1
          set IL_START_SUBSYS(2) 2
      }
   } else {
          set IL_START_SUBSYS(1) $ML_START_IL1
          set IL_START_SUBSYS(2) $ML_START_IL2
   }
}
#
# -------------------------------------------- fix the upper limit for the layer indices
#
if {$C=="1"} {
   set NLAY_SUBSYS(1) $ML_Ntot($ML,$C)
} else {
    if {$case=="a"} {
       set NLAY_SUBSYS(1) $ML_Na1($ML)
       set NLAY_SUBSYS(2) $ML_Na2($ML)
    } else {
       set NLAY_SUBSYS(1) $ML_Nb1($ML)
       set NLAY_SUBSYS(2) $ML_Nb2($ML)
    }
}

set SU_SUBSYS(1) 1
set SU_SUBSYS(2) 2

}
#=======================================================================================
#
set NLAY_SU(1) $ML_NLAY($ML,$C,1)
for {set IL 1} {$IL <= $NLAY_SU(1) } {incr IL} {
    set NSITES_SU($IL,1) $ML_NSITES($ML,$C,$IL,1)
}   
if {$C==2} {
set NLAY_SU(2) $ML_NLAY($ML,$C,2)
for {set IL 1} {$IL <= $NLAY_SU(2) } {incr IL} {
    set NSITES_SU($IL,2) $ML_NSITES($ML,$C,$IL,2)
}   
}

#
#----------------------------------------------- find number of sites in structure units
#
set NQ_SU1 0
set NQ_SU2 0
for {set IL 1} {$IL <= $ML_NLAY($ML,$C,1) } {incr IL} {
    for {set IS 1} {$IS <= $ML_NSITES($ML,$C,$IL,1)} {incr IS} { incr NQ_SU1 }
}
if {$C==2} {
for {set IL 1} {$IL <= $ML_NLAY($ML,$C,2) } {incr IL} {
    for {set IS 1} {$IS <= $ML_NSITES($ML,$C,$IL,2)} {incr IS} { incr NQ_SU2 }
}
}
#
#----------------------------------------------------------- scale distances and vectors
#
set SCALE_ISD [expr (100.0+$ML_SCALE_ISD)/100.0]
set SCALE_C   [expr (100.0+$ML_SCALE_C  )/100.0]
set SCALE_C1  [expr (100.0+$ML_SCALE_C1 )/100.0]
set SCALE_C2  [expr (100.0+$ML_SCALE_C2 )/100.0]

if {$C==1} {
   debug $PRC "scaling factor C  $SCALE_C"
   set ML_CX($ML,$C,1) [expr $SCALE_C * $ML_CX($ML,$C,1)]
   set ML_CY($ML,$C,1) [expr $SCALE_C * $ML_CY($ML,$C,1)]
   set ML_CZ($ML,$C,1) [expr $SCALE_C * $ML_CZ($ML,$C,1)]
} else {
   debug $PRC "scaling factor C1  $SCALE_C1"
   set ML_CX($ML,$C,1) [expr $SCALE_C1 * $ML_CX($ML,$C,1)]
   set ML_CY($ML,$C,1) [expr $SCALE_C1 * $ML_CY($ML,$C,1)]
   set ML_CZ($ML,$C,1) [expr $SCALE_C1 * $ML_CZ($ML,$C,1)]

   debug $PRC "scaling factor C2  $SCALE_C2"
   set ML_CX($ML,$C,2) [expr $SCALE_C2 * $ML_CX($ML,$C,2)]
   set ML_CY($ML,$C,2) [expr $SCALE_C2 * $ML_CY($ML,$C,2)]
   set ML_CZ($ML,$C,2) [expr $SCALE_C2 * $ML_CZ($ML,$C,2)]

   set CHECK [expr abs($ML_CY($ML,$C,1)*$ML_CZ($ML,$C,2)  \
                      -$ML_CZ($ML,$C,1)*$ML_CY($ML,$C,2)) \
                  +abs($ML_CZ($ML,$C,1)*$ML_CX($ML,$C,2)  \
                      -$ML_CX($ML,$C,1)*$ML_CZ($ML,$C,2)) \
                  +abs($ML_CX($ML,$C,1)*$ML_CY($ML,$C,2)  \
                      -$ML_CY($ML,$C,1)*$ML_CX($ML,$C,2))]
                  
   if {$CHECK>0.0000001} {give_warning "." \
              "WARNING \n\n vectors C1 and C2  \n\n  are not collinear \n\n " ; return}
#
# -------------------- modify ALAT if input refers to subsystem 2
#
   if {$ML_SUBSYS_REF == 2 } { set ALAT  [expr $ALAT * $ML_ALAT2to1($ML,$C)] }

}  ;#------------------------------------------------------------------------------- C=2

#
#------------------------------------------------------------ find NQ_bulk and NLAY_bulk
#
if {$dimension=="2D"} {
if {$ZRANGE_TYPE == "extended"} {

   for {set loop 1} {$loop <= 2 } {incr loop} {

      if {$loop==1} {
         set su $SU_SUBSYS(1)
      } else {
         set su $SU_SUBSYS($NSUBSYS)
      }

      set IQ   0 
      set ILAY 0
      for {set IL 1} {$IL <= $NLAY_SU($su) } {incr IL} {
          set IQ [expr $IQ + $NSITES_SU($IL,$su)]
      }

      if {$loop==1} {
         set NQ_bulk_L   [ expr $N_rep_SU_bulk_L * $IQ ]
         set NLAY_bulk_L [ expr $N_rep_SU_bulk_L * $NLAY_SU($su) ]
         set RBASX_L(3)  [ expr $N_rep_SU_bulk_L * $ML_CX($ML,$C,$su) ]
         set RBASY_L(3)  [ expr $N_rep_SU_bulk_L * $ML_CY($ML,$C,$su) ]
         set RBASZ_L(3)  [ expr $N_rep_SU_bulk_L * $ML_CZ($ML,$C,$su) ]
      } else {
         set NQ_bulk_R   [ expr $N_rep_SU_bulk_R * $IQ ]
         set NLAY_bulk_R [ expr $N_rep_SU_bulk_R * $NLAY_SU($su) ]
         set RBASX_R(3)  [ expr $N_rep_SU_bulk_R * $ML_CX($ML,$C,$su) ]
         set RBASY_R(3)  [ expr $N_rep_SU_bulk_R * $ML_CY($ML,$C,$su) ]
         set RBASZ_R(3)  [ expr $N_rep_SU_bulk_R * $ML_CZ($ML,$C,$su) ]
      }
    
   } 

}}
#
# ------------- find the thickness LDIST($IL,1) associated with layer $IL in subsystem 1
#
set ILTOP1 $ML_NLAY($ML,$C,1)
set WTOP  [expr 1.0+$ML_W1($ML,$C,1,1) ]
set sum_LDIST 0
set sum_NLDIST 0
if {$ML_NLAY($ML,$C,1)==1} {
    set LDIST(1,1) 1.0
    set NLDIST(1,1) 1.0
    set sum_LDIST  1.0
    set sum_NLDIST 1.0
} else {
for {set IL 1} {$IL <= $ILTOP1 } {incr IL} {
    set ILp [expr $IL + 1]
    set ILm [expr $IL - 1]
    if { $IL==1 } {
       set LDIST($IL,1) [expr 0.5*($ML_W1($ML,$C,$ILp,1)-$ML_W1($ML,$C,1,1) \
                                                  +$WTOP-$ML_W1($ML,$C,$ILTOP1,1))]
       set NLDIST($IL,1) [expr 1.0-$ML_W1($ML,$C,$ILTOP1,1)]
    } elseif { $IL==$ILTOP1 } {
       set LDIST($IL,1) [expr 0.5*($WTOP-$ML_W1($ML,$C,$ILm,1))]
       set NLDIST($IL,1) [expr abs($ML_W1($ML,$C,$IL,1)-$ML_W1($ML,$C,$ILm,1))]
    } else {
       set LDIST($IL,1) [expr 0.5*($ML_W1($ML,$C,$ILp,1)-$ML_W1($ML,$C,$ILm,1))]
       set NLDIST($IL,1) [expr abs($ML_W1($ML,$C,$IL,1)-$ML_W1($ML,$C,$ILm,1))]
    }
    set sum_LDIST [expr $sum_LDIST + $LDIST($IL,1)]
    set sum_NLDIST [expr $sum_NLDIST + $NLDIST($IL,1)]
}
}
if { [expr abs($sum_LDIST-1.0)] > 0.0000001 } {give_warning "." \
              "WARNING from <structure_multilayer_create> \n \
               \n sum interlayer distances /= 1 \n\n \
               for sub-system 1" ; return}
debug $PRC "SUM 1   $sum_LDIST "
puts "NSUM 1   $sum_NLDIST"

#
# ------------- find the thickness LDIST($IL,2) associated with layer $IL in subsystem 2
#
if {$C==2} {
set ILTOP2 $ML_NLAY($ML,$C,2)
set WTOP  [expr 1.0+$ML_W2($ML,$C,1,1) ]
set sum_LDIST 0
set sum_NLDIST 0
if {$ML_NLAY($ML,$C,1)==1} {
    set LDIST(1,2) 1.0
    set NLDIST(1,2) 1.0
    set sum_LDIST  1.0
} else {
for {set IL 1} {$IL <= $ILTOP2 } {incr IL} {
    set ILp [expr $IL + 1]
    set ILm [expr $IL - 1]
    if { $IL==1 } {
       set LDIST($IL,2) [expr 0.5*($ML_W2($ML,$C,$ILp,1)-$ML_W2($ML,$C,1,1) \
                                                 +$WTOP-$ML_W2($ML,$C,$ILTOP2,1))]
       set NLDIST($IL,2) [expr 1.0-$ML_W2($ML,$C,$ILTOP2,1)]
    } elseif { $IL==$ILTOP2 } {
       set LDIST($IL,2) [expr 0.5*($WTOP-$ML_W2($ML,$C,$ILm,1))]
       set NLDIST($IL,2) [expr abs($ML_W2($ML,$C,$IL,1)-$ML_W2($ML,$C,$ILm,1))]
    } else {
       set LDIST($IL,2) [expr 0.5*($ML_W2($ML,$C,$ILp,1)-$ML_W2($ML,$C,$ILm,1))]
       set NLDIST($IL,2) [expr abs($ML_W2($ML,$C,$IL,1)-$ML_W2($ML,$C,$ILm,1))]
    }
    set sum_LDIST [expr $sum_LDIST + $LDIST($IL,2)]
    set sum_NLDIST [expr $sum_NLDIST + $NLDIST($IL,2)]
}
}
if { [expr abs($sum_LDIST-1.0)] > 0.0000001 } {give_warning "." \
              "WARNING from <structure_multilayer_create> \n\n  \
               sum interlayer distances /= 1 \n\n \
               for sub-system 1" ; return}
debug $PRC "SUM 2   $sum_LDIST "
puts "NSUM 2   $sum_NLDIST"
}

#=======================================================================================
#
# ----------------------------------- get the atomic positions  RQ  in subsystem isubsys
#
#SW: set first layer at z=0 if no scaling is applied
set IQ 0
set wsu(1) 0.0
set wsu(2) 0.0

for {set isubsys 1} {$isubsys <= $NSUBSYS } {incr isubsys} {

   set su $SU_SUBSYS($isubsys)

   set IL [expr $IL_START_SUBSYS($isubsys) - 1]

   for {set ILAY 1} {$ILAY <= $NLAY_SUBSYS($isubsys) } {incr ILAY} {
      incr IL
      if {$IL>$NLAY_SU($su)} {set IL 1} 

      if {$ILAY==1} {
         if {$SCALE_ISD > 1.000} {
            set dw [expr 0.5*$LDIST($IL,$su)*$SCALE_ISD]
         } else {
            set dw 0.0
         }
      } else {
         set dw $NLDIST($IL,$su)
      }
      set wsu($su) [expr $wsu($su) + $dw]

      for {set IS 1} {$IS <= $NSITES_SU($IL,$su)} {incr IS} {
         if {$su==1} {
         set u0 $ML_U1($ML,$C,$IL,$IS)
         set v0 $ML_V1($ML,$C,$IL,$IS)
         } else {
         set u0 $ML_U2($ML,$C,$IL,$IS)
         set v0 $ML_V2($ML,$C,$IL,$IS)
         }
   
         for {set I_rep_SU_a 1} {$I_rep_SU_a <= $N_rep_SU_a} {incr I_rep_SU_a} {
         for {set I_rep_SU_b 1} {$I_rep_SU_b <= $N_rep_SU_b} {incr I_rep_SU_b} {

	 set u [expr $u0 + $I_rep_SU_a - 1]
	 set v [expr $v0 + $I_rep_SU_b - 1]
        
         incr IQ
         set SU_IQ($IQ)  $su 

         set RQX($IQ) [expr $u*$ML_AX($ML,$C) + $v*$ML_BX($ML,$C)]
         set RQY($IQ) [expr $u*$ML_AY($ML,$C) + $v*$ML_BY($ML,$C)]
         set RQZ($IQ) [expr $u*$ML_AZ($ML,$C) + $v*$ML_BZ($ML,$C)]
   
         for {set sup 1} {$sup <= $C } {incr sup} {
            set RQX($IQ) [expr $RQX($IQ) + $wsu($sup)*$ML_CX($ML,$C,$sup)]
            set RQY($IQ) [expr $RQY($IQ) + $wsu($sup)*$ML_CY($ML,$C,$sup)]
            set RQZ($IQ) [expr $RQZ($IQ) + $wsu($sup)*$ML_CZ($ML,$C,$sup)]
         }
   
         debug $PRC "isubsys $isubsys SU $su ILAY $ILAY IQ $IQ IL $IL IS $IS w $wsu($su)"
         set aux [format "%3i  %12.6f%12.6f%12.6f" $IQ $RQX($IQ) $RQY($IQ) $RQZ($IQ)]
         debug $PRC "coord $aux"

         }
         } 

      }
   
   
      if {$ILAY==$NLAY_SUBSYS($isubsys)} {
         if {$SCALE_ISD > 1.0} {
            set dw [expr 0.5*$LDIST($IL,$su)*$SCALE_ISD]
         } else {
#           set dw 0.0
            set dw [expr $LDIST($IL,$su)*$SCALE_ISD]
         }
      } else {
       set dw 0.0
      }
      set wsu($su) [expr $wsu($su) + $dw]

   }

}

#=======================================================================================

set NQ $IQ

set w1 $wsu(1) 
set w2 $wsu(2) 

set RBASX(1) [expr $N_rep_SU_a * $ML_AX($ML,$C)]
set RBASY(1) [expr $N_rep_SU_a * $ML_AY($ML,$C)]
set RBASZ(1) [expr $N_rep_SU_a * $ML_AZ($ML,$C)]
		    	       	 
set RBASX(2) [expr $N_rep_SU_b * $ML_BX($ML,$C)]
set RBASY(2) [expr $N_rep_SU_b * $ML_BY($ML,$C)]
set RBASZ(2) [expr $N_rep_SU_b * $ML_BZ($ML,$C)]

if {$C=="1"} {
   set RBASX(3) [expr $w1*$ML_CX($ML,$C,1)]
   set RBASY(3) [expr $w1*$ML_CY($ML,$C,1)]
   set RBASZ(3) [expr $w1*$ML_CZ($ML,$C,1)]
} else {     	      
   set RBASX(3) [expr $w1*$ML_CX($ML,$C,1) + $w2*$ML_CX($ML,$C,2)]
   set RBASY(3) [expr $w1*$ML_CY($ML,$C,1) + $w2*$ML_CY($ML,$C,2)]
   set RBASZ(3) [expr $w1*$ML_CZ($ML,$C,1) + $w2*$ML_CZ($ML,$C,2)]
}

for {set i 1} {$i <= 2} {incr i} {
   set RBASX_L($i) $RBASX($i)
   set RBASY_L($i) $RBASY($i)
   set RBASZ_L($i) $RBASZ($i)
   		             
   set RBASX_R($i) $RBASX($i)
   set RBASY_R($i) $RBASY($i)
   set RBASZ_R($i) $RBASZ($i)
}   

#
#----------------------------------------------------- set_lattice_parameters_using_RBAS
#

set_lattice_parameters_using_RBAS


#===============================================================================
destroy $Wcrml

set BRAVAIS 0

structure_per_pedes_read_coord use_primitive_vectors

}   
#                                                        structure_multilayer_create END
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
