########################################################################################
#                                       TB-LMTO
########################################################################################
#                                                                         create_inpfile
#     
#     this procedure supplies the PACKAGE DEPENDENT part of the inpfile set up and 
#     adds the corresponding entries to the input mask created by   create_inpfile
#     
#  
proc create_inpfile_TB-LMTO {} {
    global structure_window_calls inpfile inpsuffix 
    global sysfile syssuffix  PACKAGE
    global BOA COA SPACEGROUP SPACEGROUP_AP TABCHSYM iprint IREL Wcsys Wcinp
    global system NQ NT ZT NQCL RQX RQY RQZ NOQ MAG_DIR NLQ CONC TXTT
    global NCL  NQCL  ITOQ TABBRAVAIS BRAVAIS IQAT RWS
    global RBASX RBASY RBASZ ALAT LATPAR LATANG STRUCTURE_TYPE
    global editor edback edxterm edoptions W_rasmol_button  W_findsym_button
    global Wprog inpsuffixlist VAR VARLST NCPA COLOR

# default: inpfile = inpsuffix = "CTRL"  

   pack forget $Wcinp.a.aa.edit
   pack forget $Wcinp.a.bb.write
   pack forget $Wcinp.a.bb.append

   if {$NCPA>0} {
      give_warning "." "WARNING \n\n \
      NCPA > 0   in  <create_inpfile> \n\n\n \
      program package \n\n $PACKAGE \n\n \
      cannot deal with non-stoichiometric compounds \n\n "
      destroy $Wcinp 
      set sysfile ""
      create_inpfile     
      return
    }
   set inpfile   "CTRL"   
} 
#                                                                     END create_inpfile
########################################################################################



########################################################################################
#                                                                          write_inpfile
proc write_inpfile_TB-LMTO {Wcinp tuwas} {
    global structure_window_calls inpfile inpsuffix potfile potsuffix
    global sysfile syssuffix  PACKAGE
    global BOA COA SPACEGROUP SPACEGROUP_AP TABCHSYM iprint IREL Wcsys
    global system NQ NT ZT NQCL RQX RQY RQZ NOQ MAG_DIR NLQ CONC TXTT NAT
    global ITOQ TABBRAVAIS BRAVAIS IQAT RWS IBAS LAT 
    global RBASX RBASY RBASZ ALAT LATPAR LATANG STRUCTURE_TYPE
    global Wprog inpsuffixlist  VAR VARLST 
    global editor edtext edxterm edoptions edsyntaxhelp edback  
    global NM IMT IMQ RWSM QMTET QMPHI 
    global buttonlist_PROGS
    global NCL NQCL IQECL ICLQ

    set tcl_precision 17
#
   set inpfile   "CTRL"   
   set inpsuffix "CTRL"   

   set file_name "CTRL"
   if {$tuwas == "append" && [file exists $file_name]==1} {
      set inp [open $file_name a]
   } else {
      set inp [open $file_name w]
   }

   puts  $inp  "STRUC   ALAT=$ALAT"
   for {set I 1} {$I <= 3} {incr I} {
     set BV($I) [format "%14.8f%14.8f%14.8f" $RBASX($I) $RBASY($I) $RBASZ($I)]
   }
   puts  $inp  "        PLAT=$BV(1)"
   puts  $inp  "             $BV(2)"
   puts  $inp  "             $BV(3)"
   set head "CLASS  "
   for {set IT 1} {$IT <= $NT} {incr IT} {
     set TXT [string range "$TXTT($IT)       " 0 7]
     puts $inp "$head ATOM=$TXT Z=$ZT($IT)"
   set head "       "
   }
   set head "SITE   "
   for {set IQ 1} {$IQ <= $NQ} {incr IQ} {
      set IT  $ITOQ(1,$IQ) 
      set XYZ [format "%14.8f%14.8f%14.8f" $RQX($IQ) $RQY($IQ) $RQZ($IQ)]
      set TXT [string range "$TXTT($IT)       " 0 7]
      puts $inp "$head ATOM=$TXT POS=$XYZ"
      set head "       "
   }

   close $inp

} 
#                                                                      END write_inpfile
########################################################################################
