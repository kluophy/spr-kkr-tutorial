      character infile*80,outfile 
c      call getarg(1,infile)
      call atom(infile,outfile,1,1,1)
      end
      subroutine atom(infile,outfile,n1,n2,n3)
      include 'ATOMS.FI'
c
c
      dimension s_sort(MAXSRT)
     $     ,qsp(3) 
      character*24 atname(MAXSRT)
      dimension tau(3,MAXAT),is(MAXAT),spinangle(3,MAXAT),qspir(3)
      dimension rbas(3,3),rcell(3,4),vcor(3),planes(4,8),axes(3,4),cg(3)
      dimension r_c(3,2,18),r_p(3,2,12)
      dimension r_at(3,MAXR),is_at(MAXR) ! atoms
      dimension r_sp(2,MAXR),is_sp(MAXR) ! spins
      character infile*(*),outfile*(*)

      if(n1.eq.0)n1=1
      if(n2.eq.0)n2=1
      if(n3.eq.0)n3=1

c
      call read_inp(infile,nsort,s_sort,ispins
     $     ,qsp,atname,rbas,ibcell,rcell,ncell,epscell,natom,tau
     $     ,is,nbond,iaxes,axes,vcor,iprim,iqspir,qspir,spinangle
     $     ,n1,n2,n3)
c
      call cutting_planes(ibcell,rcell,epscell,nplanes,planes,cg
     $     ,nr_c,r_c)
      call prim_cell(iprim,rbas,cg,nr_p,r_p)
c generate coordinates for atoms and spins
      call gen_atpos(rbas,rcell,ncell,nplanes,planes,cg
     $     ,natom,tau,is,s_sort,ispins,qsp,nr_at,r_at
     $     ,is_at,nr_sp,r_sp,is_sp,iqspir,qspir,spinangle)
c data for OpenGL
      open(18,file=outfile)
      call data4gl(nr_at,r_at,is_at,nsort,s_sort,atname
     $     ,nr_c,r_c,r_sp,iqspir,rbas)
      call def_bz(rbas,18)
      close(18)
      end
      subroutine read_inp(infile,nsort,s_sort,ispins
     $     ,qsp,atname,rbas,ibcell,rcell,ncell,epscell,natom,tau
     $     ,is,nbond,iaxes,axes,vcor,iprim,iqspir,qspir,spinangle,
     $     n1,n2,n3)
c
      include 'ATOMS.FI'
      parameter (au2a=0.529177)
c
c
      character*(*) infile
      character*100 sfile
      dimension s_sort(*),qsp(3) ! output
      character*24 atname(*),buf
      dimension tau(3,*),is(*),spinangle(3,*),qspir(3)
      dimension rbas(3,3),rcell(3,4),vcor(3),axes(3,4)
      dimension tc(3,3),r0(3,3,7),rext(3,4),nx(3) ! local
      double precision alat8,boa8,coa8,a(3),b(3),c(3),tau8(3)

c      character sdt_file*80,outfile*80
      logical hex
C
      data tc/1.,0.,0.,0.,1.,0.,0.,0.,1./
      data nx /1,1,1/
      data r0 
     $     / 1.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 1.0 ! bcc
     $     , 1.0, 1.0,-1.0,-1.0, 1.0, 1.0, 1.0,-1.0, 1.0 ! fcc
     $     , 1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0,-1.0, 1.0 ! ac
     $     , 1.0, 0.0, 1.0, 0.0, 1.0, 0.0,-1.0, 0.0, 1.0 ! bc
     $     , 1.0, 1.0, 0.0,-1.0, 1.0, 0.0, 0.0, 0.0, 1.0 ! cc
     $     , 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0 ! hcp
     $     , 1.0,-1.0, 0.0, 0.0, 1.0,-1.0, 1.0, 1.0, 1.0 / ! r

c$$$      tc(1,1)=tc(1,1)*n1
c$$$      tc(2,2)=tc(2,2)*n2
c$$$      tc(3,3)=tc(3,3)*n3

      pi=4.*atan(1.)
      g2r=pi/180.
      hex=.false.
      iaxes=0
      nbond=0
      rscale=1.0
      ibcell=0
      iprim=0
      epscell=1.e-3!5
      ispins=0
      do i=1,3
        qsp(i)=0.
      enddo
      do i=1,3
        do j=1,4
          axes(i,j)=0.
        enddo
        axes(i,i)=0.25
      enddo
      iun11=16
      if(infile.ne.'-')then
        open(iun11,file=infile,form='formatted',status='old')
      endif
      k=0
      do i=1,80
        if(infile(i:).eq.'.lmt')then
          k=i
          exit
        endif
      enddo
      if(k.ne.0)then
        read(iun11,*)
        read(iun11,*)
        read(iun11,*)
        read(iun11,*)nsort
        read(iun11,*)
        do j=1,nsort
          read(iun11,*)x,x,x,atname(j),x,x,s_sort(j)
        enddo
        close(iun11)
        sfile=infile(:k)//'sdt'
        open(iun11,file=sfile,form='unformatted')
        read(iun11)
        read(iun11)a,b,c
        read(iun11)
        read(iun11)natom,nsort
        read(iun11)alat8,boa8,coa8
        rbas(1,1)=a(1)
        rbas(2,1)=a(2)*boa8
        rbas(3,1)=a(3)*coa8
        rbas(1,2)=b(1)
        rbas(2,2)=b(2)*boa8
        rbas(3,2)=b(3)*coa8
        rbas(1,3)=c(1)
        rbas(2,3)=c(2)*boa8
        rbas(3,3)=c(3)*coa8
        do j=1,natom
          read(iun11)(tau8(i),i=1,3),is(j)
          tau(1,j)=tau8(1)
          tau(2,j)=tau8(2)*boa8
          tau(3,j)=tau8(3)*coa8
        enddo
        close(iun11)
        open(iun11,file=infile,form='formatted',status='old')
        iostat=0
        do while (iostat.eq.0)
          read(iun11,'(a)',iostat=iostat)sfile
          if(sfile.eq.'SPIN_WAVE')exit
        enddo
        if(iostat.eq.0)then
          iqspir=1
          read(iun11,*)iq,ih
          read(iun11,*)(qspir(i),i=1,3)
          if(iq.gt.0)then
            qspir(2)=qspir(2)/boa8
            qspir(3)=qspir(3)/coa8
          endif
c          print*,'qspir=',qspir
          do i=1,natom
            read(iun11,*)tet,fi
            phase=(tau(1,i)*qspir(1)+tau(2,i)*qspir(2)
     $           +tau(3,i)*qspir(3))*360
            if(fi.gt.360.)fi=phase
            spinangle(1,i)=tet*g2r
            spinangle(2,i)=fi*g2r
          enddo
        endif
        close(iun11)
      else
        keyrd=iun11
        read(iun11,*)buf
        do j=1,3
          read(iun11,*)(rbas(i,j),i=1,3)
        enddo
        read(iun11,*)natom,nsort
        do j=1,natom
          read(iun11,*)(tau(i,j),i=1,3),is(j)
        enddo
        k=0
        do i=1,80
          if(infile(i:).eq.'struc.out')then
            k=i
            exit
          endif
        enddo
        if(k.eq.0)then
          do j=1,nsort
            read(iun11,*)s_sort(j),atname(j)
          enddo
        else
          do j=1,nsort
            s_sort(j)=1
            write(atname(j),'(i2.2)')j
          enddo
        endif
        close(iun11)
      endif
      ibcell=ibc(rbas)
      call scalerad(rbas,natom,nsort,s_sort)
      ncell=3
      if(ibcell.eq.0)then
        do j=1,3
          do i=1,3
            rext(i,j)=rbas(i,j)
          enddo
        enddo
      elseif(ibcell.le.5)then
        do i=1,3
          do j=1,3
            rext(j,i)=rbas(j,1)*r0(1,i,ibcell)
     $           +rbas(j,2)*r0(2,i,ibcell)+rbas(j,3)*r0(3,i,ibcell)
          enddo
        enddo
      elseif(ibcell.eq.6)then
        ncell=4
        do i=1,3
            rext(i,1)=rbas(i,1)
            rext(i,2)=rbas(i,2)
            rext(i,3)=-rbas(i,1)-rbas(i,2)
            rext(i,4)=rbas(i,3)
          enddo
        elseif(ibcell.eq.7)then         ! trigonal cell
          ncell=4
          do i=1,3
            do j=1,3
              rext(j,i)=rbas(j,1)*r0(1,i,ibcell)
     $             +rbas(j,2)*r0(2,i,ibcell)+rbas(j,3)*r0(3,i,ibcell)
            enddo
          enddo
          do i=1,3
            rext(i,4)=rext(i,3)
            rext(i,3)=-rext(i,1)-rext(i,2)
          enddo
        endif
c 
        if(ibcell.lt.6)then
c linear combination of the cell vectors
c$$$          print*,ibcell
c$$$          do k=1,3
c$$$            print*,(rext(j,k),j=1,3)
c$$$          enddo

          ix=imx3(rext(1,1),rext(1,2),rext(1,3))
          iy=imx3(rext(2,1),rext(2,2),rext(2,3))
          iz=imx3(rext(3,1),rext(3,2),rext(3,3))
c          print*,ix,iy,iz
          nx(ix)=n1
          nx(iy)=n2
          nx(iz)=n3
c          print*,nx
          

          do i=1,3
            do j=1,3
              rcell(j,i)=rext(j,1)*tc(1,i)*nx(1)
     $             +rext(j,2)*tc(2,i)*nx(2)
     $             +rext(j,3)*tc(3,i)*nx(3)
            enddo
          enddo
        else
c tc(1,1) scales in xy plane
          n=max(n1,n2)
          do j=1,3
            do i=1,3
              rcell(i,j)=rext(i,j)*tc(1,1)*n
          enddo
        enddo
c tc(3,3) scales along z
        do i=1,3
          rcell(i,4)=rext(i,4)*tc(3,3)*n3
        enddo
      endif
      end
c


      subroutine cutting_planes(ibcell,rcell,epscell,nplanes
     $     ,planes,cg,nr_c,r_c)
c prepares cutting planes

c
      dimension rcell(3,4)              ! input
      dimension cg(3),planes(4,8),r_c(3,2,*) ! output
      dimension v(3)                    ! local
      if(ibcell.lt.6)then
c structures with 6 cutting planes
        do i=1,3
          cg(i)=0.
        enddo
        do j=1,3
          do i=1,3
            cg(i)=cg(i)+rcell(i,j)
          enddo
        enddo
        do i=1,3
          cg(i)=cg(i)/2.
        enddo
        nplanes=0
        do j1=1,3
          j2=mod(j1,3)+1
          j3=mod(j2,3)+1
c v is the normal vector to the plane containing vectors j2 and j3
          call cross(v,rcell(1,j2),rcell(1,j3))
          t=sdot3(cg,v)
          if(t.lt.0.)then
            do i=1,3
              v(i)=-v(i)
            enddo
          endif
          t=sqrt(v(1)*v(1)+v(2)*v(2)+v(3)*v(3))
          do i=1,3
            v(i)=v(i)/t
          enddo
          d=abs(sdot3(rcell(1,j1),v))
c plane containing the point (0,0,0)
          nplanes=nplanes+1
          do i=1,3
            planes(i,nplanes)=-v(i)
          enddo
          planes(4,nplanes)=epscell
c plane containing the end of the vector j1
          nplanes=nplanes+1
          do i=1,3
            planes(i,nplanes)=v(i)
          enddo
          planes(4,nplanes)=d+epscell
        enddo                           ! j1
c edges of the cell
        nr_c=0
        do j1=1,3
          nr_c=nr_c+1
          do i=1,3
            r_c(i,1,nr_c)=0.
            r_c(i,2,nr_c)=r_c(i,1,nr_c)+rcell(i,j1)
          enddo
          do j2=1,3
            if(j2.eq.j1)cycle
            nr_c=nr_c+1
            do i=1,3
              r_c(i,1,nr_c)=rcell(i,j1)
              r_c(i,2,nr_c)=r_c(i,1,nr_c)+rcell(i,j2)
            enddo
          enddo                         ! j2
          nr_c=nr_c+1
          do i=1,3
            r_c(i,1,nr_c)=rcell(i,1)+rcell(i,2)+rcell(i,3)
            r_c(i,2,nr_c)=r_c(i,1,nr_c)-rcell(i,j1)
          enddo
        enddo                           ! j1
      else
c hcp structure with 8 cutting planes
        cg(1)=0.
        cg(2)=0.
        cg(3)=rcell(3,4)/2.
c xy planes
        planes(1,1)=0.
        planes(2,1)=0.
        planes(3,1)=1.
        planes(4,1)=rcell(3,4)+epscell  ! distance
        planes(1,2)=0.
        planes(2,2)=0.
        planes(3,2)=-1.
        planes(4,2)=epscell             ! distance
        nplanes=2
        do j1=1,3
          j2=mod(j1,3)+1
          j3=mod(j2,3)+1
c v is the normal vector to the plane containing vectors j1 and j4
          call cross(v,rcell(1,j1),rcell(1,4))
          t=sqrt(v(1)*v(1)+v(2)*v(2)+v(3)*v(3))
          do i=1,3
            v(i)=v(i)/t
          enddo
          d=sdot3(rcell(1,j2),v)
          d=abs(d)
c two planes containing the ends of j2 and j3 which are at the same
c distance
          nplanes=nplanes+1
          do i=1,3
            planes(i,nplanes)=v(i)
          enddo
          planes(4,nplanes)=d+epscell
          nplanes=nplanes+1
          do i=1,3
            planes(i,nplanes)=-v(i)
          enddo
          planes(4,nplanes)=d+epscell
        enddo
c edges of the cell
        nr_c=0
        do j1=1,3
          j2=mod(j1,3)+1
          j3=mod(j2,3)+1
          j=(j1-1)*6
          do i=1,3
            r_c(i,1,j+1)= rcell(i,j1)
            r_c(i,2,j+1)=-rcell(i,j3)
            r_c(i,1,j+2)=-rcell(i,j3)
            r_c(i,2,j+2)= rcell(i,j2)
            r_c(i,1,j+3)= rcell(i,j1)+rcell(i,4)
            r_c(i,2,j+3)=-rcell(i,j3)+rcell(i,4)
            r_c(i,1,j+4)=-rcell(i,j3)+rcell(i,4)
            r_c(i,2,j+4)= rcell(i,j2)+rcell(i,4)
            r_c(i,1,j+5)= rcell(i,j1)
            r_c(i,2,j+5)= rcell(i,j1)+rcell(i,4)
            r_c(i,1,j+6)=-rcell(i,j3)
            r_c(i,2,j+6)=-rcell(i,j3)+rcell(i,4)
          enddo                         ! i
        enddo                           ! j1
        nr_c=18
      endif

      do ir=1,nr_c
        do i=1,3
          r_c(i,1,ir)=r_c(i,1,ir)-cg(i)
          r_c(i,2,ir)=r_c(i,2,ir)-cg(i)
        enddo
      enddo                             ! ir
      end
c
      subroutine prim_cell(iprim,rbas,cg,nr_p,r_p)
c generates primitive cell
      dimension rbas(3,*),cg(3)         ! input
      dimension r_p(3,2,*)              ! output

c
      nr_p=0
      if(iprim.le.0)return
c edges of the cell
      do j1=1,3
        nr_p=nr_p+1
        do i=1,3
          r_p(i,1,nr_p)=0.
          r_p(i,2,nr_p)=r_p(i,1,nr_p)+rbas(i,j1)
        enddo
        do j2=1,3
          if(j2.eq.j1)cycle
          nr_p=nr_p+1
          do i=1,3
            r_p(i,1,nr_p)=rbas(i,j1)
            r_p(i,2,nr_p)=r_p(i,1,nr_p)+rbas(i,j2)
          enddo
        enddo                           ! j2
        nr_p=nr_p+1
        do i=1,3
          r_p(i,1,nr_p)=rbas(i,1)+rbas(i,2)+rbas(i,3)
          r_p(i,2,nr_p)=r_p(i,1,nr_p)-rbas(i,j1)
        enddo
      enddo                             ! j1
      do ir=1,nr_p
        do i=1,3
          r_p(i,1,ir)=r_p(i,1,ir)-cg(i)
          r_p(i,2,ir)=r_p(i,2,ir)-cg(i)
        enddo
      enddo                             ! ir
      end
c
c
      subroutine gen_atpos(rbas,rcell,ncell,nplanes,planes,cg
     $     ,natom,tau,is,s_sort,ispins,qsp,nr_at,r_at
     $     ,is_at,nr_sp,r_sp,is_sp,iqspir,qspir,spinangle)
C This subroutine generates all the atoms'' positions in the 
C unit cell and in the cell on the linear combination of 
C the basis vectors
      include 'ATOMS.FI'
      parameter (eps=1.e-4)

      dimension rbas(3,3),rcell(3,4),planes(4,8),cg(3) ! input
      dimension tau(3,*),is(*),s_sort(*),qsp(3),spinangle(3,*),qspir(3)
      dimension r_at(3,*),is_at(*)      ! output
     $     ,r_sp(2,*),is_sp(*),vt(3)
      dimension qbas(3,3),nb(2,3),v(3),t1(3)
      pi=4.*atan(1.)
      twopi=2.*pi
      do j1=1,3
        j2=mod(j1,3)+1
        j3=mod(j2,3)+1
        call cross(qbas(1,j1),rbas(1,j2),rbas(1,j3))
      enddo
      w=rbas(1,1)*qbas(1,1)+rbas(2,1)*qbas(2,1)+rbas(3,1)*qbas(3,1)
      do j=1,3
        do i=1,3
          qbas(i,j)=qbas(i,j)/w
        enddo
      enddo
c limits for the cycle over basis vectors
      do i=1,3
        nb(1,i)=0
        nb(2,i)=0
        do j=1,ncell
          w=sdot3(rcell(1,j),qbas(1,i))
          n=nint(w)
          if(nb(1,i).gt.n)nb(1,i)=n
          if(nb(2,i).lt.n)nb(2,i)=n
        enddo
        nb(1,i)=nb(1,i)-1
        nb(2,i)=nb(2,i)+1
      enddo
c
      isrto=0
      nr_at=0
      nr_sp=0
      do iat=1,natom
        isrt=is(iat)
        s=s_sort(isrt)
c skip atoms with a negativ radius
        if(s.lt.-eps)cycle
c cycles over basis vectors
        do i1=nb(1,1),nb(2,1)
          do i2=nb(1,2),nb(2,2)
            do i3=nb(1,3),nb(2,3)
              vt(1)=rbas(1,1)*i1+rbas(1,2)*i2+rbas(1,3)*i3
              vt(2)=rbas(2,1)*i1+rbas(2,2)*i2+rbas(2,3)*i3
              vt(3)=rbas(3,1)*i1+rbas(3,2)*i2+rbas(3,3)*i3

              v(1)=tau(1,iat)+vt(1)
              v(2)=tau(2,iat)+vt(2)
              v(3)=tau(3,iat)+vt(3)
c check if the atom is inside the cell
              do ip=1,nplanes
                w=v(1)*planes(1,ip)+v(2)*planes(2,ip)+v(3)*planes(3,ip)
                if(w.gt.planes(4,ip))exit
              enddo                     ! ip
c
              if(ip.le.nplanes)cycle    ! atom is outside
c
              nr_at=nr_at+1
              if(nr_at.gt.MAXR)stop 'GEN_ATPOS: nr_at>MAXR'
              do i=1,3
                v(i)=v(i)-cg(i)
                r_at(i,nr_at)=v(i)
              enddo
              if(iqspir.eq.1)then
                phase=(vt(1)*qspir(1)+vt(2)*qspir(2)+vt(3)*qspir(3))
                r_sp(1,nr_at)=spinangle(1,iat)
                r_sp(2,nr_at)=spinangle(2,iat)+phase*2*pi
c                print*,vt,':',qspir
c                print*,phase
c                print*,spinangle(2,iat),r_sp(2,nr_at)

              endif
              is_at(nr_at)=isrt
            enddo                       ! i3
          enddo                         ! i2
        enddo                           ! i1
      enddo                             ! iat
      end
c
c
c
      function dist(a,b)
      dimension a(3),b(3)
*
      dist=sqrt((a(1)-b(1))**2+(a(2)-b(2))**2+(a(3)-b(3))**2)
      end
c
      subroutine cross(a,b,c)
c cross product (ax,ay,az)=(bx,by,bz)*(cx,cy,cz)
      dimension a(3),b(3),c(3)
      a(1)=b(2)*c(3)-b(3)*c(2)
      a(2)=b(3)*c(1)-b(1)*c(3)
      a(3)=b(1)*c(2)-b(2)*c(1)
      end
c
      function sdot3(a,b)
c cross product (ax,ay,az)=(bx,by,bz)*(cx,cy,cz)
      dimension a(3),b(3)
c
      sdot3=a(1)*b(1)+a(2)*b(2)+a(3)*b(3)
      end
c
      subroutine data4gl(nr_at,r_at,is_at,nsort,s_sort,atname,nr_c,r_c,
     $     r_sp,iqspir,rbas)
c print data for OpenGL
      parameter (eps=1.e-4)
c
      dimension r_at(3,*),is_at(*),s_sort(*),atname(*),r_c(3,2,*)
     $     ,r_sp(2,*),rbas(3,3)
      dimension iedges(2,18)             ! local
c
      character*24 atname
      iun=18
      write(iun,'(i4)')nsort
      do isrt=1,nsort
        do i=1,24
          if(atname(isrt)(i:i).eq.' '.and.atname(isrt)(i:).ne.' ')
     $         atname(isrt)(i:i)='_'
        enddo
        write(iun,'(f8.4,2x,a)')s_sort(isrt),atname(isrt)
      enddo
c Find pointers to the array of distances
c If there is no atom in a vertex its coordinate are added to the end
c of array r_at
      nr=nr_at
      do ie=1,nr_c
        do j=1,2
          do ir=1,nr
            if(dist(r_c(1,j,ie),r_at(1,ir)).lt.eps)then
              iedges(j,ie)=ir
              exit
            endif
          enddo                         ! ir
          if(ir.gt.nr)then
            nr=nr+1
            is_at(nr)=0
            do i=1,3
              r_at(i,nr)=r_c(i,j,ie)
            enddo
            iedges(j,ie)=nr
          endif
        enddo                           ! j
      enddo                             ! ie
      write(iun,'(i4)')nr
      do ir=1,nr
        isrt=abs(is_at(ir))
        if(iqspir.eq.0)then
          write(iun,'(3f12.6,i4," 0 0 0")')(r_at(i,ir),i=1,3),isrt
        else
          theta=r_sp(1,ir)
          fi=r_sp(2,ir)
c          print*,':',theta,fi
          write(iun,'(3f12.6,i4,2x,3f12.6)')(r_at(i,ir),i=1,3),isrt,
     $         sin(theta)*cos(fi),sin(theta)*sin(fi),cos(theta)

        endif
      enddo
      write(iun,'(i4)')nr_c
      do ie=1,nr_c
        write(iun,'(2i4)')iedges(1,ie),iedges(2,ie)
      enddo
      write(iun,'(i4)')0
      write(iun,'(i4)')3
      do i=1,3
        write(iun,*)(rbas(j,i),j=1,3)
      enddo
      end

      function ibc(vc)
      dimension vc(3,3),a(3),d(3)
      logical eqq
      eqq(x,y)=abs(x-y).lt.0.01
      do i=1,3
        d(i)=vc(1,i)*vc(1,i)+vc(2,i)*vc(2,i)+vc(3,i)*vc(3,i)
        d(i)=sqrt(d(i))
      enddo
      do i=1,3
        j=mod(i,3)+1
        k=mod(j,3)+1
        a(i)=vc(1,j)*vc(1,k)+vc(2,j)*vc(2,k)+vc(3,j)*vc(3,k)
        a(i)=a(i)/(d(j)*d(k))
      enddo
      if( eqq(d(3),d(2)).and.eqq(d(3),d(1)))then
        if(eqq(a(3),a(2)).and.eqq(a(3),a(1)))then
          ang=abs(a(1))
          if(eqq(3*ang,1.))then
c            print*,'BCC'
            ibcell=1
          else  if(eqq(2*ang,1.))then
c            print*,'FCC'
            ibcell=2
          else  if(eqq(ang,0.))then
            ibcell=0
c            print*,'SC'
          else
c            print*,'R'
            ibcell=7
          endif
        else
c          print*,'BCsmth'
            ibcell=1
        endif
      else if(eqq(2*a(3),-1.).and.eqq(a(2),0.).and.eqq(a(1),0.))then
c        print*,'HEX'
            ibcell=6
      else if(eqq(d(3),d(2)))then
c        print*,'AC'
        ibcell=3
      else if(eqq(d(1),d(2)))then
c        print*,'CC'
            ibcell=5
      else if(eqq(d(3),d(1)))then
c        print*,'BC'
            ibcell=4
      else
c        print*,'PRIM'
            ibcell=0
      endif
      ibc=ibcell
      end
      subroutine scalerad(bas,nat,n,s)
      dimension bas(3,3),s(*),v(3)
      call cross(v,bas(1,1),bas(1,2))
      vol=abs(sdot3(bas(1,3),v))/nat
      swz=3*vol/(4*3.1415)
      swz=swz**(.333333)
      smax=0
      do i=1,n
        smax=max(smax,s(i))
      enddo
      smax=swz/smax*0.5
      do i=1,n
        s(i)=s(i)*smax
      enddo
      end
      function imx3(r1,r2,r3)
      if(r1.gt.r2.and.r1.gt.r3)then
        i=1
      elseif(r2.gt.r1.and.r2.gt.r3)then
        i=2
      else
        i=3
      endif
      imx3=i
      end
