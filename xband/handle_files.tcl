# handle files
#
#
# Copyright (C) 2002 H. Ebert
#


proc handle_files { } {

########################################################################################
 proc files_sel_d {d} {
  global Wfiles Wfiles_L Wfiles_R
  update; files_focus $Wfiles; $Wfiles_L.c.d.f.f.li delete 0 end; update; dirselected_comp $d
 }

##########################################################################################
proc dirselected_comp {d} {# fills the directory selection box; sets files to default
    global Wfiles Wfiles_L Wfiles_R working_directory file_to_handle liste
    global jobfile outfile sysdirlist

    set cdir [expand_dollar_home $d]

    if {[file isdirectory $cdir]==0} {
	writescr0 .d.tt "\n the directory  $cdir  does not exist"
	give_warning .  "WARNING \n\n the directory   $cdir \n\n does not exist " 
	return
    }
    
    if {[winfo exists $Wfiles]} {set w $Wfiles.d.tt} else {set w .d.tt}
    
    if {[catch {cd $cdir} m]!=0} {
	set working_directory [pwd]
	writescr0 $w "error while changing directory!\nerror message:\n$m\n"
	return 0
    } else {
	writescr0 $w "new directory chosen: [pwd]\n\n"
	files_refresh_top_panel
	set working_directory [pwd]
	set file_to_handle "NONE"
	set jobfile   "" 
	set outfile  "" 
	if {[winfo exists $Wfiles]} {
	    files_anzeige $Wfiles
	    
	    $Wfiles_L.c.d.f.f.li delete 0 end
	    $Wfiles_L.c.d.f.f.li insert end "\$HOME" ".."
	    foreach x $sysdirlist { $Wfiles_L.c.d.f.f.li insert end "$x"}
	    foreach k [lsort [glob -nocomplain -- *]] {
		if {[file isdirectory $k]} {$Wfiles_L.c.d.f.f.li insert end $k}
	    }
	    fill_file_list $Wfiles_L.c.m.f.f.li
	}
	return 1
    }
}


########################################################################################
 proc files_dir_update {} {
  global Wfiles Wfiles_L Wfiles_R liste working_directory sysdirlist sysdirlistmaxindex

    set diraux [collapse_dollar_home $working_directory]

    if {[llength $sysdirlist]>0} {
       set sysdirlist [update_dirlist $sysdirlist $sysdirlistmaxindex $diraux]
    } else {
       lappend sysdirlist $diraux
    }
    $Wfiles_L.c.d.f.f.li delete 0 end
    $Wfiles_L.c.d.f.f.li insert end "\$HOME" ".."
    foreach x $sysdirlist { $Wfiles_L.c.d.f.f.li insert end "$x"}

 }



########################################################################################
 proc files_refresh_top_panel {} {
  global Wfiles Wfiles_L Wfiles_R filter file_to_handle jobfile outfile

  set filter   "*"
  set file_to_handle "NONE"
  set jobfile ""
  set outfile ""

  files_anzeige $Wfiles

 }


########################################################################################
 proc files_sel_m {f} {
  global Wfiles Wfiles_L Wfiles_R file_to_handle sorttype
  files_focus $Wfiles
  if {[file exists $f]} {
    set file_to_handle $f
    writescr0 $Wfiles.d.tt "new file_to_handle selected $file_to_handle\n\n"
    files_anzeige $Wfiles

    handle_file_menu 

    files_sel_order $sorttype
    files_dir_update
  }
 }

########################################################################################
 proc files_sel_m2 {f} {
  global Wfiles Wfiles_L Wfiles_R file_to_handle sorttype
  global editor edback edxterm edoptions

  files_focus $Wfiles
  if {[file exists $f]} {
    set file_to_handle $f
    writescr0 $Wfiles.d.tt "new file_to_handle selected $file_to_handle\n\n"
    files_anzeige $Wfiles

    handle_file_menu 

    set ed_file $file_to_handle
    eval set res [catch "exec $edxterm $editor $edoptions $file_to_handle $edback" message]

    files_sel_order $sorttype
    files_dir_update
  }
 }


########################################################################################
 proc files_sel_order {s} {
  global Wfiles Wfiles_L Wfiles_R sorttype

  if {$s=="alphabetical"} {set sorttype alphabetical} else {set sorttype access}
  fill_file_list $Wfiles_L.c.m.f.f.li
 }

########################################################################################
 proc files_dummy {s} { }


########################################################################################
 proc files_focus {w} {
  global Wfiles Wfiles_L Wfiles_R files_foc
  if     {$files_foc==0} {set r 0} \
  elseif {$files_foc==1} {set r [files_edn]} \
  elseif {$files_foc==2} {set r [files_emfn]} \
  elseif {$files_foc==3} {set r [files_eefn]} \
  elseif {$files_foc==4} {set r [files_esfn]}
  if {$w!=""} {focus $w} else {focus $Wfiles; return $r}
 }


########################################################################################
 proc files_emfn {} {#                                             enter file_to_handle name
  global Wfiles Wfiles_L Wfiles_R inpsuffix file_to_handle jobfile outfile liste files_foc

  set files_foc 0
  writescr0 $Wfiles.d.tt ""
  if {$f=="$inpsuffix"} {set f ""; writescr $Wfiles.d.tt "filename cannot consist of file suffix only\n"}
##  regsub "$inpsuffix$" $f "" h; if {"$h$inpsuffix"=="$f"} {set f "$h"}
  testfilename $f $Wfiles.d.tt $inpsuffix;  if {$f==""} {return 0}

  set file_to_handle $f
  writescr $Wfiles.d.tt "input of new file to handle name: $file_to_handle\n\n"
  files_anzeige $Wfiles
  return 1
 }



########################################################################################
proc handle_file_menu { } {#                  
  global file_to_handle jobfile outfile PRINTPS
  global Wfiles Wfiles_L Wfiles_R
  global edxterm editor edoptions edback
  global file_to_handle target
  global unix_prog_suffix_list unix_prog_call
  global file_suffix
 
  set target $file_to_handle

  set p [string last . $file_to_handle]
  if {$p<0} {
     set file_suffix "NONE"
  } else {
     set file_suffix [crange $file_to_handle [expr {$p+1}] end]
  }                                     

  set p [string last ".tar.gz" $file_to_handle]
  if {$p>0} {set file_suffix "tar.gz"}                                     

  writescr0 $Wfiles.d.tt  " handle_file_menu  $target $file_to_handle  suffix $file_suffix"

  set c3bg green
  set c3wb 10
  set c3hb 2
  set c3py 0

#----------------------------------------------------------------------------------------
if {[winfo exists $Wfiles_R.make]==1} {pack forget $Wfiles_R.make}

if {$file_to_handle=="makefile" || $file_to_handle=="Makefile" } {  
   if {[winfo exists $Wfiles_R.make]==0} {
      button $Wfiles_R.make -text "make" -width 10 -height $c3hb \
     -command "handle_file_execute make 0 " -bg $c3bg -pady $c3py -padx 7
   }
   pack configure $Wfiles_R.make -in  $Wfiles_R -anchor nw -side top
}
#----------------------------------------------------------------------------------------
foreach suff $unix_prog_suffix_list { 
   if {$suff=="tar.gz"} {
      set suff_aux tar_gz
   } else {
      set suff_aux $suff
   }
   if {[winfo exists $Wfiles_R.$suff_aux]==1} {pack forget $Wfiles_R.$suff_aux}

   if {$suff==$file_suffix} { 
      if {[winfo exists $Wfiles_R.$suff_aux]==0} {
         set prog [lindex $unix_prog_call($suff) 0]
         button $Wfiles_R.$suff_aux -text "$prog" -width 10 -height $c3hb \
        -command "handle_file_execute $suff 1 " -bg $c3bg -pady $c3py -padx 7
      }
      pack configure $Wfiles_R.$suff_aux -in  $Wfiles_R -anchor nw -side top
   }
}

#----------------------------------------------------------------------------------------
if {[winfo exists $Wfiles_R.poscar]==1} {pack forget $Wfiles_R.poscar}

if {$file_to_handle=="POSCAR" } {  
   if {[winfo exists $Wfiles_R.poscar]==0} {
      button $Wfiles_R.poscar -text "view" -width 10 -height $c3hb \
     -command "handle_file_execute POSCAR 0 " -bg $c3bg -pady $c3py -padx 7
   }
   pack configure $Wfiles_R.poscar -in  $Wfiles_R -anchor nw -side top
}
#----------------------------------------------------------------------------------------

}
#                                                                   handle_file_menu END
########################################################################################



########################################################################################
proc handle_file_execute {key unixcmd} {#                  
  global file_to_handle jobfile outfile PRINTPS
  global Wfiles Wfiles_L Wfiles_R
  global edxterm editor edoptions edback
  global file_to_handle target
  global unix_prog_suffix_list unix_prog_call sorttype
  global file_suffix

  writescr0   $Wfiles.d.tt  "handle_file_execute   --  file to handle: $file_to_handle   with $key $unixcmd "

  if {$file_to_handle=="NONE"} {return}

  if {$key=="edit"} {
      set ed_file $file_to_handle
      eval set res [catch "exec $edxterm $editor $edoptions $file_to_handle $edback" message]
  } elseif {$key=="copy" || $key=="move"} {
      if {$target==$file_to_handle} {
             give_warning .  "WARNING \n\n same name for \n\n source and target file  "
             return
      }
      if {[file exists $target]==1} {
             handle_file_confirm $key
      } else {
          if {$key=="move"} {
              writescr   $Wfiles.d.tt  "\n\n moving $file_to_handle  to  $target"
              eval set res [catch "exec mv $file_to_handle $target" message]
              files_sel_order $sorttype
              files_dir_update              
              set file_to_handle "NONE"
              set target ""
          } else {
              writescr   $Wfiles.d.tt  "\n\n copying $file_to_handle  to  $target"
              eval set res [catch "exec cp $file_to_handle $target" message]
              files_sel_order $sorttype
              files_dir_update              
          }
      }
  } elseif {$key=="delete"} {
      handle_file_confirm $key
  } elseif {$key=="print"} {
      print_file $file_to_handle $file_suffix $Wfiles.d.tt
  } elseif {$key=="email"} {
      eval set res [catch "exec xterm -e pine -attach $file_to_handle &" message]
  } elseif {$key=="POSCAR"} {
      handle_file_POSCAR
  } elseif {$unixcmd==1} {
#----------------------------------------------------------------------------------------
     foreach suff $unix_prog_suffix_list { 
        if {$suff==$file_suffix} { 
	    if { $suff=="tar.gz"} {
                eval set res [catch "exec gzip -cd $file_to_handle | tar xpf - " message]
                writescr  $Wfiles.d.tt "$message \n"
            } else {
                eval set res [catch "exec $unix_prog_call($suff) $file_to_handle & " message]
                writescr  $Wfiles.d.tt "$message \n"
            }
        }
     }
#----------------------------------------------------------------------------------------
  }

  return

  if     {$f=="m"} {set ff "$file_to_handle"} \
  elseif {$f=="e"} {set ff "$jobfile"} \
  elseif {$f=="s"} {set ff "$outfile"} \
  else             {set ff ""}

  if {$k=="e"} { 
     set ed_file $ff
     eval set res [catch "exec $edxterm $editor $edoptions $ff $edback" message]
     set pid "$message"
     return 
  }

  if {$k=="l"} { execunixcmd "cat $ff" ; return }

  if {$k=="p"} {$PRINTPS $ff;  writescr  .d.tt "\n file $ff send to the printer \n" ; return }

#  if {$k=="t"} { execunixcmd "tail -f $ff" ; return }
  if {$k=="t"} {give_warning .  "WARNING \n\n the tail-command \n\n is not yet available " ; return }

 
  set source $ff
  set target $ff
  
  toplevel_init $cw " " 0 0

  wm geometry $cw +300+50
 
  if       {$k=="c"} {
  wm title    $cw "Copy File"
  wm iconname $cw "Copy File"
  label $cw.head -text "modify source and/or target file name and press <RETURN> to copy" -height 2 -padx 0
  set TEXT1 "source file"
  set TEXT2 "copy"
  } elseif {$k=="m"} {
  wm title    $cw "Move File"
  wm iconname $cw "Move File"
  label $cw.head -text "modify source and/or target file name and press <RETURN> to move" -height 2 -padx 0
  set TEXT1 "source file"
  set TEXT2 "move"
  } else {
  wm title    $cw "Delete File"
  wm iconname $cw "Delete File"
  label $cw.head -text "modify file name and press <RETURN> to delete" -height 2 -padx 0
  set TEXT1 "file name"
  set TEXT2 "delete"
  }
  pack  $cw.head -anchor n -side top -fill x
  
  frame $cw.source -borderwidth 10
  pack  $cw.source -side top -fill x
  
  label $cw.source.l   -text "$TEXT1" -padx 0
  entry $cw.source.cmd -width 45 -relief sunken -textvariable source
        $cw.source.cmd delete 0 end
        $cw.source.cmd insert 0 $source
  pack  $cw.source.l   -side left
  pack  $cw.source.cmd -side right -fill x

  
  if       {$k!="d"} {
  frame $cw.target -borderwidth 10
  pack  $cw.target -side top -fill x
  
  label $cw.target.l   -text "target file" -padx 0
  entry $cw.target.cmd -width 45 -relief sunken -textvariable target
        $cw.target.cmd delete 0 end
        $cw.target.cmd insert 0 $target
  pack  $cw.target.l   -side left
  pack  $cw.target.cmd -side right -fill x
  }

 
 		     
  button $cw.close -text Close  -command "destroy $cw"  -height 2 -width 50 -bg tomato1
 
  pack $cw.execute $cw.close -side top -expand y -fill x -pady 2m
                                                                                                                                                                                                   
  proc execute_cpmvdl { } {
     global Wfiles Wfiles_L Wfiles_R key_cpmvdl file_cpmvdl
     global file_to_handle jobfile outfile

     set cw $Wfiles_L.cpmvdl_file

     set source [string trim [$cw.source.cmd get]]
     if       {$key_cpmvdl!="d"} {
     set target [string trim [$cw.target.cmd get]]
     }  

     if {[file exists $source]} {

       if       {$key_cpmvdl=="c"} {
          eval set res [catch "exec cp $source $target" message]
       } elseif {$key_cpmvdl=="m"} {
          eval set res [catch "exec mv $source $target" message]
          if     {$file_cpmvdl=="m"} {set file_to_handle "$target"} \
          elseif {$file_cpmvdl=="e"} {set jobfile "$target"} \
          elseif {$file_cpmvdl=="s"} {set outfile "$target"} \
          else   {writescr0 $Wfiles.d.tt "file $file_cpmvdl not allowed \n"}
       } elseif {$key_cpmvdl=="d"} {
          eval set res [catch "exec rm $source "        message]
          if     {$file_cpmvdl=="m"} {set file_to_handle "NONE"} \
          elseif {$file_cpmvdl=="e"} {set jobfile ""} \
          elseif {$file_cpmvdl=="s"} {set outfile ""} \
          else   {writescr0 $Wfiles.d.tt "file $file_cpmvdl not allowed \n"}
       } else {
         writescr0 $Wfiles.d.tt "key $key_cpmvdl not allowed \n"
         destroy $cw
       }

       writescr0 $Wfiles.d.tt "$message \n"
       fill_file_list $Wfiles_L.c.m.f.f.li
       files_anzeige $Wfiles
     }
     destroy $cw
  }
}

#                                                                handle_file_execute END
########################################################################################



########################################################################################
proc handle_file_confirm {key} {#                                   handle_file_confirm 

  global file_to_handle jobfile outfile PRINTPS
  global Wfiles Wfiles_L Wfiles_R
  global edxterm editor edoptions edback
  global file_to_handle target
  global unix_prog_suffix_list unix_prog_call sorttype
  global file_suffix

 
  set cw $Wfiles.confirm 

  toplevel_init $cw " " 0 0

  wm geometry $cw +300+50

  wm title    $cw "confirm action"
  wm iconname $cw "confirm action"

  if {$key=="copy" || $key=="move"} {
     label $cw.head1 -text "target file $target exists " -height 2 -padx 0 
  } elseif  {$key=="delete"} {
     label $cw.head1 -text "file to delete:  $file_to_handle " -height 2 -padx 0 
  } 
  pack $cw.head1

  button $cw.confirm  -text "confirm $key" -width 50 -height 5 -bg green \
         -command "handle_file_confirm_do_it $key $cw"
  button $cw.close  -text "close"      -width 50 -height 5 -bg tomato1 \
         -command "destros $cw"
  pack $cw.confirm $cw.close
 

}
#                                                                END handle_file_confirm
########################################################################################
 



########################################################################################
proc handle_file_confirm_do_it {key cw} {#                     handle_file_confirm_do_it 

  global file_to_handle jobfile outfile PRINTPS
  global Wfiles Wfiles_L Wfiles_R
  global edxterm editor edoptions edback
  global file_to_handle target
  global unix_prog_suffix_list unix_prog_call sorttype
  global file_suffix

 
  if {$key=="move"} {
      writescr   $Wfiles.d.tt  "\n\n moving $file_to_handle  to  $target"
      eval set res [catch "exec mv $file_to_handle $target" message]
      set target ""
  } elseif {$key=="copy"} {
      writescr   $Wfiles.d.tt  "\n\n copying $file_to_handle  to  $target"
      eval set res [catch "exec cp $file_to_handle $target" message]
      set file_to_handle "NONE"
  } elseif {$key=="delete"} {
      writescr   $Wfiles.d.tt  "\n\n deleting $file_to_handle"
      eval set res [catch "exec rm $file_to_handle" message]
      set target ""
      set file_to_handle "NONE"
  }

   files_sel_order $sorttype
   files_dir_update              
   destros $cw
}
#                                                          END handle_file_confirm_do_it
########################################################################################



########################################################################################
 proc files_anzeige {w} {#                                              update display
  global working_directory 
  $w.main.left.b.c1.d.t configure -text "$working_directory"
 }

########################################################################################
 proc fill_file_list  {w} {#                fill filelist into widget w

     global sub sorttype filter

     $w delete 0 end
     set subalt $sub

     if {$sorttype=="alphabetical"} {
	 set liste [exec ls] 
     } else {
	 set liste [exec ls -t]
     }
     
     foreach i $liste {
	 if {[file isfile $i]} {
	     if {[string match "*$filter*" $i]} {$w insert end $i}
	 }
     } 
     
     lock
     set sub $subalt
 }

########################################################################################
 proc run_files {ifiles key} {#                        run filesram number i in  FILESS_LIST

     global Wfiles Wfiles_L Wfiles_R file_to_handle jobfile outfile sorttype 
     global working_directory
     global wishcall xband_path  
     
     set PRC "run_files"
     
     set l2 [expr {[string length $file_to_handle] -1}]
     
     if {[string range $file_to_handle [expr {$l2 - 2}] $l2] == "inp"} {set l2 [expr {$l2 - 4}]}
     
     if {$l2 < 0} {
	 set jobfile "_job"
	 set outfile "_out"
     } else {
	 set jobfile "[string range $file_to_handle 0 $l2].job"
	 set outfile "[string range $file_to_handle 0 $l2].out"
     }
     
     $Wfiles_L.b.c1.e.mb configure -state normal -bg LightBlue
     $Wfiles_L.b.c1.s.mb configure -state normal -bg LightBlue
     
     files_anzeige $Wfiles
     
#---------------------------------------------------------------------------------------

     if {$key == "i"} {
	 set START_DATE [exec date]
	 set   ia [open "xband_aux" w] 
	 puts  $ia "\# interactive script created by   xband "
	 puts  $ia "\# "
	 puts  $ia "echo \" *******************************************************************************\" "
	 puts  $ia "echo \"      starting interactive run of  \" "
	 puts  $ia "echo \"      input file:   $file_to_handle  \" "
	 puts  $ia "echo \"      output will be appended to:  $outfile \" "
	 puts  $ia "echo \" *******************************************************************************\" "
	 puts  $ia "cd $working_directory "
	 puts  $ia "echo \"  \" >> $outfile "
	 puts  $ia "echo \"  \" >> $outfile "
	 puts  $ia "echo \" *******************************************************************************\" >> $outfile "
	 puts  $ia "echo \"    execution via xband:     < $file_to_handle  \"  >> $outfile "
	 puts  $ia "echo \" *******************************************************************************\" >> $outfile "
	 
	 puts  $ia "echo \"  \" >> $outfile "
	 puts  $ia "echo \"  \" >> $outfile "
	 puts  $ia "echo \" *******************************************************************************\" >> $outfile "
	 puts  $ia "echo \"    run started at: $START_DATE  \"      >> $outfile "
	 puts  $ia "echo \"    and ended   at: `date`       \"      >> $outfile "
	 puts  $ia "echo \" *******************************************************************************\" >> $outfile "
	 puts  $ia "echo \"  \" >> $outfile "
	 puts  $ia "echo \" *******************************************************************************\" "
	 puts  $ia "echo \"      input file:   $file_to_handle  \" "
	 puts  $ia "echo \"      output appended to:  $outfile \" "
	 puts  $ia "echo \" *******************************************************************************\" "
	 puts  $ia "exec $wishcall -f $xband_path/message.tcl -n message 1 \$* & "
	 puts  $ia "bash "
	 close $ia
	 exec chmod 755 xband_aux
	 
     } else {
	 
	 set    job [open $jobfile w] 
	 puts  $job  [prepare_batchjob_header $jobfile]
	 puts  $job "cd $working_directory "
	 close $job
	 
	 if {$key=="b"} {submit_batch_job $jobfile }
	 if {$key=="s"} {submit_batch_job $jobfile }
	 if {$key=="q"} { queue_batch_job $jobfile }
	 
	 files_sel_order $sorttype
	 files_dir_update
     }
     
 }
 
########################################################################################
 proc do_with_job {key} {#                            use job-file

     global jobfile outfile working_directory sorttype Wfiles Wfiles_L Wfiles_R 
     
     if {[file exists $jobfile]==0} {
	 give_warning . "WARNING \n\n the jobfile \n\n $jobfile \n\n is not available " 
	 return
     } else {
	 
	 set l2 [expr {[string length $jobfile] -1}]
	 if {$l2 < 0} {
	     set outfile "_out"
	 } else {
	     set outfile "[string range $jobfile 0 $l2].out"
	 }
	 
	 if {$key=="i"} {
	     exec chmod 755 $jobfile
	     
	     set   ia [open "xband_aux_do_with_job" w] 
	     puts  $ia "\# interactive script to run job-file created by   xband "
	     puts  $ia "\# "
	     puts  $ia "cd $working_directory "
	     puts  $ia "$jobfile | tee -a  $outfile "
	     puts  $ia "bash "
	     close $ia
	     exec chmod 755 xband_aux_do_with_job
	     exec xterm -T "run $jobfile " -e ./xband_aux_do_with_job &
	     
	 } elseif {$key=="b"} {
	     
	     submit_batch_job $jobfile
	     
	 } elseif {$key=="q"} {
	     
	     queue_batch_job $jobfile
	     
	 } else {
	     
	     submit_batch_job $jobfile
	     
	 }
	 
	 files_anzeige   $Wfiles
	 files_sel_order $sorttype
	 files_dir_update
     }
     
 }
 
########################################################################################
 
 
########################################################################################
proc submit_batch_job {jobfile} {#          

    global Wfiles Wfiles_L Wfiles_R 

    set w ""

    eval set res [catch "exec whoami " user]
    eval set res [catch "exec cat $jobfile "     message1]
    eval set res [catch "exec batch < $jobfile " message]

    writescr0  $w.d.tt "\nINFO from <submit_batch_job> \n\n \
                         submitting batch job file $jobfile  \
                         \n\n=======================================================  \
                         \n$message1  \
                         \n\n=======================================================  \
                         \n\n$message  \
                         \n\n=======================================================  \
                         \n"
   
    set    mf [open "xband_mail" w] 
    puts  $mf $message
    puts  $mf "======================================================="
    puts  $mf "jobfile $jobfile"
    puts  $mf "======================================================="
    close $mf
    eval set res [catch "exec cat  $jobfile >> xband_mail " message1]
    eval set res [catch "exec mail $user@[exec hostname] < xband_mail " message1]
    eval set res [catch "exec rm xband_mail " message]
    writescr $w.d.tt "$message1 \n"
    
}

########################################################################################
proc queue_batch_job {jobfile} {#          

    global Wfiles Wfiles_L Wfiles_R 

    set w ""

    eval set res [catch "exec whoami " user]
    eval set res [catch "exec cat $jobfile "     message1]
    eval set res [catch "exec qsub $jobfile " message]
    
    writescr0  $w.d.tt "\nINFO from <queue_batch_job> \n\n \
                         submitting batch job file $jobfile to queue  \
                         \n\n=======================================================  \
                         \n$message1  \
                         \n\n=======================================================  \
                         \n\n$message  \
                         \n\n=======================================================  \
                         \n"
   
    set   mf [open "xband_mail" w] 
    puts  $mf $message
    puts  $mf "======================================================="
    puts  $mf "jobfile $jobfile"
    puts  $mf "======================================================="
    close $mf
    eval set res [catch "exec cat  $jobfile >> xband_mail " message1]
    eval set res [catch "exec mail $user@[exec hostname] < xband_mail " message1]
    eval set res [catch "exec rm xband_mail " message]
    writescr   $w.d.tt "$message1 \n"
    
}
########################################################################################


########################################################################################
#                                                                                   MAIN
########################################################################################

global Wfiles Wfiles_L Wfiles_R
global Hfiles hlp_dir working_directory files_foc file_to_handle jobfile outfile
global mfold sysdirlist
global editor edback edxterm edoptions
global dir0_handle_files    
global COLOR WIDTH HEIGHT FONT

set PRC handle_files


set Hfiles files_h.hlp

set file_to_handle "NONE"
set jobfile ""
set outfile ""

toplevel_init $Wfiles "handle files" 0 0

set tyh 14;  set working_directory [pwd] ;  set mfold "$file_to_handle"

# top buttons

insert_topbuttons $Wfiles $Hfiles
bind $Wfiles.a.e <Button-1> {
  handle_files_reset_dir
  if [files_focus ""] {
    writescr $Wfiles.d.tt "input accepted; press once more \"close\""
  } else {
    destros $Wfiles; unlock_list
    if {($mfold!=$file_to_handle)&&([file exists "${file_to_handle}.vst"])} {.b.c1.1.vl invoke}
    Bend
  }
}

#--------------------------------------------------------
proc handle_files_reset_dir { } {
    global dir0_handle_files
    set PRC handle_files_reset_dir

    if {[catch {cd $dir0_handle_files} m]!=0} {
	if {[winfo exists $Wfiles]} {set w $Wfiles.d.tt} else {set w .d.tt}
	writescr0 $w "error while changing directory!\nerror message:\n$m\n"
	return 0
    } else {
	debug $PRC "returned to directory  $dir0_handle_files"  
    } 
    
}
#--------------------------------------------------------

########################################################################################
#                  left and right column for MAIN section
########################################################################################

frame $Wfiles.main                       
 pack $Wfiles.main
frame $Wfiles.main.left ; frame $Wfiles.main.right 
 pack $Wfiles.main.left         $Wfiles.main.right -side left

set Wfiles_L $Wfiles.main.left
set Wfiles_R $Wfiles.main.right

########################################################################################
#                                                                    frame b for display
########################################################################################

frame $Wfiles_L.b   
pack configure $Wfiles_L.b -in $Wfiles_L -anchor w -pady 8

frame $Wfiles_L.b.c1 
frame $Wfiles_L.b.c12 
frame $Wfiles_L.b.c2 
frame $Wfiles_L.b.c3 

pack configure $Wfiles_L.b.c1  -in $Wfiles_L.b -anchor w -side left -pady 8
pack configure $Wfiles_L.b.c12 -in $Wfiles_L.b -anchor w -side left -padx 20
pack configure $Wfiles_L.b.c2  -in $Wfiles_L.b -anchor nw -side left -pady 8 -padx 5
pack configure $Wfiles_L.b.c3  -in $Wfiles_L.b -anchor nw -side left -pady 8 -padx 2

#=======================================================================================
#                                     b   COLUMN 1
#=======================================================================================
set wmb  22
set wbut 10
set wt1  15
set went 23
set wt2  30

frame $Wfiles_L.b.c1.d; frame $Wfiles_L.b.c1.x;  frame $Wfiles_L.b.c1.m; frame $Wfiles_L.b.c1.m2
frame $Wfiles_L.b.c1.e; frame $Wfiles_L.b.c1.s
pack configure $Wfiles_L.b.c1.d $Wfiles_L.b.c1.x  $Wfiles_L.b.c1.m  $Wfiles_L.b.c1.m2 \
               $Wfiles_L.b.c1.e $Wfiles_L.b.c1.s -in $Wfiles_L.b.c1 -anchor w

label $Wfiles_L.b.c1.d.l -width $wt1 -text "current directory" -anchor w -padx 10
label $Wfiles_L.b.c1.d.t -anchor w
pack configure $Wfiles_L.b.c1.d.l $Wfiles_L.b.c1.d.t -in $Wfiles_L.b.c1.d -anchor w -side left


#=======================================================================================
frame $Wfiles_L.b.c1.sor
frame $Wfiles_L.b.c1.fil
pack configure $Wfiles_L.b.c1.fil $Wfiles_L.b.c1.sor -in $Wfiles_L.b.c1 -anchor w -side left

label $Wfiles_L.b.c1.fil.l -width 4 -text "Filter" -anchor w  -padx 10
entry $Wfiles_L.b.c1.fil.e -background {lightblue} -textvariable {filter} -width {15} 
#          -font "-Adobe-Helvetica-Medium-R-Normal--*-160-*-*-*-*-*-*"
bind $Wfiles_L.b.c1.fil.e <Key-Return> {files_sel_order $sorttype}
 
pack configure $Wfiles_L.b.c1.fil.l $Wfiles_L.b.c1.fil.e -in $Wfiles_L.b.c1.fil -anchor w -side left


label $Wfiles_L.b.c1.sor.l -width 4 -text "sort mode" -anchor w  -padx 50
frame $Wfiles_L.b.c1.sor.b
pack configure $Wfiles_L.b.c1.sor.l $Wfiles_L.b.c1.sor.b -in $Wfiles_L.b.c1.sor -anchor w -side left
CreateLSBoxSBx $Wfiles_L.b.c1.sor.b " " top 11 3 0 $Hfiles $Wfiles.d.tt \
    "" "\{alphabetical\} \{last access\}" files_sel_order ""

$Wfiles_L.b.c1.sor.b.f.f.1 select


########################################################################################
#   frame c for modification: d - directory, m - file_to_handle
########################################################################################

frame $Wfiles_L.c; pack configure $Wfiles_L.c -in $Wfiles_L -anchor w

frame $Wfiles_L.c.d;  frame $Wfiles_L.c.m;  frame $Wfiles_L.c.e;  frame $Wfiles_L.c.s
pack configure $Wfiles_L.c.d -in $Wfiles_L.c -side left -anchor se -ipadx 3
pack configure $Wfiles_L.c.s $Wfiles_L.c.e  $Wfiles_L.c.m -in $Wfiles_L.c -side right -anchor s


########################################################################################
#                                                                        directory: .c.d
########################################################################################
#
# 
CreateLSBoxSBx $Wfiles_L.c.d "select directory" top 24 21 2 $Hfiles $Wfiles.d.tt "" "" files_sel_d sel
$Wfiles_L.c.d.f.f.li insert end "\$HOME" ".."
foreach x $sysdirlist { $Wfiles_L.c.d.f.f.li insert end "$x"}
foreach i [lsort [glob -nocomplain -- *]] {
    if {[file isdirectory $i]} {$Wfiles_L.c.d.f.f.li insert end $i}
}


########################################################################################
#                                                                    file_to_handle: c.m
########################################################################################
#

# file name selection box
CreateLSBoxSBx2 $Wfiles_L.c.m "select | edit file" top 30 21 2 $Hfiles \
             $Wfiles.d.tt "" "" files_sel_m files_sel_m2 sel




#=======================================================================================
#                                RIGHT COLUMN
#=======================================================================================
set c3bg green
set c3wb 10
set c3hb 2
set c3py 0

frame $Wfiles_R.top  
label  $Wfiles_R.top.lab1  -text "file to handle " -font $FONT(GEN)
label  $Wfiles_R.top.lab2  -textvariable file_to_handle -font $FONT(GEN) \
                           -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)
label  $Wfiles_R.top.spac  -text " " -height 1
pack   $Wfiles_R.top.lab1   $Wfiles_R.top.lab2 $Wfiles_R.top.spac -side top -anchor nw
#----------------------------------------------------------------------------------------
button $Wfiles_R.list -text "ls -l"   -width $c3wb -height $c3hb -command "execunixcmd \"ls -l \" "  \
   -bg $c3bg -pady $c3py -padx 7
#----------------------------------------------------------------------------------------
button $Wfiles_R.xterm -text "xterm" -width $c3wb -height $c3hb -command "exec xterm &"             \
   -bg $c3bg -pady $c3py -padx 7
#----------------------------------------------------------------------------------------
button $Wfiles_R.email -text "email" -width 10 -height $c3hb -command "handle_file_execute email 0" \
   -bg $c3bg -pady $c3py -padx 7
#----------------------------------------------------------------------------------------
button $Wfiles_R.print -text "print" -width 10 -height $c3hb -command "handle_file_execute print 0" \
   -bg $c3bg -pady $c3py -padx 7
#----------------------------------------------------------------------------------------
frame  $Wfiles_R.copy
button $Wfiles_R.copy.but  -text "copy" -width 10 -height $c3hb  \
      -command "handle_file_execute copy 0"   -bg $c3bg -pady $c3py -padx 7
label  $Wfiles_R.copy.lab  -text " to " -width 3
entry  $Wfiles_R.copy.ent  -width 30 -relief sunken -textvariable target -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)
label  $Wfiles_R.copy.spa  -text " " -width 3
pack configure $Wfiles_R.copy.but $Wfiles_R.copy.lab $Wfiles_R.copy.ent $Wfiles_R.copy.spa -side left
#----------------------------------------------------------------------------------------
frame  $Wfiles_R.move
button $Wfiles_R.move.but  -text "move" -width 10 -height $c3hb  \
      -command "handle_file_execute move 0"   -bg $c3bg -pady $c3py -padx 7
label  $Wfiles_R.move.lab  -text " to " -width 3
entry  $Wfiles_R.move.ent  -width 30 -relief sunken -textvariable target -bg $COLOR(ENTRY) -fg $COLOR(ENTRYFG)
label  $Wfiles_R.move.spa  -text " " -width 3
pack configure $Wfiles_R.move.but $Wfiles_R.move.lab $Wfiles_R.move.ent $Wfiles_R.move.spa -side left
#----------------------------------------------------------------------------------------
button $Wfiles_R.edit -text "edit"      -width 10 -height $c3hb  \
      -command "handle_file_execute edit 0"   -bg $c3bg -pady $c3py -padx 7
#----------------------------------------------------------------------------------------
button $Wfiles_R.delete -text "delete" -width $c3wb -height $c3hb  \
      -command "handle_file_execute delete 0"  -bg tomato -pady $c3py -padx 7    
#----------------------------------------------------------------------------------------

label $Wfiles_R.label6 -text " "
button $Wfiles_R.close -text "close"      -width $c3wb -height $c3hb \
   -command "destros $Wfiles; unlock_list; handle_files_reset_dir " \
   -bg tomato -pady $c3py -padx 7
#----------------------------------------------------------------------------------------

pack configure $Wfiles_R.top $Wfiles_R.list $Wfiles_R.xterm $Wfiles_R.email $Wfiles_R.print \
               $Wfiles_R.copy $Wfiles_R.move $Wfiles_R.edit  -in  $Wfiles_R -anchor nw -side top

pack configure $Wfiles_R.close $Wfiles_R.label6 $Wfiles_R.delete  \
                -in $Wfiles_R -anchor nw -side bottom

global unix_prog_suffix_list unix_prog_call

set unix_prog_suffix_list [list tar gz tar.gz dvi tex ps eps bmp gif f ras xmgr agr mpk mtv]
set unix_prog_call(tar)  "tar"
set unix_prog_call(gz)   "gunzip"
set unix_prog_call(tar.gz) "unpack"
set unix_prog_call(dvi)  "xdvi"
set unix_prog_call(tex)  "xtem"
set unix_prog_call(ps)   "gv"
set unix_prog_call(eps)  "gv"
set unix_prog_call(bmp)  "xv"
set unix_prog_call(gif)  "xv"
set unix_prog_call(f)    "ftnchek"
set unix_prog_call(ras)  "rasmol -script"
set unix_prog_call(xmgr) "xmgr"
set unix_prog_call(agr)  "xmgrace"
set unix_prog_call(mpk)  "xmatrix"
set unix_prog_call(mtv)  "plotmtv"
#set unix_prog_call()  ""


########################################################################################


insert_textframe $Wfiles $tyh

# initialisation, focus etc.

global sorttype filter
 
set dir0_handle_files [pwd]
debug $PRC "called starting from directory  $dir0_handle_files"  

set sorttype alphabetical
set filter   "*"

files_anzeige $Wfiles

files_sel_order $sorttype

focus $Wfiles; set files_foc 0

}  
#########################################################  END of proc  handle files
########################################################################################

