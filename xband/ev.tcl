# Voreinstellungsmenü für Editor
#
# Copyright (C) 1994  G. Lamprecht, W. Lotz, R. Weibezahn; LRW c/o Uni Bremen
# Copyright (C) 1996  G. Lamprecht, W. Lotz, R. Weibezahn; IWD, Bremen University

proc ev  {} {

 proc evanzeigen {w s1} {
  global vv edtext editor edback edxterm edoptions 
  writescr0 $w.d.tt "$s1\n\n$edtext\n $vv(ev2) $editor$edback\n" \
	" $vv(ev1) $edxterm\n $vv(ev3) $edoptions\n" 
 }

 proc evesel {s} {
  global We vv evsep edtext edxterm editor edoptions edback evesel_v
  set edtext [getvalue $s 0 $evsep]; set edxterm   [getvalue $s 1 $evsep]
  set editor [getvalue $s 2 $evsep]; set edoptions [getvalue $s 3 $evsep]
  progback editor edback; evanzeigen $We "$vv(evt1):"
  set evesel_v "$s"
 }


global We vv editor edback edtext evsep evssel_v

toplevel_init $We "$vv(evvor)" 0 0;  set tyh 17


# top buttons

insert_topbuttons $We ev_h.hlp
bind   $We.a.e <Button-1> {destros $We
  if {[lsearch $liste .c.1.ed]<0} {set liste [linsert $liste 1 .c.2.ed]};  unlock_list
  Bend
}
button $We.a.g -text "$vv(ag)" 	-command {
  vstr_editor; progback editor edback; evanzeigen $We "$vv(ausg1)"
}
bind   $We.a.g  <Button-3>   {cathfile0 ev_grund $We}
pack configure $We.a.g -in $We.a -side left -padx 3 -pady 2


# frame bv for display of personal settings

frame $We.bv; pack configure $We.bv -in $We -pady 5 -anchor w

label  $We.bv.dv -text "$vv(bvdv)   " -anchor w
bind   $We.bv.dv <Button-3> {cathfile0 z_voreinst1" $We}
button $We.bv.va -text "$vv(bvva)" -command {evanzeigen $We "$vv(aus0)"}
bind   $We.bv.va <Button-3> {cathfile0 z_voreinst1 $We}
pack configure $We.bv.dv $We.bv.va -in $We.bv -side left


#frame for editor selection

frame $We.b; pack configure $We.b -in $We -pady 5 -anchor w
frame $We.bs; pack configure $We.bs -in $We.b -pady 5 -padx 4 -anchor e
frame $We.c; pack configure $We.c -in $We.b -pady 5 -anchor e

# editor selection
set evsep [getsep editor.vst]
CreateLSBoxSBx $We.c $vv(evc1el) left 50 8 2 ev_edisel.hlp $We.d.tt \
	$evsep [vst2list editor.vst 1 $evsep "$edtext" evesel_v] evesel ""


insert_textframe $We $tyh

focus $We
}
