#######################################################################################
# main menu for xband version 6.3 
#
# Copyright (C) 2001, ... ,  2008 H. Ebert, University of Munich
#
# based on  xtem by    G. Lamprecht, W. Lotz, R. Weibezahn; IWD, Bremen University
#
########################################################################################



#
# define globals (paths/names) for external utilities and programms
#
proc define_external_helper {} {
    
    global xband_path OSTYPE
    
    global PLOTPROG         ; set PLOTPROG          "$xband_path/plot"
    global RASMOL           ; set RASMOL            "rasmol"
    # DATAEXPLORER is later overwritten from .xband.vst
    global DATAEXPLORER     ; set DATAEXPLORER      "dx"
    global XMGR             ; set XMGR              "xmgr"
    global XMGRACE          ; set XMGRACE           "xmgrace"
    global SYMMETRY         ; set SYMMETRY          "$xband_path/findsym"
    global SPHERES          ; set SPHERES           "$xband_path/spheres"
    global GEOMETRYPRG      ; set GEOMETRYPRG       "$xband_path/geometry"
    
#    append PLOTPROG    "_$OSTYPE"
#    append SYMMETRY    "_$OSTYPE"
#    append SPHERES     "_$OSTYPE"
#    append GEOMETRYPRG "_$OSTYPE"
    append PLOTPROG    "_linux-gnu"
    append SYMMETRY    "_linux-gnu"
    append SPHERES     "_linux-gnu"
    append GEOMETRYPRG "_linux-gnu"

    if {$OSTYPE == "Cygwin"} {
	append PLOTPROG ".exe"
	append SYMMETRY ".exe"
	append SPHERES  ".exe"
	append RASMOL   ".exe"
	append XMGRACE  ".exe"
	# get fullpath 
	# (there is a problem if command in path is actually a link to another place)
	set p [which_silent $XMGRACE] ; if {$p != ""} { set XMGRACE $p }
	append DATAEXPLORER ".exe"
	append GEOMETRYPRG  ".exe"
    }

}


# Library chapter
proc Library_UpdateIndex { libdir } {

    set PRC "Library_UpdateIndex"
      
    if {![file exists $libdir/tclIndex]} {
        set doit 1
    } else {
        set age [file mtime $libdir/tclIndex]
        set doit 0
        foreach file [glob $libdir/*.tcl] {
            if {[file mtime $file] > $age} {
                set doit 1
                break
            }
        }
    }
    if { $doit } {
	puts "tclIndex will be created - please wait a second";
        auto_mkindex $libdir *.tcl
        debug $PRC "tcl-library index created in $libdir" 
    }
}

#
# :TODO: shouldn't the set be in global scope ????
#
proc install_lesen {} {

    set PRC "install_lesen"
    
    global env InstDir
    global fonted 
    global hlp_font xtermcall edsyntaxhelp
    global whichcheck geom
    
    if [info exists env(XBANDINSTALL)] {
	set f [open "$env(XBANDINSTALL)" r]
	set InstDir "${InstDir} (read own fonts settings)\n"
    } else {
	set f [open_vst install.vst]
    }
    
    while {[getscl $f z]>0} {
	set [string trim [lindex $z 0]] [string trim [lrange $z 1 end]]
    }
    close $f
}


proc xband_hlptext {} {

  global  copyright email www version tclVersion patchlevel tk_version
  global  tk_patchLevel InstDir
  cathfile0 xt_xband ""
  set cprt "$copyright\n$email\n$www\n\n"
  set cprt "${cprt}xband:$version Tcl:$tclVersion-$patchlevel Tk:${tk_version}-$tk_patchLevel "
  set cprt "${cprt}InstDir: $InstDir\n\n"
  set cprt "${cprt}xband is free under the Gnu license conditions  \
       (public domain)\nIt requires Tcl/Tk from John K. Ousterhout (also public domain)."
  writescr .d.tt $cprt
}

#
# update tclIndex and compile helper utilities
#
proc setup_xband {{guimode 1}} {

    global xband_path OSTYPE
    global PLOTPROG SYMMETRY SPHERES GEOMETRYPRG
    global XCRYSMODE

    #-----------------------------------------------------------------
    # update tclIndex

    if 0 {
	set run_auto_mkindex 0
	if {[file exists $xband_path/tclIndex]==0} {
	    set run_auto_mkindex 1
	} else {
	    set newest_file [lindex [exec ls -t $xband_path] 0]
	    if {[string last ".tcl" $newest_file]>-1} {
		if {[file mtime $xband_path/$newest_file]>[file mtime $xband_path/tclIndex ]} {
		    set run_auto_mkindex 1
		}
	    }
	}
	
	if {$run_auto_mkindex==1} {
	    puts "tclIndex will be created - please wait a second";
	    auto_mkindex $xband_path *.tcl
	}
    } else {
	Library_UpdateIndex $xband_path
    }
    #--------------------------------------------------------------------------

    #
    #------------------------------------------- SYMMETRY program
    #
    if {[file executable "$SYMMETRY"]==0} {

	if {$guimode} {
	    writescr .d.tt "\n\nWARNING \n\n the symmetry program $SYMMETRY  \
		    \n\n is not available and will be created now \n\n"
	    give_warning "." "WARNING \n\n the symmetry program \n\n $SYMMETRY  \
		    \n\n is not available \n\n IT WILL BE CREATED NOW "
	    update idletasks
	} else {
	    puts "The symmetry program $SYMMETRY is created now."
	}
	set    job [open "zzzzzz_job" w]
	puts  $job "cd $xband_path/symmetry"
	puts  $job "make clean"
	puts  $job "make findsym"
	if {$OSTYPE == "Cygwin" } {
	    puts  $job "/bin/cp findsym.exe $SYMMETRY"
	} else {
	    puts  $job "/bin/cp findsym $SYMMETRY"
	}
	puts  $job "chmod 755 $SYMMETRY" 
	close $job
	set res [catch "exec chmod 755 zzzzzz_job" message]
	set res [catch "exec /bin/sh ./zzzzzz_job" message]

	append message " \n"
	if {$guimode} {
	    writescr .d.tt $message 
	} else {
	    puts $message
	}
    }

   
    #
    #------------------------------------------- SPHERES program
    #
    if {[file executable "$SPHERES"]==0} {
	if {$guimode} {
	    writescr .d.tt "\n\nWARNING \n\n the spheres program $SPHERES  \
		    \n\n is not available and will be created now \n\n"
	    give_warning "." "WARNING \n\n the spheres program \n\n $SPHERES  \
		    \n\n is not available \n\n IT WILL BE CREATED NOW "
	    update idletasks
	} else {
	    puts "The spheres program $SPHERES is created now."
	}
	set    job [open "zzzzzz_job" w]
	puts  $job "cd $xband_path/spheres"
	puts  $job "make clean"
	puts  $job "make spheres"
	if {$OSTYPE == "Cygwin" } {
	    puts  $job "/bin/cp spheres.exe $SPHERES"
	} else {
	    puts  $job "/bin/cp spheres $SPHERES"
	}
	puts  $job "chmod 755 $SPHERES" 
	close $job
	set res [catch "exec chmod 755 zzzzzz_job  " message]
	set res [catch "exec /bin/sh ./zzzzzz_job " message]

	append message " \n"
	if {$guimode} {
	    writescr .d.tt $message 
	} else {
	    puts $message
	}
    }

    #
    #------------------------------------------- GEOMETRYPRG program
    #
    if {[file executable "$GEOMETRYPRG"]==0} {
	
	if {$guimode} {
	    writescr .d.tt "\n\nWARNING \n\n the geometry program   $GEOMETRYPRG  \
		    \n\n is not available and will be created now \n\n"
	    give_warning "." "WARNING \n\n the geometry program \n\n $GEOMETRYPRG  \
		    \n\n is not available "
	    update idletasks
	} else {
	    puts "The geometry program $GEOMETRYPRG is created now."
	}
	
	set    job [open "zzzzzz_job" w]
	puts  $job "cd $xband_path/geometry"
	puts  $job "make clean "
	puts  $job "make geometry"
	if {$OSTYPE == "Cygwin" } {
	    puts  $job "/bin/cp geometry.exe $GEOMETRYPRG" 
	} else {
	    puts  $job "/bin/cp geometry $GEOMETRYPRG" 
	}
	puts  $job "chmod 755 $GEOMETRYPRG" 
	close $job
	set res [catch "exec chmod 755 zzzzzz_job  " message]
	set res [catch "exec /bin/sh ./zzzzzz_job " message]

	append message "\n"
	if {$guimode} {
	    writescr .d.tt $message 
	} else {
	    puts $message
	}

    }
    
    # plot and gopenmol not required/available in XCRYSMODE 
  if {!$XCRYSMODE} {

    #
    #------------------------------------------- PLOTPROG program
    #
    if {[file executable "$PLOTPROG"]==0} {
	
	if {$guimode} {
	    writescr .d.tt "\n\nWARNING \n\n the plot program   $PLOTPROG  \
		    \n\n is not available and will be created now \n\n"
	    give_warning "." "WARNING \n\n the plot program \n\n $PLOTPROG  \
		    \n\n is not available "
	    update idletasks
	} else {
	    puts "The plot program $PLOTPROG is created now."
	}
	
	set    job [open "zzzzzz_job" w]
	puts  $job "cd $xband_path/graphics"
	puts  $job "make clean "
	puts  $job "make plot"
	if {$OSTYPE == "Cygwin" } {
	    puts  $job "/bin/cp plot.exe $PLOTPROG" 
	} else {
	    puts  $job "/bin/cp plot $PLOTPROG" 
	}
	puts  $job "chmod 755 $PLOTPROG" 
	close $job
	set res [catch "exec chmod 755 zzzzzz_job  " message]
	set res [catch "exec /bin/sh ./zzzzzz_job " message]

	append message "\n"
	if {$guimode} {
	    writescr .d.tt $message 
	} else {
	    puts $message
	}

    }
    
    
 } ; # end !XCRYSMODE
    
    catch {file remove zzzzzz_job}
    ##catch {exec /bin/rm zzzzzz_job}
    
}

#
# main entry for xband
#
proc main {{wishcall_arg ""} {xband_path_arg ""} {sysfile_arg ""}} {
    
    global env auto_path 

    # if set to 1 hide compute engines/visualisers functionality 
    global XCRYSMODE ; set XCRYSMODE 0

    global PRC ; set PRC xband
    global DEBUG ; set DEBUG 1
   
    # if not provided try to figure out arguments 
    if {$wishcall_arg == ""} {
	set wishcall_arg [info nameofexecutable] 
    }
    
    if {$xband_path_arg == ""} {
	if {[info exists env(XBANDPATH)]} {
	    set xband_path_arg $env(XBANDPATH)
	} else {
	    set xband_path_arg [file join $env(HOME) xband]
	}
    }
    
    global xband_path wishcall sysfile sysfile_3DBULK 

    set wishcall    "$wishcall_arg" 
    set xband_path  "$xband_path_arg" 
    set sysfile     "$sysfile_arg"
    
    if {$sysfile=="NONE"} {set sysfile ""}

    # make utilities available
    source $xband_path/ut.tcl
    
    global OSTYPE ; set OSTYPE $env(OSTYPE)
    define_external_helper 

    # update tclidx and compile helper applications 
    # special case: directly exit after setup
    if {$sysfile == "-setup" || $sysfile == "-install"} {
	setup_xband 0  ; # no gui mode
	exit
    } else {
	setup_xband 0  ; # 1=gui mode, here not possible 'cause .d.tt not yet available  
    }    
    
# unfortuately some vars are set before claimed global and
# some are set without signaling the global need  
uplevel #0 {

    lappend auto_path $xband_path
    set auto_path "$xband_path $auto_path"
    
    debug $PRC "OSTYPE    $OSTYPE"
    debug $PRC "plot      program  $PLOTPROG"
    debug $PRC "symmetry  program  $SYMMETRY"
    debug $PRC "spheres   program  $SPHERES"
    debug $PRC "geometry  program  $GEOMETRYPRG"
    debug $PRC "tcl version  [info tclversion]"


########################################################################################
set copyright "Copyright (C) 2001, 2002, 2003, 2004 H.Ebert, University of Munich"
set email     "e-mail: Hubert.Ebert@cup.uni-muenchen.de" 
set www       "http://olymp.phys.chemie.uni-muenchen.de/ak/ebert/"
########################################################################################

set version "6.3";               # change only version number
set version_incompatible "";     # .vst-file compatibility

global xband_version ; set xband_version $version

reset_struc_parameter

# dead code
#set v ""; set n ""
#set fln ""
#set l [string length $fln]; set p [string last . $fln]
#if {$p<0} {
#  set main_file $fln
#} elseif {$p==[expr [string length $fln]-1]} {
#  set main_file [crange $fln 0 $p-1]
#} else {
#  if {$p==0} {set b ""; set e $fln} \
#  else {set b [crange $fln 0 $p-1]; set e [crange $fln $p end]}
#}

set main_file "" ; # [file rootname $fln]

if {[info exists env(XBANDVSTDIR)]} {
    set InstDir "$env(XBANDVSTDIR):$InstDir"
} else {
    set InstDir "$xband_path/locals"
}

set hlp_dir "$xband_path/help/"
set f [open "${hlp_dir}button.texts" r]
while {[gets $f z]>=0} {
	set z "[string trim $z] "; set p [string first " " $z]
	set vv([crange $z 0 $p-1]) [string trim [crange $z $p end]]
}
close $f

###############  Feststellen der Tcl-Version   ####################
set tclVersion [info tclversion]
set tclversion $tclVersion
set patchlevel [info patchlevel]

########################################################################################
####### set your variables in the files  locals_*/*.vst --- not here
####### the following settings are  only for "security" 
####### (initialization of all variables)!!!
########################################################################################
set makesuffix "make*" 
set srcsuffix  ".f" 
set dimsuffix  ".dim" 
set sysfile    ""
set sysfile_3DBULK    "UNKNOWN"
set syssuffix  ".sys"
set potfile    ""
set potsuffix  ".pot"
set datafile   ""
set datasuffix ".dos"
set inpfile    ""
set inpsuffix  ".inp"
set pinfile    ""
set pinsuffix  ".pin"
set xmgrfile   ""
set xmgrsuffix ".agr"

#----------------------------------------------------------------------------

global PROG_PATH PROG_PATH0 
set PROG_PATH0 "$env(HOME)/bin/"   ; # later overwritten with preferences

global PROGS_NAME_LIST PROGS_NAME_LIST_TAB PACKAGE_NAME
set PROGS_NAME_LIST ""
global PROGS_VERS_LIST PROGS_VERS_LIST_TAB ; set PROGS_VERS_LIST ""

global GREP1  ;  set GREP1 "ETOT"
global GREP2  ;  set GREP2 "ERR"
global GREP3  ;  set GREP3 "CPA"

set NPACKAGE 4

set PACKAGE_NAME(1) "TB-LMTO"
set PROGS_NAME_LIST_TAB(TB-LMTO)  [list tblmto ]
set PROGS_VERS_LIST_TAB(TB-LMTO)  [list "" ""]

set PACKAGE_NAME(2) "LMTO-AP"
set PROGS_NAME_LIST_TAB(LMTO-AP) [list lmtoap]
set PROGS_VERS_LIST_TAB(LMTO-AP) [list "" ]

set PACKAGE_NAME(3) "KKR-AKAI"
set PROGS_NAME_LIST_TAB(KKR-AKAI) [list size spec ]
set PROGS_VERS_LIST_TAB(KKR-AKAI) [list "2" "4" ]

set PACKAGE_NAME(4) "SPR-KKR"
set PROGS_NAME_LIST_TAB(SPR-KKR) [list kkrscf kkrgen kkrspec kkrchi  embscf embgen ]
set PROGS_VERS_LIST_TAB(SPR-KKR) [list "6.3.0"  "6.3.0" "6.3.0" "6.3.0" "6.3.0" "6.3.0" ]

set PACKAGE_NAME(5) "EXCITING"
set PROGS_NAME_LIST_TAB(EXCITING) [list exciting ]
set PROGS_VERS_LIST_TAB(EXCITING) [list "" ]

set PROGS_KKRCPA93 [list cpa93  ]
set PROGS_VERS_KKRCPA93 [list "" "" ]
set PROGS_SPRLMTO [list sprlmto rdos rjdos]
set PROGS_VERS_SPRLMTO [list "" "" "" ""]

global sys_data_available ; set sys_data_available 0

#=======================================================================================
#     read in the available program packages from    $xband_path/locals/packages.vst
#=======================================================================================
set file $xband_path/locals/packages.vst

if {[file readable $file]==1} {

   set fpack [open $file r]

   set NPACKAGE 0

   while {[gets $fpack line] > -1} {
       set line [string trim $line ]
       if {$line!=""} { 
          if { [string range $line 0 0] != "#" } {

             incr NPACKAGE

             set PACKAGE_NAME($NPACKAGE) $line
             set PACKAGE $PACKAGE_NAME($NPACKAGE)
             if { [gets $fpack line] > -1 } {
                set PROGS_NAME_LIST_TAB($PACKAGE) $line
	     } else {
                 give_warning "." "WARNING \n\n trouble reading  \n\n $fpack  \
                             \n\n list of programs not available "
             }
             debug $PRC "program package $NPACKAGE: $PACKAGE -- $PROGS_NAME_LIST_TAB($PACKAGE)"
          }
       } 
    }

} else {
  give_warning "." "WARNING \n\n package table file \n\n $file  \
                \n\n is not available \n\n DEFAULTS will be used "
}

#----------------------------------------------------------------------------
global PRINTPS          ; set PRINTPS           "lpr -Plp"
global SPACEGROUPS_FILE ; set SPACEGROUPS_FILE  "ITXC_spacegroups.dat"
global STRUCTURE_FILE   ; set STRUCTURE_FILE    "ITXC_structure.dat"

###############################################################################

set unixcmd     ""
set unixcmd_but ""
set unixcmd_log ""

global working_directory ; set working_directory [pwd]

# before split into:   prgdirwork exedirwork sysdirwork

global prgdirlistmax prgdirlistmaxindex prgdirlist
global exedirlistmax exedirlistmaxindex exedirlist exedirwork
global sysdirlistmax sysdirlistmaxindex sysdirlist
set prgdirlistmax  0
set prgdirlistmaxindex 4
set prgdirlist ""

set exedirlistmax  0
set exedirlistmaxindex 4
set exedirlist ""
set exedirwork {$HOME}

set sysdirlistmax  0
set sysdirlistmaxindex 4
set sysdirlist ""

global gesliste ; set gesliste [list ]
global liste ; set liste $gesliste

set mdatsort 0; set edatsort 0; set sdatsort 0; set pdatsort 0
set xtermcall xterm
set whichcheck yes

set Wa  .xbandak;
set Wdm .xbanddm; set Wdv   .xbanddv
set We  .xbandev; set Wf    .xbandssys;   ; set Wwdir .xbandwdir   
                  set Wpfil .xbandpfil;   ; set Wcsys .xbandcsys   
                  set Wcomp .xbandcomp
                  set Wplot .xbandplot
                  set Wssys .xbandssys
                  set Wprog .xbandprog
                  set Wfiles .xbandfiles
                  set Wsetdir .xbandsetdir
                  set Wpath .xbandsppath
set Wh  .syntaxhelp ; set Wi .xbandiv
set Wp  .xbandpv    ; set Wq .xbandq
set Wr  .xbandrv    ; set Ws .xbandsv;    set Wt    .xbandtv
set Wx  .xbandx

install_lesen; set version_vst "${version}"

set tyh 30
set sub 0; set edsubback 0
set anl0 0
set pid ""; set xtAbbruch 0

set editor vi; set edxterm xterm; set edoptions ""
set index makeindex
set aufmax 13
set aufsuff " *~ *bak *xmgr *pin *dos *pshift *vbxps *clxps *.o *.ps *.out *.inp *job"
set auftoggle  "1   1   1     1    1    1       1      1      1   1    1     0     1 "                             
set edback ""; set edliste ""; set edtext ""
set asuff ""; set ssuff ""


#----------------------------------------------------------------------------
#                              COLORS AND GEOMETRY
#----------------------------------------------------------------------------

global VISUAL W_screen H_screen 
global COLOR WIDTH HEIGHT FONT
global key_COLOR key_GEOMETRY
global list_FONT_KEYS 

set W_screen [winfo screenwidth  .]
set H_screen [winfo screenheight .]
#debug $PRC "HHH $H_screen"
#debug $PRC "WWW $W_screen"

set VISUAL best

set key_COLOR 1
set COLOR(DONE)         SteelBlue1
set COLOR(GOON)         green1
set COLOR(CLOSE)        tomato1
set COLOR(LIGHTBLUE)    lightblue
set COLOR(GREEN)        green
set COLOR(BUT1)         green1
set COLOR(BUT2)         red
set COLOR(ENTRY)        snow
set COLOR(STEELBLUE1)   SteelBlue1
set COLOR(LIGHTSALMON1) LightSalmon1
set COLOR(CORNSILK1)  	cornsilk1  
set COLOR(MOCCASIN)   	moccasin   
set COLOR(ORANGE)   	orange
set COLOR(YELLOW)       yellow
set COLOR(ENTRYFG)      black 

global BGSU
set BGSU(1) cornsilk1
set BGSU(2) lightblue

set key_GEOMETRY 1
set WIDTH(BUT1)     15  ;# 12
set HEIGHT(BUT1)     3  ;#  2
set WIDTH(BUT2)     20  ;# 15
set HEIGHT(BUT2)     3  ;#  2
set HEIGHT(.d.tt)   20  ;# 15
set HEIGHT(ssysLS)  12  ;# 10
set WIDTH(ssysLS)   24  ;# 20
set HEIGHT(ssys.tt) 12  ;# 10
 
set list_FONT_KEYS [list GEN SBL BIG]

set FONT(GEN) "-adobe-courier-bold-r-normal--14-100-100-100-m-90-iso8859-1"
set FONT(TAB) "-adobe-courier-bold-r-normal--14-100-100-100-m-90-iso8859-1"
set FONT(SBL) "-adobe-symbol-medium-r-normal--14-140-75-75-p-85-adobe-fontspecific"
set FONT(BIG) "-adobe-courier-bold-r-normal--34-240-100-100-m-200-iso8859-1"

global list_ALPHA    
set list_ALPHA [list a b c d e f g h i j k l m n o p q r s t u v w x y z \
                      A B C D E F G H I J K L M N O P Q R S T U V W X Y Z ]


#
#------------------------------------------- try to read defaults from  locals/*.vst
#

read_preferences


#######################################################################################

set stt ""
set vst_ret [vst_read "" start.vst]
if {$vst_ret!="0"} {
  set stt "${stt} ***************************************************\n"
  set stt "${stt} **                                               **\n"
  set stt "${stt} ** New version of xband, incompatible with your  **\n"
  set stt "${stt} ** xband setting file; settings are therefore    **\n"
  set stt "${stt} ** reset to the local default values!            **\n"
  set stt "${stt} ** Please `save' your `personal settings' again! **\n"
  set stt "${stt} **                                               **\n"
  set stt "${stt} ***************************************************\n"
  set stt "${stt}\n $vst_ret ---> $vst_ret.bak\n\n"
  set vst_ret [vst_read "" .xband.vst]
  if {$vst_ret!="0"} {
    set stt "${stt}\n $vst_ret ---> $vst_ret.bak\n\n"
    vst_read "" default.vst
  }
}
###############################################################################
# forget these test, use always a recent tcl/tk version
#
if 0 {
    set NeedsMinimumTclOlderXband   7.0;  set XbandRunningWith 4.16
    set NeedsMinimumTclThisXband    7.4;
    set UpgradeRecommendedTclOlder 7.4;
    set TestedUpToTcl              8.0;
    set SeemsToBeRunningTcl        7.5;
    if {($tclVersion<$NeedsMinimumTclOlderXband)} { 
	# < 7.0:  will never run with this tcl version
	debug $PRC "xband doesn't run with your old Tcl Version ($tclVersion), see README!"
	exit
    } elseif {($tclVersion>=$NeedsMinimumTclOlderXband)&&($tclVersion<$NeedsMinimumTclThisXband)} {
	debug $PRC "This version of xband presumes Tcl-version >= $NeedsMinimumTclThisXband"
	debug $PRC "yet there exists an older version of xband (xband 4.16)"
	debug $PRC "running with your Tcl/Tk version $tclVersion (see README!)"
	exit
    } elseif {$tclVersion<$UpgradeRecommendedTclOlder} {
	set stt "${stt}You should install a newer Tcl version (see README)!\n\n"
    } elseif  {$tclVersion<=$TestedUpToTcl} {# OK
	### for {set i 0} {$i<=1} {incr i} {after 100}
    } elseif {$tclVersion<$SeemsToBeRunningTcl} {# couldn't test it for myself!
	set stt "${stt}We couldn't test xband with this version of tcl (you have installed $tclVersion).\n "
	set stt "${stt}Yet we were informed that xband runs with this tcl version. \n"
	set tclversion $TestedUpToTcl
    } else {#  no information
	set stt "${stt}** We couldn't test xband with this version of tcl (you have installed $tclVersion)**\n"
	set tclversion $TestedUpToTcl
    }
}
###################################################################


####################################################################;#####@@@@@Testweise
if {[tk appname]!="xband"} {tk appname xband1}; set appnm [tk appname];#####@@@@@Testweise!
####################################################################;#####@@@@@Testweise
option add ${appnm}*Radiobutton.Pad 0
option add ${appnm}*Pad 1
option add *insertOffTime 0

if [info exists hlp_font(normal)]   {option add ${appnm}$Wh.normal.font   $hlp_font(normal)}
if [info exists hlp_font(tag)]      {option add ${appnm}$Wh.tag.font      $hlp_font(tag)}
if [info exists hlp_font(italic)]   {option add ${appnm}$Wh.italic.font   $hlp_font(italic)}
if [info exists hlp_font(bold)]     {option add ${appnm}$Wh.bold.font     $hlp_font(bold)}
if [info exists hlp_font(boldlrge)] {option add ${appnm}$Wh.boldlrge.font $hlp_font(boldlrge)}
if [info exists hlp_font(title)]    {option add ${appnm}$Wh.title.font    $hlp_font(title)}
if [info exists hlp_font(teletype)] {option add ${appnm}$Wh.teletype.font $hlp_font(teletype)}
if [info exists hlp_font(telelrge)] {option add ${appnm}$Wh.telelrge.font $hlp_font(telelrge)}
if [info exists hlp_font(telesmll)] {option add ${appnm}$Wh.telesmll.font $hlp_font(telesmll)}

if [info exists env(XBANDXDEFAULTS)] {
  set f "$env(XBANDXDEFAULTS)"; if {[string first "/" "$f"]<0} {set f "$env(HOME)/$f"}
  option readfile $f
} elseif [file exists $env(HOME)/.Xdefaults] {
  option readfile $env(HOME)/.Xdefaults
}

if {$XCRYSMODE} {
    wm iconname . "xcrys";  wm title . "xcrys $version"
} else {
    wm iconname . "xband";  wm title . "xband $version"
}

wm minsize . 0 0
if [info exists geom(xband)] {wm geometry . $geom(xband)}

set pys 0; # pady-value

########################################################################################
# Button-range            a         for first Buttons
########################################################################################

frame .a -visual $VISUAL ; pack configure .a -in . -side top -anchor w -fill x

frame .a.1  -relief raised -borderwidth 1
frame .a.11 -relief raised -borderwidth 1
frame .a.2  -relief raised -borderwidth 1

pack configure .a.1  -in .a -side left  -anchor w -pady 4
pack configure .a.11 -in .a -side left  -anchor c -expand 1
pack configure .a.2  -in .a -side right -anchor e

								### Ende
button .a.1.e -text "exit" -command {

    if {![file exists $env(HOME)/.xband.vst]} {vst_write_file "$env(HOME)/.xband.vst"}

    #######################################
    if [file exists $env(HOME)/.xband.vst] {
        vst_getvalues "" "$env(HOME)/.xband.vst"
        vst_write_file "$env(HOME)/.xband.vst"
    } else {
        vst_write_file "$env(HOME)/.xband.vst"
    }

    if [file exists ${main_file}.vst] {
        vst_getvalues "" "${main_file}.vst"
        vst_write_file   "${main_file}.vst"
    } 
    #######################################
    destroy .; Bend
}

#bind   .a.1.e <Button-3> {cathfile0 xt_quit ""}

								### Ende
button .a.1.es -text "exit + store settings" -command {
    vst_write_file "$env(HOME)/.xband.vst"
    destroy .; Bend
}
#bind   .a.1.e <Button-3> {cathfile0 xt_quit ""}


								### Hilfen
button .a.1.h -text "help" -command {xband_hlptext}
#bind   .a.1.h <Button-3> {xband_hlptext}

								### L�schen Text-Feld
button .a.1.l -text "clear text field" -command {clearscr .d.tt}
#bind   .a.1.l <Button-3> {cathfile0 z_loeschetf ""}

								### Grundstellung
button .a.1.g -text "reset to defaults" -command {
    vst_read .d.tt default.vst
}
#bind   .a.1.g <Button-3> {cathfile0 xt_grund ""}

#### pack configure .a.1.e .a.1.h .a.1.l .a.1.g -in .a.1 -side left -padx 3 -pady 3
pack configure .a.1.e .a.1.es .a.1.h .a.1.l -in .a.1 -side left -padx 3 -pady 3


								### Entriegeln
button .a.11.r -text "unlock" -command {unlock $gesliste};
#bind .a.11.r <Button-3> {cathfile0 xt_entrieg ""}
pack configure .a.11.r -in .a.11 -side left -padx 3 -pady 3


# Bereich b fuer eigene Voreinstellungen + lokale News

frame .b -visual $VISUAL ; pack configure .b -in . -side top -anchor w -fill x

frame .b.1 -relief raised -borderwidth 1; pack configure .b.1 -in .b -side left -anchor w
frame .b.2;				  pack configure .b.2 -after .b.1 -side right -pady 2

label  .b.1.dv -text "personal (current) settings:"  -width 23 -anchor w
#bind   .b.1.dv <Button-3> {cathfile0 xt_voreinst3 ""}

button .b.1.va -text "display" -command {writescr0 .d.tt "at the moment the following items are valid:\n\n"; disp_prefs2 .d.tt}
#bind   .b.1.va <Button-3> {cathfile0 xt_voreinst3 ""}

button .b.1.vs -text "save" -command {vst_write .d.tt}
#bind   .b.1.vs <Button-3> {cathfile0 xt_voreinst3 ""}

button .b.1.vl -text "load" -command {vst_read .d.tt eigene.vst}
#bind   .b.1.vl <Button-3> {cathfile0 xt_voreinst3 ""}

# SUPPRESSED pack configure .b.1.dv .b.1.va .b.1.vs .b.1.vl -in .b.1 -side left -padx 3 -pady 3

button .b.2.ln -text "local news" -command {
  lock;  set sub 1
  if [file exists "$xband_path/../xband_locals/local.news"] \
       {set localnews "$xband_path/../xband_locals/local.news"}
  newsfile   .d.tt austext; writescr .d.tt "$austext"
  unlock_list
}

#bind .b.2.ln <Button-3> {cathfile0 xt_localnews ""}

# SUPPRESSED pack configure .b.2.ln -in .b.2 -padx 3 -pady 3


########################################################################################
# Button-range            c         for initialisation
########################################################################################
set cbgcolor LightBlue
set buttonheight  2

frame .c -visual $VISUAL -relief raised -bg $cbgcolor -borderwidth 2
#pack configure .c -in . -pady 8 -fill x
pack configure .c -in .          -fill x 


########################################################################################
# Button-Bereich .c.1 fuer Aktionen und Bindungen
########################################################################################

frame .c.1 -bg $cbgcolor; frame .c.2 -bg $cbgcolor; frame .c.3 -bg $cbgcolor
pack configure .c.2 -in .c -side left -padx 5 -anchor e
pack configure .c.3 -in .c -side left -padx 5 -anchor ne
pack configure .c.1 -in .c -side left -padx 5 -anchor e

label .c.3.bit -fg black -bg $cbgcolor -width 295  \
               -bitmap @$xband_path/MobiusStripIILg.xbm

#set Moebius [image create photo -file $xband_path/MM.ppm]

#label .c.3.bit -fg black -bg $cbgcolor -width 295  \
#               -image $Moebius

pack configure .c.3.bit  -in .c.3  -anchor w -side left

########################################################################################
                                                                   ### WORKING DIRECTORY
frame .c.1.wdir -bg $cbgcolor

button .c.1.wdir.but -text "       DIRECTORIES" -anchor w -width 20 -height $buttonheight \
	-command {lock; set sub 1; set_directory} -bg $COLOR(BUT1)

#bind   .c.1.wdir.but <Button-2> {writescr0 .d.tt  \
#                   "directory/system/potfile selection: left mouse button"}
#bind   .c.1.wdir.but <Button-3> {cathfile0 xt_filesel ""}

label .c.1.wdir.dir -width 30 -anchor w -textvariable working_directory -bg $cbgcolor
pack configure .c.1.wdir.but .c.1.wdir.dir  -in .c.1.wdir  -anchor w -side left



########################################################################################
                                                                      ### CREATE SYSTEM
frame .c.1.csys -bg $cbgcolor

button .c.1.csys.but -text "    CREATE SYSTEM" -anchor w -height $buttonheight \
	-width 20 -command {lock; set sub 1; create_system} -bg $COLOR(BUT1)

#bind  .c.1.csys.but <Button-2> {writescr0 .d.tt "specify structure: left mouse button"}
#bind  .c.1.csys.but <Button-3> {cathfile0 xt_filesel ""}

pack configure .c.1.csys.but   -in .c.1.csys  -anchor w -side left

########################################################################################
                                                                       ### SELECT SYSTEM
frame .c.1.ssys -bg $cbgcolor

button .c.1.ssys.but -text "    SELECT / MODIFY" -anchor w -height $buttonheight \
	-width 20 -command {lock; set sub 1; select_system} -bg $COLOR(BUT1)

if {$XCRYSMODE} {
    # modify button label
    .c.1.ssys.but configure -text "      SELECT / SHOW"
}

#bind   .c.1.ssys.but <Button-2> {writescr0 .d.tt  \
#                     "directory/system/potfile selection: left mouse button"}
#bind   .c.1.ssys.but <Button-3> {cathfile0 xt_filesel ""}

label .c.1.ssys.lab -anchor w -textvariable {sysfile} -bg $cbgcolor -font $FONT(GEN)

pack configure .c.1.ssys.but .c.1.ssys.lab -in .c.1.ssys  -anchor w -side left


########################################################################################
                                                                      ### POTENTIAL FILE 
frame .c.1.pfil -bg $cbgcolor

button .c.1.pfil.but -text "     POTENTIAL FILE" -anchor w -width 20 -height $buttonheight \
	-command {lock; set sub 1; select_system} -bg $COLOR(BUT1)

#bind   .c.1.pfil.but <Button-2> {writescr0 .d.tt \
#            "directory/system/potfile selection: left mouse button"}
#bind   .c.1.pfil.but <Button-3> {cathfile0 xt_filesel ""}

if {$potfile==""}  {set p "NONE"} else {set p "$potfile$potsuffix"}
label .c.1.pfil.dir -width 20 -anchor w -text "$p" -bg $cbgcolor
pack configure .c.1.pfil.but .c.1.pfil.dir  -in .c.1.pfil  -anchor w -side left

####### pack configure .c.1.wdir .c.1.csys .c.1.ssys .c.1.pfil -in .c.1 -anchor w -pady $pys
pack configure .c.1.wdir .c.1.csys .c.1.ssys  -in .c.1 -anchor w -pady $pys




########################################################################################
#                          FIRST  COLUMN
########################################################################################

########################################################################################

if {!$XCRYSMODE} { 
                                                                    ### COMPILE PROGRAMS
    button .c.2.comp -text " COMPILE PROGRAMS" -anchor w -width 20 -height $buttonheight \
	    -command {lock; set sub 1; compile_progs} -bg $COLOR(BUT1)

    #bind   .c.2.comp <Button-2> {writescr0 .d.tt "copile programs: left mouse button"}
    #bind   .c.2.comp <Button-3> {cathfile0 xt_filesel ""}

}

########################################################################################
	        	                                             ### SET PREFERENCES
button .c.2.ed -text "   SET PREFERENCES" -anchor w -width 20 -height $buttonheight \
        -command {set_preferences} -bg $COLOR(BUT1)

########################################################################################
                     	                                                     ### TIDY UP
button .c.2.au -text "          TIDY UP" -anchor w -width 20 -height $buttonheight \
        -command {lock;  set sub 1;  clear .d.tt} -bg $COLOR(BUT1)

#bind .c.2.au <Button-2> {writescr0 .d.tt "delete: left mouse button"}
#bind .c.2.au <Button-3> {cathfile0 xt_delauxs ""}

########################################################################################

#pack configure .c.2.comp .c.2.ed .c.2.au  -in .c.2  -pady $pys
eval pack [winfo children .c.2] -pady $pys

########################################################################################
# Button-range            cc         for program calls
########################################################################################


set PROG_PATH $PROG_PATH0

# hide program package section in main window
if {!$XCRYSMODE} {

    set ccbgcolor  tomato

    frame .cc -relief raised -bg $ccbgcolor -borderwidth 2
    pack configure .cc -in . -fill x

    label .cc.headerline -text " select and run program package "  -bg $ccbgcolor -height 2
    pack configure .cc.headerline  -in .cc -side top -padx 5 -anchor n

#--------------------------------------------------------------------------------------
    frame .cc.progs -bg $ccbgcolor
    pack configure .cc.progs  -in .cc -side top -padx 5 -anchor n

    set widthprog 12

    for {set i 1} {$i<=$NPACKAGE} {incr i} {
	
	set PACKAGE $PACKAGE_NAME($i)
	
	if {[info exists PROG_DIRECT($PACKAGE)]==0} {set PROG_DIRECT($PACKAGE) "<"}
	if {[info exists PROG_OPTION($PACKAGE)]==0} {set PROG_OPTION($PACKAGE) " "}
	
	button .cc.progs.pack_$PACKAGE -text "$PACKAGE" -anchor c -width $widthprog\
		-height $buttonheight \
		-command "lock; set sub 1 ; start_run_programs $i"  -bg $COLOR(BUT1)
	
	pack .cc.progs.pack_$PACKAGE  -in .cc.progs  -side left -padx 10 -pady 2
    }

    proc start_run_programs {i} {
	global PROGS_NAME_LIST PROGS_NAME_LIST_TAB PACKAGE_NAME inpsuffix
	global PROGS_VERS_LIST PROGS_VERS_LIST_TAB
	set PACKAGE $PACKAGE_NAME($i)
	set PROGS_NAME_LIST $PROGS_NAME_LIST_TAB($PACKAGE)
	set PROGS_VERS_LIST $PROGS_VERS_LIST_TAB($PACKAGE)
	if {$PACKAGE=="TB-LMTO"} { set inpsuffix "CTRL"}
 	run_programs $PACKAGE
    }

    button .cc.progs.plot -text "PLOT" -anchor c -width $widthprog -height $buttonheight \
	    -command {lock; set sub 1; plot_data 1} -bg $COLOR(BUT1)
    
    pack configure  .cc.progs.plot -in .cc.progs  -side left -padx 10 -pady 2
    
}

########################################################################################


dir_file

set gesliste [list .a.1.e  .a.1.h  .a.1.l  .a.1.g  \
	           .b.1.va .b.1.vs .b.1.vl .b.2.ln \
	           .c.1.wdir.but .c.1.ssys.but \
	           .c.1.pfil.but .c.1.csys.but \
	           .c.2.ed .c.2.au ]

if {!$XCRYSMODE} {

    lappend gesliste .c.2.comp    ; # compile button

    for {set i 1} {$i<=$NPACKAGE} {incr i} {   
	lappend gesliste .cc.progs.pack_$PACKAGE_NAME($i)
    }
    lappend gesliste .cc.progs.plot
}

set liste $gesliste

# create .d.tt 
insert_textframe "" 30 ;  # $HEIGHT(.d.tt) ;  # Bereich d fuer Hilfe-Ausgaben
frame .font; label .font.error; label .font.errortag; label .font.normal
.d.tt tag configure err -font   [option get .font.error font text]
.d.tt tag configure end -relief raised -borderwidth 1 -background white

#=======================================================================================

# final checks

if {[info commands unlink]!="unlink"} {proc unlink {nocompl args} {return}}

if {$XCRYSMODE} {
    writescr0 .d.tt "\n welcome to     xcrys      version $version\n"
} else {
    writescr0 .d.tt "\n welcome to     xband      version $version\n" 
}

writescr  .d.tt "\n settings loaded from: $vstdat\n"

set sfl "$xband_path/locals/xbandstart.news"
if [file exists $sfl] {writescr .d.tt "\n"; cat_file $sfl .d.tt}

.d.tt yview 0
if {[winfo exists $Wq]} {raise $Wq}


#
#------------------------------------------- FONTS
#
font_select check

#--------------------------------------------------------------------------------------- 
global QUICK
set QUICK 0



#TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
global QUICK_tut
set QUICK_tut 0
if {$QUICK_tut==1} {
   puts "*********************** $QUICK_tut"

   set PACKAGE "SPR-KKR"
   puts "*********************** $QUICK_tut $PACKAGE"
   set PROGS_NAME_LIST $PROGS_NAME_LIST_TAB($PACKAGE)
   set PROGS_VERS_LIST $PROGS_VERS_LIST_TAB($PACKAGE)
   run_programs $PACKAGE
}
#TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT



if {$QUICK==1} {

   set sysfile Co

   read_sysfile

#   create_system

   set PACKAGE SPR-KKR
   set PROGS_NAME_LIST $PROGS_NAME_LIST_TAB($PACKAGE)
   run_programs $PACKAGE
}

#_2D_2D_2D_2D_2D_2D_2D_2D_2D_2D_2D_2D_2D_2D_2D_2D_2D_2D_2D_2D_2D_2D_2D_2D_2D_2D_2D_2D_2D
global QUICK_2D  sysfile_2D
set QUICK_2D 0
if {$QUICK_2D==1} {
   set sysfile_2D Vc4Fe9
   set sysfile $sysfile_2D ; read_sysfile
   create_system
}
#_2D_2D_2D_2D_2D_2D_2D_2D_2D_2D_2D_2D_2D_2D_2D_2D_2D_2D_2D_2D_2D_2D_2D_2D_2D_2D_2D_2D_2D


#_0D_0D_0D_0D_0D_0D_0D_0D_0D_0D_0D_0D_0D_0D_0D_0D_0D_0D_0D_0D_0D_0D_0D_0D_0D_0D_0D_0D_0D
global QUICK_0D 
set QUICK_0D 0
if {$QUICK_0D==1} {
   global DEBUG ; set DEBUG 1
   set sysfile Pt
   read_sysfile
   create_system
}
#_0D_0D_0D_0D_0D_0D_0D_0D_0D_0D_0D_0D_0D_0D_0D_0D_0D_0D_0D_0D_0D_0D_0D_0D_0D_0D_0D_0D_0D

# if xband is called with argument -plot only the plotwindow is opended
# and the whole application quits if the plotwindow is closed
if {[lindex $argv 2] == "-plot"} {
    .cc.progs.plot invoke     ; # call plot menu via plot button  
    wm withdraw .             ; # hide xband main window
    tkwait window .xbandplot  ; # wait for window to be closed
    destroy .                 ; #  close whole application (destroy mainwindow)
} 


##
##
} ; # end uplevel #0


}   ; # end main


switch $argc {
    
    3 { 
	# wishcall xband_path sysfile
	main [lindex $argv 0] [lindex $argv 1] [lindex $argv 2] 
    }
    2 { 
	main [lindex $argv 0] [lindex $argv 1] 
    }
    0 { 
	# manually
	main 
    }
    default { 
	puts "wrong number of parameters; only single sysfile allowed"
	exit 1
    }

}

